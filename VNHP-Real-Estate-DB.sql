create database vnhp_real_estate
drop database vnhp_real_estate
use vnhpt
select * from product
update product set block_id = 2
select * from blocks
select * from region
select * from TranferContract

select sc.sc_id, sc.sc_time_open, sc.sc_number, sc.sc_total_payment, sc.saler_id, sc.payer_id, p.product_name, ct.cont_name, cs.cons_name from SaleContract sc join ContractType ct on (sc.cont_id=ct.cont_id) join ContractStatus cs on (sc.cons_id=cs.cons_id) 
join Product p on (sc.product_id=p.product_id)
select top 10 * from Utilities
where utilities_name like '%true%' or utilities_status like '%1%' order by utilities_id desc

create table Position(
	position_id int identity(1,1) primary key,
	position_name nvarchar(200) constraint default_position_name default '',
	position_status bit constraint default_position_status default 1
)
alter table Position
drop  constraint default_position_name
alter table Position
alter  column position_name nvarchar(200)

create table AccountStatus(
	as_id int identity(1,1) primary key,
	as_name nvarchar(200) constraint default_as_name default '',
	as_status bit constraint default_as_status default 1
)


create table AccountLoginStatus(
	als_id int identity(1,1) primary key,
	als_name nvarchar(200) constraint default_als_name default '',
	als_status bit constraint default_als_status default 1
)

create table DepartmentStatus(
	ds_id int identity(1,1) primary key,
	ds_name varchar(200) constraint default_dt_name default '',
	ds_status bit constraint default_dt_status default 1
)

create table Department(
	department_id int identity(1,1) primary key,
	department_name nvarchar(200) constraint default_department_name default '',
	department_info nvarchar(1000) constraint default_department_info default '',
	ds_id int constraint fk_depertment_status references DepartmentStatus(ds_id)
)

create table Account(
	account_id int identity(1,1) primary key,
	account_name nvarchar(200) constraint default_account_name default '',
	account_email varchar(500) constraint default_account_email default '',
	account_password varchar(500) constraint default_account_password default '',
	account_image varchar(500) constraint default_account_image default '',
	account_address nvarchar(500) constraint default_account_address default '',
	account_contact varchar(20) constraint default_account_contact default '',
	manager_id int default 0,
	als_id int constraint fk_account_login_status references AccountLoginStatus(als_id),
    department_id int constraint fk_account_department_id references Department(department_id),
	as_id int constraint fk_account_status references AccountStatus(as_id),
	position_id int constraint fk_account_position references Position(position_id)
)



create table InvestorStatus(
	is_id int identity(1,1) primary key,
	is_name nvarchar(200) constraint default_is_name default '',
	is_status bit constraint default_is_status default 1,
)

create table Investor(
	investor_id int identity(1,1) primary key,
	investor_name nvarchar(500) constraint default_investor_name default '',
	investor_logo varchar(500) constraint default_investor_logo default '',
	investor_upcoming_project_quatity  tinyint constraint defualt_investor_upcoming_project_quatity default 0,
	investor_open_project_quatity  tinyint constraint default_investor_open_project_quatity default 0,
	investor_done_project_quatity  tinyint constraint default_investor_done_project_quatity default 0,
	investor_contact varchar(20) constraint default_investor_contact default '',
	investor_email varchar(500) constraint default_investor_email default '',
	investor_address nvarchar(500) constraint default_investor_address default '',
	investor_info nvarchar(4000) constraint default_investor_info default '',
	is_id int constraint fk_investor_status references InvestorStatus(is_id)
)

create table ConstructionUnitStatus(
	cus_id int identity(1,1) primary key,
	cus_name nvarchar(200) constraint default_cus_name default '',
	cus_status bit constraint default_cus_status default 1
)


update product set block_id = 1
select * from Blocks
select p.product_id,p.product_name,p.product_descrbe,p.product_price,ps.ps_name,r.region_name 
             from Product p 
            join ProductStatus ps on(p.ps_id = ps.ps_id) 
            join Region r on(p.region_id = r.region_id) 
            where p.ps_id = 1 order by p.product_id
create table ConstructionUnit(
	cu_id int identity(1,1) primary key,
	cu_name nvarchar(1000) constraint default_cu_name default '',
	cu_email varchar(500) constraint default_cu_email default '',
	cu_address nvarchar(1000) constraint default_cu_address default '',
	cu_logo varchar(500) constraint default_cu_logo default '',
	cu_done_project_quatity int constraint default_cu_done_project_quatity default 0,
	cus_id int constraint default_cu_status references ConstructionUnitStatus(cus_id),
	cu_contact varchar(50) constraint default_cu_contact default ''
)


create table ProjectStatus(
	pjs_id int identity(1,1) primary key,
	pjs_name nvarchar(200) constraint default_ps_name default '',
	pjs_status bit constraint default_ps_status default 1
)

create table ProjectType(
	pt_id int identity(1,1) primary key,
	pt_name nvarchar(200) constraint default_pt_name default '',
	pt_status bit constraint default_pt_status default 1
)

create table Project(
	project_id int identity(1,1) primary key,
	project_name nvarchar(200) constraint default_project_name default '',
	project_info nvarchar(4000) constraint default_project_info default '',
	project_location nvarchar(500) constraint default_project_location default '',
	project_area float constraint default_project_area default 0,
	project_juridical nvarchar(200) constraint default_project_juridical default '',
	project_density nvarchar(200) constraint default_project_building_density default '',
	project_utilities nvarchar(500) constraint default_project_attached_utilities default '',
	cu_id int constraint fk_project_cu_id references ConstructionUnit(cu_id),
	investor_id int constraint fk_project_investor_id references Investor(investor_id),
	pt_id int constraint fk_project_type references ProjectType(pt_id),
	pjs_id int constraint fk_project_status references ProjectStatus(pjs_id)
)


alter table project
alter column project_info nvarchar(2000)
drop constraint default_project_info
create table RegionStatus(
	rs_id int identity(1,1) primary key,
	rs_name nvarchar(200) constraint default_rs_name default '',
	rs_status bit constraint default_rs_status default 1
)

create table Direction(
	direction_id int identity(1,1) primary key,
	direction_name nvarchar(200) constraint default_direction_name default '',
	direction_status bit constraint default_direction_status default 1
)
create table Region(
	region_id int identity(1,1) primary key,
	region_name nvarchar(200) constraint default_region_name default '',
	region_area float constraint default_region_area default 0,
	direction_id int constraint fk_region_direction references Direction(direction_id),
	rs_id int constraint fk_region_status references RegionStatus(rs_id)
)

select  * from CustomerType
where ct_name like '%a%' or ct_status like '%a%' order by ct_id desc

create table Product(
	product_id int identity(1,1) primary key,
	product_name nvarchar(500) constraint default_product_name default '',
	product_descrbe nvarchar(4000) constraint default_product_descrbe default '',
	product_price decimal constraint default_product_price default 0,
	ps_id int constraint fk_product_status references ProductStatus(ps_id),
	region_id int constraint fk_product_redion_id references Region(region_id),
	block_id int constraint fk_product_block_id references Blocks(block_id),
	direction_id int constraint fk_product_direction_id references Direction(direction_id)
)
alter table product 
add 

select * from product
update product set ps_id = 1,region_id  = 1,block_id = 2,direction_id = 1

create table CustomerStatus(
	cs_id int identity(1,1) primary key,
	cs_name nvarchar(200) constraint default_cs_name default '',
	cs_status bit constraint default_cs_status default 1
)

create table CustomerType(
	ct_id int identity(1,1) primary key,
	ct_name nvarchar(200) constraint default_ct_name default '',
	ct_status bit constraint default_ct_status default 1
)

create table Customer(
	customer_id int identity(1,1) primary key,
	customer_name nvarchar(200) constraint default_customer_name default '',
	customer_email varchar(500) constraint default_customer_email default '',
	customer_contact varchar(500) constraint default_customer_contact default '',
	customer_address varchar(500) constraint default_customer_address default '',
	customer_image varchar(500) constraint default_customer_image default '',
	ct_id int constraint fk_customer_type references CustomerType(ct_id),
	cs_id int constraint fk_customer_status references CustomerStatus(cs_id)
)

create table PaymentType(
	pm_id int identity(1,1) primary key,
	pm_name nvarchar(500) constraint default_pm_name default '',
	pm_status bit constraint default_pm_status default 1
)

create table ContractType(
	cont_id int identity(1,1) primary key,
	cont_name nvarchar(500) constraint default_contracttype_name default '',
	cont_status bit constraint default_contracttype_status default 1
)

create table ContractStatus(
	cons_id int identity(1,1) primary key,
	cons_name nvarchar(500) constraint default_contract_type_name default '',
	cons_status bit constraint default_contract_type_status default 1
)

create table SaleContract(
    sc_id int identity(1,1) primary key,
    sc_time_open varchar(50) constraint default_sc_time_open default '',
    sc_number int constraint default_sc_number default 0,
    sc_total_payment decimal constraint default_contract_payment default 0,
    saler_id int constraint fk_sc_saler_id references Account(account_id),
    payer_id int constraint fk_sc_payer_id references Customer(customer_id),
    product_id int constraint fk_sc_product_id references Product(product_id),
    pm_id int constraint fk_sc_pm_id references PaymentType(pm_id),
    cont_id int constraint fk_sc_cont_id references ContractType(cont_id),
    cons_id int constraint fk_sc_cons_id references ContractStatus(cons_id),
	company_id int constraint fk_company_id references Company(company_id)
)
select * from Statistic
add company_id int constraint fk_company_id references Company(company_id)
update SaleContract
set company_id = 1

select cu.cu_id,cu.cu_name,cu.cu_email,cu.cu_address,cu.cu_logo,cu.cu_done_project_quatity,cu.cu_contact,cus.cus_name  
from ConstructionUnit cu 
join ConstructionUnitStatus cus on(cu.cus_id = cus.cus_id) 
where cu_name like '%a%' or cu_email like '%a%' or cu_address like '%a%' or cu_logo like '%a%' or cu_done_project_quatity like '%a%' or cu_contact like '%a%' or cus_name like '%a%' order by cu_id desc
add 

create table DepositContract(
	dc_id int identity(1,1) primary key,
    dc_time_open varchar(50) constraint default_dc_time_open default '',
    dc_number int constraint default_dc_number default 0,
    dc_payment decimal constraint default_dc_payment default 0,
	sc_id int constraint fk_dc_sc_id references SaleContract(sc_id),
	dc_status bit constraint default_dc_status default 0
)
alter table DepositContract
add constraint default_dc_status default 0 for dc_status
select * from ProductStatus
create table TransferContract(
    tc_id int identity(1,1) primary key,
    tc_time_open varchar(50) constraint default_tc_time_open default '',
    tc_number varchar(100) constraint default_cc_number default '',
    tc_fee decimal constraint default_tc_fee default 0,
    tc_saler_id int constraint fk_tc_customer_id references Customer(customer_id),
	tc_transfer_id int constraint fk_tc_tranfer_id references Customer(customer_id),
	sc_id int constraint fk_tc_sc_id references SaleContract(sc_id),
	tc_status bit constraint default_tc_status default 1
)

create table Company(
	company_id int identity(1,1) primary key,
	company_name nvarchar(1000) constraint default_company_name default '',
	company_CNKD nvarchar(1000) constraint default_company_CNKD default '',
	company_address nvarchar(1000) constraint default_company_address default '',
	company_sales_office nvarchar(1000) constraint default_company_sales_office default '',
	company_contact varchar(50) constraint default_company_contract default '',
	company_fax varchar(50) constraint default_company_fax default '',
	company_bank nvarchar(1000) constraint default_company_bank default '',
	director_id int constraint fk_director_id references Account(account_id),
	company_status bit constraint default_company_status default 1
)
select * from Account
delete from Account where account_id > 1000
drop table Company
add
create table CancelContract(
    cc_id int identity(1,1) primary key,
    cc_time_open nvarchar(50) constraint default_cc_time_open default '',
    cc_number int constraint default_ccc_number default 0,
    cc_fee decimal constraint default_cc_fee default 0,
    sc_id int constraint fk_cc_sccompany_CNKD_id references SaleContract(sc_id),
	cc_status bit constraint default_cc_status default 1
)

create table PaymentPeriodStatus(
    pps_id int identity(1,1) primary key,
    pps_name nvarchar(200) constraint default_pps_name default '',
    pps_status bit constraint default_pps_status default 1
)
select * from Position
create table PaymentPeriod(
	pp_id int identity(1,1) primary key,
	pp_code varchar(500) constraint default_pp_code default '',
	pp_time varchar(50) constraint default_pp_time default '',
	pp_payment decimal constraint default_pp_payment default 0,
	sc_id int constraint fk_pp_product_id references SaleContract(sc_id),
	pp_print_count tinyint constraint default_pp_print_count default 0,
	pps_id int constraint default_pp_status references PaymentPeriodStatus(pps_id)
)

select * from GroupBlocks where project_id = 1
update GroupBlocks set project_id = 2 where gb_id >2
select * from Investor
select * from TransferContract

where pr_id > 1 and 
MONTH(DATEADD(MILLISECOND,CAST('1667236972343' AS BIGINT) % 1000, DATEADD(SECOND, CAST('1667236972343' AS BIGINT) / 1000, '19700101'))) = 11 and
Year(DATEADD(MILLISECOND, CAST('1667236972343' AS BIGINT) % 1000, DATEADD(SECOND, CAST('1667236972343' AS BIGINT) / 1000, '19700101'))) = 2022

select * from ProductRevenue
delete from ProductRevenue where pr_id > 6
select * from PaymentPeriod
update PaymentPeriod set pp_time = '1664276342343' where pp_id = 6

print convert(datetime,'1667236972343',101)

DATEADD(MILLISECOND, 1667236972343 % 1000, DATEADD(SECOND, 1667236972343 / 1000, '19700101'))

print CAST('1667236972343' AS datetime)

print DATEADD(MILLISECOND, 1667236972343 % 1000, DATEADD(SECOND, 1667236972343 / 1000, '19700101'))

select * from ProductRevenue
where pr_id > 1 and 
MONTH(dbo.convertBigIntToDateTime('1667276972343')) = MONTH(dbo.convertBigIntToDateTime(pr_name)) and
Year(dbo.convertBigIntToDateTime('1667276972343')) = Year(dbo.convertBigIntToDateTime(pr_name))

select * from position
select * from account

select * from ProductRevenue where pr_id > 1 and 
pr_name >= 1659360892009 and pr_name <= 1667309692008
select * from ProductRevenue where pr_id > 1 and 
pr_name >= 1651412093514 and pr_name <= 1667309693514
select * from ProductRevenue where pr_id > 1 and 
pr_name >= 1572615294689 and pr_name <= 1667309694689
select * from ProductRevenue where pr_id > 1 and 
pr_name >= 1509543295499 and pr_name <= 1667309695499
select * from ProductRevenue where pr_id > 1 and 
pr_name >= 1351776896820 and pr_name <= 1667309696820
select * from ProductRevenue where pr_id > 1 and 
MONTH(dbo.convertBigIntToDateTime('1667309731657')) = MONTH(dbo.convertBigIntToDateTime(pr_name)) and
Year(dbo.convertBigIntToDateTime('1667309731657')) = Year(dbo.convertBigIntToDateTime(pr_name))

print dbo.convertBigIntToDateTime('1667276972343')
print getDate()
ALTER trigger paymentPeriodTrigger
on PaymentPeriod
for insert,update
as
begin
	update ProductRevenue 
	set pr_revenue = dbo.getTotalRevenue()
	where pr_name like 'Total'

	declare @iPp_time varchar(50),@iPp_Payment decimal,@iPps_id int 
	select @iPp_time = pp_time,@iPp_Payment = pp_payment,@iPps_id = pps_id from inserted

	if(@iPps_id = 2)
		begin
			if (dbo.checkProductRevenueName(@iPp_time) = 1)
				begin
					update ProductRevenue 
					set pr_revenue = dbo.updateRevenue(@iPp_time)
					where pr_id > 1 and 
					MONTH(dbo.convertBigIntToDateTime(pr_name)) = MONTH(dbo.convertBigIntToDateTime(@iPp_time)) and
					Year(dbo.convertBigIntToDateTime(pr_name)) = Year(dbo.convertBigIntToDateTime(@iPp_time))
				end
			else
				begin
					insert into ProductRevenue(pr_name,pr_revenue) values(@iPp_time,@iPp_Payment)
				end
		end
end

ALTER function [dbo].[checkProductRevenueName](@time varchar(50)) returns bit 
as
begin
	declare @tmpTime datetime = dbo.convertBigIntToDateTime(@time)
	if exists (select pr_name from  ProductRevenue where pr_id > 1 and 
	MONTH(dbo.convertBigIntToDateTime(pr_name)) = MONTH(@tmpTime) and
	Year(dbo.convertBigIntToDateTime(pr_name)) = YEAR(@tmpTime))
		begin
			return 1
		end
	return 0
end

print dbo.checkProductRevenueName('1627236972342')

ALTER function [dbo].[updateRevenue](@time varchar(50)) returns decimal 
as
begin
	declare @tmpTime datetime = dbo.convertBigIntToDateTime(@time)
	declare @payment decimal = (select sum(pp_payment) from  PaymentPeriod where MONTH(dbo.convertBigIntToDateTime(pp_time)) = MONTH(@tmpTime) and Year(dbo.convertBigIntToDateTime(pp_time)) = YEAR(@tmpTime))
	return @payment
end

create function convertBigIntToDateTime(@time BigInt) returns DateTime
as
begin
	declare @tmpDateTime datetime = (DATEADD(MILLISECOND,CAST(@time AS BIGINT) % 1000, DATEADD(SECOND, CAST(@time AS BIGINT) / 1000, '19700101')))
	return @tmpDateTime
end