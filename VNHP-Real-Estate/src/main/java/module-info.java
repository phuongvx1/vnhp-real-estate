module com.vnhp.real.estate {
    requires javafx.controls;
    requires javafx.fxml;

    requires java.sql;
    requires com.microsoft.sqlserver.jdbc;
    requires jasperreports;
    requires java.base;
    requires org.json;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires javafx.web;
    
    opens com.vnhp.real.estate.controller;
    exports com.vnhp.real.estate.controller;
    
    opens com.vnhp.real.estate.controller.Business;
    exports com.vnhp.real.estate.controller.Business;
    
    opens com.vnhp.real.estate.controller.Director;
    exports com.vnhp.real.estate.controller.Director;
    
    opens com.vnhp.real.estate.controller.human;
    exports com.vnhp.real.estate.controller.human;
    
    opens com.vnhp.real.estate.model.entities;
    exports com.vnhp.real.estate.model.entities;    
    
    opens com.vnhp.real.estate to javafx.fxml;
    exports com.vnhp.real.estate;
}
