/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class CustomerType {

    public Integer index;
    @ColumnId
    public Integer ct_id;
    @ColumnName
    @DbColumn
    public String ct_name;
    @ColumnStatus
    @DbColumn
    public Boolean ct_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setCt_id(Integer ct_id) {
        this.ct_id = ct_id;
    }

    public void setCt_name(String ct_name) {
        this.ct_name = ct_name;
    }

    public void setCt_status(Boolean ct_status) {
        this.ct_status = ct_status;
    }

    public Integer getCt_id() {
        return ct_id;
    }

    public String getCt_name() {
        return ct_name;
    }

    public Boolean getCt_status() {
        return ct_status;
    }

    public CustomerType() {
    }

    public CustomerType(String ct_name) {
        this.ct_name = ct_name;
    }

    public CustomerType(Integer ct_id, String ct_name) {
        this.ct_id = ct_id;
        this.ct_name = ct_name;
    }

    public CustomerType(Integer ct_id, Boolean ct_status) {
        this.ct_id = ct_id;
        this.ct_status = ct_status;
    }

    public CustomerType(Integer ct_id, String ct_name, Boolean ct_status, Integer index) {
        this.index = index;
        this.ct_id = ct_id;
        this.ct_name = ct_name;
        this.ct_status = ct_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
