package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.model.db.Investor_jpa;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import java.io.IOException;
import java.util.LinkedList;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class BusinessProductController {

    @FXML
    public Button allProductBtn, investorBtn, projectBtn;

    @FXML
    private ScrollPane businessProductBorder;

    public void init(Stage stage) {
        FXMLLoader defaultLoad = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_ALL_PRODUCT));
        try {
            Pane allProduct = defaultLoad.load();
            businessProductBorder.setContent(allProduct);

            var control = (AllProductController) defaultLoad.getController();
            control.init();

        } catch (IOException e) {
            e.printStackTrace();
        }

        allProductBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_ALL_PRODUCT));

            try {
                Pane allProduct = loader.load();
                businessProductBorder.setContent(allProduct);

                var control = (AllProductController) loader.getController();
                control.init();

            } catch (IOException e) {
            }
        });

        investorBtn.setOnMousePressed(mouseEvent -> {
            MyGridPane iComponent = new MyGridPane(1000.0, 1550.0, "iComponent", 10.0, 15.0);

            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_INVESTOR));

            LinkedList<Investor> investor = Investor_jpa.get();
            LinkedList<LinkedList<Investor>> investor1 = new LinkedList<>();
            investor1.add(investor);
            try {
                Pane investorDisplay = loader.load();
                iComponent.createComponent(stage, investor1, TableName.Investor);
                investorDisplay.getChildren().add(iComponent);
                businessProductBorder.setContent(investorDisplay);

                var control = (InvestorController) loader.getController();
                control.init(stage);

            } catch (IOException e) {
            }
        });

        projectBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_PROJECT));

            try {
                Pane projectDisplay = loader.load();
                businessProductBorder.setContent(projectDisplay);

                var control = (ProjectController) loader.getController();
                control.init();

            } catch (IOException e) {
            }
        });
    }

}