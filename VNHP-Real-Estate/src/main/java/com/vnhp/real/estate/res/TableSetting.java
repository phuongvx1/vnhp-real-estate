package com.vnhp.real.estate.res;

import java.util.LinkedList;
import javafx.scene.layout.ColumnConstraints;

/**
 *
 * @author FairyHunter
 */
public class TableSetting<T> {

    private TableName tableName;
    private T data;
    private LinkedList<String> labels;
    private LinkedList<ColumnConstraints> setting;

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setTableName(TableName tableName) {
        this.tableName = tableName;
    }

    public TableName getTableName() {
        return tableName;
    }

    public void setSetting(LinkedList<ColumnConstraints> setting) {
        this.setting = setting;
    }

    public LinkedList<ColumnConstraints> getSetting() {
        return setting;
    }

    public void setLabels(LinkedList<String> labels) {
        this.labels = labels;
    }

    public LinkedList<String> getLabels() {
        return labels;
    }

    public TableSetting() {
    }

    public TableSetting(LinkedList<String> labels, LinkedList<ColumnConstraints> setting) {
        this.labels = labels;
        this.setting = setting;
    }
}
