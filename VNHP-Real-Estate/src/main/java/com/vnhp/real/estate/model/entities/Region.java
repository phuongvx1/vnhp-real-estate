/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class Region {

    public Integer index;
    @ColumnId
    public Integer region_id;
    @ColumnName
    @DbColumn
    public String region_name;
    @DbColumn
    public Float region_area;
    @ColumnStatus
    public Integer rs_id;
    @DbColumn
    public String rs_name;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setRs_id(Integer rs_id) {
        this.rs_id = rs_id;
    }

    public void setRs_name(String rs_name) {
        this.rs_name = rs_name;
    }

    public void setRegion_area(Float region_area) {
        this.region_area = region_area;
    }

    public void setRegion_id(Integer region_id) {
        this.region_id = region_id;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public void setRegion_status(Integer rs_id) {
        this.rs_id = rs_id;
    }

    public Float getRegion_area() {
        return region_area;
    }

    public Integer getRegion_id() {
        return region_id;
    }

    public String getRegion_name() {
        return region_name;
    }

    public Integer getRegion_status() {
        return rs_id;
    }

    public Integer getRs_id() {
        return rs_id;
    }

    public String getRs_name() {
        return rs_name;
    }

    public Region() {
    }

    public Region(String region_name, Float region_area, Integer rs_id) {
        this.region_name = region_name;
        this.region_area = region_area;
        this.rs_id = rs_id;
    }

    public Region(Integer region_id, String region_name, Float region_area) {
        this.region_id = region_id;
        this.region_name = region_name;
        this.region_area = region_area;
    }

    public Region(Integer region_id, Integer rs_id) {
        this.region_id = region_id;
        this.rs_id = rs_id;
    }

    public Region(String region_name) {
        this.region_name = region_name;
    }

    public Region(Integer region_id, String region_name, Float region_area, String rs_name, Integer index) {
        this.region_id = region_id;
        this.region_name = region_name;
        this.region_area = region_area;
        this.rs_name = rs_name;
        this.index = index;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
