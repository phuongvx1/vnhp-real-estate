/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Investor;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.ObjectType;
import com.vnhp.real.estate.res.ServerService;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableName;
import com.vnhp.real.estate.res.TableSetting;
import java.util.LinkedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.ColumnConstraints;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorInvestorViewController {

    @FXML
    private ScrollPane pDirInvestorView;

    @FXML
    private TextField tfDirInvestorSearch;

    public void init() {
        setup();
    }

    private void setupSearch(MyGridPane grid, TableSetting tableSetting) {
        tfDirInvestorSearch.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var searchText = tfDirInvestorSearch.getText();
                var data = new Investor();
                var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(BaseDao.Instance().searchQuery(data, StringValue.Instance().INVESTOR_JOIN, searchText), Investor[].class), ServerService.Instance().GET_INVESTOR);
                grid.clearAll();
                grid.setRow(grid, listData, tableSetting, TableName.Investor);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setup() {
        var gridPane = new MyGridPane();

        var titleList = new LinkedList<String>();
        titleList.add("ID");
        titleList.add("Name");
        titleList.add("Logo");
        titleList.add("Up");
        titleList.add("Open");
        titleList.add("Done");
        titleList.add("Contact");
        titleList.add("Email");
        titleList.add("Address");
        titleList.add("Status");
        titleList.add("Change Status");
        titleList.add("Update");

        gridPane.addTitles(titleList, TableName.Investor);
        var rowSettings = new LinkedList<String>();
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyImage.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyButton.toString());
        rowSettings.add(ObjectType.MyButton.toString());

        var col1 = new ColumnConstraints();
        col1.setPrefWidth(30);
        var col2 = new ColumnConstraints();
        col2.setPrefWidth(150);
        var col3 = new ColumnConstraints();
        col3.setPrefWidth(100);
        var col4 = new ColumnConstraints();
        col4.setPrefWidth(70);
        var col5 = new ColumnConstraints();
        col5.setPrefWidth(70);
        var col6 = new ColumnConstraints();
        col6.setPrefWidth(70);
        var col7 = new ColumnConstraints();
        col7.setPrefWidth(150);
        var col8 = new ColumnConstraints();
        col8.setPrefWidth(150);
        var col9 = new ColumnConstraints();
        col9.setPrefWidth(150);
        var col10 = new ColumnConstraints();
        col10.setPrefWidth(70);
        var col11 = new ColumnConstraints();
        col11.setPrefWidth(150);
        var col12 = new ColumnConstraints();
        col12.setPrefWidth(100);
        

        var setting = new LinkedList<ColumnConstraints>();
        setting.add(col1);
        setting.add(col2);
        setting.add(col3);
        setting.add(col4);
        setting.add(col5);
        setting.add(col6);
        setting.add(col7);
        setting.add(col8);
        setting.add(col9);
        setting.add(col10);
        setting.add(col11);
        setting.add(col12);

        var tableSetting = new TableSetting(rowSettings, setting);
        var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().INVESTOR_INFO, Investor[].class), ServerService.Instance().GET_INVESTOR);

        gridPane.setLayoutX(10);
        gridPane.setRow(gridPane, listData, tableSetting, TableName.Investor);
        gridPane.getColumnConstraints().addAll(tableSetting.getSetting());
        pDirInvestorView.setLayoutX(50);
        pDirInvestorView.setLayoutY(80);
        pDirInvestorView.setBackground(Background.EMPTY);
        pDirInvestorView.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pDirInvestorView.setContent(gridPane);
        setupSearch(gridPane, tableSetting);

        pDirInvestorView.vvalueProperty().addListener(eh -> update(gridPane, tableSetting));
    }
    private boolean ck = true;

    private void update(MyGridPane grid, TableSetting tableSetting) {
        if (ck) {
            ck = false;
            var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().INVESTOR_INFO, Investor[].class), ServerService.Instance().GET_INVESTOR);
            grid.clearAll();
            grid.setRow(grid, listData, tableSetting, TableName.Investor);
        }
    }
}
