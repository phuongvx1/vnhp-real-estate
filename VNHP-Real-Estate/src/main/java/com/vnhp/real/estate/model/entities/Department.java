/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class Department {

    public Integer index;
    @ColumnId
    public Integer department_id;
    public Integer ds_id;
    public String department_info;
    @ColumnName
    public String department_name;
    public String ds_name;
    @ColumnStatus
    public Boolean department_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public Department(String department_name, Integer ds_id) {
        this.department_name = department_name;
        this.ds_id = ds_id;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
    
    public Integer getIndex() {
        return index;
    }
    
    public void setds_name(String ds_name) {
        this.ds_name = ds_name;
    }
    
    public String getds_name() {
        return ds_name;
    }

    public Department(Integer department_id, String department_name, String ds_name, Integer index) {
        this.department_id = department_id;
        this.department_name = department_name;
        this.ds_name = ds_name;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
        this.index = index;
    }

    public Department(String department_name) {
        this.department_name = department_name;
    }

    public void setDepartment_id(Integer department_id) {
        this.department_id = department_id;
    }

    public void setDepartment_info(String department_info) {
        this.department_info = department_info;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public void setDepartment_status(Boolean department_status) {
        this.department_status = department_status;
    }

    public Integer getDepartment_id() {
        return department_id;
    }

    public String getDepartment_info() {
        return department_info;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public Boolean getDepartment_status() {
        return department_status;
    }
    
    public Department() {
    }

    public Department(String department_name, String department_info, Boolean department_status) {
        this.department_name = department_name;
        this.department_info = department_info;
        this.department_status = department_status;
    }

    public Department(Integer department_id, String department_name, String department_info) {
        this.department_id = department_id;
        this.department_name = department_name;
        this.department_info = department_info;
    }

    public Department(Integer department_id, Boolean department_status) {
        this.department_id = department_id;
        this.department_status = department_status;
    }

    public Department(Integer department_id, String department_name, String department_info, Boolean department_status) {
        this.department_id = department_id;
        this.department_name = department_name;
        this.department_info = department_info;
        this.department_status = department_status;
    }
}
