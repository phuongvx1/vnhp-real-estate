/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Position;
import com.vnhp.real.estate.res.TimeManager;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRPositionNewController {
   @FXML
    public TextField tfHrPositionNew;

    @FXML
    private Label tfHrPositionResult;

    @FXML
    private Button btnAddNewPosition;
   

    public void init() {
        setup();
    }

    private void setup() {
        btnAddNewPosition.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfHrPositionNew.getText();

            var data = new Position(name);
            var retult = BaseDao.Instance().insert(data);
            tfHrPositionResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfHrPositionResult, 2);
        });
    }
}      
