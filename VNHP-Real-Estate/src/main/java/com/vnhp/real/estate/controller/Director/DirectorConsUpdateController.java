/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.ContractStatus;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorConsUpdateController{

      @FXML
    private TextField tfDirConsUpdateName;
    
    @FXML
    private Label tfDirConsUpdateResult;
    
    @FXML
    private Button btnDirConsUpdate;
    
    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, ContractStatus data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }
    
    private void setupDefault(ContractStatus data) {
        tfDirConsUpdateName.setText(data.getCons_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, ContractStatus data) {
        btnDirConsUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirConsUpdateName.getText();
            
            var newData = new ContractStatus(data.getCons_id(),name);
            var retult = BaseDao.Instance().update(newData);
            tfDirConsUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirConsUpdateResult, 2);
            
            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (ContractStatus) data;
            tmp.setCons_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getCons_id(), tmp);
        });
    }
    
}
