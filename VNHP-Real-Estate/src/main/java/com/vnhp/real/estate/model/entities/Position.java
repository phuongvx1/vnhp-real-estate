/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class Position {
    public Integer index;
    @ColumnId
    public Integer position_id;
    @ColumnName
    public String position_name;
    @ColumnStatus
    public Boolean position_status;
    public String btnChangePassword;
    public String btnUpdate;

    public Position(Integer position_id, Boolean position_status) {
        this.position_id = position_id;
        this.position_status = position_status;
    }
    
    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }
    public Position(Integer position_id, String position_name, Boolean position_status, Integer index) {
        this.position_id = position_id;
        this.position_name = position_name;
        this.position_status = position_status;
        this.index = index;
        this.btnChangePassword = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }

    public void setPosition_id(Integer position_id) {
        this.position_id = position_id;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public void setPosition_status(Boolean position_status) {
        this.position_status = position_status;
    }

    public Integer getPosition_id() {
        return position_id;
    }

    public String getPosition_name() {
        return position_name;
    }

    public Boolean getPosition_status() {
        return position_status;
    }

    public Position() {
    }

    public Position(String position_name) {
        this.position_name = position_name;
    }

    public Position(Integer position_id, String position_name) {
        this.position_id = position_id;
        this.position_name = position_name;
    }

}
