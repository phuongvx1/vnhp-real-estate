/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnQuantity;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;

/**
 *
 * @author FairyHunter
 */
public class Statistic {

    @ColumnId
    public Integer statistic_id;
    @DbColumn
    public String statistic_name;
    @ColumnQuantity
    public Integer statistic_quantity;
    @ColumnStatus
    @DbColumn
    public Boolean statistic_status;

    public void setStatistic_id(Integer statistic_id) {
        this.statistic_id = statistic_id;
    }

    public void setStatistic_name(String statistic_name) {
        this.statistic_name = statistic_name;
    }

    public void setStatistic_quantity(Integer statistic_quantity) {
        this.statistic_quantity = statistic_quantity;
    }

    public Integer getStatistic_id() {
        return statistic_id;
    }

    public String getStatistic_name() {
        return statistic_name;
    }

    public Integer getStatistic_quantity() {
        return statistic_quantity;
    }

    public Statistic() {
    }

    public Statistic(String statistic_name) {
        this.statistic_name = statistic_name;
    }

    public Statistic(Integer statistic_id, String statistic_name, Integer statistic_quantity) {
        this.statistic_id = statistic_id;
        this.statistic_name = statistic_name;
        this.statistic_quantity = statistic_quantity;
    }
}
