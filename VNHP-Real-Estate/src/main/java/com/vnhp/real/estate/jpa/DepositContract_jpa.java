/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.jpa;

import com.vnhp.real.estate.model.db.DbConnection;
import com.vnhp.real.estate.model.entities.DepositContract;
import com.vnhp.real.estate.model.entities.DepositReceipt;
import com.vnhp.real.estate.model.entities.SaleContract;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import javafx.scene.layout.StackPane;

/**
 *
 * @author Lip
 */
public class DepositContract_jpa {

    public final LinkedList<DepositReceipt> getAll() {
        Connection conn = DbConnection.Instance().Connect();
        var ls = new LinkedList<DepositReceipt>();
        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery("select * from depositcontract p\n"
                    + "join salecontract s on (p.sc_id = s.sc_id)\n"
                    + "join product pr on (s.product_id = pr.product_id)");
            while (rs.next()) {
                var item = newData(rs);
                ls.add(item);
            }
            sm.close();
            conn.close();
        } catch (Exception e) {
        }
        return ls;
    }

    public final int CountNoti() throws SQLException {
        Connection conn = DbConnection.Instance().Connect();
        int count = 0;
        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery("Select count(dc_id) as count from depositcontract where dc_status = 0");
            while (rs.next()) {
                count = rs.getInt("count");
            }
            sm.close();
            conn.close();
            return count;
        } catch (SQLException e) {
        }
        return count;
    }

    public final void UpdateStatus(DepositReceipt subject) {
        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            sm.executeQuery("Update DepositContract Set dc_status = 1 where dc_id = " + subject.dc_id);
        } catch (SQLException e) {
        }
    }

    public final void UpdateSaleContract(DepositReceipt subject) {
        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            sm.executeQuery("Update SaleContract Set cons_id = 2 where sc_id = " + subject.sc_id);
        } catch (SQLException e) {
        }
    }

    public final void UpdatePeriod(DepositReceipt subject) {
        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            sm.executeQuery("Update PaymentDeposit Set pps_id = 2 where dc_id = " + subject.dc_id);
        } catch (SQLException e) {
        }
    }

    private DepositReceipt newData(ResultSet rs) {
        var tmp = new DepositReceipt();
        try {
            tmp = new DepositReceipt(
                    rs.getInt("dc_id"),
                    rs.getString("dc_time_open"),
                    rs.getInt("dc_number"),
                    rs.getString("dc_payment"),
                    rs.getInt("sc_id"),
                    rs.getString("dc_status"),
                    rs.getString("sc_total_payment"),
                    rs.getString("product_name"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tmp;
    }

}
