/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Department;
import com.vnhp.real.estate.model.entities.DepartmentStatus;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRAddNewDepartmentController  {
    @FXML
    private TextField tfHrNewDepartmentName;

    @FXML
    private Label tfHrDepartmentNameResult;

    @FXML
    private Button btnAddDepartment;
    @FXML
    private ComboBox<String> tfDepartmentStatus;
    public void init(Stage stage) {
        setup();
        setupDefault(stage);
    }    
    private void setupDefault(Stage stage) {
        var status = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().DEPARTMENT_STATUS, DepartmentStatus[].class));
        tfDepartmentStatus.getItems().addAll(status);
        tfDepartmentStatus.setValue((String) status.get(0));
    }
    
    private void setup() {
        btnAddDepartment.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfHrNewDepartmentName.getText();
            var status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new DepartmentStatus(tfDepartmentStatus.getValue()), DepartmentStatus[].class, 1), ColumnId.class);
            var data = new Department(name,status);
            var retult = BaseDao.Instance().insert(data);
            tfHrDepartmentNameResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfHrDepartmentNameResult, 2);
        });
    }
}
