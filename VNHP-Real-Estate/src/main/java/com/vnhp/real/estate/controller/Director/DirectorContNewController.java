/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.ContractStatus;
import com.vnhp.real.estate.model.entities.ContractType;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorContNewController {

    @FXML
    private TextField tfDirContNewName;

    @FXML
    private Label tfDirContNewResult;

    @FXML
    private Button btnDirContNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirContNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirContNewName.getText();

            var data = new ContractType(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirContNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirContNewResult, 2);
        });
    }
}
