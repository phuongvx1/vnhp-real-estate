/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.model.entities.MyThread;
import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProductsLayoutController {

    @FXML
    private Button btnDirProductNew, btnDirProductView, btnDirPSNew, btnDirPSView, btnDirDirectionNew, btnDirDirectionView,
            btnDirCUNew, btnDirCUView, btnDirCUSNew, btnDirCUSView, btnDirRegionNew, btnDirRegionView, btnDirRsNew, btnDirRsView;
    @FXML
    private Pane pDirProductNew;

    
    public void init(Stage stage) {
        var thread = new MyThread();
        thread.start();
        Runnable runnable = () -> {
//            setupDefault(stage);
            setup(stage);
        };

        try {
            thread.join();

        } catch (InterruptedException ex) {
            Logger.getLogger(DirectorProductNewController.class.getName()).log(Level.SEVERE, null, ex);
        }
        runnable.run();
    }

    private void setupDefault(Stage stage) {
        var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PRODUCT_NEW));
        try {
            AnchorPane dashboard = dataNew.load();
            var control = (DirectorProductNewController) dataNew.getController();
            control.init(stage);
            pDirProductNew.getChildren().clear();
            pDirProductNew.getChildren().add(dashboard);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setup(Stage stage) {
        btnDirProductNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PRODUCT_NEW));
            try {
                AnchorPane dashboard = dataNew.load();
                var control = (DirectorProductNewController) dataNew.getController();
                control.init(stage);
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirProductView.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PRODUCT_VIEW));
            try {
                AnchorPane dashboard = dataView.load();
                var control = (DirectorProductViewController) dataView.getController();
                control.init(stage);
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PRODUCT_STATUS_NEW));
            try {
                AnchorPane dashboard = dataNew.load();
                var control = (DirectorProductStatusNewController) dataNew.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPSView.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PRODUCT_STATUS_VIEW));
            try {
                AnchorPane dashboard = dataView.load();
                var control = (DirectorProductStatusViewController) dataView.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirDirectionNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_DIRECTION_NEW));
            try {
                AnchorPane dashboard = dataNew.load();
                var control = (DirectorDirectionNewController) dataNew.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirDirectionView.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_DIRECTION_VIEW));
            try {
                AnchorPane dashboard = dataView.load();
                var control = (DirectorDirectionViewController) dataView.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirCUNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CU_NEW));
            try {
                AnchorPane dashboard = dataNew.load();
                var control = (DirectorCUNewController) dataNew.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirCUView.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CU_VIEW));
            try {
                AnchorPane dashboard = dataView.load();
                var control = (DirectorCUViewController) dataView.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirCUSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CUS_NEW));
            try {
                AnchorPane dashboard = dataNew.load();
                var control = (DirectorCUSNewController) dataNew.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirCUSView.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CUS_VIEW));
            try {
                AnchorPane dashboard = dataView.load();
                var control = (DirectorCUSViewController) dataView.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirRegionNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_REGION_NEW));
            try {
                AnchorPane dashboard = dataNew.load();
                var control = (DirectorRegionNewController) dataNew.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirRegionView.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_REGION_VIEW));
            try {
                AnchorPane dashboard = dataView.load();
                var control = (DirectorRegionViewController) dataView.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirRsNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_RS_NEW));
            try {
                AnchorPane dashboard = dataNew.load();
                var control = (DirectorRsNewController) dataNew.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirRsView.addEventHandler(ActionEvent.ACTION, eh -> {
            var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_RS_VIEW));
            try {
                AnchorPane dashboard = dataView.load();
                var control = (DirectorRsViewController) dataView.getController();
                control.init();
                pDirProductNew.getChildren().clear();
                pDirProductNew.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
