/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Investor;
import com.vnhp.real.estate.model.entities.InvestorStatus;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorInvestorNewController {

    @FXML
    private TextField tfDirInvestorNewName, tfDirInvestorNewLogo, tfDirInvestorNewContact, tfDirInvestorNewEmail, tfDirInvestorNewAddress;

    @FXML
    private TextArea tfDirInvestorNewInfo;
    @FXML
    private ComboBox<String> tfDirInvestorNewStatus;
    @FXML
    private Label tfDirInvestorNewResult;

    @FXML
    private Button btnDirInvestorNew;

    public void init() {
        setupDefault();
        setup();
    }

    private void setupDefault() {
        var is = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().INVESTOR_STATUS_ACTIVE, InvestorStatus[].class));
        tfDirInvestorNewStatus.getItems().addAll(is);
        tfDirInvestorNewStatus.setValue((String) is.get(0));
    }

    private void setup() {
        btnDirInvestorNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirInvestorNewName.getText();
            var logo = tfDirInvestorNewLogo.getText();
            var contact = tfDirInvestorNewContact.getText();
            var email = tfDirInvestorNewEmail.getText();
            var address = tfDirInvestorNewAddress.getText();
            var info = tfDirInvestorNewInfo.getText();

            var is_status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new InvestorStatus(tfDirInvestorNewStatus.getValue()), InvestorStatus[].class, 1), ColumnId.class);

            var data = new Investor(name, logo, contact,email,address,info, is_status);
            var retult = BaseDao.Instance().insert(data);
            tfDirInvestorNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirInvestorNewResult, 2);
        });
    }

}
