package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import java.util.LinkedList;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class AllCustomerController {

    @FXML
    ScrollPane allCustomerDisplay;

    @FXML
    TextField customerSearchField;

    @FXML
    Button customerSearchBtn;

    public static Customer customer = new Customer();
    public static Customer saleCustomer = new Customer();

    public void init() {
        setup();
    }

    private void setupSearch(MyGridPane grid, TableSetting tableSetting) {
        customerSearchBtn.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var searchText = customerSearchField.getText();
                var data = new Customer();
                var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(BaseDao.Instance().searchQuery(data, StringValue.Instance().CUSTOMER_ALL, searchText), Customer[].class), ServerService.Instance().GET_CUSTOMER);
                grid.clearAll();
                grid.setRow(grid, listData, tableSetting, TableName.Customer);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setup() {
        var gridPane = new MyGridPane();

        var titleList = new LinkedList<String>();
        titleList.add("");
        titleList.add("Name");
        titleList.add("Email");
        titleList.add("Contact");
        titleList.add("DOB");
        titleList.add("Address");
        titleList.add("Current Address");
        titleList.add("ID");
        titleList.add("ID_Date");
        titleList.add("ID_Place");
        titleList.add("Customer Type");
        titleList.add("Status");
        titleList.add("Note");
        titleList.add("Change Status");
        titleList.add("Update");
        titleList.add("Transfer");

//        gridPane.setGridLinesVisible(true);
        gridPane.addTitles(titleList, TableName.Customer);
        var rowSettings = new LinkedList<String>();
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyButton.toString());
        rowSettings.add(ObjectType.MyButton.toString());
        rowSettings.add(ObjectType.MyButton.toString());

        var col1 = new ColumnConstraints();
        col1.setPrefWidth(30);
        var col2 = new ColumnConstraints();
        col2.setPrefWidth(70);
        var col3 = new ColumnConstraints();
        col3.setPrefWidth(150);
        var col4 = new ColumnConstraints();
        col4.setPrefWidth(70);
        var col5 = new ColumnConstraints();
        col5.setPrefWidth(70);
        var col6 = new ColumnConstraints();
        col6.setPrefWidth(100);
        var col7 = new ColumnConstraints();
        col7.setPrefWidth(100);
        var col8 = new ColumnConstraints();
        col8.setPrefWidth(100);
        var col9 = new ColumnConstraints();
        col9.setPrefWidth(70);
        var col10 = new ColumnConstraints();
        col10.setPrefWidth(70);
        var col11 = new ColumnConstraints();
        col11.setPrefWidth(70);
        var col12 = new ColumnConstraints();
        col12.setPrefWidth(70);
        var col13 = new ColumnConstraints();
        col13.setPrefWidth(100);
        var col14 = new ColumnConstraints();
        col14.setPrefWidth(150);
        var col15 = new ColumnConstraints();
        col15.setPrefWidth(150);
        var col16 = new ColumnConstraints();
        col16.setPrefWidth(150);

        var setting = new LinkedList<ColumnConstraints>();
        setting.add(col1);
        setting.add(col2);
        setting.add(col3);
        setting.add(col4);
        setting.add(col5);
        setting.add(col6);
        setting.add(col7);
        setting.add(col8);
        setting.add(col9);
        setting.add(col10);
        setting.add(col11);
        setting.add(col12);
        setting.add(col13);
        setting.add(col14);
        setting.add(col15);
        setting.add(col16);

        var tableSetting = new TableSetting(rowSettings, setting);
        var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().CUSTOMER_ALL, Customer[].class), ServerService.Instance().GET_CUSTOMER);

        gridPane.setLayoutX(10);
        gridPane.setRow(gridPane, listData, tableSetting, TableName.Customer);
        gridPane.getColumnConstraints().addAll(tableSetting.getSetting());
        allCustomerDisplay.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        allCustomerDisplay.setContent(gridPane);
        setupSearch(gridPane, tableSetting);

    }
}
