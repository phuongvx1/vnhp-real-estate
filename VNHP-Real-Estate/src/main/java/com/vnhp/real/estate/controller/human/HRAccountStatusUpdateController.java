/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.AccountStatus;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.ProjectStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRAccountStatusUpdateController  {


        @FXML
    private TextField tfHrASUpdate;

    @FXML
    private Label tfHrASUpdateResult;

    @FXML
    private Button btnHrASUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, AccountStatus data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(AccountStatus data) {
        tfHrASUpdate.setText(data.getas_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, AccountStatus data) {
        btnHrASUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfHrASUpdate.getText();

            var newData = new AccountStatus(data.getas_id(), name);
            var retult = BaseDao.Instance().update(newData);
            tfHrASUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfHrASUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (AccountStatus) data;
            tmp.setas_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getas_id(), tmp);
        });
    }


}    

