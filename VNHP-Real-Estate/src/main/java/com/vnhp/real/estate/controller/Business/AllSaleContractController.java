package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import java.util.LinkedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class AllSaleContractController {

    @FXML
    ScrollPane allSaleContractDisplay;

    @FXML
    TextField saleContractSearchField;

    @FXML
    Button saleContractSearchBtn;

    public static SaleContract saleContract = new SaleContract();

    public void init(Stage stage) {
        allSaleContract();
    }

    private void setupSearch(MyGridPane grid, TableSetting tableSetting) {
        saleContractSearchBtn.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var searchText = saleContractSearchField.getText();
                var data = new SaleContract();
                var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(BaseDao.Instance().searchQuery(data, StringValue.Instance().SALE_CONTRACT_ALL + 1, searchText), SaleContract[].class), ServerService.Instance().GET_SALE_CONTRACT);
                grid.clearAll();
                grid.setRow(grid, listData, tableSetting, TableName.SaleContract);

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void allSaleContract() {
        var gridPane = new MyGridPane();

        var titleList = new LinkedList<String>();
        titleList.add("");
        titleList.add("Open Time");
        titleList.add("Contract Number");
        titleList.add("Total Payment");
        titleList.add("Saler ID");
        titleList.add("Saler Name");
        titleList.add("Payer ID");
        titleList.add("Product Name");
        titleList.add("Contract Type");
        titleList.add("Contract Status");
        titleList.add("Product ID");
        titleList.add("Contract Type ID");
        titleList.add("Contract Status ID");
        titleList.add("Change Status");
        titleList.add("Update");
        titleList.add("Transfer");

        gridPane.addTitles(titleList, TableName.Customer);
        var rowSettings = new LinkedList<String>();
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyButton.toString());
        rowSettings.add(ObjectType.MyButton.toString());
        rowSettings.add(ObjectType.MyButton.toString());

        var col1 = new ColumnConstraints();
        col1.setPrefWidth(30);
        var col2 = new ColumnConstraints();
        col2.setPrefWidth(70);
        var col3 = new ColumnConstraints();
        col3.setPrefWidth(70);
        var col4 = new ColumnConstraints();
        col4.setPrefWidth(100);
        var col5 = new ColumnConstraints();
        col5.setPrefWidth(70);
        var col6 = new ColumnConstraints();
        col6.setPrefWidth(70);
        var col7 = new ColumnConstraints();
        col7.setPrefWidth(100);
        var col8 = new ColumnConstraints();
        col8.setPrefWidth(100);
        var col9 = new ColumnConstraints();
        col9.setPrefWidth(70);
        var col10 = new ColumnConstraints();
        col10.setPrefWidth(70);
        var col11 = new ColumnConstraints();
        col11.setPrefWidth(70);
        var col12 = new ColumnConstraints();
        col12.setPrefWidth(70);
        var col13 = new ColumnConstraints();
        col13.setPrefWidth(70);
        var col14 = new ColumnConstraints();
        col14.setPrefWidth(150);
        var col15 = new ColumnConstraints();
        col15.setPrefWidth(150);
        var col16 = new ColumnConstraints();
        col16.setPrefWidth(150);

        var setting = new LinkedList<ColumnConstraints>();
        setting.add(col1);
        setting.add(col2);
        setting.add(col3);
        setting.add(col4);
        setting.add(col5);
        setting.add(col6);
        setting.add(col7);
        setting.add(col8);
        setting.add(col9);
        setting.add(col10);
        setting.add(col11);
        setting.add(col12);
        setting.add(col13);
        setting.add(col14);
        setting.add(col15);
        setting.add(col16);

        var tableSetting = new TableSetting(rowSettings, setting);
        var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().SALE_CONTRACT_ALL + 2, SaleContract[].class), ServerService.Instance().GET_SALE_CONTRACT);

        gridPane.setLayoutX(10);
        gridPane.setRow(gridPane, listData, tableSetting, TableName.SaleContract);
        gridPane.getColumnConstraints().addAll(tableSetting.getSetting());
        allSaleContractDisplay.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        allSaleContractDisplay.setContent(gridPane);
        setupSearch(gridPane, tableSetting);

    }
}
