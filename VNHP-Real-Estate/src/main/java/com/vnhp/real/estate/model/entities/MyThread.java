/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.controller.Director.DirectorProductNewController;
import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class MyThread extends Thread {

    public static Integer i = 0;

    @Override
    public void run() {
        DirectorProductNewController.blocks = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().BLOCKS_ACTIVE, Blocks[].class));
        DirectorProductNewController.region = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().REGION_ACTIVE, Region[].class));
        DirectorProductNewController.ps = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PRODUCT_STATUS_ACTIVE, ProductStatus[].class));
        DirectorProductNewController.direction = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().DIRECTION_ACTIVE, Direction[].class));

//        try {
//            Thread.sleep(1000);
//        } catch (Exception e) {
//        }
//        if (this.isAlive()) {
//            System.out.println(i);
//
//        } else {
//            System.out.println("end");
//        }
//        run();
    }
}
