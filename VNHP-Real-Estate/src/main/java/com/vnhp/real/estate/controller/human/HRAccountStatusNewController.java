/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.AccountStatus;
import com.vnhp.real.estate.model.entities.ProjectStatus;
import com.vnhp.real.estate.res.TimeManager;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRAccountStatusNewController  {

 
    @FXML
    private TextField tfHrNewAS;

    @FXML
    private Label tfHrNewASResult;

    @FXML
    private Button btnHrAddNewAS;

    public void init() {
        setup();
    }

    private void setup() {
        btnHrAddNewAS.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfHrNewAS.getText();

            var data = new AccountStatus(name);
            var retult = BaseDao.Instance().insert(data);
            tfHrNewASResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfHrNewASResult, 2);
        });
    }

}    
    
