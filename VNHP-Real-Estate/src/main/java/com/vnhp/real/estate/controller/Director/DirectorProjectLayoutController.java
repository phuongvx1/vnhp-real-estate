/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProjectLayoutController {

    @FXML
    private Button btnDirProjectNew, btnDirProjectView, btnDirPJSNew, btnDirPJSView, btnDirPTNew, btnDirPTView,
            btnDirUtilitiesNew, btnDirUtilitiesView, btnDirGBNew, btnDirGBView, btnDirGBSNew, btnDirGBSView, btnDirBlockNew, btnDirBlockView, btnDirBSNew, btnDirBSView;
    @FXML
    private Pane pDirProjectNew;

    public void init(Stage stage) {
//        setupDefault(stage);
        setup(stage);
    }

    private void setupDefault(Stage stage) {
        try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PROJECT_NEW));
            AnchorPane dashboard = dataNew.load();
            pDirProjectNew.getChildren().clear();
            pDirProjectNew.getChildren().add(dashboard);
            var control = (DirectorProjectNewController) dataNew.getController();
            control.init(stage);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setup(Stage stage) {
        btnDirProjectNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PROJECT_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorProjectNewController) dataNew.getController();
                control.init(stage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirProjectView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PROJECT_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorProjectViewController) dataView.getController();
                control.init(stage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPJSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PS_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorPSNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPJSView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PS_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorPSViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPTNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PT_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorPTNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPTView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PT_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorPTViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirUtilitiesNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_UTILITIES_NEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorUtilitiesNewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirUtilitiesView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_UTILITIES_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorUtilitiesViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirGBNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_GB_NEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorGbNewController) dataView.getController();
                control.init(stage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirGBView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_GB_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorGbViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirGBSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_GBS_NEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorGbsNewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirGBSView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_GBS_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorGbsViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirBlockNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_BLOCK_NEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorBlockNewController) dataView.getController();
                control.init(stage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirBlockView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_BLOCK_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorBlockViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        btnDirBSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_BS_NEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorBsNewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirBSView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_BS_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirProjectNew.getChildren().clear();
                pDirProjectNew.getChildren().add(dashboard);
                var control = (DirectorBsViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
