package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author bichv
 */
public class BusinessCancelContractController {

    @FXML
    private TextField customerName, customerDOB, customerCCCD, customerPlace, customerDate, customerAddress, customerCurrentAddress,
            companyName, companyCNDK, companyAddress, companySalesOffice, companyContact, companyFax, companyBank, companyDirectorName, companyDirectorPosition;

    @FXML
    private Button cancelContractAddBtn;

    @FXML
    private Label addResult;

    public void init(Stage stage) {

        customerName.setText(AllCustomerController.saleCustomer.getCustomer_name());
        customerDOB.setText(AllCustomerController.saleCustomer.getCustomer_dob());
        customerCCCD.setText(AllCustomerController.saleCustomer.customer_cccd);
        customerPlace.setText(AllCustomerController.saleCustomer.getCustomer_cccd_place());
        customerDate.setText(AllCustomerController.saleCustomer.getCustomer_cccd_date());
        customerAddress.setText(AllCustomerController.saleCustomer.getCustomer_address());
        customerCurrentAddress.setText(AllCustomerController.saleCustomer.getCustomer_current_address());

        var company = (Company) ConvertObject.Instance().toData(BaseDao.Instance().getListData(StringValue.Instance().COMPANY, Company[].class));
        companyName.setText(company.getCompany_name());
        companyCNDK.setText(company.getCompany_cnkd());
        companyAddress.setText(company.getCompany_address());
        companySalesOffice.setText(company.getDirector_position());
        companyContact.setText(company.getCompany_contact());
        companyFax.setText(company.getCompany_fax());
        companyBank.setText(company.getCompany_bank());
        companyDirectorName.setText(company.getDirector_name());
        companyDirectorPosition.setText(company.getDirector_position());

        addCancelContract();
    }

    private void addCancelContract() {
        cancelContractAddBtn.addEventHandler(ActionEvent.ACTION, mouseEvent -> {
            var cc_time_open = String.valueOf(TimeManager.Instance().getServerTime().getTimeInMillis());

            var statisticData = BaseDao.Instance().getListData(StringValue.Instance().STATISTIC_ALL, Statistic[].class);
            var cc_number = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "CancelContract") + 1;

            var cc_fee = (float) 0;

            var sc_id = AllSaleContractController.saleContract.getSc_id();

            var data = new CancelContract(cc_time_open, cc_number, cc_fee, sc_id);
            var result = BaseDao.Instance().insert(data);

            addResult.setText(result);
            TimeManager.Instance().clearResult(mouseEvent, addResult, 2);
        });
    }
}
