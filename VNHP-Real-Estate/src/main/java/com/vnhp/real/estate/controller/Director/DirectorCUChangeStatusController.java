/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.ConstructionUnit;
import com.vnhp.real.estate.model.entities.ConstructionUnitStatus;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorCUChangeStatusController {

    @FXML
    private Label tfDirCUCSName;
    @FXML
    private ComboBox<String> tfDirCUCSStatus;
    @FXML
    public Label tfDirCUCSResult;
    @FXML
    private Button btnDirCUCS;
//    @FXML
//    private ImageView btnClose, btnMinimize;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, ConstructionUnit data) {
//        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        setupDefault(data);

        setup(grid, gridIndex, tableSetting, data);
    }

    private void setupDefault(ConstructionUnit data) {
        tfDirCUCSName.setText(data.getCu_name());

        var cus = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CONTRUCTION_UNIT_STATUS_ACTIVE, ConstructionUnitStatus[].class));
        tfDirCUCSStatus.getItems().addAll(cus);
        tfDirCUCSStatus.setValue(data.getCus_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, ConstructionUnit data) {
        btnDirCUCS.addEventHandler(ActionEvent.ACTION, eh -> {
            var cus_status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ConstructionUnitStatus(tfDirCUCSStatus.getValue()), ConstructionUnitStatus[].class, 1), ColumnId.class);
            var tmpData = new ConstructionUnit(data.getCu_id(), cus_status);
            var retult = BaseDao.Instance().update(tmpData);
            tfDirCUCSResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirCUCSResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (ConstructionUnit) data;
            tmp.setCus_name(tfDirCUCSStatus.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getCu_id(), tmp);
        });

    }

}
