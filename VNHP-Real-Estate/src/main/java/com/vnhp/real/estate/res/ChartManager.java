/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.ProductStatistic;
import com.vnhp.real.estate.model.entities.RevenueDept;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author FairyHunter
 */
public class ChartManager<T> {

    private static ChartManager instance;

    private ChartManager() {

    }

    public static ChartManager Instance() {
        if (instance == null) {
            instance = new ChartManager();
        }

        return instance;
    }

    public PieChart pieChart(String title, ArrayList<PieChart.Data> data, Label caption) {

        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(data);
        final PieChart pieChart = new PieChart(pieChartData);
        pieChart.setTitle(title);

        for (PieChart.Data pieData : pieChart.getData()) {
            pieData.getNode().addEventHandler(MouseEvent.MOUSE_ENTERED, (MouseEvent e) -> {
                if (pieData.getName().equals("House")) {
                    caption.setTranslateX(120);
                    caption.setTranslateY(150);
                }
                if (pieData.getName().equals("Land")) {
                    caption.setTranslateX(160);
                    caption.setTranslateY(250);
                }
                caption.setStyle("-fx-font : bold 25px \"serief\"; ");
                caption.setText(String.valueOf((int) pieData.getPieValue()));
            });
            pieData.getNode().addEventHandler(MouseEvent.MOUSE_EXITED, (MouseEvent e) -> {
                caption.setText("");
            });
        }
        return pieChart;
    }

    public XYChart.Series xyChartSeries(String title, ArrayList<XYChart.Data> data) {

        final XYChart.Series series = new XYChart.Series();
        series.getData().addAll(data);
        series.setName(title);

        return series;
    }

    public LineChart<String, Number> lineChart(String title, XYChart.Series data) {

        var x = new CategoryAxis();
        var y = new NumberAxis();
        x.setLabel(title);
        var lineChart = new LineChart<String, Number>(x, y);
        lineChart.getData().add(data);

        return lineChart;
    }

    public AreaChart<Number, Number> areaChart(String title, ArrayList<XYChart.Series> data) {

        var x = new NumberAxis();
        var y = new NumberAxis();
        x.setLabel(title);
        var lineChart = new AreaChart<Number, Number>(x, y);
        for (XYChart.Series tmp : data) {
            lineChart.getData().add(tmp);
        }

        return lineChart;
    }

    public BarChart<String, Number> barChart(String titleLabel, String valueLabel, ArrayList<XYChart.Series> data) {

        var x = new CategoryAxis();
        var y = new NumberAxis();
        x.setLabel(titleLabel);
        y.setLabel(valueLabel);
        var barChart = new BarChart<String, Number>(x, y);
        for (XYChart.Series tmp : data) {
            barChart.getData().add(tmp);
        }

        return barChart;
    }

    public XYChart.Series<String, Integer> convertData(String year, String serverService) {
        var dataByMonth = new XYChart.Series<String, Integer>();
        var currentTime = TimeManager.Instance().getServerTime();
        var tmp = Calendar.getInstance();
        var productStatisticList = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().PRODUCT_STATISTIC + year, ProductStatistic[].class));

        for (Integer i = 1; i <= currentTime.getTime().getMonth(); i++) {
            var check = true;
            if (serverService.equals(ServerService.Instance().GET_PRODUCT_STATISTIC)) {
                for (Object data : productStatisticList) {
                    var tmpData = (ProductStatistic) data;
                    tmp.setTimeInMillis(Long.parseLong(tmpData.getPs_name()));
                    if (i == tmp.getTime().getMonth()) {
                        dataByMonth.getData().add(new XYChart.Data(monthText(i), tmpData.getPs_quantity()));
                        productStatisticList.remove(data);
                        check = false;
                        break;
                    }
                }
                if (check) {
                    dataByMonth.getData().add(new XYChart.Data(monthText(i), 0));
                }
            }
        }

        return dataByMonth;
    }

    public XYChart.Series<String, Float> convertData(LinkedList<String> years, String serverService) {
        var dataYear = new XYChart.Series<String, Float>();
        var dataTable = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().REVENUE_DEPT, RevenueDept[].class));

        for (String year : years) {
            var check = true;
            if (serverService.equals(ServerService.Instance().GET_NET_REVENUE)) {
                for (Object data : dataTable) {
                    var tmpData = (RevenueDept) data;
                    if (year.equals(tmpData.getRd_name())) {
                        dataYear.getData().add(new XYChart.Data(year, tmpData.getRd_revenue()));
                        dataTable.remove(data);
                        check = false;
                        break;
                    }
                }
                if (check) {
                    dataYear.getData().add(new XYChart.Data(year, 0));
                }
            }

            if (serverService.equals(ServerService.Instance().GET_DEPT)) {
                for (Object data : dataTable) {
                    var tmpData = (RevenueDept) data;
                    if (year.equals(tmpData.getRd_name())) {
                        System.out.println(tmpData.getRd_dept());
                        dataYear.getData().add(new XYChart.Data(year, tmpData.getRd_dept()));
                        dataTable.remove(data);
                        check = false;
                        break;
                    }
                }
                if (check) {
                    dataYear.getData().add(new XYChart.Data(year, 0));
                }
            }
        }

        return dataYear;
    }

    private String monthText(Integer index) {
        var string = "";

        switch (index) {
            case 1:
                string = "Jan";
                break;
            case 2:
                string = "Feb";
                break;
            case 3:
                string = "Mar";
                break;
            case 4:
                string = "Apr";
                break;
            case 5:
                string = "May";
                break;
            case 6:
                string = "Jun";
                break;
            case 7:
                string = "Jul";
                break;
            case 8:
                string = "Aug";
                break;
            case 9:
                string = "Sep";
                break;
            case 10:
                string = "Oct";
                break;
            case 11:
                string = "Nov";
                break;
            case 12:
                string = "Dec";
                break;
        }
        return string;
    }
}
