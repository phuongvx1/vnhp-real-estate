package com.vnhp.real.estate.res;

import com.vnhp.real.estate.controller.Business.InvestorInformationController;
import java.io.IOException;
import javafx.fxml.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author phongv
 */
public class InvestorComponent<T> extends Pane {

    public InvestorComponent(T data, Stage stage) {
        this.setPrefHeight(50.0);
        this.setPrefWidth(200.0);
        this.getStyleClass().add("investorTab");

        var iId = Integer.parseInt(ConvertObject.Instance().getData(data, ColumnId.class));
        var iLogo = ConvertObject.Instance().getData(data, ColumnLogo.class);
        var iName = ConvertObject.Instance().getData(data, ColumnName.class);

        var image = new Image(getClass().getResourceAsStream(StringValue.Instance().ASSET_BUSINESS_INVESTOR + iLogo));
        ImageView investor_logo = new ImageView();
        investor_logo.setImage(image);
        investor_logo.setLayoutX(15.0);
        investor_logo.setLayoutY(10.0);
        investor_logo.setFitHeight(30.0);
        investor_logo.setFitWidth(30.0);

        Label investor_name = new Label();
        investor_name.setText(iName);
        investor_name.setLayoutX(60.0);
        investor_name.setLayoutY(12.5);
        investor_name.setPrefHeight(25.0);
        investor_name.setPrefWidth(200.0);
        investor_name.getStyleClass().add("investorName");

        this.getChildren().addAll(investor_logo, investor_name);

        this.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BI_INFOMATION));

            try {
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
                subStage.initStyle(StageStyle.TRANSPARENT);
                
                var control = (InvestorInformationController) loader.getController();
                control.init(subStage, iId);

                subStage.show();

            } catch (IOException e) {
            }
        });

    }

    public InvestorComponent(Integer iId, String iLogo, String iName) {
        this.setPrefHeight(50.0);
        this.setPrefWidth(200.0);
        this.getStyleClass().add("investorTab");

        var image = new Image(getClass().getResourceAsStream(StringValue.Instance().ASSET_BUSINESS_INVESTOR + iLogo));
        ImageView investor_logo = new ImageView();
        investor_logo.setImage(image);
        investor_logo.setLayoutX(15.0);
        investor_logo.setLayoutY(10.0);
        investor_logo.setFitHeight(30.0);
        investor_logo.setFitWidth(30.0);

        Label investor_name = new Label();
        investor_name.setText(iName);
        investor_name.setLayoutX(60.0);
        investor_name.setLayoutY(12.5);
        investor_name.setPrefHeight(25.0);
        investor_name.setPrefWidth(200.0);
        investor_name.getStyleClass().add("investorName");

        this.getChildren().addAll(investor_logo, investor_name);

    }
}
