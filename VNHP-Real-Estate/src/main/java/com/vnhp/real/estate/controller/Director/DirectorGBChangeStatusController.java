/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.Gb_Status;
import com.vnhp.real.estate.model.entities.GroupBlocks;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorGBChangeStatusController {

    @FXML
    private Label tfDirGbCSName;
    @FXML
    private ComboBox<String> tfDirGbCSStatus;
    @FXML
    public Label tfDirGbCSResult;
    @FXML
    private Button btnDirGbCS;
//    @FXML
//    private ImageView btnClose, btnMinimize;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, GroupBlocks data) {
//        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        setupDefault(data);

        setup(grid, gridIndex, tableSetting, data);
    }

    private void setupDefault(GroupBlocks data) {
        tfDirGbCSName.setText(data.getGb_name());

        var gb_Status = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().GB_STATUS_ACTIVE, Gb_Status[].class));
        tfDirGbCSStatus.getItems().addAll(gb_Status);
        tfDirGbCSStatus.setValue(data.getGbs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, GroupBlocks data) {
        btnDirGbCS.addEventHandler(ActionEvent.ACTION, eh -> {
            var gb_status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Gb_Status(tfDirGbCSStatus.getValue()), Gb_Status[].class, 1), ColumnId.class);
            var tmpData = new Blocks(data.getGb_id(), gb_status);
            var retult = BaseDao.Instance().update(tmpData);
            tfDirGbCSResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirGbCSResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (GroupBlocks) data;
            tmp.setGbs_name(tfDirGbCSStatus.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getGb_id(), tmp);
        });

    }

}
