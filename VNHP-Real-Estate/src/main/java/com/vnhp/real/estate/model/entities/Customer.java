/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;

/**
 *
 * @author FairyHunter
 */
public final class Customer {

    public Integer index;
    @ColumnId
    @DbColumn
    public Integer customer_id;
    @DbColumn
    public String customer_name;
    @DbColumn
    public String customer_email;
    @DbColumn
    public String customer_contact;
    @DbColumn
    public String customer_dob;
    @DbColumn
    public String customer_address;
    @DbColumn
    public String customer_current_address;
    @DbColumn
    public String customer_cccd;
    @DbColumn
    public String customer_cccd_date;
    @DbColumn
    public String customer_cccd_place;
    @DbColumn
    public Integer ct_id;
    @DbColumn
    public String cs_name;
    public String customer_note;
    public String btnChangeStatus;
    public String btnUpdate;
    public String btnTransfer;
    public String customer_image;
    @ColumnStatus
    public Integer cs_id;
    public String ct_name;
    public String sc_total_payment;

    public void setSc_total_payment(String sc_total_payment) {
        this.sc_total_payment = sc_total_payment;
    }

    public String getSc_total_payment() {
        return sc_total_payment;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setCs_id(Integer cs_id) {
        this.cs_id = cs_id;
    }

    public void setCustomer_cccd(String customer_cccd) {
        this.customer_cccd = customer_cccd;
    }

    public void setCustomer_note(String customer_note) {
        this.customer_note = customer_note;
    }

    public void setCs_name(String cs_name) {
        this.cs_name = cs_name;
    }

    public void setCt_id(Integer ct_id) {
        this.ct_id = ct_id;
    }

    public void setCt_name(String ct_name) {
        this.ct_name = ct_name;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public void setCustomer_current_address(String customer_current_address) {
        this.customer_current_address = customer_current_address;
    }

    public void setCustomer_contact(String customer_contact) {
        this.customer_contact = customer_contact;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public void setCustomer_image(String customer_image) {
        this.customer_image = customer_image;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public void setCustomer_dob(String customer_dob) {
        this.customer_dob = customer_dob;
    }

    public void setCustomer_cccd_date(String customer_cccd_date) {
        this.customer_cccd_date = customer_cccd_date;
    }

    public void setCustomer_cccd_place(String customer_cccd_place) {
        this.customer_cccd_place = customer_cccd_place;
    }

    public Integer getCs_id() {
        return cs_id;
    }

    public String getCs_name() {
        return cs_name;
    }

    public Integer getCt_id() {
        return ct_id;
    }

    public String getCt_name() {
        return ct_name;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public String getCustomer_current_address() {
        return customer_current_address;
    }

    public String getCustomer_contact() {
        return customer_contact;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public String getCustomer_image() {
        return customer_image;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public String getCustomer_cccd() {
        return customer_cccd;
    }

    public String getCustomer_dob() {
        return customer_dob;
    }

    public String getCustomer_cccd_date() {
        return customer_cccd_date;
    }

    public String getCustomer_cccd_place() {
        return customer_cccd_place;
    }

    public String getCustomer_note() {
        return customer_note;
    }

    public Customer() {
    }

    public Customer(String cs_name) {
        this.cs_name = cs_name;
    }

    public Customer(String customer_name, String customer_email, String customer_contact, String customer_address, String customer_image, Integer ct_id, Integer cs_id) {
        this.customer_name = customer_name;
        this.customer_email = customer_email;
        this.customer_contact = customer_contact;
        this.customer_address = customer_address;
        this.customer_image = customer_image;
        this.ct_id = ct_id;
        this.cs_id = cs_id;
    }

    public Customer(Integer customer_id, String customer_name, String customer_email, String customer_contact, String customer_address, String customer_image, Integer ct_id) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_email = customer_email;
        this.customer_contact = customer_contact;
        this.customer_address = customer_address;
        this.customer_image = customer_image;
        this.ct_id = ct_id;
    }

    public Customer(Integer customer_id, Integer cs_id) {
        this.customer_id = customer_id;
        this.cs_id = cs_id;
    }

    public Customer(Integer customer_id, String customer_name, String customer_email, String customer_contact, String customer_address, String customer_image, Integer ct_id, String ct_name, Integer cs_id, String cs_name) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_email = customer_email;
        this.customer_contact = customer_contact;
        this.customer_address = customer_address;
        this.customer_image = customer_image;
        this.ct_id = ct_id;
        this.ct_name = ct_name;
        this.cs_id = cs_id;
        this.cs_name = cs_name;
    }

    public Customer(String customer_name, String customer_email, String customer_contact, String customer_address, String customer_current_address, String customer_cccd, String customer_dob, String customer_cccd_date, String customer_cccd_place, String customer_note, Integer ct_id, Integer cs_id) {
        this.customer_name = customer_name;
        this.customer_email = customer_email;
        this.customer_contact = customer_contact;
        this.customer_address = customer_address;
        this.customer_current_address = customer_current_address;
        this.customer_cccd = customer_cccd;
        this.customer_dob = customer_dob;
        this.customer_cccd_date = customer_cccd_date;
        this.customer_cccd_place = customer_cccd_place;
        this.customer_note = customer_note;
        this.ct_id = ct_id;
        this.cs_id = cs_id;
    }

    public Customer(Integer customer_id, String customer_name, String customer_email, String customer_contact,String customer_dob, String customer_address, String customer_current_address, String customer_cccd, String customer_cccd_date, String customer_cccd_place, Integer ct_id, String cs_name, String customer_note, Integer index) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_email = customer_email;
        this.customer_contact = customer_contact;
        this.customer_dob = customer_dob;
        this.customer_address = customer_address;
        this.customer_current_address = customer_current_address;
        this.customer_cccd = customer_cccd;
        this.customer_cccd_date = customer_cccd_date;
        this.customer_cccd_place = customer_cccd_place;
        this.ct_id = ct_id;
        this.cs_name = cs_name;
        this.customer_note = customer_note;
        this.btnChangeStatus = "Change Status";
        this.btnUpdate = "Update";
        this.btnTransfer = "Transfer";
        this.index = index;
    }

    public Customer(Integer customer_id, String customer_name, String customer_contact, String sc_total_payment, Integer index) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_contact = customer_contact;
        this.sc_total_payment = sc_total_payment;
        this.index = index;
    }
}
