/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyImage;
import com.vnhp.real.estate.model.entities.ProductRevenue;
import java.util.LinkedList;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

/**
 *
 * @author FairyHunter
 */
public class RevenuePane extends Pane {

    private final MyImage icon;
    private final Label tittle;
    private final Label content;
    private final ComboBox<String> choice;

    public RevenuePane(String icon, String tittle, String content, LinkedList<String> choice, double height, double width, String backgroundStyle) {
        this.setPrefHeight(height);
        this.setPrefWidth(width);
        this.getStyleClass().add(backgroundStyle);

        this.icon = new MyImage(StringValue.Instance().ASSET + icon, 70, 70);
        this.icon.setLayoutX(25);
        this.icon.setLayoutY(40);

        this.tittle = new Label(tittle);
        this.tittle.setLayoutX(85);
        this.tittle.setLayoutY(8);
        this.tittle.getStyleClass().add("Revenue-Tittle");

        this.content = new Label(content);
        this.content.setLayoutX(130);
        this.content.setLayoutY(75);
        this.content.getStyleClass().add("Revenue-Content");

        this.choice = new ComboBox<>();
        this.choice.getItems().addAll(choice);
        this.choice.setValue(choice.get(0));
        this.choice.addEventHandler(ActionEvent.ACTION, eh -> {
            updateContent(this.choice.getValue());
        });
        this.choice.setLayoutX(200);
        this.choice.setLayoutY(130);

        this.getChildren().addAll(this.icon, this.tittle, this.content, this.choice);
    }

    private void updateContent(String choiceText) {
        var tmpContent = "$ ";
        var data = new ProductRevenue();
        var current = TimeManager.Instance().getServerTime();
        var last = "";

        if (choiceText.equals(StringValue.Instance().CURRENT_MONTH)) {
            current.getTime().setDate(1);
        }

        if (choiceText.equals(StringValue.Instance().LAST_3_MONTHS)) {
            var tmpLast = TimeManager.Instance().getMonthServerTime(-3);
            current.getTime().setDate(1);
            tmpLast.setDate(1);
            last = String.valueOf(tmpLast.getTime());
        }

        if (choiceText.equals(StringValue.Instance().LAST_6_MONTHS)) {
            var tmpLast = TimeManager.Instance().getMonthServerTime(-6);
            current.getTime().setDate(1);
            tmpLast.setDate(1);
            last = String.valueOf(tmpLast.getTime());
        }

        if (choiceText.equals(StringValue.Instance().CURRENT_YEAR)) {

        }

        if (choiceText.equals(StringValue.Instance().LAST_3_YEARS)) {
            var tmpLast = TimeManager.Instance().getYearServerTime(-3);
            current.getTime().setDate(1);
            tmpLast.setDate(1);
            last = String.valueOf(tmpLast.getTime());
        }

        if (choiceText.equals(StringValue.Instance().LAST_5_YEARS)) {
            var tmpLast = TimeManager.Instance().getYearServerTime(-5);
            current.getTime().setDate(1);
            tmpLast.setDate(1);
            last = String.valueOf(tmpLast.getTime());
        }

        if (choiceText.equals(StringValue.Instance().LAST_10_YEARS)) {
            var tmpLast = TimeManager.Instance().getYearServerTime(-10);
            current.getTime().setDate(1);
            tmpLast.setDate(1);
            last = String.valueOf(tmpLast.getTime());
        }

        if (last.isBlank() || last.isEmpty()) {
            tmpContent += ConvertObject.Instance().toRevenue(BaseDao.Instance().getListData(BaseDao.Instance().dateQuery(data, String.valueOf(current.getTimeInMillis()), last), ProductRevenue[].class), ServerService.Instance().GET_PRODUCT_REVENUE).toString();
        } else {
            tmpContent += ConvertObject.Instance().toRevenue(BaseDao.Instance().getListData(BaseDao.Instance().dateQuery(data, last, String.valueOf(current.getTimeInMillis())), ProductRevenue[].class), ServerService.Instance().GET_PRODUCT_REVENUE).toString();
        }

        this.content.setText(tmpContent);
    }
}
