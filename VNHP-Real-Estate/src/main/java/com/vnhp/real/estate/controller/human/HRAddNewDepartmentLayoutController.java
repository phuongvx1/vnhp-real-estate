/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRAddNewDepartmentLayoutController  {
    @FXML
    private Pane pHRDepartment;
    @FXML
    private Button btnNewDepartment, btnViewDepartment, btnDSNew, btnDSView;

    public void init(Stage stage){
        setup(stage);
    }    
        private void setup(Stage stage) {
        btnNewDepartment.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_ADDNEWDEPT));
                AnchorPane dashboard = dataNew.load();
                pHRDepartment.getChildren().clear();
                pHRDepartment.getChildren().add(dashboard);
                var control = (HRAddNewDepartmentController) dataNew.getController();
                control.init(stage);
            } catch (IOException e) {
                e.printStackTrace();
            }}); 
        btnViewDepartment.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_VIEWDEPTLAYOUT));
                AnchorPane dashboard = dataNew.load();
                pHRDepartment.getChildren().clear();
                pHRDepartment.getChildren().add(dashboard);
                var control = (HRViewDepartmentController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }}); 
        btnDSNew.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_DEPTSTATUSNEW));
                AnchorPane dashboard = dataNew.load();
                pHRDepartment.getChildren().clear();
                pHRDepartment.getChildren().add(dashboard);
                var control = (HRDepartmentStatusNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }});
        btnDSView.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_DEPTSTATUSVIEW));
                AnchorPane dashboard = dataNew.load();
                pHRDepartment.getChildren().clear();
                pHRDepartment.getChildren().add(dashboard);
                var control = (HRDepartmentStatusViewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }});
}}
    
