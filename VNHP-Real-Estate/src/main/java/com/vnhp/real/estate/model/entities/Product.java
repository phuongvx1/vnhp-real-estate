/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class Product {

    public Integer index;
    @ColumnId
    public Integer product_id;
    @ColumnName
    @DbColumn
    public String product_name;
    @DbColumn
    public String product_descrbe;
    @DbColumn
    public Float product_price;
    public Integer block_id;
    public String block_name;
    public Integer region_id;
    @DbColumn
    public String region_name;
    public Integer direction_id;
    @DbColumn
    public String direction_name;
    @ColumnStatus
    public Integer ps_id;
    @DbColumn
    public String ps_name;
    public String project_name;
    public String gb_name;
    public String pm_name;
    public String cont_name;
    public String cons_name;
    public String cont_id;
    public String cons_id;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setDirection_id(Integer direction_id) {
        this.direction_id = direction_id;
    }

    public void setDirection_name(String direction_name) {
        this.direction_name = direction_name;
    }

    public Integer getDirection_id() {
        return direction_id;
    }

    public String getDirection_name() {
        return direction_name;
    }

    public void setBlock_id(Integer block_id) {
        this.block_id = block_id;
    }

    public String getBlock_name() {
        return block_name;
    }

    public void setBlock_name(String block_name) {
        this.block_name = block_name;
    }

    public Integer getBlock_id() {
        return block_id;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setRegion_id(Integer region_id) {
        this.region_id = region_id;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public Integer getIndex() {
        return index;
    }

    public Integer getRegion_id() {
        return region_id;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setProduct_descrbe(String product_descrbe) {
        this.product_descrbe = product_descrbe;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public void setProduct_price(Float product_price) {
        this.product_price = product_price;
    }

    public void setPs_id(Integer ps_id) {
        this.ps_id = ps_id;
    }

    public void setPs_name(String ps_name) {
        this.ps_name = ps_name;
    }

    public void setCons_name(String cons_name) {
        this.cons_name = cons_name;
    }

    public void setCont_name(String cont_name) {
        this.cont_name = cont_name;
    }

    public void setGb_name(String gb_name) {
        this.gb_name = gb_name;
    }

    public void setPm_name(String pm_name) {
        this.pm_name = pm_name;
    }

    public String getProduct_descrbe() {
        return product_descrbe;
    }

    public Integer getProduct_id() {
        return product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public Float getProduct_price() {
        return product_price;
    }

    public Integer getPs_id() {
        return ps_id;
    }

    public String getPs_name() {
        return ps_name;
    }

    public String getCons_name() {
        return cons_name;
    }

    public String getCont_name() {
        return cont_name;
    }

    public String getGb_name() {
        return gb_name;
    }

    public String getPm_name() {
        return pm_name;
    }

    public Product() {
    }

    public Product(String product_name) {
        this.product_name = product_name;
    }

    public Product(String product_name, String product_descrbe, Float product_price, Integer block_id, String block_name, Integer ps_id) {
        this.product_name = product_name;
        this.product_descrbe = product_descrbe;
        this.product_price = product_price;
        this.block_id = block_id;
        this.block_name = block_name;
        this.ps_id = ps_id;
    }

    public Product(String product_name, String product_descrbe, Float product_price, Integer block_id, Integer region_id, Integer ps_id, Integer direction_id) {
        this.product_name = product_name;
        this.product_descrbe = product_descrbe;
        this.product_price = product_price;
        this.block_id = block_id;
        this.ps_id = ps_id;
        this.region_id = region_id;
        this.direction_id = direction_id;
    }

    public Product(Integer product_id, String product_name, String product_descrbe, Float product_price, Integer region_id, Integer block_id) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_descrbe = product_descrbe;
        this.product_price = product_price;
        this.region_id = region_id;
        this.block_id = block_id;
    }

    public Product(Integer product_id, Integer ps_id) {
        this.product_id = product_id;
        this.ps_id = ps_id;
    }

    public Product(Integer product_id, String product_name, String product_descrbe, Float product_price, Integer block_id, String block_name, Integer ps_id, String ps_name, Integer direction_id, String direction_name) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_descrbe = product_descrbe;
        this.product_price = product_price;
        this.block_id = block_id;
        this.block_name = block_name;
        this.ps_id = ps_id;
        this.ps_name = ps_name;
        this.direction_id = direction_id;
        this.direction_name = direction_name;
    }

    public Product(Integer product_id, String product_name, Float product_price, String block_name, String region_name, String ps_name, String direction_name, Integer index) {
        this.index = index;
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_price = product_price;
        this.block_name = block_name;
        this.ps_name = ps_name;
        this.region_name = region_name;
        this.direction_name = direction_name;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }

}
