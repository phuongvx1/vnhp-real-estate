/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller;

import com.vnhp.real.estate.res.AppSetting;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.*;
import javafx.util.Duration;
import javafx.scene.Scene;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class AccountantLayoutController {

    private static Scene scene;
    @FXML
    private Pane ActionGroup;

    @FXML
    private Button notiBtn;

    @FXML
    private Button AccountantHome;

    @FXML
    private AnchorPane AccountantBody;

    @FXML
    private Label dateTime;

    @FXML
    private ImageView btnClose, btnMinimize;

    @FXML
    private AnchorPane ParentLayout;
    
    @FXML
    private Button btnLogout;

    public void init(Stage stage) throws IOException, SQLException, ParseException {
        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        TimeManager.Instance().initClock(dateTime);
        MaintananceView();
        NotificationController.init(ActionGroup);
    }


    private void MaintananceView() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().MAINTANANCE));
        try {
            AnchorPane newAnchor = loader.load();
            MaintananceController control = loader.getController();
            control.init();
            AccountantBody.getChildren().clear();
            AccountantBody.getChildren().add(newAnchor);
        } catch (IOException ex) {
            Logger.getLogger(AccountantLayoutController.class.getName()).log(Level.SEVERE, null, ex);
        }
        AccountantHome.setOnMousePressed(eh -> {
            FXMLLoader loader1 = new FXMLLoader(getClass().getResource(StringValue.Instance().MAINTANANCE));
            try {
                AnchorPane newAnchor = loader1.load();
                MaintananceController control = loader1.getController();
                control.init();
                AccountantBody.getChildren().clear();
                AccountantBody.getChildren().add(newAnchor);
            } catch (IOException ex) {
                Logger.getLogger(AccountantLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void SalaryView(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().SALARY));
        try {
            AnchorPane newAnchor = loader.load();
            SalaryController control = loader.getController();
            control.init();
            AccountantBody.getChildren().clear();
            AccountantBody.getChildren().add(newAnchor);
        } catch (IOException ex) {
            Logger.getLogger(AccountantLayoutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void RevenueView(ActionEvent event) {
        FXMLLoader loader1 = new FXMLLoader(getClass().getResource(StringValue.Instance().REVENUE));
        try {
            AnchorPane newAnchor = loader1.load();
            RevenueController control = loader1.getController();
            control.init();
            AccountantBody.getChildren().clear();
            AccountantBody.getChildren().add(newAnchor);
        } catch (IOException ex) {
            Logger.getLogger(AccountantLayoutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void InvoiceView(ActionEvent event) {
        FXMLLoader loader1 = new FXMLLoader(getClass().getResource(StringValue.Instance().INVOICE));
        try {
            AnchorPane newAnchor = loader1.load();
            InvoiceController control = loader1.getController();
            control.init();
            AccountantBody.getChildren().clear();
            AccountantBody.getChildren().add(newAnchor);
        } catch (IOException ex) {
            Logger.getLogger(AccountantLayoutController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void NotificationView(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(StringValue.Instance().NOTIPANE));
        Scene scene = notiBtn.getScene();

        root.translateYProperty().set(scene.getHeight() - 830);
        ParentLayout.getChildren().add(root);
        root.setLayoutX(1100);
        root.setLayoutY(110);
        Timeline timeline = new Timeline();
        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        KeyFrame kf = new KeyFrame(Duration.seconds(0.15), kv);
        timeline.getKeyFrames().add(kf);
        timeline.play();
        root.setOnMouseExited((MouseEvent eh) -> {
            ParentLayout.getChildren().remove(root);
        });
    }

    public void LogoutView(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource(StringValue.Instance().LOGOUTPANE));
        Scene scene = notiBtn.getScene();
        root.translateYProperty().set(scene.getHeight() - 830);
        ParentLayout.getChildren().add(root);
        root.setLayoutX(1330);
        root.setLayoutY(110);
        Timeline timeline = new Timeline();
        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
        KeyFrame kf = new KeyFrame(Duration.seconds(0.15), kv);
        timeline.getKeyFrames().add(kf);
        timeline.play();
        root.setOnMouseExited((MouseEvent eh) -> {
            ParentLayout.getChildren().remove(root);
        });
    }

    public void Logout(ActionEvent event) throws IOException {
        btnLogout.getScene().getWindow().hide();
        FXMLLoader loader = FXMLLoader.load(getClass().getResource(StringValue.Instance().LOGIN_PAGE));
        AnchorPane newAnchor = loader.load();

        Stage stage = new Stage();
        scene = new Scene(newAnchor);
        stage.setScene(scene);
        var control = (LoginPageController) loader.getController();
        control.init(stage);

        stage.show();
    }
}
