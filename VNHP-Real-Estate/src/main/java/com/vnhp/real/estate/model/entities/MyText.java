/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.TableName;
import javafx.scene.control.TextField;

/**
 *
 * @author FairyHunter
 */
public final class MyText extends TextField {

    public MyText(String value, String title, TableName tableName) {
        this.setText(value);
        this.getStyleClass().add(tableName + "-" + title);
    }
}
