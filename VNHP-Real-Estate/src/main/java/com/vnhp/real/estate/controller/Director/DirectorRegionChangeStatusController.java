/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import static com.vnhp.real.estate.controller.Director.DirectorProductNewController.ps;
import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.Region;
import com.vnhp.real.estate.model.entities.RegionStatus;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorRegionChangeStatusController {

    @FXML
    private Label tfDirRegionCSName;
    @FXML
    private ComboBox<String> tfDirRegionCSStatus;
    @FXML
    public Label tfDirRegionCSResult;
    @FXML
    private Button btnDirRegionCS;
//    @FXML
//    private ImageView btnClose, btnMinimize;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, Region data) {
//        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        setupDefault(data);

        setup(grid, gridIndex, tableSetting, data);
    }

    private void setupDefault(Region data) {
        tfDirRegionCSName.setText(data.getRegion_name());
        var regionStatus = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().REGION_STATUS_ACTIVE, RegionStatus[].class));
        tfDirRegionCSStatus.getItems().addAll(regionStatus);
        tfDirRegionCSStatus.setValue(data.getRs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Region data) {
        btnDirRegionCS.addEventHandler(ActionEvent.ACTION, eh -> {
            var rs_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new RegionStatus(tfDirRegionCSStatus.getValue()), RegionStatus[].class, 1), ColumnId.class);
            var tmpData = new Region(data.getRegion_id(), rs_id);
            var retult = BaseDao.Instance().update(tmpData);
            tfDirRegionCSResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirRegionCSResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Region) data;
            tmp.setRs_name(tfDirRegionCSStatus.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getRegion_id(), tmp);
        });

    }

}
