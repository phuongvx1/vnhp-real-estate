/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.PaymentType;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorPaymentTypeUpdateController {

    
     @FXML
    private TextField tfDirPMUpdateName;
    
    @FXML
    private Label tfDirPMUpdateResult;
    
    @FXML
    private Button btnDirPMUpdate;
    
    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, PaymentType data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }
    
    private void setupDefault(PaymentType data) {
        tfDirPMUpdateName.setText(data.getPmt_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, PaymentType data) {
        btnDirPMUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirPMUpdateName.getText();
            
            var newData = new PaymentType(data.getPmt_id(),name);
            var retult = BaseDao.Instance().update(newData);
            tfDirPMUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirPMUpdateResult, 2);
            
            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (PaymentType) data;
            tmp.setPmt_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getPmt_id(), tmp);
        });
    }
}
