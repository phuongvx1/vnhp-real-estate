/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRPositionLayoutController {
    @FXML
    private Pane pHRPosition;
    @FXML
    private Button btnNewPosition, btnViewPosition;
    public void init() {
        setup();
    }    

    private void setup() {
        btnNewPosition.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HR_POSITION_NEW));
                AnchorPane dashboard = dataNew.load();
                pHRPosition.getChildren().clear();
                pHRPosition.getChildren().add(dashboard);
                var control = (HRPositionNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }});
        btnViewPosition.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HR_POSITION_VIEW));
                AnchorPane dashboard = dataNew.load();
                pHRPosition.getChildren().clear();
                pHRPosition.getChildren().add(dashboard);
                var control = (HRPositionViewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }});
    }
    
}
