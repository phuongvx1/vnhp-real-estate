package com.vnhp.real.estate.jpa;

import com.vnhp.real.estate.model.db.DbConnection;
import com.vnhp.real.estate.model.entities.SaleReceipt;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

/**
 *
 * @author Lip
 */
public class SaleContract_jpa {

   public final LinkedList<SaleReceipt> getAll() {
        Connection conn = DbConnection.Instance().Connect();
        var ls = new LinkedList<SaleReceipt>();
        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery("select s.sc_id, s.sc_time_open, s.sc_total_payment, s.cont_id, cs.cons_name, a.account_name, c.customer_name, p.product_name "
                    + "from salecontract s\n"
                    + "join account a on (a.account_id = s.saler_id)\n"
                    + "join customer c on (c.customer_id = s.payer_id)\n"
                    + "join product p on (p.product_id = s.product_id)\n"
                    + "join contractstatus cs on (cs.cons_id = s.cons_id)\n"
                    + "order by s.sc_time_open DESC");
            while (rs.next()) {
                var item = newData(rs);
                ls.add(item);
            }
        } catch (SQLException e) {}
        return ls;
    }

    public final void UpdateStatus(int sc_id) {
        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            sm.executeQuery("Update SaleContract Set cons_id = 3 where sc_id = " + sc_id);
        } catch (SQLException e) {
        }
    }

    private SaleReceipt newData(ResultSet rs) {
        var tmp = new SaleReceipt();
        try {
            tmp = new SaleReceipt(
                    rs.getInt("sc_id"),
                    rs.getString("sc_time_open"),
                    rs.getFloat("sc_total_payment"),
                    rs.getString("account_name"),
                    rs.getString("product_name"),
                    rs.getString("customer_name"),
                    rs.getString("cons_name"),
                    rs.getInt("cont_id"));
        } catch (SQLException e) {}
        return tmp;
    }

    public final void UpdatePeriod(int sc_id) {
        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            sm.executeQuery("Update PaymentPeriod Set pps_id = 2 where sc_id = " + sc_id);
        } catch (SQLException e) {}
    }

}
