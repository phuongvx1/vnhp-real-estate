/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.GroupBlocks;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.Project;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import com.vnhp.real.estate.res.UtilitiesManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorGbUpdateController {

    @FXML
    private TextField tfDirGbUpdateName;

    @FXML
    private ComboBox<String> cbDirGbUpdateProject;
    @FXML
    private Pane pDirGbUpdateUtilities;
    @FXML
    private Label tfDirGbUpdateResult;

    @FXML
    private Button btnDirGbUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, GroupBlocks data) {
        setupDefault(subStage, data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(Stage stage, GroupBlocks data) {
        tfDirGbUpdateName.setText(data.getGb_name());

        pDirGbUpdateUtilities.getChildren().clear();
        pDirGbUpdateUtilities.getChildren().add(UtilitiesManager.Instance().Utilities(stage));

        var project = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_ACTIVE, Project[].class));
        cbDirGbUpdateProject.getItems().addAll(project);
        cbDirGbUpdateProject.setValue(data.getProject_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, GroupBlocks data) {
        btnDirGbUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirGbUpdateName.getText();
            var utilities = UtilitiesManager.Utilities.toString().substring(1, UtilitiesManager.Utilities.toString().length() - 1);
            var project_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Project(cbDirGbUpdateProject.getValue()), Project[].class, 1), ColumnId.class);

            var newData = new GroupBlocks(data.getGb_id(), name, utilities, project_id);
            var retult = BaseDao.Instance().update(newData);
            tfDirGbUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirGbUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (GroupBlocks) data;
            tmp.setGb_name(name);
            tmp.setGb_utilities(utilities);
            tmp.setProject_name(cbDirGbUpdateProject.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getGb_id(), tmp);
        });
    }

}
