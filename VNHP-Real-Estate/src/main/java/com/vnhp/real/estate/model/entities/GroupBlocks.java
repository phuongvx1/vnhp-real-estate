/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public class GroupBlocks {

    public Integer index;
    @ColumnId
    public Integer gb_id;
    @ColumnName
    @DbColumn
    public String gb_name;
    @DbColumn
    public String project_name;
    @DbColumn
    public String gb_utilities;
    @ColumnStatus
    public Integer gbs_id;
    @DbColumn
    public String gbs_name;
    public Integer project_id;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setGb_id(Integer gb_id) {
        this.gb_id = gb_id;
    }

    public void setGb_name(String gb_name) {
        this.gb_name = gb_name;
    }

    public void setGb_status(Integer gb_id) {
        this.gb_id = gb_id;
    }

    public void setGb_utilities(String gb_utilities) {
        this.gb_utilities = gb_utilities;
    }

    public void setGbs_name(String gbs_name) {
        this.gbs_name = gbs_name;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public Integer getGb_id() {
        return gb_id;
    }

    public String getGb_name() {
        return gb_name;
    }

    public Integer getGb_status() {
        return gb_id;
    }

    public String getGb_utilities() {
        return gb_utilities;
    }

    public String getGbs_name() {
        return gbs_name;
    }

    public Integer getIndex() {
        return index;
    }

    public Integer getProject_id() {
        return project_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public GroupBlocks() {
    }

    public GroupBlocks(String gb_name) {
        this.gb_name = gb_name;
    }

    public GroupBlocks(Integer gb_id, String gb_name, String gb_utilities, Integer project_id) {
        this.gb_id = gb_id;
        this.gb_name = gb_name;
        this.gb_utilities = gb_utilities;
        this.project_id = project_id;
    }

    public GroupBlocks(String gb_name, String gb_utilities, Integer gbs_id, Integer project_id) {
        this.gb_name = gb_name;
        this.gb_utilities = gb_utilities;
        this.gbs_id = gbs_id;
        this.project_id = project_id;
    }

    public GroupBlocks(Integer gb_id, String gb_name, String project_name, String gb_utilities, String gbs_name, Integer index) {
        this.index = index;
        this.gb_id = gb_id;
        this.gb_name = gb_name;
        this.gb_utilities = gb_utilities;
        this.gbs_name = gbs_name;
        this.project_name = project_name;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
