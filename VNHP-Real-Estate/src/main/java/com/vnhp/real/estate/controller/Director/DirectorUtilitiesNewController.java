/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Utilities;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorUtilitiesNewController {

    @FXML
    private TextField tfDirUtilitiesNewName;

    @FXML
    private Label tfDirUtilitiesNewResult;

    @FXML
    private Button btnDirUtilitiesNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirUtilitiesNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirUtilitiesNewName.getText();

            var data = new Utilities(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirUtilitiesNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirUtilitiesNewResult, 2);
        });
    }

}
