/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class Account {
    public Integer index;
    @ColumnId()
    public Integer account_id;
    public String account_name;
    public String account_email;
    public String account_password;
    public String account_image;
    public String account_address;
    public String account_contact;
    public String manager_id;
    public Integer als_id;
    public String als_name;
    public Integer department_id;
    public String dt_name;
    @ColumnStatus()
    public Integer as_id;
    public Integer position_id;
    public String as_name;
    public String account_dob;
    public String btnChangeStatus;
    public String btnUpdate;

    public Integer getIndex() {
        return index;
    }
     
    public void setIndex(Integer index) {
        this.index = index;
    } 
    
    public Account(Integer account_id, String account_name, String account_contact, String account_email, String account_address, String account_password, Integer department_id, Integer position_id) {
       this.account_id = account_id;
       this.account_name = account_name;
       this.account_contact = account_contact;
       this.account_email = account_email;
       this.account_address = account_address;
       this.account_password = account_password;
       this.department_id = department_id;
       this.position_id = position_id;
    }

    public Account(String account_name, String account_email, String account_address, String account_contact, Integer department_id, Integer position_id) {
        this.account_name = account_name;
        this.account_email = account_email;
        this.account_address = account_address;
        this.account_contact = account_contact;
        this.department_id = department_id;
        this.position_id = position_id;
    }
    
    public Account(String account_name, String account_email, String account_address, String account_contact, Integer department_id, Integer position_id, Integer index) {
        this.account_name = account_name;
        this.account_email = account_email;
        this.account_address = account_address;
        this.account_contact = account_contact;
        this.department_id = department_id;
        this.position_id = position_id;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }

    public void setAccount_address(String account_address) {
        this.account_address = account_address;
    }

    public void setAccount_contact(String account_contact) {
        this.account_contact = account_contact;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public void setPosition_id(Integer position_id) {
        this.position_id = position_id;
    }
    
    public void setAccount_email(String account_email) {
        this.account_email = account_email;
    }

    public void setAccount_id(Integer account_id) {
        this.account_id = account_id;
    }

    public void setAccount_image(String account_image) {
        this.account_image = account_image;
    }

    public void setAccount_login_status(Integer als_id) {
        this.als_id = als_id;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public void setAccount_password(String account_password) {
        this.account_password = account_password;
    }

    public void setAccount_status(Integer as_id) {
        this.as_id = as_id;
    }

    public void setAls_name(String als_name) {
        this.als_name = als_name;
    }

    public void setAs_name(String as_name) {
        this.as_name = as_name;
    }

    public void setDepartment_id(Integer department_id) {
        this.department_id = department_id;
    }

    public void setDt_name(String dt_name) {
        this.dt_name = dt_name;
    }

    public void setAs_id(Integer as_id) {
        this.as_id = as_id;
    }

    public void setAls_id(Integer als_id) {
        this.als_id = als_id;
    }

    public void setAccount_dob(String account_dob) {
        this.account_dob = account_dob;
    }

    public String getAccount_address() {
        return account_address;
    }

    public String getAccount_contact() {
        return account_contact;
    }

    public String getManager_id() {
        return manager_id;
    }
    
    public String getAccount_email() {
        return account_email;
    }

    public Integer getAccount_id() {
        return account_id;
    }

    public String getAccount_image() {
        return account_image;
    }

    public Integer getAccount_login_status() {
        return als_id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public String getAccount_password() {
        return account_password;
    }

    public Integer getAccount_status() {
        return as_id;
    }

    public String getAls_name() {
        return als_name;
    }

    public String getAs_name() {
        return as_name;
    }

    public Integer getDepartment_id() {
        return department_id;
    }

    public String getDt_name() {
        return dt_name;
    }

    public Integer getAs_id() {
        return as_id;
    }

    public Integer getPosition_id() {
        return position_id;
    }
    
    public Integer getAls_id() {
        return als_id;
    }

    public String getAccount_dob() {
        return account_dob;
    }

    public Account() {
    }

    public Account(String account_name, String account_email, String account_password, String account_image, String account_address, String account_contact, Integer als_id, Integer department_id, Integer account_status) {
        this.account_name = account_name;
        this.account_email = account_email;
        this.account_password = account_password;
        this.account_image = account_image;
        this.account_address = account_address;
        this.account_contact = account_contact;
        this.als_id = als_id;
        this.department_id = department_id;
        this.as_id = account_status;
    }

    public Account(Integer account_id, Integer als_id) {
        this.account_id = account_id;
        this.als_id = als_id;
    }

    public Account(Integer account_id, Integer als_id, Integer account_status) {
        this.account_id = account_id;
        this.als_id = als_id;
        this.as_id = account_status;
    }

    public Account(Integer account_id, String account_name, String account_email, String account_password, String account_image, String account_address, String account_contact, Integer department_id) {
        this.account_id = account_id;
        this.account_name = account_name;
        this.account_email = account_email;
        this.account_password = account_password;
        this.account_image = account_image;
        this.account_address = account_address;
        this.account_contact = account_contact;
        this.department_id = department_id;
    }

    public Account(Integer account_id, String account_name, String account_email, String account_password, String account_image, String account_address, String account_contact, Integer als_id, String als_name, Integer department_id, String dt_name, Integer as_id, String as_name) {
        this.account_id = account_id;
        this.account_name = account_name;
        this.account_email = account_email;
        this.account_password = account_password;
        this.account_image = account_image;
        this.account_address = account_address;
        this.account_contact = account_contact;
        this.als_id = als_id;
        this.als_name = als_name;
        this.department_id = department_id;
        this.dt_name = dt_name;
        this.as_id = as_id;
        this.as_name = as_name;
    }

    public Account(Integer account_id, String account_image) {
        this.account_id = account_id;
        this.account_image = account_image;
    }

    public Account(Integer account_id, String account_image, Integer as_id) {
        this.account_id = account_id;
        this.account_image = account_image;
        this.as_id = as_id;
    }
    
    public Account(Integer account_id, String account_name, String account_dob, String account_address, String account_contact, String account_email) {
        this.account_id = account_id;
        this.account_name = account_name;
        this.account_dob = account_dob;
        this.account_address = account_address;
        this.account_contact = account_contact;
        this.account_email = account_email;
    }

    public Account(Integer account_id, String account_name, String account_dob, String account_address, String account_contact, String account_email, String account_password) {
        this.account_id = account_id;
        this.account_name = account_name;
        this.account_dob = account_dob;
        this.account_address = account_address;
        this.account_contact = account_contact;
        this.account_email = account_email;
        this.account_password = account_password;
    }
}
