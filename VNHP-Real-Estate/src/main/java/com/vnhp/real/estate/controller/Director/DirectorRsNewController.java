/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.RegionStatus;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorRsNewController {

    @FXML
    private TextField tfDirRSNewName;

    @FXML
    private Label tfDirRSNewResult;

    @FXML
    private Button btnDirRSNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirRSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirRSNewName.getText();

            var data = new RegionStatus(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirRSNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirRSNewResult, 2);
        });
    }

}
