/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class ConstructionUnit {

    public Integer index;
    @ColumnId
    public Integer cu_id;
    @ColumnName
    @DbColumn
    public String cu_name;
    @DbColumn
    public String cu_email;
    @DbColumn
    public String cu_address;
    @DbColumn
    public String cu_logo;
    @DbColumn
    public Integer cu_done_project_quatity;
    @DbColumn
    public String cu_contact;
    @ColumnStatus
    public Integer cus_id;
    @DbColumn
    public String cus_name;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setCu_address(String cu_address) {
        this.cu_address = cu_address;
    }

    public void setCu_contact(String cu_contact) {
        this.cu_contact = cu_contact;
    }

    public void setCu_done_project_quatity(Integer cu_done_project_quatity) {
        this.cu_done_project_quatity = cu_done_project_quatity;
    }

    public void setCu_email(String cu_email) {
        this.cu_email = cu_email;
    }

    public void setCu_id(Integer cu_id) {
        this.cu_id = cu_id;
    }

    public void setCu_logo(String cu_logo) {
        this.cu_logo = cu_logo;
    }

    public void setCu_name(String cu_name) {
        this.cu_name = cu_name;
    }

    public void setCu_status(Integer cus_id) {
        this.cus_id = cus_id;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCu_address() {
        return cu_address;
    }

    public String getCu_contact() {
        return cu_contact;
    }

    public Integer getCu_done_project_quatity() {
        return cu_done_project_quatity;
    }

    public String getCu_email() {
        return cu_email;
    }

    public Integer getCu_id() {
        return cu_id;
    }

    public String getCu_logo() {
        return cu_logo;
    }

    public String getCu_name() {
        return cu_name;
    }

    public Integer getCu_status() {
        return cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public ConstructionUnit() {
    }

    public ConstructionUnit(Integer cu_id, String cu_name, String cu_email, String cu_address, String cu_logo, String cu_contact) {
        this.cu_id = cu_id;
        this.cu_name = cu_name;
        this.cu_email = cu_email;
        this.cu_address = cu_address;
        this.cu_logo = cu_logo;
        this.cu_contact = cu_contact;
    }

    public ConstructionUnit(String cu_name, String cu_email, String cu_address, String cu_logo, String cu_contact, Integer cus_id) {
        this.cu_name = cu_name;
        this.cu_email = cu_email;
        this.cu_address = cu_address;
        this.cu_logo = cu_logo;
        this.cu_contact = cu_contact;
        this.cus_id = cus_id;
    }

    public ConstructionUnit(Integer cu_id, Integer is_id) {
        this.cu_id = cu_id;
        this.cus_id = is_id;
    }

    public ConstructionUnit(String cu_name) {
        this.cu_name = cu_name;
    }

    public ConstructionUnit(Integer cu_id, Integer cu_done_project_quatity, Integer cus_id) {
        this.cu_id = cu_id;
        this.cu_done_project_quatity = cu_done_project_quatity;
        this.cus_id = cus_id;
    }

    public ConstructionUnit(Integer cu_id, String cu_name, String cu_email, String cu_address, String cu_logo, Integer cu_done_project_quatity, String cu_contact, String cus_name, Integer index) {
        this.cu_id = cu_id;
        this.cu_name = cu_name;
        this.cu_email = cu_email;
        this.cu_address = cu_address;
        this.cu_logo = cu_logo;
        this.cu_done_project_quatity = cu_done_project_quatity;
        this.cu_contact = cu_contact;
        this.cus_name = cus_name;
        this.index = index;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
