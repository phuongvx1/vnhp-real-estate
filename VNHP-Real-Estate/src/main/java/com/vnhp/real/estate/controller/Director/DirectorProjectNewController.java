/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.web.*;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProjectNewController {

    @FXML
    TextField tfDirProjectNewName, tfDirProjectNewLocation, tfDirProjectNewArea, tfDirProjectNewJuridical, tfDirProjectNewDensity;

    @FXML
    Pane tfDirProjectNewUtilities;

    @FXML
    ComboBox<String> tfDirType, tfDirProjectNewCU, tfDirProjectNewInvestor,tfDirStatus;

    @FXML
    HTMLEditor tfDirProjectNewInfo;

    @FXML
    Button btnDirProjectNew;
    
    @FXML
    Label tfDirProjectNewResult;

    public void init(Stage stage) {
        setupDefault(stage);
        setup();
    }

    private void setupDefault(Stage stage) {
        tfDirProjectNewUtilities.getChildren().clear();
        tfDirProjectNewUtilities.getChildren().add(UtilitiesManager.Instance().Utilities(stage));

        var projectCU = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CONTRUCTION_ACTIVE, ConstructionUnit[].class));
        tfDirProjectNewCU.getItems().addAll(projectCU);
        tfDirProjectNewCU.setValue((String) projectCU.get(0));

        var investor = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().INVESTOR_ACTIVE, Investor[].class));
        tfDirProjectNewInvestor.getItems().addAll(investor);
        tfDirProjectNewInvestor.setValue((String) investor.get(0));

        var type = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_TYPE, ProjectType[].class));
        tfDirType.getItems().addAll(type);
        tfDirType.setValue((String) type.get(0));
        
        var status = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_STATUS, ProjectStatus[].class));
        tfDirStatus.getItems().addAll(status);
        tfDirStatus.setValue((String) status.get(0));
    }

    private void setup() {

        btnDirProjectNew.addEventHandler(ActionEvent.ACTION, mouseEvent -> {

            var name = tfDirProjectNewName.getText();
            var location = tfDirProjectNewLocation.getText();
            var area = Float.parseFloat(tfDirProjectNewArea.getText());
            var juridical = tfDirProjectNewJuridical.getText();
            var density = tfDirProjectNewDensity.getText();
            var pInfo = tfDirProjectNewInfo.getHtmlText();

            var utilities = UtilitiesManager.Utilities.toString().substring(1, UtilitiesManager.Utilities.toString().length() - 1);
            var projectCU = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ConstructionUnit(tfDirProjectNewCU.getValue()), ConstructionUnit[].class, 1), ColumnId.class);
            var investor = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Investor(tfDirProjectNewInvestor.getValue()), Investor[].class, 1), ColumnId.class);
            var type = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ProjectType(tfDirType.getValue()), ProjectType[].class, 1), ColumnId.class);
            var status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ProjectStatus(tfDirStatus.getValue()), ProjectStatus[].class, 1), ColumnId.class);

            var data = new Project(name,pInfo, location, area, juridical, density, utilities, projectCU, investor, type,status);
            var retult = BaseDao.Instance().insert(data);
            tfDirProjectNewResult.setText(retult);
            UtilitiesManager.Utilities.clear();
            TimeManager.Instance().clearResult(mouseEvent, tfDirProjectNewResult, 2);
        });
    }
}
