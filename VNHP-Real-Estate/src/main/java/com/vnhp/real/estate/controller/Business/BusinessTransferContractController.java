package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class BusinessTransferContractController {

    @FXML
    private TextField customer1Name, customer1DOB, customer1CCCD, customer1Place, customer1Date, customer1Address, customer1CurrentAddress,
            customer2Name, customer2DOB, customer2CCCD, customer2Place, customer2Date, customer2Address, customer2CurrentAddress;

    @FXML
    private Button transferContractAddBtn;

    @FXML
    private Label addResult;

    public void init(Stage stage) {
        customer1Name.setText(AllCustomerController.saleCustomer.getCustomer_name());
        customer1DOB.setText(AllCustomerController.saleCustomer.getCustomer_dob());
        customer1CCCD.setText(AllCustomerController.saleCustomer.customer_cccd);
        customer1Place.setText(AllCustomerController.saleCustomer.getCustomer_cccd_place());
        customer1Date.setText(AllCustomerController.saleCustomer.getCustomer_cccd_date());
        customer1Address.setText(AllCustomerController.saleCustomer.getCustomer_address());
        customer1CurrentAddress.setText(AllCustomerController.saleCustomer.getCustomer_current_address());

        customer2Name.setText(AllCustomerController.customer.getCustomer_name());
        customer2DOB.setText(AllCustomerController.customer.getCustomer_dob());
        customer2CCCD.setText(AllCustomerController.customer.customer_cccd);
        customer2Place.setText(AllCustomerController.customer.getCustomer_cccd_place());
        customer2Date.setText(AllCustomerController.customer.getCustomer_cccd_date());
        customer2Address.setText(AllCustomerController.customer.getCustomer_address());
        customer2CurrentAddress.setText(AllCustomerController.customer.getCustomer_current_address());

        addTransferContract();
    }

    private void addTransferContract() {
        transferContractAddBtn.addEventHandler(ActionEvent.ACTION, mouseEvent -> {
            var current_time = TimeManager.Instance().getServerTime();
            var tc_time_open = String.valueOf(current_time.getTimeInMillis());

            var statisticData = BaseDao.Instance().getListData(StringValue.Instance().STATISTIC_ALL, Statistic[].class);
            var tc_number = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "TransferContract") + current_time.getTime().getYear() + "/HĐNT/PVI-ST";

            var tc_fee = AllSaleContractController.saleContract.getSc_total_payment();
            var tc_saler_id = AllCustomerController.saleCustomer.getCustomer_id();
            var tc_transfer_id = AllCustomerController.customer.getCustomer_id();
            var sc_id = AllSaleContractController.saleContract.getSc_id();

            var data = new TransferContract(tc_time_open, tc_number, tc_fee, tc_saler_id, tc_transfer_id, sc_id);

            var result = BaseDao.Instance().insert(data);

            addResult.setText(result);
            TimeManager.Instance().clearResult(mouseEvent, addResult, 2);
        });
    }
}
