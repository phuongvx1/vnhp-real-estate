/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Gb_Status;
import com.vnhp.real.estate.model.entities.GroupBlocks;
import com.vnhp.real.estate.model.entities.Project;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import com.vnhp.real.estate.res.UtilitiesManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorGbNewController {

    @FXML
    private TextField tfDirGbNewName;

    @FXML
    private Label tfDirGbNewResult;

    @FXML
    private ComboBox<String> cbDirGbNewProject, cbDirGbNewStatus;

    @FXML
    private Pane pDirGbNewUtilities;

    @FXML
    private Button btnDirGbNew;

    public void init(Stage stage) {
        setupDefault(stage);
        setup();
    }

    private void setupDefault(Stage stage) {
        pDirGbNewUtilities.getChildren().clear();
        pDirGbNewUtilities.getChildren().add(UtilitiesManager.Instance().Utilities(stage));

        var project = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_ACTIVE, Project[].class));
        cbDirGbNewProject.getItems().addAll(project);
        cbDirGbNewProject.setValue((String) project.get(0));

        var gb_Status = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().GB_STATUS_ACTIVE, Gb_Status[].class));
        cbDirGbNewStatus.getItems().addAll(gb_Status);
        cbDirGbNewStatus.setValue((String) gb_Status.get(0));
    }

    private void setup() {
        btnDirGbNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirGbNewName.getText();
            var utilities = UtilitiesManager.Utilities.toString().substring(1, UtilitiesManager.Utilities.toString().length() - 1);
            var gbs_id= ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Gb_Status(cbDirGbNewStatus.getValue()), Gb_Status[].class, 1), ColumnId.class);
            var project_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Project(cbDirGbNewProject.getValue()), Project[].class, 1), ColumnId.class);

            var data = new GroupBlocks(name, utilities, gbs_id, project_id);
            var retult = BaseDao.Instance().insert(data);
            tfDirGbNewResult.setText(retult);
            UtilitiesManager.Utilities.clear();
            TimeManager.Instance().clearResult(eh, tfDirGbNewResult, 2);
        });
    }

}
