/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class PaymentPeriodStatus {

    public Integer index;
    @ColumnId
    public Integer pps_id;
    @ColumnName
    @DbColumn
    public String pps_name;
    @ColumnStatus
    @DbColumn
    public Boolean pps_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setPps_id(Integer pps_id) {
        this.pps_id = pps_id;
    }

    public void setPps_name(String pps_name) {
        this.pps_name = pps_name;
    }

    public void setPps_status(Boolean pps_status) {
        this.pps_status = pps_status;
    }

    public Integer getPps_id() {
        return pps_id;
    }

    public String getPps_name() {
        return pps_name;
    }

    public Boolean getPps_status() {
        return pps_status;
    }

    public PaymentPeriodStatus() {
    }

    public PaymentPeriodStatus(String pps_name) {
        this.pps_name = pps_name;
    }

    public PaymentPeriodStatus(Integer pps_id, String pps_name) {
        this.pps_id = pps_id;
        this.pps_name = pps_name;
    }

    public PaymentPeriodStatus(Integer pps_id, Boolean pps_status) {
        this.pps_id = pps_id;
        this.pps_status = pps_status;
    }

    public PaymentPeriodStatus(Integer pps_id, String pps_name, Boolean pps_status, Integer index) {
        this.index = index;
        this.pps_id = pps_id;
        this.pps_name = pps_name;
        this.pps_status = pps_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
