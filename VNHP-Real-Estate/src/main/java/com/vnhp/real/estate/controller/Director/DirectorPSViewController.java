/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.ProjectStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.ObjectType;
import com.vnhp.real.estate.res.ServerService;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableName;
import com.vnhp.real.estate.res.TableSetting;
import java.util.LinkedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.ColumnConstraints;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorPSViewController {

    @FXML
    private ScrollPane pDirPSView;

    @FXML
    private TextField tfDirPSSearch;

    public void init() {
        setup();
    }

    private void setupSearch(MyGridPane grid, TableSetting tableSetting) {
        tfDirPSSearch.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var searchText = tfDirPSSearch.getText();
                var data = new ProjectStatus();
                var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(BaseDao.Instance().searchQuery(data, "", searchText), ProjectStatus[].class), ServerService.Instance().GET_PROJECT_STATUS);
                grid.clearAll();
                grid.setRow(grid, listData, tableSetting, TableName.ProjectStatus);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setup() {
        var gridPane = new MyGridPane();

        var titleList = new LinkedList<String>();
        titleList.add("ID");
        titleList.add("Name");
        titleList.add("Status");
        titleList.add("Change Status");
        titleList.add("Update");

        gridPane.addTitles(titleList, TableName.ProjectStatus);
        var rowSettings = new LinkedList<String>();
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyButton.toString());
        rowSettings.add(ObjectType.MyButton.toString());

        var col1 = new ColumnConstraints();
        col1.setPrefWidth(30);
        var col2 = new ColumnConstraints();
        col2.setPrefWidth(150);
        var col3 = new ColumnConstraints();
        col3.setPrefWidth(100);
        var col4 = new ColumnConstraints();
        col4.setPrefWidth(150);
        var col5 = new ColumnConstraints();
        col5.setPrefWidth(100);

        var setting = new LinkedList<ColumnConstraints>();
        setting.add(col1);
        setting.add(col2);
        setting.add(col3);
        setting.add(col4);
        setting.add(col5);

        var tableSetting = new TableSetting(rowSettings, setting);
        var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_STATUS, ProjectStatus[].class), ServerService.Instance().GET_PROJECT_STATUS);

        gridPane.setLayoutX(10);
        gridPane.setRow(gridPane, listData, tableSetting, TableName.ProjectStatus);
        gridPane.getColumnConstraints().addAll(tableSetting.getSetting());
        pDirPSView.setLayoutX(50);
        pDirPSView.setLayoutY(80);
        pDirPSView.setBackground(Background.EMPTY);
        pDirPSView.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pDirPSView.setContent(gridPane);
        setupSearch(gridPane, tableSetting);

        pDirPSView.vvalueProperty().addListener(eh -> update(gridPane, tableSetting));
    }
    private boolean ck = true;

    private void update(MyGridPane grid, TableSetting tableSetting) {
        if (ck) {
            ck = false;
            var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_STATUS, ProjectStatus[].class), ServerService.Instance().GET_PROJECT_STATUS);
            grid.clearAll();
            grid.setRow(grid, listData, tableSetting, TableName.ProjectStatus);
        }
    }
}
