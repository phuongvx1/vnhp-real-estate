/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.controller;

import com.vnhp.real.estate.jpa.DepositContract_jpa;
import com.vnhp.real.estate.jpa.PaymentPeriod_jpa;
import com.vnhp.real.estate.model.entities.PaymentPeriod;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;

/**
 *
 * @author Lip
 */
public class NotificationController {

    public static void init(Pane ActionGroup) throws SQLException, ParseException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c1 = Calendar.getInstance();
        c1.add(Calendar.DATE, 5);
        
        PaymentPeriod_jpa newJpa = new PaymentPeriod_jpa();
        var arr = newJpa.getAllFromPaymentPeriod();
        for (PaymentPeriod paymentPeriod : arr) {
            if(dateFormat.format(c1.getTime()).toString().equals(paymentPeriod.pp_time)){
                System.out.print("Da den han thanh toan hop dong so "+ paymentPeriod.sc_id);
            }
        }

        
        setup(ActionGroup);
    }

    public static void setup(Pane ActionGroup) throws SQLException {
        int DepNotiCount = 0;
        int PeriodNotiCount = 0;
        DepositContract_jpa newDepJpa = new DepositContract_jpa();
        PaymentPeriod_jpa newPerJpa = new PaymentPeriod_jpa();
        DepNotiCount = newDepJpa.CountNoti();
        PeriodNotiCount = newPerJpa.CountNoti();
        if (DepNotiCount + PeriodNotiCount > 0) {
            StackPane newStk = NotificationBadge(DepNotiCount + PeriodNotiCount);
            ActionGroup.getChildren().add(newStk);
        }
    }

    public static StackPane NotificationBadge(int Count) {
        StackPane stkPane = new StackPane();
        stkPane.setLayoutX(59);
        stkPane.setLayoutY(-4);
        stkPane.prefHeight(17);
        stkPane.prefWidth(17);

        Circle cir = new Circle();
        cir.setRadius(9);
        cir.setStrokeType(StrokeType.INSIDE);
        cir.setFill(javafx.scene.paint.Color.RED);

        Label count = new Label(String.valueOf(Count));
        count.setFont(Font.font("Verdana", 11));
        count.setStyle("-fx-text-fill: white");

        stkPane.getChildren().addAll(cir, count);
        return stkPane;
    }
;
}
