/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.controller.Business.AllCustomerChangeStatusController;
import com.vnhp.real.estate.controller.Business.AllCustomerController;
import com.vnhp.real.estate.controller.Business.AllSaleContractController;
import com.vnhp.real.estate.controller.Director.DirectorBlockChangeStatusController;
import com.vnhp.real.estate.controller.Director.DirectorBlockUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorBsUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorCUChangeStatusController;
import com.vnhp.real.estate.controller.Director.DirectorCUSUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorCUUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorConsUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorContUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorCsUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorCtUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorDirectionUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorGBChangeStatusController;
import com.vnhp.real.estate.controller.Director.DirectorGbUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorGbsUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorISUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorInvestorChangeStatusController;
import com.vnhp.real.estate.controller.Director.DirectorInvestorUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorPSUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorPTUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorPaymentTypeUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorPpsUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorProductChangeStatusController;
import com.vnhp.real.estate.controller.Director.DirectorProductStatusUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorProductUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorProjectUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorRegionChangeStatusController;
import com.vnhp.real.estate.controller.Director.DirectorRegionUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorRsUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorUtilitiesUpdateController;
import com.vnhp.real.estate.controller.Director.DirectorProjectChangeStatusController;
import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.res.ButtonType;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableName;
import com.vnhp.real.estate.res.TableSetting;
import java.io.IOException;
import java.util.Objects;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author FairyHunter
 */
public final class MyButton<T> extends Pane {

    private Integer value;
    private Integer gridIndex;
    private T data;
    private MyImage defaultButton;

    public void setGridIndex(Integer gridIndex) {
        this.gridIndex = gridIndex;
    }

    public Integer getGridIndex() {
        return gridIndex;
    }

    private void btnOnClick(MyGridPane grid, TableName tableName, String value, TableSetting tableSetting, Integer gridIndex, Integer id) {
        try {
            if (value.replaceAll(" ", "").equals(ButtonType.ChangeStatus.toString())) {
                changeStatusData(grid, tableName, tableSetting, gridIndex, id);
            }
            if (value.replaceAll(" ", "").equals(ButtonType.ChangePassword.toString())) {
                changePasswordData(grid, tableName, tableSetting);
            }
            if (value.replaceAll(" ", "").equals(ButtonType.Transfer.toString())) {
                transferData(tableName, id);
            }
            if (value.equals(ButtonType.Update.toString())) {
                updateData(grid, tableName, tableSetting, gridIndex, id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeStatusData(MyGridPane grid, TableName tableName, TableSetting tableSetting, Integer gridIndex, Integer id) {

        if (!this.value.equals(id)) {
            return;
        }
        if (tableName.equals(TableName.Account)) {

        }

        if (tableName.equals(TableName.AccountLoginStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (AccountLoginStatus) data;
                var newData = new AccountLoginStatus(tmpData.getAls_id(), tmpStatus);
                tmpData.setAls_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getAls_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.AccountStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (AccountStatus) data;
                var newData = new AccountStatus(tmpData.getas_id(), tmpStatus);
                tmpData.setas_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getas_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.B_Status)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (B_Status) data;
                var newData = new B_Status(tmpData.getBs_id(), tmpStatus);
                tmpData.setBs_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getBs_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Blocks)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_BLOCK_CS));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorBlockChangeStatusController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Blocks) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.CancelContract)) {

        }

        if (tableName.equals(TableName.ConstructionUnit)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CU_CS));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorCUChangeStatusController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (ConstructionUnit) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ConstructionUnitStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (ConstructionUnitStatus) data;
                var newData = new ConstructionUnitStatus(tmpData.getCus_id(), tmpStatus);
                tmpData.setCus_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getCus_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ContractStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (ContractStatus) data;
                var newData = new ContractStatus(tmpData.getCons_id(), tmpStatus);
                tmpData.setCons_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getCons_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ContractType)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (ContractType) data;
                var newData = new ContractType(tmpData.getCont_id(), tmpStatus);
                tmpData.setCont_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getCont_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Customer)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_ALL_CHANGESTT));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (AllCustomerChangeStatusController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Customer) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.CustomerStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (CustomerStatus) data;
                var newData = new CustomerStatus(tmpData.getCs_id(), tmpStatus);
                tmpData.setCs_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getCs_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.CustomerType)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (CustomerType) data;
                var newData = new CustomerType(tmpData.getCt_id(), tmpStatus);
                tmpData.setCt_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getCt_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Department)) {

        }

        if (tableName.equals(TableName.DepartmentStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (DepartmentStatus) data;
                var newData = new DepartmentStatus(tmpData.getds_id(), tmpStatus);
                tmpData.setds_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getds_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tableName.equals(TableName.Direction)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (Direction) data;
                var newData = new Direction(tmpData.getDirection_id(), tmpStatus);
                tmpData.setDirection_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getDirection_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Gb_Status)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (Gb_Status) data;
                var newData = new Gb_Status(tmpData.getGbs_id(), tmpStatus);
                tmpData.setGbs_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getGbs_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.GroupBlocks)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_GB_CS));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorGBChangeStatusController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (GroupBlocks) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Investor)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_INVESTOR_CS));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorInvestorChangeStatusController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Investor) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.InvestorStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (InvestorStatus) data;
                var newData = new InvestorStatus(tmpData.getIs_id(), tmpStatus);
                tmpData.setIs_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getIs_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.PaymentPeriod)) {

        }

        if (tableName.equals(TableName.PaymentPeriodStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (PaymentPeriodStatus) data;
                var newData = new PaymentPeriodStatus(tmpData.getPps_id(), tmpStatus);
                tmpData.setPps_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getPps_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.PaymentType)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (PaymentType) data;
                var newData = new PaymentType(tmpData.getPmt_id(), tmpStatus);
                tmpData.setPmt_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getPmt_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Position)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (Position) data;
                var newData = new Position(tmpData.getPosition_id(), tmpStatus);
                tmpData.setPosition_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getPosition_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (tableName.equals(TableName.Product)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PRODUCT_CS));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorProductChangeStatusController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Product) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ProductStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (ProductStatus) data;
                var newData = new ProductStatus(tmpData.getPs_id(), tmpStatus);
                tmpData.setPs_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getPs_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Project)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PROJECT_CS));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorProjectChangeStatusController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Project) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ProjectStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (ProjectStatus) data;
                var newData = new ProjectStatus(tmpData.getPs_id(), tmpStatus);
                tmpData.setPs_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getPs_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ProjectType)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (ProjectType) data;
                var newData = new ProjectType(tmpData.getPt_id(), tmpStatus);
                tmpData.setPt_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getPt_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Region)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_REGION_CS));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorRegionChangeStatusController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Region) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.RegionStatus)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (RegionStatus) data;
                var newData = new RegionStatus(tmpData.getRs_id(), tmpStatus);
                tmpData.setRs_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);
                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getRs_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.SaleContract)) {

        }

        if (tableName.equals(TableName.TranferContract)) {

        }

        if (tableName.equals(TableName.Utilities)) {
            try {
                var tmpStatus = ConvertObject.Instance().getBool(data, ColumnStatus.class);
                var tmpData = (Utilities) data;
                var newData = new Utilities(tmpData.getUtilities_id(), tmpStatus);
                tmpData.setUtilities_status(tmpStatus);
                var ck = BaseDao.Instance().update(newData);

                if (ck.equals(StringValue.Instance().SUCCESSFULL)) {
                    grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
                    var values = ConvertObject.Instance().dataToString(tmpData);
                    grid.addRow(grid, gridIndex, values, tableSetting, tmpData.getUtilities_id(), tmpData);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateData(MyGridPane grid, TableName tableName, TableSetting tableSetting, Integer gridIndex, Integer id) {
        if (!this.value.equals(id)) {
            return;
        }

        if (tableName.equals(TableName.Account)) {

        }

        if (tableName.equals(TableName.AccountLoginStatus)) {

        }

        if (tableName.equals(TableName.AccountStatus)) {

        }

        if (tableName.equals(TableName.B_Status)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_BS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorBsUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (B_Status) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Blocks)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_BLOCK_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorBlockUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Blocks) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.CancelContract)) {

        }

        if (tableName.equals(TableName.ConstructionUnit)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CU_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorCUUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (ConstructionUnit) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ConstructionUnitStatus)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CUS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorCUSUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (ConstructionUnitStatus) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ContractStatus)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CONS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorConsUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (ContractStatus) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ContractType)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CONT_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorContUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (ContractType) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Customer)) {

        }

        if (tableName.equals(TableName.CustomerStatus)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorCsUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (CustomerStatus) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.CustomerType)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CT_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorCtUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (CustomerType) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Department)) {

        }

        if (tableName.equals(TableName.DepartmentStatus)) {

        }

        if (tableName.equals(TableName.Gb_Status)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_GBS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorGbsUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Gb_Status) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.GroupBlocks)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_GB_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorGbUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (GroupBlocks) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Direction)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_DIRECTION_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorDirectionUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Direction) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Investor)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_INVESTOR_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorInvestorUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Investor) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.InvestorStatus)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_IS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorISUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (InvestorStatus) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.PaymentPeriod)) {

        }

        if (tableName.equals(TableName.PaymentPeriodStatus)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PPS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorPpsUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (PaymentPeriodStatus) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.PaymentType)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PM_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorPaymentTypeUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (PaymentType) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Position)) {

        }

        if (tableName.equals(TableName.Product)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PRODUCT_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorProductUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Product) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ProductStatus)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PRODUCT_STATUS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorProductStatusUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (ProductStatus) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Project)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PROJECT_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorProjectUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Project) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ProjectStatus)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorPSUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (ProjectStatus) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.ProjectType)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PT_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorPTUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (ProjectType) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.Region)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_REGION_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorRegionUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Region) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.RegionStatus)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_RS_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));

                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorRsUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (RegionStatus) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tableName.equals(TableName.SaleContract)) {

        }

        if (tableName.equals(TableName.TranferContract)) {

        }

        if (tableName.equals(TableName.Utilities)) {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_UTILITIES_UPDATE));
                AnchorPane scene = loader.load();
                var subStage = new Stage();
                subStage.initModality(Modality.APPLICATION_MODAL);
                subStage.setScene(new Scene(scene));
                subStage.setResizable(false);
                subStage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
//                        subStage.initStyle(StageStyle.TRANSPARENT);
                var control = (DirectorUtilitiesUpdateController) loader.getController();
                control.init(grid, gridIndex, tableSetting, subStage, (Utilities) data);

                subStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void changePasswordData(MyGridPane grid, TableName tableName, TableSetting tableSetting) {

    }

    private void transferData(TableName tableName, Integer id) {
        if (!this.value.equals(id)) {
            return;
        }
        if (tableName.equals(TableName.Customer)) {
            AllCustomerController.customer = (Customer) data;
        }
        if (tableName.equals(TableName.SaleContract)) {
            AllSaleContractController.saleContract = (SaleContract) data;
            
            var saleCustomer = (Customer) ConvertObject.Instance().toData(BaseDao.Instance().getListData(StringValue.Instance().SALE_CUSTOMER + AllSaleContractController.saleContract.getPayer_id(), Customer[].class));
            AllCustomerController.saleCustomer = saleCustomer;
        }
    }

    public MyButton(MyGridPane grid, String value, String title, TableName tableName, TableSetting tableSetting, Integer gridIndex, Integer id, T data, Integer index) {
        if (value.replaceAll(" ", "").equals(ButtonType.ChangeStatus.toString())) {
            this.defaultButton = new MyImage(StringValue.Instance().CHANGE_STATUS, 30, 30);
            this.defaultButton.setLayoutX(50);
        }
        if (value.replaceAll(" ", "").equals(ButtonType.Update.toString())) {
            this.defaultButton = new MyImage(StringValue.Instance().UPDATE_HAND, 30, 30);
            this.defaultButton.setLayoutX(20);
        }
        if (value.replaceAll(" ", "").equals(ButtonType.Transfer.toString())) {
            this.defaultButton = new MyImage(StringValue.Instance().TRANSFER, 30, 30);
            this.defaultButton.setLayoutX(20);
        }
        this.data = data;
        this.gridIndex = gridIndex;
        this.value = id;
        if (index % 2 == 0) {
            this.getStyleClass().add(tableName + "-Even-" + title);
        } else {
            this.getStyleClass().add(tableName + "-Odd-" + title);
        }
        this.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        this.defaultButton.setCursor(Cursor.HAND);
        this.defaultButton.setOnMousePressed(eh -> {
            try {
                btnOnClick(grid, tableName, value, tableSetting, gridIndex, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        this.getChildren().add(this.defaultButton);
    }
}
