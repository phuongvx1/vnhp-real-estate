/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public class Blocks {

    public Integer index;
    @ColumnId
    public Integer block_id;
    @ColumnName
    @DbColumn
    public String block_name;
    @DbColumn
    public String gb_name;
    @DbColumn
    public String block_utilities;
    @DbColumn
    public String bs_name;
    @ColumnStatus
    public Integer bs_id;
    public Integer gb_id;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setBlock_id(Integer block_id) {
        this.block_id = block_id;
    }

    public void setBlock_name(String block_name) {
        this.block_name = block_name;
    }

    public void setBlock_utilities(String block_utilities) {
        this.block_utilities = block_utilities;
    }

    public void setBs_id(Integer bs_id) {
        this.bs_id = bs_id;
    }

    public void setBs_name(String bs_name) {
        this.bs_name = bs_name;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setGb_name(String gb_name) {
        this.gb_name = gb_name;
    }

    public void setGb_id(Integer gb_id) {
        this.gb_id = gb_id;
    }

    public Integer getBlock_id() {
        return block_id;
    }

    public String getBlock_name() {
        return block_name;
    }

    public String getBlock_utilities() {
        return block_utilities;
    }

    public Integer getBs_id() {
        return bs_id;
    }

    public String getBs_name() {
        return bs_name;
    }

    public Integer getIndex() {
        return index;
    }

    public String getGb_name() {
        return gb_name;
    }

    public Integer getGb_id() {
        return gb_id;
    }

    public Blocks() {
    }

    public Blocks(String block_name) {
        this.block_name = block_name;
    }

    public Blocks(Integer block_id, String block_name, String block_utilities, Integer gb_id) {
        this.block_id = block_id;
        this.block_utilities = block_utilities;
        this.block_name = block_name;
        this.gb_id = gb_id;
    }

    public Blocks(String block_name, String block_utilities, Integer bs_id, Integer gb_id) {
        this.block_name = block_name;
        this.block_utilities = block_utilities;
        this.bs_id = bs_id;
        this.gb_id = gb_id;
    }

    public Blocks(Integer block_id, Integer bs_id) {
        this.block_id = block_id;
        this.bs_id = bs_id;
    }

    public Blocks(Integer block_id, String block_name, String gb_name, String block_utilities, String bs_name, Integer index) {
        this.index = index;
        this.block_id = block_id;
        this.block_name = block_name;
        this.block_utilities = block_utilities;
        this.bs_name = bs_name;
        this.gb_name = gb_name;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
