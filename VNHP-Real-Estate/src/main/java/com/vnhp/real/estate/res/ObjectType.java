/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

/**
 *
 * @author FairyHunter
 */
public enum ObjectType {
    MyLabel,
    MyText,
    MyButton,
    MyImage,
    InvestorComponent
}
