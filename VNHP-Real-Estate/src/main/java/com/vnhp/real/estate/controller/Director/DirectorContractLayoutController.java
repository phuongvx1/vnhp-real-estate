/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorContractLayoutController {

    @FXML
    private Button btnDirConsNew, btnDirConsView, btnDirContNew, btnDirContView, btnDirCSNew, btnDirCSView,
            btnDirCTNew, btnDirCTView, btnDirPPSNew, btnDirPPSView;
    @FXML
    private Pane pDirContactNew;

    public void init() {
//        setupDefault();
        setup();
    }

    private void setupDefault() {
        try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CONS_NEW));
            AnchorPane dashboard = dataNew.load();
            pDirContactNew.getChildren().clear();
            pDirContactNew.getChildren().add(dashboard);
            var control = (DirectorConsNewController) dataNew.getController();
            control.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setup() {
        btnDirConsNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CONS_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorConsNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirConsView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CONS_VIEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorConsViewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirContNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CONT_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorContNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirContView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CONT_VIEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorContViewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirCSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CS_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorCsNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirCSView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CS_VIEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorCsViewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirCTNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CT_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorCtNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirCTView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CT_VIEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorCtViewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPPSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PPS_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorPpsNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPPSView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PPS_VIEW));
                AnchorPane dashboard = dataNew.load();
                pDirContactNew.getChildren().clear();
                pDirContactNew.getChildren().add(dashboard);
                var control = (DirectorPpsViewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
