/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;

/**
 *
 * @author FairyHunter
 */
public final class AccountStatus {

    public Integer index;
    @ColumnId
    public Integer as_id;
    @ColumnName
    public String as_name;
    @ColumnStatus
    public Boolean as_status;
    public String btnChangePassword;
    public String btnUpdate;
    
    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }
    
    public void setas_id(Integer as_id) {
        this.as_id = as_id;
    }

    public void setas_name(String as_name) {
        this.as_name = as_name;
    }

    public void setas_status(Boolean as_status) {
        this.as_status = as_status;
    }

    public Integer getas_id() {
        return as_id;
    }

    public String getas_name() {
        return as_name;
    }

    public Boolean getas_status() {
        return as_status;
    }

    public AccountStatus() {
    }

    public AccountStatus(String as_name) {
        this.as_name = as_name;
    }

    public AccountStatus(Integer as_id, String as_name) {
        this.as_id = as_id;
        this.as_name = as_name;
    }

    public AccountStatus(Integer as_id ,Boolean as_status) {
        this.as_id = as_id;
        this.as_status = as_status;
    }

    public AccountStatus(Integer as_id, String as_name, Boolean as_status, Integer index) {
        this.index = index;
        this.as_id = as_id;
        this.as_name = as_name;
        this.as_status = as_status;
        this.btnChangePassword = "Change Status";
        this.btnUpdate = "Update";
    }
}
