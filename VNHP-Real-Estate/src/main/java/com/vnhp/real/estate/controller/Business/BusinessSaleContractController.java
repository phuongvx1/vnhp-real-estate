package com.vnhp.real.estate.controller.Business;

import static com.vnhp.real.estate.controller.Business.AllCustomerController.*;
import com.vnhp.real.estate.controller.Business.AllSaleContractController;
import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class BusinessSaleContractController {

    @FXML
    private TextField customerName, customerDOB, customerCCCD, customerPlace, customerDate, customerAddress, customerCurrentAddress,
            companyName, companyCNDK, companyAddress, companySalesOffice, companyContact, companyFax, companyBank, companyDirectorName, companyDirectorPosition;

    @FXML
    private Button saleContractAddBtn;

    @FXML
    private Label addResult;

    @FXML
    private ComboBox<String> projectSelect, groupBlockSelect, blockSelect, productSelect, paymentTypeSelect, contractTypeSelect, contractStatusSelect;

    public static SaleContract transferFromSaleContract = new SaleContract();

    public void init(Stage stage) {
        customerName.setText(customer.getCustomer_name());
        customerDOB.setText(customer.getCustomer_dob());
        customerCCCD.setText(customer.customer_cccd);
        customerPlace.setText(customer.getCustomer_cccd_place());
        customerDate.setText(customer.getCustomer_cccd_date());
        customerAddress.setText(customer.getCustomer_address());
        customerCurrentAddress.setText(customer.getCustomer_current_address());

        var productDetail = (Product) ConvertObject.Instance().toData(BaseDao.Instance().getListData(StringValue.Instance().PRODUCT_DETAIL + AllSaleContractController.saleContract.getProduct_id(), Product[].class));
        if (productDetail == null) {
            var project = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_ACTIVE, Project[].class));
            projectSelect.getItems().clear();
            projectSelect.getItems().addAll(project);
            projectSelect.setValue((String) project.get(0));

            var groupBlock = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().GROUP_BLOCKS_ACTIVE, GroupBlocks[].class));
            groupBlockSelect.getItems().clear();
            groupBlockSelect.getItems().addAll(groupBlock);
            groupBlockSelect.setValue((String) groupBlock.get(0));

            var block = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().BLOCKS_ACTIVE, Blocks[].class));
            blockSelect.getItems().clear();
            blockSelect.getItems().addAll(block);
            blockSelect.setValue((String) block.get(0));

            var product = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PRODUCT_ACTIVE, Product[].class));
            productSelect.getItems().clear();
            productSelect.getItems().addAll(product);
            productSelect.setValue((String) product.get(0));

            var paymentType = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PAYMENT_TYPE_ACTIVE, PaymentType[].class));
            paymentTypeSelect.getItems().clear();
            paymentTypeSelect.getItems().addAll(paymentType);
            paymentTypeSelect.setValue((String) paymentType.get(0));

            var contractType = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CONTRACT_TYPE_ACTIVE, ContractType[].class));
            contractTypeSelect.getItems().clear();
            contractTypeSelect.getItems().addAll(contractType);
            contractTypeSelect.setValue((String) contractType.get(0));

            var contractStatus = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CONTRACT_STATUS_ACTIVE, ContractStatus[].class));
            contractStatusSelect.getItems().clear();
            contractStatusSelect.getItems().addAll(contractStatus);
            contractStatusSelect.setValue((String) contractStatus.get(0));
        } else {
            projectSelect.setValue(productDetail.getProject_name());
            groupBlockSelect.setValue(productDetail.getGb_name());
            blockSelect.setValue(productDetail.getBlock_name());
            productSelect.setValue(productDetail.product_name);

            paymentTypeSelect.setValue(AllSaleContractController.saleContract.getPm_name());
            contractTypeSelect.setValue(AllSaleContractController.saleContract.getCont_name());
            contractStatusSelect.setValue(AllSaleContractController.saleContract.getCons_name());
        }

        var company = (Company) ConvertObject.Instance().toData(BaseDao.Instance().getListData(StringValue.Instance().COMPANY, Company[].class));
        companyName.setText(company.getCompany_name());
        companyCNDK.setText(company.getCompany_cnkd());
        companyAddress.setText(company.getCompany_address());
        companySalesOffice.setText(company.getDirector_position());
        companyContact.setText(company.getCompany_contact());
        companyFax.setText(company.getCompany_fax());
        companyBank.setText(company.getCompany_bank());
        companyDirectorName.setText(company.getDirector_name());
        companyDirectorPosition.setText(company.getDirector_position());

        setUp();
        addSaleContract();
    }

    private void setUp() {
        var project = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_ACTIVE, Project[].class));
        projectSelect.getItems().clear();
        projectSelect.getItems().addAll(project);
        projectSelect.setValue((String) project.get(0));

        projectSelect.addEventHandler(ActionEvent.ACTION, mouseEvent -> {
            var groupBlock = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().GROUP_BLOCKS_ACTIVE_NEW + "'" + projectSelect.getValue() + "'", GroupBlocks[].class));
            groupBlockSelect.getItems().clear();
            groupBlockSelect.getItems().addAll(groupBlock);
            groupBlockSelect.setValue((String) groupBlock.get(0));
        });

        groupBlockSelect.addEventHandler(ActionEvent.ACTION, mouseEvent -> {
            var block = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().BLOCKS_ACTIVE_NEW + "'" + groupBlockSelect.getValue() + "'", Blocks[].class));
            blockSelect.getItems().clear();
            blockSelect.getItems().addAll(block);
            blockSelect.setValue((String) block.get(0));
        });

        blockSelect.addEventHandler(ActionEvent.ACTION, mouseEvent -> {
            var product = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PRODUCT_ACTIVE_NEW + "'" + blockSelect.getValue() + "'", Product[].class));
            productSelect.getItems().clear();
            productSelect.getItems().addAll(product);
            productSelect.setValue((String) product.get(0));
        });

        var paymentType = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PAYMENT_TYPE_ACTIVE, PaymentType[].class));
        paymentTypeSelect.getItems().clear();
        paymentTypeSelect.getItems().addAll(paymentType);
        paymentTypeSelect.setValue((String) paymentType.get(0));

        var contractType = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CONTRACT_TYPE_ACTIVE, ContractType[].class));
        contractTypeSelect.getItems().clear();
        contractTypeSelect.getItems().addAll(contractType);
        contractTypeSelect.setValue((String) contractType.get(0));

        var contractStatus = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CONTRACT_STATUS_ACTIVE, ContractStatus[].class));
        contractStatusSelect.getItems().clear();
        contractStatusSelect.getItems().addAll(contractStatus);
        contractStatusSelect.setValue((String) contractStatus.get(0));
    }

    private void addSaleContract() {
        saleContractAddBtn.addEventHandler(ActionEvent.ACTION, mouseEvent -> {
            var sc_time_open = String.valueOf(TimeManager.Instance().getServerTime().getTimeInMillis());
            var saler_id = 1; //Hanh lam xong thay bien vao
            var payer_id = customer.getCustomer_id();

            var product_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Product(productSelect.getValue()), Product[].class, 1), ColumnId.class);

            var cont_id = 0;
            var cons_id = 0;
            var pm_id = 0;
            var company_id = 1;

            var sc_total_payment = (float) 0;
            var sc_number = 0;
            if (AllSaleContractController.saleContract.getSc_number() == null) {
                var statisticData = BaseDao.Instance().getListData(StringValue.Instance().STATISTIC_ALL, Statistic[].class);
                sc_number = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "SaleContract") + 1;
                cont_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ContractType(contractTypeSelect.getValue()), ContractType[].class, 1), ColumnId.class);
                cons_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ContractStatus(contractStatusSelect.getValue()), ContractStatus[].class, 1), ColumnId.class);
                pm_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new PaymentType(paymentTypeSelect.getValue()), PaymentType[].class, 1), ColumnId.class);

            } else {
                sc_number = AllSaleContractController.saleContract.getSc_number();
                cont_id = AllSaleContractController.saleContract.getCont_id();
                cons_id = AllSaleContractController.saleContract.getCons_id();
                pm_id = AllSaleContractController.saleContract.getSaler_id();
            }

            var data = new SaleContract(sc_time_open, sc_number, sc_total_payment, saler_id, payer_id, product_id, pm_id, cont_id, cons_id, company_id);
            var result = BaseDao.Instance().insert(data);

            transferFromSaleContract = (SaleContract) ConvertObject.Instance().toData(BaseDao.Instance().getData(new SaleContract(sc_time_open), SaleContract[].class, 1));

            addResult.setText(result);
            TimeManager.Instance().clearResult(mouseEvent, addResult, 2);
        });
    }

}
