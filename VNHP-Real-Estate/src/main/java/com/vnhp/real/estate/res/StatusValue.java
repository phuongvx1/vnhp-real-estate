/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

/**
 *
 * @author FairyHunter
 */
public class StatusValue {

    private static StatusValue instance;

    private StatusValue() {

    }

    public static StatusValue Instance() {
        if (instance == null) {
            instance = new StatusValue();
        }

        return instance;
    }

    public final Integer CU_ACTIVE = 1;
    public final Integer CU_INACTIVE = 2;
    public final Integer CUS_ACTIVE = 1;
    public final Integer CUS_INACTIVE = 2;
    public final Integer DIRECTION_ACTIVE = 1;
    public final Integer DIRECTION_INACTIVE = 2;
}
