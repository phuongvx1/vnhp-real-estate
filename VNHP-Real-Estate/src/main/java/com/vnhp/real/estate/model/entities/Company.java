/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.*;

/**
 *
 * @author bichv
 */
public class Company {

    public Integer index;
    @ColumnId
    public Integer company_id;
    public String company_name;
    public String company_cnkd;
    public String company_address;
    public String company_sales_office;
    public String company_contact;
    public String company_fax;
    public String company_bank;
    public Integer director_id;
    public String director_name;
    public String director_position;
    @ColumnStatus
    public String company_status;

    public Company() {

    }

    public Company(String company_name, String company_cnkd, String company_address, String company_sales_office, String company_contact, String company_fax, String company_bank, String director_name, String director_position) {
        this.company_name = company_name;
        this.company_cnkd = company_cnkd;
        this.company_address = company_address;
        this.company_sales_office = company_sales_office;
        this.company_contact = company_contact;
        this.company_fax = company_fax;
        this.company_bank = company_bank;
        this.director_name = director_name;
        this.director_position = director_position;
    }

    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public void setCompany_cnkd(String company_cnkd) {
        this.company_cnkd = company_cnkd;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public void setCompany_sales_office(String company_sales_office) {
        this.company_sales_office = company_sales_office;
    }

    public void setCompany_contact(String company_contact) {
        this.company_contact = company_contact;
    }

    public void setCompany_fax(String company_fax) {
        this.company_fax = company_fax;
    }

    public void setCompany_bank(String company_bank) {
        this.company_bank = company_bank;
    }

    public void setDirector_id(Integer director_id) {
        this.director_id = director_id;
    }

    public void setDirector_name(String director_name) {
        this.director_name = director_name;
    }

    public void setDirector_position(String director_position) {
        this.director_position = director_position;
    }

    public void setCompany_status(String company_status) {
        this.company_status = company_status;
    }

    public Integer getCompany_id() {
        return company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public String getCompany_cnkd() {
        return company_cnkd;
    }

    public String getCompany_address() {
        return company_address;
    }

    public String getCompany_sales_office() {
        return company_sales_office;
    }

    public String getCompany_contact() {
        return company_contact;
    }

    public String getCompany_fax() {
        return company_fax;
    }

    public String getCompany_bank() {
        return company_bank;
    }

    public Integer getDirector_id() {
        return director_id;
    }

    public String getDirector_name() {
        return director_name;
    }

    public String getDirector_position() {
        return director_position;
    }

    public String getCompany_status() {
        return company_status;
    }

}
