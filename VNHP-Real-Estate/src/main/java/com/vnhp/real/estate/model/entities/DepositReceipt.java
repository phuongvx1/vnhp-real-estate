/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.StringValue;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author bichv
 */
public class DepositReceipt {

    public int dc_id;
    public String dc_time_open;
    public int dc_number;
    public String dc_payment;
    public int sc_id;
    public String dc_status;
    public String sc_total_payment;
    public String product_name;
    public Button button;
    public Label sttLabel;

    public DepositReceipt() {
    }

    public DepositReceipt(String dc_time_open, int dc_number, String dc_payment, int sc_id, String sc_total_payment) {
        this.dc_time_open = dc_time_open;
        this.dc_number = dc_number;
        this.dc_payment = dc_payment;
        this.sc_id = sc_id;
        this.sc_total_payment = sc_total_payment;
    }
    
    public DepositReceipt(int dc_id, String dc_time_open, int dc_number, String dc_payment, int sc_id, String dc_status, String sc_total_payment, String product_name) {
        this.dc_id = dc_id;
        this.dc_time_open = dc_time_open;
        this.dc_number = dc_number;
        this.dc_payment = dc_payment;
        this.sc_id = sc_id;
        this.dc_status = dc_status;
        this.sc_total_payment = sc_total_payment;
        this.product_name = product_name;
    }

    public DepositReceipt(int dc_number, int sc_id, String product_name, String sc_total_payment, String dc_payment, String dc_time_open, String dc_status, Button button) throws FileNotFoundException {
        this.dc_time_open = dc_time_open;
        this.dc_number = dc_number;
        this.dc_payment = dc_payment;
        this.sc_id = sc_id;
        this.button = button;
        this.dc_status = dc_status;
        this.sc_total_payment = sc_total_payment;
        this.product_name = product_name;
        ImageView imageView = new ImageView(new Image(new FileInputStream(StringValue.Instance().ASSET + "printer.png")));

        if ("0".equals(this.dc_status)) {
            this.sttLabel = new Label("Unpaid");
            this.sttLabel.setPrefWidth(70);
            this.sttLabel.setPrefHeight(20);
            this.sttLabel.setAlignment(Pos.CENTER);
            this.sttLabel.setStyle("-fx-background-color: #dc3545; -fx-text-fill: white;");
            this.button.setText("Export");
            this.button.setStyle("-fx-background-color: #28a745; -fx-text-fill: white;");
            this.button.setGraphic(imageView);
        } else {
            this.sttLabel = new Label("Paid");
            this.sttLabel.setPrefWidth(70);
            this.sttLabel.setPrefHeight(20);
            this.sttLabel.setAlignment(Pos.CENTER);
            this.sttLabel.setStyle("-fx-background-color: #007bff; -fx-text-fill: white;");
            this.button.setText("Export");
            this.button.setStyle("-fx-background-color: #28a745; -fx-text-fill: white;");
            this.button.setDisable(true);
            this.button.setGraphic(imageView);

        }
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public Label getSttLabel() {
        return sttLabel;
    }

    public void setSttLabel(Label sttLabel) {
        this.sttLabel = sttLabel;
    }

    public void setDc_id(int dc_id) {
        this.dc_id = dc_id;
    }

    public void setDc_number(int dc_number) {
        this.dc_number = dc_number;
    }

    public void setDc_payment(String dc_payment) {
        this.dc_payment = dc_payment;
    }

    public void setDc_status(String dc_status) {
        this.dc_status = dc_status;
    }

    public void setDc_time_open(String dc_time_open) {
        this.dc_time_open = dc_time_open;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public void setSc_id(int sc_id) {
        this.sc_id = sc_id;
    }

    public void setSc_total_payment(String sc_total_payment) {
        this.sc_total_payment = sc_total_payment;
    }

    public int getDc_id() {
        return dc_id;
    }

    public int getDc_number() {
        return dc_number;
    }

    public String getDc_payment() {
        return dc_payment;
    }

    public String getDc_status() {
        return dc_status;
    }

    public String getDc_time_open() {
        return dc_time_open;
    }

    public String getProduct_name() {
        return product_name;
    }

    public int getSc_id() {
        return sc_id;
    }

    public String getSc_total_payment() {
        return sc_total_payment;
    }
}
