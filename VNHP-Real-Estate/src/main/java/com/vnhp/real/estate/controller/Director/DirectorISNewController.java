/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.InvestorStatus;
import com.vnhp.real.estate.model.entities.ProjectStatus;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorISNewController {

    @FXML
    private TextField tfDirISNewName;

    @FXML
    private Label tfDirISNewResult;

    @FXML
    private Button btnDirISNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirISNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirISNewName.getText();

            var data = new InvestorStatus(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirISNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirISNewResult, 2);
        });
    }
}
