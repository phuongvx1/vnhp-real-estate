/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.ConstructionUnit;
import com.vnhp.real.estate.model.entities.ConstructionUnitStatus;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorCUNewController {

    @FXML
    private TextField tfDirCUNewName, tfDirCUNewEmail, tfDirCUNewAddress, tfDirCUNewLogo, tfDirCUNewContract;

    @FXML
    private Label tfDirCUNewResult;

    @FXML
    private ComboBox<String> tfDirCUNewStatus;

    @FXML
    private Button btnDirCUNew;

    public void init() {
        setupDefault();
        setup();
    }

    private void setupDefault() {
        var cus = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CONTRUCTION_UNIT_STATUS_ACTIVE, ConstructionUnitStatus[].class));
        tfDirCUNewStatus.getItems().addAll(cus);
        tfDirCUNewStatus.setValue((String) cus.get(0));
    }

    private void setup() {
        btnDirCUNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirCUNewName.getText();
            var email = tfDirCUNewEmail.getText();
            var address = tfDirCUNewAddress.getText();
            var logo = tfDirCUNewLogo.getText();
            var contract = tfDirCUNewContract.getText();

            var cus_status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ConstructionUnitStatus(tfDirCUNewStatus.getValue()), ConstructionUnitStatus[].class, 1), ColumnId.class);

            var data = new ConstructionUnit(name, email, address, logo, contract, cus_status);
            var retult = BaseDao.Instance().insert(data);
            tfDirCUNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirCUNewResult, 2);
        });
    }

}
