/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller;

import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;



public class MaintananceController {
    @FXML
    private TextField searchBox;
    @FXML
    private ScrollPane depositConstract, saleConstract, creditConstract;
    @FXML
    private Tab depTab, saleTab, creditTab;

    public void init() {
        Setup();
    }

    public void Setup() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DEPOSITCONSTRACT));
        try {
            loader.load();
            DepositConstractController control = loader.getController();
            control.init(depositConstract, searchBox);
        } catch (IOException ex) {}
        
        depTab.setOnSelectionChanged(eh -> {
            FXMLLoader loader1 = new FXMLLoader(getClass().getResource(StringValue.Instance().DEPOSITCONSTRACT));
            try {
                loader1.load();
                DepositConstractController control = loader1.getController();
                control.init(depositConstract, searchBox);
            } catch (IOException ex) {}
        });
        
        saleTab.setOnSelectionChanged(eh -> {
            FXMLLoader loader2 = new FXMLLoader(getClass().getResource(StringValue.Instance().SALECONSTRACT));
            try {
                loader2.load();
                SaleConstractController control = loader2.getController();
                control.init(saleConstract, searchBox);
            } catch (IOException ex) {}
        });
        
        creditTab.setOnSelectionChanged(eh -> {
            FXMLLoader loader3 = new FXMLLoader(getClass().getResource(StringValue.Instance().CREDITCONSTRACT));
            try {
                loader3.load();
                CreditConstractController control = loader3.getController();
                control.init(creditConstract, searchBox);
            } catch (IOException ex) {}
        });
        
    }

}