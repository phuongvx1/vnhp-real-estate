/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class Gb_Status {

    public Integer index;
    @ColumnId
    public Integer gbs_id;
    @ColumnName
    @DbColumn
    public String gbs_name;
    @ColumnStatus
    @DbColumn
    public Boolean gbs_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setGbs_id(Integer gbs_id) {
        this.gbs_id = gbs_id;
    }

    public Integer getIndex() {
        return index;
    }

    public Boolean getGbs_status() {
        return gbs_status;
    }

    public String getGbs_name() {
        return gbs_name;
    }

    public Integer getGbs_id() {
        return gbs_id;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setGbs_status(Boolean gbs_status) {
        this.gbs_status = gbs_status;
    }

    public void setGbs_name(String gbs_name) {
        this.gbs_name = gbs_name;
    }

    public Gb_Status() {
    }

    public Gb_Status(String gbs_name) {
        this.gbs_name = gbs_name;
    }

    public Gb_Status(Integer gbs_id, String gbs_name) {
        this.gbs_id = gbs_id;
        this.gbs_name = gbs_name;
    }

    public Gb_Status(Integer gbs_id, String gbs_name, Boolean gbs_status, Integer index) {
        this.index = index;
        this.gbs_id = gbs_id;
        this.gbs_name = gbs_name;
        this.gbs_status = gbs_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }

    public Gb_Status(Integer gbs_id, Boolean gbs_status) {
        this.gbs_id = gbs_id;
        this.gbs_status = gbs_status;
    }
}
