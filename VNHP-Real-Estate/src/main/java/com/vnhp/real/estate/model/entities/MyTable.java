/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.ServerService;
import com.vnhp.real.estate.res.StringValue;
import java.util.LinkedList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;

/**
 *
 * @author FairyHunter
 * @param <T>
 */
public class MyTable<T> extends Pane {

    private TableView table;
    private Label label;

    public void setTable(ObservableList<T> listData) {
        this.table.setItems(listData);
    }

    public TableView getTable() {
        return table;
    }

    public MyTable(String title, LinkedList<TableColumn<T, String>> titles, ObservableList<T> listData, double height, double width, String backgroundStyle, LinkedList<String> listCombo) {
        this.setPrefHeight(height);
        this.setPrefWidth(width);
        this.getStyleClass().add(backgroundStyle);
        this.label = new Label(title);
        label.setLayoutX(170);
        label.setLayoutY(8);
        label.getStyleClass().add("Customer-Table-Tittle");

        var topCustomerCb = new ComboBox<String>();
        topCustomerCb.getItems().addAll(listCombo);
        topCustomerCb.setValue((String) listCombo.get(0));
        topCustomerCb.setLayoutX(400);
        topCustomerCb.setLayoutY(12);
        topCustomerCb.addEventHandler(ActionEvent.ACTION, eh -> {
            var newData = ConvertObject.Instance().toObservableList(ConvertObject.Instance().toList(BaseDao.Instance().getListData(topCustomerCb.getValue() + StringValue.Instance().CUSTOMER_TOP_PAYMENT, Customer[].class), ServerService.Instance().GET_TOP_10_CUSTOMER_REVENUE));
            this.label.setText(topCustomerCb.getValue() + " Customer");
            this.table.setItems(newData);
        });

        this.table = new TableView();
//        this.table.setPrefHeight(400);
//        this.table.setPrefWidth(530);
//        this.table.setLayoutY(50);
        this.table.setItems(listData);
        this.table.getColumns().addAll(titles);
        
        final ScrollPane customerScroll = new ScrollPane();
        customerScroll.setContent(this.table);
        customerScroll.setPrefHeight(400);
        customerScroll.setPrefWidth(530);
        customerScroll.setLayoutY(50);
        customerScroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        customerScroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.getChildren().addAll(label, customerScroll, topCustomerCb);
    }
}
