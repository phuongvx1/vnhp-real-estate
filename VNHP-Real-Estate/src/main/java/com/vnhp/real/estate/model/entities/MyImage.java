/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableName;
import java.io.FileInputStream;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author FairyHunter
 */
public final class MyImage extends ImageView {

    public MyImage() {
    }

    public MyImage(String url, Integer width, Integer height) {
        try {
            var input = new FileInputStream(url);
            var image = new Image(input);
            this.setImage(image);
            this.setFitWidth(width);
            this.setFitHeight(height);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MyImage(String url, String title, TableName tableName, Integer index) {
        try {
            var input = new FileInputStream(StringValue.Instance().ASSET_INVESTOR + url);
            var image = new Image(input);
            this.setImage(image);
            if (index % 2 == 0) {
                this.getStyleClass().add(tableName + "-Even-" + title);
            } else {
                this.getStyleClass().add(tableName + "-Odd-" + title);
            }
            this.setFitWidth(100);
            this.setFitHeight(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
