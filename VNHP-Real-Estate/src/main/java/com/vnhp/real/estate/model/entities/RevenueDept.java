/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;

/**
 *
 * @author FairyHunter
 */
public class RevenueDept {
    
    @ColumnId
    public Integer rd_id;
    @ColumnName
    public String rd_name;
    public Float rd_revenue;
    public Float rd_dept;
    @ColumnStatus
    public Boolean rd_status;

    public void setRd_dept(Float rd_dept) {
        this.rd_dept = rd_dept;
    }

    public void setRd_id(Integer rd_id) {
        this.rd_id = rd_id;
    }

    public void setRd_name(String rd_name) {
        this.rd_name = rd_name;
    }

    public void setRd_revenue(Float rd_revenue) {
        this.rd_revenue = rd_revenue;
    }

    public void setRd_status(Boolean rd_status) {
        this.rd_status = rd_status;
    }

    public Float getRd_dept() {
        return rd_dept;
    }

    public Integer getRd_id() {
        return rd_id;
    }

    public String getRd_name() {
        return rd_name;
    }

    public Float getRd_revenue() {
        return rd_revenue;
    }

    public Boolean getRd_status() {
        return rd_status;
    }

    public RevenueDept() {
    }

    public RevenueDept(Integer rd_id, String rd_name, Float rd_revenue, Float rd_dept, Boolean rd_status) {
        this.rd_id = rd_id;
        this.rd_name = rd_name;
        this.rd_revenue = rd_revenue;
        this.rd_dept = rd_dept;
        this.rd_status = rd_status;
    }
}
