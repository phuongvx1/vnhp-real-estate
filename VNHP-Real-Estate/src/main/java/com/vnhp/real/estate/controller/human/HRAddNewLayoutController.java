/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRAddNewLayoutController {

    @FXML
    private AnchorPane pHrAddNewStaff, pHrUpdateStaff, pHrViewStaff,pACLoginStatusNew,pACLoginStatusUpdate,pACLoginStatusView,pACStatusNew,pACStatusUpdate,pACStatusView;
    @FXML
    private Button btnHrNewStaff, btnHrViewStaff, btnHRASNew, btnHRASView, btnHRALSNew, btnHRALSView, btnHRANECView;
    @FXML
    private Pane pHRAddNew;

    public void init(Stage stage) {
        setup(stage);
    }

    private void setup(Stage stage) {
        btnHrNewStaff.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOURCE_ADDNEW));
            AnchorPane dashboard = dataNew.load();
            pHRAddNew.getChildren().clear();
            pHRAddNew.getChildren().add(dashboard);
            var control = (HRAddNewStaffController) dataNew.getController();
            control.init(stage);
        } catch (IOException e) {
            e.printStackTrace();
        }});
//        btnHrViewStaff.addEventHandler(ActionEvent.ACTION, eh -> {
//        try {
//            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_VIEW));
//            AnchorPane dashboard = dataNew.load();
//            pHRAddNew.getChildren().clear();
//            pHRAddNew.getChildren().add(dashboard);
//            var control = (HRViewStaffController) dataNew.getController();
//            control.init();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }});
       btnHRASNew.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_ACSTATUSNEW));
            AnchorPane dashboard = dataNew.load();
            pHRAddNew.getChildren().clear();
            pHRAddNew.getChildren().add(dashboard);
            var control = (HRAccountStatusNewController) dataNew.getController();
            control.init();
        } catch (IOException e) {
            e.printStackTrace();
        }});
       btnHRASView.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HR_ACCOUNT_STATUS_VIEW));
            AnchorPane dashboard = dataNew.load();
            pHRAddNew.getChildren().clear();
            pHRAddNew.getChildren().add(dashboard);
            var control = (HRAccountStatusViewController) dataNew.getController();
            control.init();
        } catch (IOException e) {
            e.printStackTrace();
        }});
       btnHRALSNew.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_ACLOGINSTATUSNEW));
            AnchorPane dashboard = dataNew.load();
            pHRAddNew.getChildren().clear();
            pHRAddNew.getChildren().add(dashboard);
            var control = (HRAccountLoginStatusNewController) dataNew.getController();
            control.init();
        } catch (IOException e) {
            e.printStackTrace();
        }});
       btnHRALSView.addEventHandler(ActionEvent.ACTION, eh -> {
        try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_ACLOGINSTATUSVIEW));
            AnchorPane dashboard = dataNew.load();
            pHRAddNew.getChildren().clear();
            pHRAddNew.getChildren().add(dashboard);
            var control = (HRAccountLoginStatusViewController) dataNew.getController();
            control.init();
        } catch (IOException e) {
            e.printStackTrace();
        }});
       btnHRANECView.addEventHandler(ActionEvent.ACTION, eh->{
       try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HR_CONTRACT));
            AnchorPane dashboard = dataNew.load();
            pHRAddNew.getChildren().clear();
            pHRAddNew.getChildren().add(dashboard);
            var control = (HRAccountLoginStatusViewController) dataNew.getController();
            control.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
       });
    }}
    

