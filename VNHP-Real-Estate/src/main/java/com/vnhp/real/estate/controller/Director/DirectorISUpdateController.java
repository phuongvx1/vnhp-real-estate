/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.InvestorStatus;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorISUpdateController{

     @FXML
    private TextField tfDirISUpdateName;
    
    @FXML
    private Label tfDirISUpdateResult;
    
    @FXML
    private Button btnDirISUpdate;
    
    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, InvestorStatus data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }
    
    private void setupDefault(InvestorStatus data) {
        tfDirISUpdateName.setText(data.getIs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, InvestorStatus data) {
        btnDirISUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirISUpdateName.getText();
            
            var newData = new InvestorStatus(data.getIs_id(),name);
            var retult = BaseDao.Instance().update(newData);
            tfDirISUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirISUpdateResult, 2);
            
            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (InvestorStatus) data;
            tmp.setIs_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getIs_id(), tmp);
        });
    }
}
