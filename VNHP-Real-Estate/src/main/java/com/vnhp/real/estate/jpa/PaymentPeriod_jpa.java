/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.jpa;

import com.vnhp.real.estate.model.db.DbConnection;
import com.vnhp.real.estate.model.entities.PaymentPeriod;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

/**
 *
 * @author Lip
 */
public class PaymentPeriod_jpa {

    public final LinkedList<PaymentPeriod> getAll() {
        Connection conn = DbConnection.Instance().Connect();
        var ls = new LinkedList<PaymentPeriod>();
        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery("select * from paymentperiod pp\n"
                    + "join salecontract s on (s.sc_id = pp.sc_id)\n"
                    + "join depositcontract d on (s.sc_id = d.sc_id)\n"
                    + "join product p on (p.product_id = s.product_id)\n"
                    + "join customer c on (c.customer_id = s.payer_id)\n"
                    + "join account a on (a.account_id = s.saler_id)\n"
                    + "where s.cont_id = 1");
            while (rs.next()) {
                var item = newData(rs);
                ls.add(item);
            }
        } catch (SQLException e) {
        }
        return ls;
    }

    public final int CountNoti() throws SQLException {
        Connection conn = DbConnection.Instance().Connect();
        int count = 0;
        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery("Select count(pp_id) as count from paymentperiod");
            while (rs.next()) {
                count = rs.getInt("count");
            }
            sm.close();
            conn.close();
            return count;
        } catch (SQLException e) {
        }
        return count;
    }

    public final void UpdatePeriod(PaymentPeriod subject) {
        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            sm.executeQuery("Update PaymentPeriod Set pps_id = 2 where pp_id = " + subject.pp_id);
        } catch (SQLException e) {
        }
    }

    public final void CheckContract(PaymentPeriod subject) {
        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery("select count(pp_id) as count from paymentperiod where sc_id = " + subject.sc_id + " and pps_id = 1");
            while (rs.next()) {
                if (rs.getInt("count") == 0) {
                    ChangeStatusContract(subject.sc_id);
                }
            }
        } catch (SQLException e) {
        }
    }

    public final void ChangeStatusContract(int sc_id) {
        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            sm.executeQuery("Update salecontract set cons_id = 3 where sc_id = " + sc_id);
        } catch (SQLException e) {
        }
    }

    public final LinkedList<PaymentPeriod> getAllFromPaymentPeriod() {
        Connection conn = DbConnection.Instance().Connect();
        var ls = new LinkedList<PaymentPeriod>();
        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery("Select * from paymentperiod where pps_id = 1");
            while (rs.next()) {
                var item = newData1(rs);
                ls.add(item);
            }
        } catch (SQLException e) {}
        return ls;
    }

    private PaymentPeriod newData1(ResultSet rs) {
        var tmp = new PaymentPeriod();
        try {
            tmp = new PaymentPeriod(rs.getInt("sc_id"), rs.getInt("pp_id"), rs.getString("pp_time"));
        } catch (SQLException e) {
        }

        return tmp;
    }

    private PaymentPeriod newData(ResultSet rs) {
        var tmp = new PaymentPeriod();
        try {
            tmp = new PaymentPeriod(
                    rs.getInt("pp_id"),
                    rs.getInt("sc_id"),
                    rs.getString("product_name"),
                    rs.getString("sc_total_payment"),
                    rs.getFloat("pp_payment"),
                    rs.getString("pp_time"),
                    rs.getInt("pps_id"),
                    rs.getInt("dc_status"));
        } catch (SQLException e) {
        }

        return tmp;
    }
}