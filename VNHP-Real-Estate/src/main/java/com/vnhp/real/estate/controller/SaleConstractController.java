/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller;

import com.vnhp.real.estate.jpa.SaleContract_jpa;
import com.vnhp.real.estate.model.db.DbConnection;
import com.vnhp.real.estate.model.entities.SaleReceipt;
import com.vnhp.real.estate.res.StringValue;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 * FXML Controller class
 *
 * @author Lip
 */
public class SaleConstractController {

    ObservableList<SaleReceipt> list = FXCollections.observableArrayList();
    LinkedList<SaleReceipt> arr = new LinkedList<>();
    Button[] button;
    TableView<SaleReceipt> table = new TableView<>();

    public void init(ScrollPane saleConstract, TextField searchBox) throws FileNotFoundException {
        SaleContract_jpa sale = new SaleContract_jpa();
        arr = sale.getAll();
        button = new Button[arr.size()];
        SaleConstract(saleConstract, searchBox);
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    LinkedList<SaleReceipt> newList = new SaleContract_jpa().getAll();
                    if (newList.size() > arr.size()) {
                        button = new Button[newList.size()];
                        list.clear();
                        arr = newList;
                        for (int i = 0; i < newList.size(); i++) {
                            button[i] = new Button();
                            try {
                                SaleReceipt item = new SaleReceipt(
                                        newList.get(i).sc_id,
                                        newList.get(i).sc_time_open,
                                        newList.get(i).product_name,
                                        newList.get(i).sc_total_payment,
                                        newList.get(i).payer_name,
                                        newList.get(i).saler_name,
                                        newList.get(i).cons_name,
                                        newList.get(i).cont_id,
                                        button[i]);
                                list.add(item);
                            } catch (FileNotFoundException ex) {
                            }
                        }
                        table.setItems(list);
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                    }
                }
            }
        };
        thread.setDaemon(true);
        thread.start();
    }

    public void refreshTable() throws FileNotFoundException {
        list.clear();
        SaleContract_jpa sale1 = new SaleContract_jpa();
        LinkedList<SaleReceipt> arr1 = sale1.getAll();
        for (int i = 0; i < arr.size(); i++) {
            button[i] = new Button();
            button[i].setUserData(arr.get(i).sc_id);
            button[i].setOnAction(this::handleButtonAction);
            SaleReceipt item = new SaleReceipt(
                    arr1.get(i).sc_id,
                    arr1.get(i).sc_time_open,
                    arr1.get(i).product_name,
                    arr1.get(i).sc_total_payment,
                    arr1.get(i).payer_name,
                    arr1.get(i).saler_name,
                    arr1.get(i).cons_name,
                    arr1.get(i).cont_id,
                    button[i]);
            list.add(item);
        }
        table.setItems(list);
    }

    private void SaleConstract(ScrollPane saleConstract, TextField searchBox) throws FileNotFoundException {
        table.setPrefWidth(1169);
        table.setPrefHeight(800);
        table.setStyle("-fx-selection-bar: #b78c6e; -fx-selection-bar-non-focused: #d5bbaa;");

        // Create column
        TableColumn<SaleReceipt, String> noCol = new TableColumn<>("Constract");
        TableColumn<SaleReceipt, String> customerCol = new TableColumn<>("Customer");
        TableColumn<SaleReceipt, String> productCol = new TableColumn<>("Product");
        TableColumn<SaleReceipt, Float> totalCol = new TableColumn<>("Total");
        TableColumn<SaleReceipt, String> salerCol = new TableColumn<>("Saler");
        TableColumn<SaleReceipt, String> methodCol = new TableColumn<>("Method");
        TableColumn<SaleReceipt, String> createdatCol = new TableColumn<>("Created at");
        TableColumn<SaleReceipt, Boolean> statusCol = new TableColumn<>("Status");
        TableColumn<SaleReceipt, String> actionCol = new TableColumn<>("Action");

        noCol.setPrefWidth(90);
        customerCol.setPrefWidth(90);
        productCol.setPrefWidth(230);
        totalCol.setPrefWidth(150);
        salerCol.setPrefWidth(150);
        createdatCol.setPrefWidth(150);
        statusCol.setPrefWidth(100);
        actionCol.setPrefWidth(100);
        methodCol.setPrefWidth(100);

        noCol.setCellValueFactory(new PropertyValueFactory<>("sc_id"));
        customerCol.setCellValueFactory(new PropertyValueFactory<>("payer_name"));
        productCol.setCellValueFactory(new PropertyValueFactory<>("product_name"));
        totalCol.setCellValueFactory(new PropertyValueFactory<>("sc_total_payment"));
        salerCol.setCellValueFactory(new PropertyValueFactory<>("saler_name"));
        createdatCol.setCellValueFactory(new PropertyValueFactory<>("sc_time_open"));
        methodCol.setCellValueFactory(new PropertyValueFactory<>("method"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("sttLabel"));
        actionCol.setCellValueFactory(new PropertyValueFactory<>("button"));

        table.getColumns().addAll(noCol, createdatCol, productCol, totalCol, customerCol, salerCol, methodCol, statusCol, actionCol);
        ObservableList<SaleReceipt> itemList = getConstractList();
        table.setItems(itemList);
        saleConstract.setContent(table);

        FilteredList<SaleReceipt> filteredData = new FilteredList<>(itemList, b -> true);
        searchBox.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate((SaleReceipt constract) -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                if (String.valueOf(constract.getSc_id()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (constract.getPayer_name().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (constract.getProduct_name().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (String.valueOf(constract.getSc_total_payment()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (constract.getSaler_name().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (constract.getSc_time_open().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else {
                    return false;
                }
            });
        });
        SortedList<SaleReceipt> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(table.comparatorProperty());
        table.setItems(sortedData);
    }

    private ObservableList<SaleReceipt> getConstractList() throws FileNotFoundException {
        for (int i = 0; i < arr.size(); i++) {
            button[i] = new Button();
            button[i].setUserData(arr.get(i).sc_id);
            button[i].setOnAction(this::handleButtonAction);
            SaleReceipt item = new SaleReceipt(arr.get(i).sc_id,
                    arr.get(i).sc_time_open,
                    arr.get(i).product_name,
                    arr.get(i).sc_total_payment,
                    arr.get(i).payer_name,
                    arr.get(i).saler_name,
                    arr.get(i).cons_name,
                    arr.get(i).cont_id,
                    button[i]);
            list.add(item);
        }
        return list;
    }

    private void handleButtonAction(ActionEvent event) {
        Node sourceComponent = (Node) event.getSource();
        int id = (int) sourceComponent.getUserData();
        Stage newWindow = new Stage();
        StackPane newStk = confirmStackPane(id, newWindow);

        newWindow.initStyle(StageStyle.TRANSPARENT);
        newWindow.initModality(Modality.APPLICATION_MODAL);
        newWindow.setScene(new Scene(newStk));
        newWindow.show();
    }

    private StackPane confirmStackPane(int id, Stage newWindow) {
        StackPane stack = new StackPane();
        stack.setPrefHeight(170);
        stack.setPrefWidth(350);
        stack.setStyle("-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.1), 100, 0.5, 0.0, 0.0)");

        VBox vbox = new VBox();

        Pane pane1 = new Pane();
        pane1.setPrefHeight(26);
        pane1.setPrefWidth(350);
        pane1.setStyle("-fx-background-color: #cccccc");

        Label message = new Label("Message");
        message.setPrefWidth(350);
        message.setPrefHeight(26);
        message.setStyle("-fx-font-style: italic; -fx-font-size: 14px");
        message.setAlignment(Pos.CENTER);

        Label noti = new Label("Are you sure to create receipt ?");
        noti.setAlignment(Pos.CENTER);
        noti.setPrefWidth(350);
        noti.setPrefHeight(90);
        noti.setStyle("-fx-font-size: 16");

        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER_RIGHT);
        hbox.setPrefHeight(42);
        hbox.setPrefWidth(350);
        hbox.setSpacing(20);

        Button trueBtn = new Button("Create");
        trueBtn.setPrefWidth(65);
        trueBtn.setPrefHeight(20);
        trueBtn.setStyle("-fx-font-weight: bold; fx-font-size: 14px;-fx-background-color: #007bff; -fx-text-fill: #ffffff");
        trueBtn.setOnAction(eh -> {
            exportReceipt(id);
            newWindow.close();
            SaleContract_jpa newJpa = new SaleContract_jpa();
            newJpa.UpdateStatus(id);
            newJpa.UpdatePeriod(id);
            try {
                refreshTable();
            } catch (FileNotFoundException ex) {
            }
        });

        Button falseBtn = new Button("Close");
        falseBtn.setPrefWidth(65);
        falseBtn.setPrefHeight(20);
        falseBtn.setStyle("-fx-font-weight: bold; fx-font-size: 14px;-fx-background-color: #6c757d;; -fx-text-fill: #ffffff");
        HBox.setMargin(falseBtn, new Insets(0, 20, 0, 0));
        falseBtn.setOnAction(eh -> {
            newWindow.close();
        });

        hbox.getChildren().addAll(trueBtn, falseBtn);
        pane1.getChildren().add(message);
        vbox.getChildren().addAll(pane1, noti, hbox);
        stack.getChildren().add(vbox);

        return stack;
    }

    public void exportReceipt(int sc_id) {
        try {
            JasperReport report = JasperCompileManager.compileReport(StringValue.Instance().SALERECEIPT);
            HashMap map = new HashMap();
            map.put("sc_id", sc_id);
            JasperPrint j = JasperFillManager.fillReport(report, map, DbConnection.Instance().Connect());
            JasperViewer.viewReport(j, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
