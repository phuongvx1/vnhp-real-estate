/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.CustomerStatus;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorCsUpdateController{

      @FXML
    private TextField tfDirCSUpdateName;
    
    @FXML
    private Label tfDirCSUpdateResult;
    
    @FXML
    private Button btnDirCSUpdate;
    
    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, CustomerStatus data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }
    
    private void setupDefault(CustomerStatus data) {
        tfDirCSUpdateName.setText(data.getCs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, CustomerStatus data) {
        btnDirCSUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirCSUpdateName.getText();
            
            var newData = new CustomerStatus(data.getCs_id(),name);
            var retult = BaseDao.Instance().update(newData);
            tfDirCSUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirCSUpdateResult, 2);
            
            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (CustomerStatus) data;
            tmp.setCs_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getCs_id(), tmp);
        });
    }
}
