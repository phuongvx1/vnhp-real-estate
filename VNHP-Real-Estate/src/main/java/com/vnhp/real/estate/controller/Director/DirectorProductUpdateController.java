/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import static com.vnhp.real.estate.controller.Director.DirectorProductNewController.blocks;
import static com.vnhp.real.estate.controller.Director.DirectorProductNewController.ps;
import static com.vnhp.real.estate.controller.Director.DirectorProductNewController.region;
import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.Product;
import com.vnhp.real.estate.model.entities.Region;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProductUpdateController {

    @FXML
    private TextField tfDirProductUpdateName, tfDirProductUpdatePrice;

    @FXML
    private ComboBox<String> tfDirProductUpdateBlock, tfDirProductUpdateRegion, tfDirProductUpdateStatus;

    @FXML
    private HTMLEditor tfDirProductUpdateDescrbe;

    @FXML
    public Label tfDirProductUpdateResult;

    @FXML
    private Button btnDirProductUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, Product data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, stage, data);
    }

    public void setupDefault(Product data) {
        tfDirProductUpdateName.setText(data.getProduct_name());
        tfDirProductUpdatePrice.setText(data.getProduct_price().toString());
        
        tfDirProductUpdateBlock.getItems().addAll(blocks);
        tfDirProductUpdateBlock.setValue(data.getBlock_name());
        tfDirProductUpdateRegion.getItems().addAll(region);
        tfDirProductUpdateRegion.setValue(data.getRegion_name());
        tfDirProductUpdateStatus.getItems().addAll(ps);
        tfDirProductUpdateStatus.setValue(data.getPs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, Product data) {
        btnDirProductUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirProductUpdateName.getText();
            var price = Float.parseFloat(tfDirProductUpdatePrice.getText());
            var descrbe = tfDirProductUpdateDescrbe.getHtmlText();

            var block_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Blocks(tfDirProductUpdateBlock.getValue()), Blocks[].class, 1), ColumnId.class);
            var region_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Region(tfDirProductUpdateRegion.getValue()), Region[].class, 2), ColumnId.class);
//            var ps_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ProductStatus(tfDirProductUpdateStatus.getValue()), ProductStatus[].class, 1), ColumnId.class);
            var newData = new Product(data.getProduct_id(), name, descrbe, price, region_id, block_id);
            var retult = BaseDao.Instance().update(newData);
            tfDirProductUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirProductUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Product) data;
            tmp.setProduct_name(name);
            tmp.setProduct_descrbe(descrbe);
            tmp.setProduct_price(price);
            tmp.setRegion_name(tfDirProductUpdateRegion.getValue());
            tmp.setPs_name(tfDirProductUpdateStatus.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getRegion_id(), tmp);
        });
    }
}
