/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.AccountLoginStatus;
import com.vnhp.real.estate.model.entities.AccountStatus;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.ObjectType;
import com.vnhp.real.estate.res.ServerService;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableName;
import static com.vnhp.real.estate.res.TableName.AccountLoginStatus;
import com.vnhp.real.estate.res.TableSetting;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.ColumnConstraints;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRAccountLoginStatusViewController  {
@FXML
    private ScrollPane pALSView;

    public void init() {
        setup();
    }

    private void setup() {
        var gridPane = new MyGridPane();

        var titleList = new LinkedList<String>();
        titleList.add("ID");
        titleList.add("Name");
        titleList.add("Status");
        titleList.add("Change Status");
        titleList.add("Update");
        gridPane.setGridLinesVisible(true);

        gridPane.addTitles(titleList, TableName.AccountLoginStatus);
        var rowSettings = new LinkedList<String>();
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyButton.toString());
        rowSettings.add(ObjectType.MyButton.toString());

        var col1 = new ColumnConstraints();
        col1.setPrefWidth(100);
        var col2 = new ColumnConstraints();
        col2.setPrefWidth(100);
        var col3 = new ColumnConstraints();
        col3.setPrefWidth(100);
        var col4 = new ColumnConstraints();
        col4.setPrefWidth(100);
        var col5 = new ColumnConstraints();
        col5.setPrefWidth(100);

        var setting = new LinkedList<ColumnConstraints>();
        setting.add(col1);
        setting.add(col2);
        setting.add(col3);
        setting.add(col4);
        setting.add(col5);

        var tableSetting = new TableSetting(rowSettings, setting);
        var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().ACCOUNT_LOGIN_STATUS, AccountLoginStatus[].class), ServerService.Instance().GET_ACLOGIN_STATUS);

        gridPane.setLayoutX(10);
        gridPane.setRow(gridPane, listData, tableSetting, TableName.AccountLoginStatus);
        gridPane.getColumnConstraints().addAll(tableSetting.getSetting());
        pALSView.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pALSView.setContent(gridPane);
    }
}