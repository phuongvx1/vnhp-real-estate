/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vnhp.real.estate.model.db.DbConnection;
import com.vnhp.real.estate.res.ColumnDate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnMoney;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;
import org.json.JSONArray;
import org.json.JSONObject;
import java.sql.ResultSet;

/**
 *
 * @author FairyHunter
 */
public final class BaseDao<T> {

    private static BaseDao instance;

    private BaseDao() {

    }

    public static BaseDao Instance() {
        if (instance == null) {
            instance = new BaseDao();
        }

        return instance;
    }

    private final String insertQuery(T data) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        var query = "Insert into " + data.getClass().getSimpleName() + "(";
        var cols = "";
        var values = ") values (N'";

        var dataFields = data.getClass().getFields();

        for (var dataField : dataFields) {
            if (dataField.get(data) == null) {
                continue;
            }
            cols += dataField.getName() + ",";
            values += dataField.get(data) + "',N'";
        }

        query += cols.substring(0, cols.length() - 1) + values.substring(0, values.length() - 3) + ")";
        return query;
    }

    public String dateQuery(T data, String startDay, String endday) {
        var dataFields = data.getClass().getFields();
        var fieldName = "";
        var fieldPayment = "";
        for (var dataField : dataFields) {
            if (dataField.isAnnotationPresent(ColumnDate.class)) {
                fieldName = dataField.getName();
            }
            if (dataField.isAnnotationPresent(ColumnMoney.class)) {
                fieldPayment = dataField.getName();
            }
        }
        String query = " sum(" + fieldPayment + ") as '" + fieldPayment + "' from " + data.getClass().getSimpleName();

        query += " where ";
        if (data.getClass().getSimpleName().equals("ProductRevenue")) {
            query += "pr_id > 1 and ";
        }

        if (endday.isBlank() || endday.isEmpty()) {
            for (var dataField : dataFields) {
                if (dataField.isAnnotationPresent(ColumnDate.class)) {
                    query += "MONTH(dbo.convertBigIntToDateTime('" + startDay + "')) = MONTH(dbo.convertBigIntToDateTime(" + fieldName + ")) and\n"
                            + "Year(dbo.convertBigIntToDateTime('" + startDay + "')) = Year(dbo.convertBigIntToDateTime(" + fieldName + "))";
                }
            }
        } else {
            for (var dataField : dataFields) {
                if (dataField.isAnnotationPresent(ColumnDate.class)) {
                    query += fieldName + " >= " + startDay + " and " + fieldName + " <= " + endday + "";
                }
            }
        }
        return query;
    }

    private final String updateQuery(T data) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        var query = "Update " + data.getClass().getSimpleName() + "\nset ";
        var values = "";

        var dataFields = data.getClass().getFields();
        var idValue = "";
        var idColumn = "";
        for (var dataField : dataFields) {
            if (dataField.get(data) == null) {
                continue;
            }
            if (dataField.isAnnotationPresent(ColumnId.class)) {
                idValue = dataField.get(data).toString();
                idColumn = dataField.getName();
                continue;
            }

            values += dataField.getName() + " = '" + dataField.get(data) + "',";
        }

        query += values.substring(0, values.length() - 1) + "\nwhere " + idColumn + " = " + idValue;
        return query;
    }

    private final String selectByQuery(T data) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        var query = "select * from " + data.getClass().getSimpleName() + "\nwhere ";
        var dataFields = data.getClass().getFields();
        var values = "";
        for (var dataField : dataFields) {
            if (dataField.get(data) == null) {
                continue;
            }
            values += dataField.getName() + " = '" + dataField.get(data) + "' and ";
        }
        query += values.substring(0, values.length() - 4);
        return query;
    }

    public final String searchQuery(T data, String from, String text) throws NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        var query = "";
        if (from.isBlank() || from.isEmpty()) {
            query += " * from " + data.getClass().getSimpleName();
        } else {
            query += from;
        }

        var values = "\nwhere ";

        var dataFields = data.getClass().getFields();
        var idColumn = "";
        for (var dataField : dataFields) {

            if (dataField.isAnnotationPresent(ColumnId.class)) {
                idColumn = dataField.getName();
                continue;
            }

            if (dataField.isAnnotationPresent(DbColumn.class)) {
                values += dataField.getName() + " like '%" + text + "%' or ";
            }
        }

        if (text.isBlank() || text.isEmpty()) {
            return query + " order by " + idColumn + " desc";
        }

        query += values.substring(0, values.length() - 4) + " order by " + idColumn + " desc";
        return query;
    }

    private final String selectQuery(T data, Integer status) throws IllegalAccessException {
        var query = "select * from " + data.getClass().getSimpleName();

        var dataFields = data.getClass().getFields();
        var idColumn = "";
        var idValue = "";
        var statusColumn = "";
        var values = "";

        for (var dataField : dataFields) {
            if (dataField.get(data) == null) {
                if (dataField.isAnnotationPresent(ColumnId.class)) {
                    idColumn = dataField.getName();
                }
                if (dataField.isAnnotationPresent(ColumnStatus.class)) {
                    statusColumn = dataField.getName();
                }
                continue;
            }

            if (dataField.isAnnotationPresent(ColumnId.class)) {
                idValue = dataField.get(data).toString();
                idColumn = dataField.getName();
                continue;
            }

            if (dataField.isAnnotationPresent(ColumnStatus.class)) {
                statusColumn = dataField.getName();
                continue;
            }

            values += dataField.getName() + " = '" + dataField.get(data) + "' and ";
        }
        if (idValue.equals("")) {
            query += "\nwhere " + values + statusColumn + " = " + status + " order by " + idColumn + " desc";
        } else {
            query += "\nwhere " + values + statusColumn + " = " + status + " and " + idColumn + " = " + idValue + " order by " + idColumn + " desc";
        }
        return query;
    }

    private final String selectAllQuery(String data, Integer status) {
        return "select " + data;
    }

    public String insert(T data) {
        String statusExecute = StringValue.Instance().FAIL;
        Connection conn = DbConnection.Instance().Connect();

        try {
            if (isExists(data)) {
                return data.getClass().getSimpleName() + " Already Exists.";
            }
            PreparedStatement ps = conn.prepareStatement(insertQuery(data));
            ps.execute();
            statusExecute = StringValue.Instance().SUCCESSFULL;
            ps.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return statusExecute;
    }

    public String update(T data) {
        String statusExecute = StringValue.Instance().FAIL;
        Connection conn = DbConnection.Instance().Connect();

        try {
            PreparedStatement ps = conn.prepareStatement(updateQuery(data));
            ps.execute();
            statusExecute = StringValue.Instance().SUCCESSFULL;
            ps.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return statusExecute;
    }

    private final JSONArray getAll(String data, Integer status) {

        JSONArray result = new JSONArray();

        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery(selectAllQuery(data, status));
            var md = rs.getMetaData();
            int numCols = md.getColumnCount();

            while (rs.next()) {
                JSONObject row = new JSONObject();
                for (int i = 0; i < numCols; i++) {
                    row.put(rs.getMetaData().getColumnLabel(i + 1).toLowerCase(), rs.getObject(i + 1));
                }
                result.put(row);
            }
            sm.close();
            conn.close();
        } catch (Exception e) {
            String err = e.getMessage();
            System.out.println(err);
        }
        return result;
    }

    private final JSONArray getData(T data, Integer status) {
        JSONArray result = new JSONArray();

        Connection conn = DbConnection.Instance().Connect();
        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery(selectQuery(data, status));
            var md = rs.getMetaData();
            int numCols = md.getColumnCount();

            while (rs.next()) {
                JSONObject row = new JSONObject();
                for (int i = 0; i < numCols; i++) {
                    row.put(rs.getMetaData().getColumnLabel(i + 1).toLowerCase(), rs.getObject(i + 1));
                }
                result.put(row);
            }
            sm.close();
            conn.close();
        } catch (Exception e) {
            String err = e.getMessage();
            System.out.println(err);
        }
        return result;
    }

    public final boolean changeStatus(T data) {
        var resultExecute = true;
        Connection conn = DbConnection.Instance().Connect();

        try {
            PreparedStatement ps = conn.prepareStatement(updateQuery(data));
            resultExecute = ps.execute();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultExecute;
    }

    private boolean isExists(T data) {
        var check = true;
        Connection conn = DbConnection.Instance().Connect();

        try {
            Statement sm = conn.createStatement();
            ResultSet rs = sm.executeQuery(selectByQuery(data));
            check = rs.next();
            sm.close();
            conn.close();
            rs.close();
        } catch (Exception e) {
            String err = e.getMessage();
            System.out.println(err);
        }

        return check;
    }

    public T[] getListData(String data, Class<T> classT) {
        try {
            var mapper = new ObjectMapper();

            var jsonString = getAll(data, 1).toString();
            var ls = mapper.readValue(jsonString, classT);

            return (T[]) ls;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public T[] getData(T data, Class<T> classT, Integer status) {
        try {
            var mapper = new ObjectMapper();

            var jsonString = getData(data, status).toString();
            var ls = mapper.readValue(jsonString, classT);

            return (T[]) ls;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
