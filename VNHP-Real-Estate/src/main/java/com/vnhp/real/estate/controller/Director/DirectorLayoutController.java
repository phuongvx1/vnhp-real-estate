/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.res.AppSetting;
import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.image.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.*;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorLayoutController {

    @FXML
    private ImageView btnClose, btnMinimize;
    @FXML
    private Button btnDirDashboard, btnDirProducts, btnDirProjects, btnDirInvestors, btnDirContract;
    @FXML
    private Pane pDirMainView;

    public void init(Stage stage) {
        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        setupDefault();
        setup(stage);
    }

    private void setupDefault() {
        var defaultLoad = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_DASHBOARD));
        try {
            AnchorPane dashboard = defaultLoad.load();
            var control = (DirectorDashboardController) defaultLoad.getController();
            control.init();
            pDirMainView.getChildren().clear();
            pDirMainView.getChildren().add(dashboard);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setup(Stage stage) {
        btnDirDashboard.setOnMousePressed(eh -> {
            var loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_DASHBOARD));
            try {
                AnchorPane dashboard = loader.load();
                var control = (DirectorDashboardController) loader.getController();
                control.init();
                pDirMainView.getChildren().clear();
                pDirMainView.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirProducts.setOnMousePressed(eh -> {
            var loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PRODUCT_LAYOUT));
            try {
                AnchorPane dashboard = loader.load();
                var control = (DirectorProductsLayoutController) loader.getController();
                control.init(stage);
                pDirMainView.getChildren().clear();
                pDirMainView.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirProjects.setOnMousePressed(eh -> {
            var loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PROJECT_LAYOUT));
            try {
                AnchorPane dashboard = loader.load();
                var control = (DirectorProjectLayoutController) loader.getController();
                control.init(stage);
                pDirMainView.getChildren().clear();
                pDirMainView.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirInvestors.setOnMousePressed(eh -> {
            var loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_INVESTOR_LAYOUT));
            try {
                AnchorPane dashboard = loader.load();
                var control = (DirectorInvestorLayoutController) loader.getController();
                control.init();
                pDirMainView.getChildren().clear();
                pDirMainView.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirContract.setOnMousePressed(eh -> {
            var loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CONTRACT_LAYOUT));
            try {
                AnchorPane dashboard = loader.load();
                var control = (DirectorContractLayoutController) loader.getController();
                control.init();
                pDirMainView.getChildren().clear();
                pDirMainView.getChildren().add(dashboard);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
