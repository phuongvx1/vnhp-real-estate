/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class Utilities {

    public Integer index;
    @ColumnId
    public Integer utilities_id;
    @ColumnName
    @DbColumn
    public String utilities_name;
    @ColumnStatus
    @DbColumn
    public Boolean utilities_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setUtilities_id(Integer utilities_id) {
        this.utilities_id = utilities_id;
    }

    public void setUtilities_name(String utilities_name) {
        this.utilities_name = utilities_name;
    }

    public void setUtilities_status(Boolean utilities_status) {
        this.utilities_status = utilities_status;
    }

    public Integer getIndex() {
        return index;
    }

    public Integer getUtilities_id() {
        return utilities_id;
    }

    public String getUtilities_name() {
        return utilities_name;
    }

    public Boolean getUtilities_status() {
        return utilities_status;
    }

    public Utilities() {
    }

    public Utilities(String utilities_name) {
        this.utilities_name = utilities_name;
    }

    public Utilities(Integer utilities_id, String utilities_name) {
        this.utilities_id = utilities_id;
        this.utilities_name = utilities_name;
    }

    public Utilities(Integer utilities_id, Boolean utilities_status) {
        this.utilities_id = utilities_id;
        this.utilities_status = utilities_status;
    }

    public Utilities(Integer utilities_id, String utilities_name, Boolean utilities_status, Integer index) {
        this.index = index;
        this.utilities_id = utilities_id;
        this.utilities_name = utilities_name;
        this.utilities_status = utilities_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
