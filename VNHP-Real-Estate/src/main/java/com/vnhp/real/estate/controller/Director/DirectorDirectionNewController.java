/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Direction;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorDirectionNewController{
@FXML
    private TextField tfDirDirectionNewName;

    @FXML
    private Label tfDirDirectionNewResult;

    @FXML
    private Button btnDirDirectionNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirDirectionNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirDirectionNewName.getText();

            var data = new Direction(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirDirectionNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirDirectionNewResult, 2);
        });
    }
}
