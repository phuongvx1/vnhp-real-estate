/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.GroupBlocks;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import com.vnhp.real.estate.res.UtilitiesManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorBlockUpdateController {

    @FXML
    private TextField tfDirBlockUpdateName;

    @FXML
    private ComboBox<String> cbDirBlockUpdateGb;
    @FXML
    private Pane pDirBlockUpdateUtilities;
    @FXML
    private Label tfDirBlockUpdateResult;

    @FXML
    private Button btnDirBlockUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Blocks data) {
        setupDefault(subStage, data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(Stage stage, Blocks data) {
        tfDirBlockUpdateName.setText(data.getBlock_name());

        pDirBlockUpdateUtilities.getChildren().clear();
        pDirBlockUpdateUtilities.getChildren().add(UtilitiesManager.Instance().Utilities(stage));

        var gb = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().GROUP_BLOCKS_ACTIVE, GroupBlocks[].class));
        cbDirBlockUpdateGb.getItems().addAll(gb);
        cbDirBlockUpdateGb.setValue(data.getGb_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Blocks data) {
        btnDirBlockUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirBlockUpdateName.getText();
            var utilities = UtilitiesManager.Utilities.toString().substring(1, UtilitiesManager.Utilities.toString().length() - 1);
            var gb_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new GroupBlocks(cbDirBlockUpdateGb.getValue()), GroupBlocks[].class, 1), ColumnId.class);

            var newData = new Blocks(data.getBlock_id(), name, utilities, gb_id);
            var retult = BaseDao.Instance().update(newData);
            tfDirBlockUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirBlockUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Blocks) data;
            tmp.setBlock_name(name);
            tmp.setBlock_utilities(utilities);
            tmp.setGb_name(cbDirBlockUpdateGb.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getBs_id(), tmp);
        });
    }
}
