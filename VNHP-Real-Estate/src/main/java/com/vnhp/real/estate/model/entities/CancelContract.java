/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.*;

/**
 *
 * @author FairyHunter
 */
public class CancelContract {
    
    public Integer index;
    @ColumnId
    public Integer cc_id;
    public String cc_time_open;
    public Integer cc_number;
    public Float cc_fee;
    public Integer sc_id;
    @ColumnStatus
    public Integer cc_status;

    public CancelContract() {
    }

    public CancelContract(String cc_time_open, Integer cc_number, Float cc_fee, Integer sc_id) {
        this.cc_time_open = cc_time_open;
        this.cc_number = cc_number;
        this.cc_fee = cc_fee;
        this.sc_id = sc_id;
    }
    
    public CancelContract(Integer cc_id, String cc_time_open, Integer cc_number, Float cc_fee, Integer sc_id) {
        this.cc_id = cc_id;
        this.cc_time_open = cc_time_open;
        this.cc_number = cc_number;
        this.cc_fee = cc_fee;
        this.sc_id = sc_id;
    }
    
    public void setCc_fee(Float cc_fee) {
        this.cc_fee = cc_fee;
    }

    public void setCc_id(Integer cc_id) {
        this.cc_id = cc_id;
    }

    public void setCc_number(Integer cc_number) {
        this.cc_number = cc_number;
    }

    public void setCc_time_open(String cc_time_open) {
        this.cc_time_open = cc_time_open;
    }

    public void setSc_id(Integer sc_id) {
        this.sc_id = sc_id;
    }

    public void setCc_status(Integer cc_status) {
        this.cc_status = cc_status;
    }

    public Float getCc_fee() {
        return cc_fee;
    }

    public Integer getCc_id() {
        return cc_id;
    }

    public Integer getCc_number() {
        return cc_number;
    }

    public String getCc_time_open() {
        return cc_time_open;
    }

    public Integer getSc_id() {
        return sc_id;
    }

    public Integer getCc_status() {
        return cc_status;
    }
   
}
