/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.Direction;
import com.vnhp.real.estate.model.entities.Product;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.model.entities.Region;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TimeManager;
import java.util.LinkedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProductNewController {

    public static LinkedList<String> blocks = new LinkedList<>();
    public static LinkedList<String> region = new LinkedList<>();
    public static LinkedList<String> ps = new LinkedList<>();
    public static LinkedList<String> direction = new LinkedList<>();

    @FXML
    private TextField tfDirProductNewName, tfDirProductNewPrice;

    @FXML
    private ComboBox<String> tfDirProductNewBlock, tfDirProductNewRegion, tfDirProductNewStatus,tfDirProductNewDirection;

    @FXML
    private TextArea tfDirProductNewDescrbe;

    @FXML
    public Label tfDirProductNewResult;

    @FXML
    private Button btnDirProductNew;

    public void init(Stage stage) {
        setupDefault();
        setup();
    }

    public void setupDefault() {
        tfDirProductNewBlock.getItems().addAll(blocks);
        tfDirProductNewBlock.setValue((String) blocks.get(0));
        tfDirProductNewRegion.getItems().addAll(region);
        tfDirProductNewRegion.setValue((String) region.get(0));
        tfDirProductNewStatus.getItems().addAll(ps);
        tfDirProductNewStatus.setValue((String) ps.get(0));
        tfDirProductNewDirection.getItems().addAll(direction);
        tfDirProductNewDirection.setValue((String) direction.get(0));
    }

    private void setup() {
        btnDirProductNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirProductNewName.getText();
            var price = Float.parseFloat(tfDirProductNewPrice.getText());
            var descrbe = tfDirProductNewDescrbe.getText();

            var block_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Blocks(tfDirProductNewBlock.getValue()), Blocks[].class, 1), ColumnId.class);
            var region_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Region(tfDirProductNewRegion.getValue()), Region[].class, 1), ColumnId.class);
            var ps_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ProductStatus(tfDirProductNewStatus.getValue()), ProductStatus[].class, 1), ColumnId.class);
            var direction_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Direction(tfDirProductNewDirection.getValue()), Direction[].class, 1), ColumnId.class);

            var data = new Product(name, descrbe, price, block_id, region_id, ps_id,direction_id);
            var retult = BaseDao.Instance().insert(data);
            tfDirProductNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirProductNewResult, 2);
        });
    }
}