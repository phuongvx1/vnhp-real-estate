/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;

/**
 *
 * @author bichv
 */
public class ProjectInvestorComponent extends Pane {

    public ProjectInvestorComponent() {
        this.setPrefHeight(500.0);
        this.setPrefWidth(450.0);
        this.getStyleClass().add("pi_Component");

        Label p_status = new Label();
        p_status.setLayoutX(25.0);
        p_status.setLayoutY(25.0);
        p_status.setPrefHeight(30.0);
        p_status.setPrefWidth(100.0);
        p_status.getStyleClass().add("pStatus");
        p_status.setText("Dang mo ban" + " " + ".");

        Label p_subStatus = new Label();
        p_subStatus.setLayoutX(125.0);
        p_subStatus.setLayoutY(25.0);
        p_subStatus.setPrefHeight(30.0);
        p_subStatus.setPrefWidth(150.0);
        p_subStatus.getStyleClass().add("pSubStatus");
        p_subStatus.setText("Da ban het 1500 can");

        Label p_name = new Label();
        p_name.setLayoutX(25.0);
        p_name.setLayoutY(55.0);
        p_name.setPrefHeight(50.0);
        p_name.getStyleClass().add("pName");
        p_name.setText("THE BEVERLY SOLARI - VINHOMES GRAND PARK");

        Label product_price = new Label();
        product_price.setLayoutX(25.0);
        product_price.setLayoutY(95.0);
        product_price.setPrefWidth(100.0);
        product_price.setPrefHeight(50.0);
        product_price.getStyleClass().add("productPrice");
        product_price.setText("60.0" + " " + "trieu/m2" + " ");

        Label p_area = new Label();
        p_area.setLayoutX(125.0);
        p_area.setLayoutY(95.0);
        p_area.setPrefWidth(50.0);
        p_area.setPrefHeight(50.0);
        p_area.getStyleClass().add("pArea");
        p_area.setText("8.7" + " " + "ha" + " ");

        var image = new Image(getClass().getResourceAsStream(StringValue.Instance().ASSET_BUSINESS_INVESTOR + "apartment.png"));
        ImageView product_icon = new ImageView();
        product_icon.setImage(image);
        product_icon.setLayoutX(225.0);
        product_icon.setLayoutY(110.0);
        product_icon.setFitHeight(20.0);
        product_icon.setFitWidth(20.0);
        Label total_product = new Label();
        total_product.setLayoutX(190.0);
        total_product.setLayoutY(95.0);
        total_product.setPrefWidth(40.0);
        total_product.setPrefHeight(50.0);
        total_product.getStyleClass().add("totalProduct");
        total_product.setText("7850");

        var image1 = new Image(getClass().getResourceAsStream(StringValue.Instance().ASSET_BUSINESS_INVESTOR + "building.png"));
        ImageView block_icon = new ImageView();
        block_icon.setImage(image1);
        block_icon.setLayoutX(280.0);
        block_icon.setLayoutY(110.0);
        block_icon.setFitHeight(20.0);
        block_icon.setFitWidth(20.0);
        Label total_blocks = new Label();
        total_blocks.setLayoutX(260.0);
        total_blocks.setLayoutY(95.0);
        total_blocks.setPrefWidth(20.0);
        total_blocks.setPrefHeight(50.0);
        total_blocks.getStyleClass().add("totalBlocks");
        total_blocks.setText("13");

        Label p_location = new Label();
        p_location.setLayoutX(25.0);
        p_location.setLayoutY(130.0);
        p_location.setPrefHeight(50.0);
        p_location.getStyleClass().add("pLocation");
        p_location.setText("KDT Vinhomes Grand Park - p.Long Thanh My - Q9 - HCM");
        
        Label p_info = new Label();
        p_info.setLayoutX(25.0);
        p_info.setLayoutY(165.0);
        p_info.setPrefHeight(50.0);
        p_info.getStyleClass().add("pInfo");
        p_info.setText("The Beverly Solari (tên cũ: The Miyako) là phân khu căn hộ lớn thứ ba trong tổng thể dự án Vinhomes Grand Park Quận 9, TP.HCM. Phân khu The Bever...");
        
        this.getChildren().addAll(p_status, p_subStatus, p_name, product_price, p_area, total_product, product_icon, total_blocks, block_icon, p_location, p_info);
    }

    public ProjectInvestorComponent(String p_status, String p_subStatus, String p_name, Float product_price, Float p_area, Integer total_product, Integer total_blocks, String p_location, String p_info, Double vGap, Double hgap) {
        this.setPrefHeight(500.0);
        this.setPrefWidth(1000.0);
        this.getStyleClass().add("pi_Component");

    }

}
