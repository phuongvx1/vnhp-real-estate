/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Gb_Status;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorGbsUpdateController {

    @FXML
    private TextField tfDirGBSUpdateName;

    @FXML
    private Label tfDirGBSUpdateResult;

    @FXML
    private Button btnDirGBSUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Gb_Status data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(Gb_Status data) {
        tfDirGBSUpdateName.setText(data.getGbs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Gb_Status data) {
        btnDirGBSUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirGBSUpdateName.getText();

            var newData = new Gb_Status(data.getGbs_id(), name);
            var retult = BaseDao.Instance().update(newData);
            tfDirGBSUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirGBSUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Gb_Status) data;
            tmp.setGbs_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getGbs_id(), tmp);
        });
    }

}
