/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Account;
import com.vnhp.real.estate.model.entities.Department;
import com.vnhp.real.estate.model.entities.Investor;
import com.vnhp.real.estate.model.entities.MyThread;
import com.vnhp.real.estate.model.entities.Position;
import com.vnhp.real.estate.model.entities.ProjectStatus;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import com.vnhp.real.estate.res.UtilitiesManager;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRAddNewStaffController  {
    @FXML
    private TextField tfHrNameAddName,tfHrContactAddNew,tfHrEmailAddNew,tfHrAddressAddNew;
    @FXML
    private Label tfHrAddNewStaffView;
    @FXML
    private Button btnAddAddNew;
    @FXML
    private ComboBox<String> tfHrDepartmentAddNew,tfHrPositionAddNew;
  public void init(Stage stage) {
        setupDefault(stage);
        setup();
    }

    private void setupDefault(Stage stage) {
        var department = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().DEPARTMENT_ACTIVE, Department[].class));
        tfHrDepartmentAddNew.getItems().addAll(department);
        tfHrDepartmentAddNew.setValue((String) department.get(0));
        var position = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().POSITION_ACTIVE, Position[].class));
        tfHrPositionAddNew.getItems().addAll(position);
        tfHrPositionAddNew.setValue((String) position.get(0));
    }
    private void setup() {
          btnAddAddNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfHrNameAddName.getText();
            var email = tfHrEmailAddNew.getText();
            var address = tfHrAddressAddNew.getText();
            var contract = tfHrContactAddNew.getText();
            
            var department = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Department(tfHrDepartmentAddNew.getValue()), Department[].class, 1), ColumnId.class); 
            var position = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Position(tfHrPositionAddNew.getValue()), Position[].class, 1), ColumnId.class);
//            var manager = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Manager(tfHrManagerAddNew.getValue()), HRAddNewStatus[].class, 1), ColumnId.class); 
            var data = new Account(name, email, address,contract, department, position);
            var retult = BaseDao.Instance().insert(data);
            tfHrAddNewStaffView.setText(retult);
            TimeManager.Instance().clearResult(eh, tfHrAddNewStaffView, 2);
        });
    }               



    
    
}
