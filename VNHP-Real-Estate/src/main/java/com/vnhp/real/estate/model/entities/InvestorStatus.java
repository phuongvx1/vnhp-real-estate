/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class InvestorStatus {

    public Integer index;
    @ColumnId
    public Integer is_id;
    @ColumnName
    @DbColumn
    public String is_name;
    @ColumnStatus
    @DbColumn
    public Boolean is_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIs_id(Integer is_id) {
        this.is_id = is_id;
    }

    public void setIs_name(String is_name) {
        this.is_name = is_name;
    }

    public void setIs_status(Boolean is_status) {
        this.is_status = is_status;
    }

    public Integer getIs_id() {
        return is_id;
    }

    public String getIs_name() {
        return is_name;
    }

    public Boolean getIs_status() {
        return is_status;
    }

    public InvestorStatus() {
    }

    public InvestorStatus(String is_name) {
        this.is_name = is_name;
    }

    public InvestorStatus(Integer is_id, String is_name) {
        this.is_id = is_id;
        this.is_name = is_name;
    }

    public InvestorStatus(Integer is_id, Boolean is_status) {
        this.is_id = is_id;
        this.is_status = is_status;
    }

    public InvestorStatus(Integer is_id, String is_name, Boolean is_status, Integer index) {
        this.index = index;
        this.is_id = is_id;
        this.is_name = is_name;
        this.is_status = is_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
