/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;

/**
 *
 * @author FairyHunter
 */
public final class DepartmentStatus {

    public Integer index;
    @ColumnId
    public Integer ds_id;
    @ColumnName
    public String ds_name;
    @ColumnStatus
    public Boolean ds_status;
    public String btnChangePassword;
    public String btnUpdate;
    
    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }
    
    public void setds_id(Integer ds_id) {
        this.ds_id = ds_id;
    }

    public void setds_name(String ds_name) {
        this.ds_name = ds_name;
    }

    public void setds_status(Boolean ds_status) {
        this.ds_status = ds_status;
    }

    public Integer getds_id() {
        return ds_id;
    }

    public String getds_name() {
        return ds_name;
    }

    public Boolean getds_status() {
        return ds_status;
    }

    public DepartmentStatus() {
    }

    public DepartmentStatus(Boolean ds_status) {
        this.ds_status = ds_status;
    }

    public DepartmentStatus(Integer ds_id, String ds_name) {
        this.ds_id = ds_id;
        this.ds_name = ds_name;
    }

    public DepartmentStatus(Integer ds_id ,Boolean ds_status) {
        this.ds_id = ds_id;
        this.ds_status = ds_status;
    }

    public DepartmentStatus(Integer ds_id, String ds_name, Boolean ds_status, Integer index) {
        this.index = index;
        this.ds_id = ds_id;
        this.ds_name = ds_name;
        this.ds_status = ds_status;
        this.btnChangePassword = "Change Status";
        this.btnUpdate = "Update";
    }
    public DepartmentStatus(String department_name) {
        this.ds_name = department_name;
    }
}
