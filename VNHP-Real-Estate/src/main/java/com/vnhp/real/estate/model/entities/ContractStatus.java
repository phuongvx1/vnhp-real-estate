/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class ContractStatus {

    public Integer index;
    @ColumnId
    public Integer cons_id;
    @ColumnName
    @DbColumn
    public String cons_name;
    @ColumnStatus
    @DbColumn
    public Boolean cons_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setCons_id(Integer cons_id) {
        this.cons_id = cons_id;
    }

    public void setCons_name(String cons_name) {
        this.cons_name = cons_name;
    }

    public void setCons_status(Boolean cons_status) {
        this.cons_status = cons_status;
    }

    public Integer getCons_id() {
        return cons_id;
    }

    public String getCons_name() {
        return cons_name;
    }

    public Boolean getCons_status() {
        return cons_status;
    }

    public ContractStatus() {
    }

    public ContractStatus(String cons_name) {
        this.cons_name = cons_name;
    }

    public ContractStatus(Integer cons_id, String cons_name) {
        this.cons_id = cons_id;
        this.cons_name = cons_name;
    }

    public ContractStatus(Integer cons_id, Boolean cons_status) {
        this.cons_id = cons_id;
        this.cons_status = cons_status;
    }

    public ContractStatus(Integer cons_id, String cons_name, Boolean cons_status) {
        this.cons_id = cons_id;
        this.cons_name = cons_name;
        this.cons_status = cons_status;
    }

    public ContractStatus(Integer cons_id, String cons_name, Boolean cons_status, Integer index) {
        this.index = index;
        this.cons_id = cons_id;
        this.cons_name = cons_name;
        this.cons_status = cons_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
