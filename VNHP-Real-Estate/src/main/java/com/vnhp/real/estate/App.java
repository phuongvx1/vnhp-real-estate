package com.vnhp.real.estate;

import com.vnhp.real.estate.controller.LoginPageController;
import com.vnhp.real.estate.res.StringValue;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.paint.Color;
import javafx.stage.StageStyle;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;


     @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().LOGIN_PAGE));
        Parent p = loader.load();

        scene = new Scene(p);
        stage.setScene(scene);
        scene.setFill(Color.TRANSPARENT);

        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResourceAsStream(StringValue.Instance().IMAGE + "logo.png")));
        stage.initStyle(StageStyle.TRANSPARENT);
        
        var control = (LoginPageController)loader.getController();
        control.init(stage);
        
        stage.show();
    }

}