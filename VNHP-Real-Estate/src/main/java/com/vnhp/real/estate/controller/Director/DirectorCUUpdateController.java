/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.ConstructionUnit;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorCUUpdateController {

    @FXML
    private TextField tfDirCUUpdateName, tfDirCUUpdateEmail, tfDirCUUpdateAddress, tfDirCUUpdateLogo, tfDirCUUpdateContract;

    @FXML
    private Label tfDirCUUpdateResult;

    @FXML
    private Button btnDirCUUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, ConstructionUnit data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(ConstructionUnit data) {
        tfDirCUUpdateName.setText(data.getCu_name());
        tfDirCUUpdateEmail.setText(data.getCu_email());
        tfDirCUUpdateAddress.setText(data.getCu_address());
        tfDirCUUpdateLogo.setText(data.getCu_logo());
        tfDirCUUpdateContract.setText(data.getCu_contact());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, ConstructionUnit data) {
        btnDirCUUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirCUUpdateName.getText();
            var email = tfDirCUUpdateEmail.getText();
            var address = tfDirCUUpdateAddress.getText();
            var logo = tfDirCUUpdateLogo.getText();
            var contract = tfDirCUUpdateContract.getText();

            var newData = new ConstructionUnit(data.getCu_id(), name, email, address, logo, contract);
            var retult = BaseDao.Instance().update(newData);
            tfDirCUUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirCUUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (ConstructionUnit) data;
            tmp.setCu_name(name);
            tmp.setCu_email(email);
            tmp.setCu_address(address);
            tmp.setCu_logo(logo);
            tmp.setCu_contact(contract);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getCu_id(), tmp);
        });
    }
}
