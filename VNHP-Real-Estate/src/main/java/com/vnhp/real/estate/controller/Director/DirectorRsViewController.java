/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.RegionStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.ObjectType;
import com.vnhp.real.estate.res.ServerService;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableName;
import com.vnhp.real.estate.res.TableSetting;
import java.util.LinkedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.ColumnConstraints;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorRsViewController {

    @FXML
    private ScrollPane pDirRSView;

    @FXML
    private TextField tfDirRsSearch;

    public void init() {
        setup();
    }

    private void setupSearch(MyGridPane grid, TableSetting tableSetting) {
        tfDirRsSearch.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var searchText = tfDirRsSearch.getText();
                var data = new RegionStatus();
                var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(BaseDao.Instance().searchQuery(data, "", searchText), RegionStatus[].class), ServerService.Instance().GET_REGION_STATUS);
                grid.clearAll();
                grid.setRow(grid, listData, tableSetting, TableName.RegionStatus);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void setup() {
        var gridPane = new MyGridPane();

        LinkedList<String> titleList = new LinkedList<>();
        titleList.add("ID");
        titleList.add("Name");
        titleList.add("Status");
        titleList.add("Change Status");
        titleList.add("Update");

        gridPane.addTitles(titleList, TableName.RegionStatus);
        var rowSettings = new LinkedList<String>();
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyLabel.toString());
        rowSettings.add(ObjectType.MyButton.toString());
        rowSettings.add(ObjectType.MyButton.toString());

        var col1 = new ColumnConstraints();
        col1.setPrefWidth(30);
        var col2 = new ColumnConstraints();
        col2.setPrefWidth(100);
        var col3 = new ColumnConstraints();
        col3.setPrefWidth(70);
        var col4 = new ColumnConstraints();
        col4.setPrefWidth(150);
        var col5 = new ColumnConstraints();
        col5.setPrefWidth(100);

        var setting = new LinkedList<ColumnConstraints>();
        setting.add(col1);
        setting.add(col2);
        setting.add(col3);
        setting.add(col4);
        setting.add(col5);

        var tableSetting = new TableSetting(rowSettings, setting);
        var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().REGION_STATUS_ACTIVE, RegionStatus[].class), ServerService.Instance().GET_REGION_STATUS);

        gridPane.setRow(gridPane, listData, tableSetting, TableName.RegionStatus);
        gridPane.getColumnConstraints().addAll(tableSetting.getSetting());
        pDirRSView.setLayoutX(50);
        pDirRSView.setLayoutY(80);
        pDirRSView.setBackground(Background.EMPTY);
        pDirRSView.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        pDirRSView.setContent(gridPane);
        setupSearch(gridPane, tableSetting);
        
        pDirRSView.vvalueProperty().addListener(eh-> update(gridPane, tableSetting));
    }
    private boolean ck = true;

    private void update(MyGridPane grid, TableSetting tableSetting) {
        if (ck) {
            ck = false;
            var listData = ConvertObject.Instance().toList(BaseDao.Instance().getListData(StringValue.Instance().REGION_STATUS_ACTIVE, RegionStatus[].class), ServerService.Instance().GET_REGION_STATUS);
            grid.clearAll();
            grid.setRow(grid, listData, tableSetting, TableName.RegionStatus);
        }
    }
}
