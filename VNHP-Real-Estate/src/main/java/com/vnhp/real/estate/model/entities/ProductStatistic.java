/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

/**
 *
 * @author FairyHunter
 */
public class ProductStatistic {
    
    public Integer ps_id;
    public String ps_name;
    public Integer ps_quantity;
    public Boolean ps_status;

    public void setPs_id(Integer ps_id) {
        this.ps_id = ps_id;
    }

    public void setPs_name(String ps_name) {
        this.ps_name = ps_name;
    }

    public void setPs_quantity(Integer ps_quantity) {
        this.ps_quantity = ps_quantity;
    }

    public void setPs_status(Boolean ps_status) {
        this.ps_status = ps_status;
    }

    public Integer getPs_id() {
        return ps_id;
    }

    public String getPs_name() {
        return ps_name;
    }

    public Integer getPs_quantity() {
        return ps_quantity;
    }

    public Boolean getPs_status() {
        return ps_status;
    }

    public ProductStatistic() {
    }

    public ProductStatistic(Integer ps_id, String ps_name, Integer ps_quantity, Boolean ps_status) {
        this.ps_id = ps_id;
        this.ps_name = ps_name;
        this.ps_quantity = ps_quantity;
        this.ps_status = ps_status;
    }
}
