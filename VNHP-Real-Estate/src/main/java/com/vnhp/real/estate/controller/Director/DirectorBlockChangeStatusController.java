/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorBlockChangeStatusController {

    @FXML
    private Label tfDirBlockCSName;
    @FXML
    private ComboBox<String> tfDirBlockCSStatus;
    @FXML
    public Label tfDirBlockCSResult;
    @FXML
    private Button btnDirBlockCS;
//    @FXML
//    private ImageView btnClose, btnMinimize;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, Blocks data) {
//        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        setupDefault(data);

        setup(grid, gridIndex, tableSetting, data);
    }

    private void setupDefault(Blocks data) {
        tfDirBlockCSName.setText(data.getBlock_name());

        var bs = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().B_STATUS_ACTIVE, B_Status[].class));
        tfDirBlockCSStatus.getItems().addAll(bs);
        tfDirBlockCSStatus.setValue(data.getBs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Blocks data) {
        btnDirBlockCS.addEventHandler(ActionEvent.ACTION, eh -> {
            var bs_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new B_Status(tfDirBlockCSStatus.getValue()), B_Status[].class, 1), ColumnId.class);
            var tmpData = new Blocks(data.getBlock_id(), bs_id);
            var retult = BaseDao.Instance().update(tmpData);
            tfDirBlockCSResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirBlockCSResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Blocks) data;
            tmp.setBs_name(tfDirBlockCSStatus.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getBlock_id(), tmp);
        });

    }
}
