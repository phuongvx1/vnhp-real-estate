package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.*;

/**
 *
 * @author phongv
 */
public class DepositContract {

    public Integer index;
    @ColumnId
    public Integer dc_id;
    public String dc_time_open;
    public Integer dc_number;
    public Float dc_payment;
    public Integer sc_id;
    @ColumnStatus
    public String dc_status;
    public String sc_total_payment;
    public String product_name;

    public DepositContract() {
    }

    public DepositContract(String dc_time_open, Integer dc_number, Float dc_payment, Integer sc_id) {
        this.dc_time_open = dc_time_open;
        this.dc_number = dc_number;
        this.dc_payment = dc_payment;
        this.sc_id = sc_id;
        this.sc_total_payment = sc_total_payment;
    }

    public String getSc_total_payment() {
        return sc_total_payment;
    }

    public void setSc_total_payment(String sc_total_payment) {
        this.sc_total_payment = sc_total_payment;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getDc_id() {
        return dc_id;
    }

    public void setDc_id(int dc_id) {
        this.dc_id = dc_id;
    }

    public String getDc_time_open() {
        return dc_time_open;
    }

    public void setDc_time_open(String dc_time_open) {
        this.dc_time_open = dc_time_open;
    }

    public int getDc_number() {
        return dc_number;
    }

    public void setDc_number(int dc_number) {
        this.dc_number = dc_number;
    }

    public Float getDc_payment() {
        return dc_payment;
    }

    public void setDc_payment(Float dc_payment) {
        this.dc_payment = dc_payment;
    }

    public int getSc_id() {
        return sc_id;
    }

    public void setSc_id(int sc_id) {
        this.sc_id = sc_id;
    }

    public String getDc_status() {
        return dc_status;
    }

    public void setDc_status(String dc_status) {
        this.dc_status = dc_status;
    }

}
