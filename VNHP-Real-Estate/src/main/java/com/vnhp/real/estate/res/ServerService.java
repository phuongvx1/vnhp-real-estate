/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

/**
 *
 * @author FairyHunter
 */
public class ServerService {

    private static ServerService instance;

    private ServerService() {

    }

    public static ServerService Instance() {
        if (instance == null) {
            instance = new ServerService();
        }

        return instance;
    }

    public final String GET_CONTRUCTION_UNIT = "GET_CONTRUCTION_UNIT";
    public final String GET_CONTRUCTION_UNIT_STATUS = "GET_CONTRUCTION_UNIT_STATUS";

    public final String GET_DIRECTION = "GET_DIRECTION";
    public final String GET_PROJECT = "GET_PROJECT";
    public final String GET_PROJECT_STATUS = "GET_PROJECT_STATUS";
    public final String GET_PROJECT_TYPE = "GET_PROJECT_TYPE";
    public final String GET_REGION = "GET_REGION";
    public final String GET_REGION_STATUS = "GET_REGION_STATUS";
    public final String GET_INVESTOR_STATUS = "GET_INVESTOR_STATUS";
    public final String GET_PAYMENT_TYPE = "GET_PAYMENT_TYPE";
    public final String GET_INVESTOR = "GET_INVESTOR";
    public final String GET_PRODUCT_STATUS = "GET_PRODUCT_STATUS";
    public final String GET_PRODUCT = "GET_PRODUCT";
    public final String GET_UTILITIES = "GET_UTILITIES";
    public final String GET_GB_STATUS = "GET_GB_STATUS";
    public final String GET_GROUP_BLOCK = "GET_GROUP_BLOCK";
    public final String GET_BLOCK = "GET_BLOCK";
    public final String GET_BLOCK_STATUS = "GET_BLOCK_STATUS";
    public final String GET_CONTRACT_STATUS = "GET_CONTRACT_STATUS";
    public final String GET_CONTRACT_TYPE = "GET_CONTRACT_TYPE";
    public final String GET_CUSTOMER = "GET_CUSTOMER";
    public final String GET_CUSTOMER_STATUS = "GET_CUSTOMER_STATUS";
    public final String GET_CUSTOMER_TYPE = "GET_CUSTOMER_TYPE";
    public final String GET_PPS_STATUS = "GET_PPS_STATUS";
    public final String GET_AC_STATUS = "GET_AC_STATUS";
    public final String GET_ACLOGIN_STATUS = "GET_ACLOGIN_STATUS";
    public final String GET_DEPARTMENT_STATUS = "GET_DEPARTMENT_STATUS";
    public final String GET_DEPARTMENT = "GET_DEPARTMENT";
    public final String GET_POSITION = "GET_POSITION";
    public final String GET_TOP_10_CUSTOMER_REVENUE = "GET_TOP_10_CUSTOMER_REVENUE";
    public final String GET_STATISTIC_QUANTITY = "GET_STATISTIC_QUANTITY";
    public final String GET_SALE_CONTRACT = "GET_SALE_CONTRACT";
    public final String GET_SALE_CONTRACT_ID = "GET_SALE_CONTRACT_ID";
    public final String GET_PRODUCT_REVENUE = "GET_PRODUCT_REVENUE";
    public final String GET_PRODUCT_STATISTIC = "GET_PRODUCT_STATISTIC";
    public final String GET_ACCOUNT = "GET_ACCOUNT";
    public final String GET_NET_REVENUE = "GET_NET_REVENUE";
    public final String GET_DEPT = "GET_DEPT";

}
