/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class RegionStatus {

    public Integer index;
    @ColumnId
    public Integer rs_id;
    @ColumnName
    @DbColumn
    public String rs_name;
    @ColumnStatus
    @DbColumn
    public Boolean rs_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setRs_id(Integer rs_id) {
        this.rs_id = rs_id;
    }

    public void setRs_name(String rs_name) {
        this.rs_name = rs_name;
    }

    public void setRs_status(Boolean rs_status) {
        this.rs_status = rs_status;
    }

    public Integer getRs_id() {
        return rs_id;
    }

    public String getRs_name() {
        return rs_name;
    }

    public Boolean getRs_status() {
        return rs_status;
    }

    public RegionStatus() {
    }

    public RegionStatus(String rs_name) {
        this.rs_name = rs_name;
    }

    public RegionStatus(Integer rs_id, String rs_name) {
        this.rs_id = rs_id;
        this.rs_name = rs_name;
    }

    public RegionStatus(Integer rs_id, Boolean rs_status) {
        this.rs_id = rs_id;
        this.rs_status = rs_status;
    }

    public RegionStatus(Integer rs_id, String rs_name, Boolean rs_status, Integer index) {
        this.index = index;
        this.rs_id = rs_id;
        this.rs_name = rs_name;
        this.rs_status = rs_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }

}
