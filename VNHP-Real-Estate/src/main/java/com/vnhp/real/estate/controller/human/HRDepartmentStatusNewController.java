/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.DepartmentStatus;
import com.vnhp.real.estate.res.TimeManager;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRDepartmentStatusNewController  {
    @FXML
    public TextField tfDepartStatusNew;

    @FXML
    private Label tfHrDepartStatusResult;

    @FXML
    private Button btnAddDepartStatus;
   

    public void init() {
        setup();
    }

    private void setup() {
        btnAddDepartStatus.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDepartStatusNew.getText();

            var data = new DepartmentStatus(name);
            var retult = BaseDao.Instance().insert(data);
            tfHrDepartStatusResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfHrDepartStatusResult, 2);
        });
    }
}    
