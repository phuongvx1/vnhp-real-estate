/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author bichv
 */
public class SaleReceipt {

    public Integer index;
    public Integer sc_id;
    public String sc_time_open;
    public Integer sc_number;
    public Float sc_deposit;
    public Float sc_total_payment;
    public Integer saler_id;
    public String saler_name;
    public Integer payer_id;
    public String payer_name;
    public Integer product_id;
    public String product_name;
    public Integer pm_id;
    public String pm_name;
    public Integer cont_id;
    public String cont_name;
    public Integer cons_id;
    public String cons_name;
    public Button button;
    public Label sttLabel;
    public String btnChangeStatus;
    public String btnUpdate;
    public Label method;

    public SaleReceipt() {
    }

    public SaleReceipt(Integer sc_id, String sc_time_open, Float sc_total_payment, String saler_name, String product_name, String payer_name, String cons_name, Integer cont_id) {
        this.sc_id = sc_id;
        this.sc_time_open = sc_time_open;
        this.sc_total_payment = sc_total_payment;
        this.saler_name = saler_name;
        this.product_name = product_name;
        this.payer_name = payer_name;
        this.cons_name = cons_name;
        this.cont_id = cont_id;
    }

    public SaleReceipt(Integer sc_id, String sc_time_open, String product_name, Float sc_total_payment, String payer_name, String saler_name, String cons_name, Integer cont_id, Button button) throws FileNotFoundException {
        this.sc_id = sc_id;
        this.sc_time_open = sc_time_open;
        this.sc_total_payment = sc_total_payment;
        this.saler_name = saler_name;
        this.product_name = product_name;
        this.payer_name = payer_name;
        this.cons_name = cons_name;
        this.cont_id = cont_id;
        this.button = button;
        if (this.cont_id == 1) {
            this.method = new Label("Installment");
            this.method.setAlignment(Pos.CENTER);
            this.method.setPrefWidth(70);
            this.method.setPrefHeight(25);
            this.method.setStyle("-fx-background-color: #6c757d; -fx-text-fill: white;");
        } else {
            this.method = new Label("Direct Pay");
            this.method.setAlignment(Pos.CENTER);
            this.method.setPrefWidth(70);
            this.method.setPrefHeight(25);
            this.method.setStyle("-fx-background-color: #343a40; -fx-text-fill: white;");
        }
        ImageView imageView = new ImageView(new Image(new FileInputStream("C:/Aptech/java-work/vnhp-real-estate/VNHP-Real-Estate/src/main/resources/com/vnhp/real/estate/assets/printer.png")));
        if ("In process".equals(this.cons_name)) {
            this.sttLabel = new Label("In process");
            this.sttLabel.setPrefWidth(70);
            this.sttLabel.setPrefHeight(25);
            this.sttLabel.setAlignment(Pos.CENTER);
            this.sttLabel.setStyle("-fx-background-color: #17a2b8; -fx-text-fill: white;");
            this.button.setText("Export");
            this.button.setStyle("-fx-background-color: #28a745; -fx-text-fill: white;");
            this.button.setGraphic(imageView);
        } else if ("Depositing".equals(this.cons_name)) {
            this.sttLabel = new Label("Depositing");
            this.sttLabel.setPrefWidth(70);
            this.sttLabel.setPrefHeight(25);
            this.sttLabel.setAlignment(Pos.CENTER);
            this.sttLabel.setStyle("-fx-background-color: #dc3545; -fx-text-fill: white;");
            this.button.setStyle("-fx-background-color: #28a745; -fx-text-fill: white;");
            this.button.setText("Export");
            this.button.setGraphic(imageView);
            this.button.setDisable(true);
        } else {
            this.sttLabel = new Label("Complete");
            this.sttLabel.setPrefWidth(70);
            this.sttLabel.setPrefHeight(20);
            this.sttLabel.setAlignment(Pos.CENTER);
            this.sttLabel.setStyle("-fx-background-color: #007bff; -fx-text-fill: white;");
            this.button.setStyle("-fx-background-color: #28a745; -fx-text-fill: white;");
            this.button.setText("Export");
            this.button.setGraphic(imageView);
            this.button.setDisable(true);
        }
        if (this.cont_id == 1) {
            this.button.setDisable(true);
        }
    }

    public Label getMethod() {
        return method;
    }

    public void setMethod(Label method) {
        this.method = method;
    }

    public Label getSttLabel() {
        return sttLabel;
    }

    public void setSttLabel(Label sttLabel) {
        this.sttLabel = sttLabel;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getSc_id() {
        return sc_id;
    }

    public void setSc_id(Integer sc_id) {
        this.sc_id = sc_id;
    }

    public String getSc_time_open() {
        return sc_time_open;
    }

    public void setSc_time_open(String sc_time_open) {
        this.sc_time_open = sc_time_open;
    }

    public Integer getSc_number() {
        return sc_number;
    }

    public void setSc_number(Integer sc_number) {
        this.sc_number = sc_number;
    }

    public Float getSc_deposit() {
        return sc_deposit;
    }

    public void setSc_deposit(Float sc_deposit) {
        this.sc_deposit = sc_deposit;
    }

    public Float getSc_total_payment() {
        return sc_total_payment;
    }

    public void setSc_total_payment(Float sc_total_payment) {
        this.sc_total_payment = sc_total_payment;
    }

    public Integer getSaler_id() {
        return saler_id;
    }

    public void setSaler_id(Integer saler_id) {
        this.saler_id = saler_id;
    }

    public String getSaler_name() {
        return saler_name;
    }

    public void setSaler_name(String saler_name) {
        this.saler_name = saler_name;
    }

    public Integer getPayer_id() {
        return payer_id;
    }

    public void setPayer_id(Integer payer_id) {
        this.payer_id = payer_id;
    }

    public String getPayer_name() {
        return payer_name;
    }

    public void setPayer_name(String payer_name) {
        this.payer_name = payer_name;
    }

    public Integer getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public Integer getPm_id() {
        return pm_id;
    }

    public void setPm_id(Integer pm_id) {
        this.pm_id = pm_id;
    }

    public String getPm_name() {
        return pm_name;
    }

    public void setPm_name(String pm_name) {
        this.pm_name = pm_name;
    }

    public Integer getCont_id() {
        return cont_id;
    }

    public void setCont_id(Integer cont_id) {
        this.cont_id = cont_id;
    }

    public String getCont_name() {
        return cont_name;
    }

    public void setCont_name(String cont_name) {
        this.cont_name = cont_name;
    }

    public Integer getCons_id() {
        return cons_id;
    }

    public void setCons_id(Integer cons_id) {
        this.cons_id = cons_id;
    }

    public String getCons_name() {
        return cons_name;
    }

    public void setCons_name(String cons_name) {
        this.cons_name = cons_name;
    }

    public String getBtnChangeStatus() {
        return btnChangeStatus;
    }

    public void setBtnChangeStatus(String btnChangeStatus) {
        this.btnChangeStatus = btnChangeStatus;
    }

    public String getBtnUpdate() {
        return btnUpdate;
    }

    public void setBtnUpdate(String btnUpdate) {
        this.btnUpdate = btnUpdate;
    }

}
