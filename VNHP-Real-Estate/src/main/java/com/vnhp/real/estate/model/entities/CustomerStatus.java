/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class CustomerStatus {

    public Integer index;
    @ColumnId
    public Integer cs_id;
    @ColumnName
    @DbColumn
    public String cs_name;
    @ColumnStatus
    @DbColumn
    public Boolean cs_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setCs_id(Integer cs_id) {
        this.cs_id = cs_id;
    }

    public void setCs_name(String cs_name) {
        this.cs_name = cs_name;
    }

    public void setCs_status(Boolean cs_status) {
        this.cs_status = cs_status;
    }

    public Integer getCs_id() {
        return cs_id;
    }

    public String getCs_name() {
        return cs_name;
    }

    public Boolean getCs_status() {
        return cs_status;
    }

    public CustomerStatus() {
    }

    public CustomerStatus(String cs_name) {
        this.cs_name = cs_name;
    }

    public CustomerStatus(Integer cs_id, String cs_name) {
        this.cs_id = cs_id;
        this.cs_name = cs_name;
    }

    public CustomerStatus(Integer cs_id, Boolean cs_status) {
        this.cs_id = cs_id;
        this.cs_status = cs_status;
    }

    public CustomerStatus(Integer cs_id, String cs_name, Boolean cs_status, Integer index) {
        this.index = index;
        this.cs_id = cs_id;
        this.cs_name = cs_name;
        this.cs_status = cs_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
