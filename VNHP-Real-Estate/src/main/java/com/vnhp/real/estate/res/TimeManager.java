/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

import javafx.util.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;

/**
 *
 * @author APTECH
 */
public class TimeManager {

    private static TimeManager instance;

    private TimeManager() {

    }

    public static TimeManager Instance() {
        if (instance == null) {
            instance = new TimeManager();
        }

        return instance;
    }

    public void initClock(Label datetime) {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                datetime.setText(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy   HH:mm:ss")));
            }
        };
        timer.start();
    }

    private static Timeline timeline;
    private static Integer timeSeconds = 0;

    public void clearResult(ActionEvent event, Label label, Integer seconds) {
        if (timeline != null) {
            timeline.stop();
        }
        timeSeconds = seconds;

        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(
                new KeyFrame(Duration.seconds(1), (ActionEvent t) -> {
                    timeSeconds--;
                    if (timeSeconds <= 0) {
                        label.setText("");
                        timeline.stop();
                    }
                }));

        timeline.playFromStart();
    }

    public Calendar getServerTime() {
        var cal = Calendar.getInstance();
        cal.setTime(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        return cal;
    }
    
    public java.util.Date getMonthServerTime(Integer additional) {
        var current = getServerTime();
        
        current.add(Calendar.MONTH, additional);
        return current.getTime();
    }
    
    public java.util.Date getYearServerTime(Integer additional) {
        var current = getServerTime();
        
        current.add(Calendar.YEAR, additional);
        return current.getTime();
    }
    
    public int getCurrentYear(){
        return getServerTime().getTime().getYear() + 1900;
    }
}
