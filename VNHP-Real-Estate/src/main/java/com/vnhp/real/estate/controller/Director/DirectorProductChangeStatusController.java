/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import static com.vnhp.real.estate.controller.Director.DirectorProductNewController.ps;
import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.Product;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProductChangeStatusController {

    public static Product updateData;

    @FXML
    private Label tfDirProductCSName;
    @FXML
    private ComboBox<String> tfDirProductCSStatus;
    @FXML
    public Label tfDirProductCSResult;
    @FXML
    private Button btnDirProductCS;
//    @FXML
//    private ImageView btnClose, btnMinimize;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, Product data) {
//        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        setupDefault(data);

        setup(grid, gridIndex, tableSetting, data);
    }

    private void setupDefault(Product data) {
        tfDirProductCSName.setText(data.getProduct_name());
        tfDirProductCSStatus.getItems().addAll(ps);
        tfDirProductCSStatus.setValue(data.getPs_name());
        
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Product data) {
        btnDirProductCS.addEventHandler(ActionEvent.ACTION, eh -> {
            var ps_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ProductStatus(tfDirProductCSStatus.getValue()), ProductStatus[].class, 1), ColumnId.class);
            var tmpData = new Product(data.getProduct_id(), ps_id);
            var retult = BaseDao.Instance().update(tmpData);
            tfDirProductCSResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirProductCSResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Product) data;
            tmp.setPs_name(tfDirProductCSStatus.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getProduct_id(),tmp);
        });

    }
}
