/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.res.AppSetting;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRLayoutController {

    @FXML
    private ImageView btnClose, btnMinimize;
    @FXML
    private Button btnHrAddNewStaff, btnHrAddNewDepartment, btnHrAddNewPosition;
    @FXML
    private Pane pHrMainPane;
    @FXML
    private Label dateTime;

    public void init(Stage stage) {
        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        setup(stage);
        TimeManager.Instance().initClock(dateTime);
    }

    private void setup(Stage stage) {
        try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_ADDNEWLAYOUT));
            AnchorPane dashboard = dataNew.load();
            pHrMainPane.getChildren().clear();
            pHrMainPane.getChildren().add(dashboard);
            var control = (HRAddNewLayoutController) dataNew.getController();
            control.init(stage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        btnHrAddNewStaff.setOnMousePressed(eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_ADDNEWLAYOUT));
                AnchorPane dashboard = dataNew.load();
                pHrMainPane.getChildren().clear();
                pHrMainPane.getChildren().add(dashboard);
                var control = (HRAddNewLayoutController) dataNew.getController();
                control.init(stage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        btnHrAddNewDepartment.setOnMousePressed(eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOUCE_NEWDEPTLAYOUT));
                AnchorPane dashboard = dataNew.load();
                pHrMainPane.getChildren().clear();
                pHrMainPane.getChildren().add(dashboard);
                var control = (HRAddNewDepartmentLayoutController) dataNew.getController();
                control.init(stage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        btnHrAddNewPosition.setOnMousePressed(eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().HR_POSITION_LAYOUT));
                AnchorPane dashboard = dataNew.load();
                pHrMainPane.getChildren().clear();
                pHrMainPane.getChildren().add(dashboard);
                var control = (HRPositionLayoutController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
};
