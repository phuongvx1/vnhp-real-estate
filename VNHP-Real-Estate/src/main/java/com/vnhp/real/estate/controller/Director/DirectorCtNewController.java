/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.CustomerType;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorCtNewController {

    @FXML
    private TextField tfDirCtNewName;

    @FXML
    private Label tfDirCtNewResult;

    @FXML
    private Button btnDirCtNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirCtNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirCtNewName.getText();

            var data = new CustomerType(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirCtNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirCtNewResult, 2);
        });
    }

}
