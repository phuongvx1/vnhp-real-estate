/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorBsUpdateController {

    @FXML
    private TextField tfDirBSUpdateName;

    @FXML
    private Label tfDirBSUpdateResult;

    @FXML
    private Button btnDirBSUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, B_Status data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(B_Status data) {
        tfDirBSUpdateName.setText(data.getBs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, B_Status data) {
        btnDirBSUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirBSUpdateName.getText();

            var newData = new B_Status(data.getBs_id(), name);
            var retult = BaseDao.Instance().update(newData);
            tfDirBSUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirBSUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (B_Status) data;
            tmp.setBs_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getBs_id(), tmp);
        });
    }

}
