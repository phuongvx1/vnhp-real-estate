package com.vnhp.real.estate.controller.Business;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class BusinessLayoutController {

    @FXML
    private Pane contentDisplay;

    @FXML
    private Button avatarBtn, beProductBtn, customerBtn, contractBtn;

    @FXML
    public ImageView btnClose, btnMinimize;

    public void init(Stage stage) {
        openClose(stage);

        //BEInformation
        avatarBtn.setOnMouseClicked(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BE_INFORMATION));
            try {
                ScrollPane beInformation = loader.load();
                contentDisplay.getChildren().clear();
                contentDisplay.getChildren().add(beInformation);

                var control = (BEInformationController) loader.getController();
                control.init();

            } catch (IOException e) {
            } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException | ClassNotFoundException ex) {
                Logger.getLogger(BusinessLayoutController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        //BusinessProduct
        beProductBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_PRODUCT));

            try {
                Pane businessProduct = loader.load();
                contentDisplay.getChildren().clear();
                contentDisplay.getChildren().add(businessProduct);

                var control = (BusinessProductController) loader.getController();
                control.init(stage);

            } catch (IOException e) {
            }
        });

        //Customer
        customerBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_CUSTOMER));

            try {
                Pane businessCustomer = loader.load();
                contentDisplay.getChildren().clear();
                contentDisplay.getChildren().add(businessCustomer);

                var control = (BusinessCustomerController) loader.getController();
                control.init(stage);

            } catch (IOException e) {
            }
        });
        
        //Contract
        contractBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_CONTRACT));

            try {
                Pane businessContract = loader.load();
                contentDisplay.getChildren().clear();
                contentDisplay.getChildren().add(businessContract);

                var control = (BusinessContractController) loader.getController();
                control.init(stage);

            } catch (IOException e) {
            }
        });

    }

    void openClose(Stage stage) {
        btnClose.setOnMouseClicked(mouseEvent -> {
            stage.close();
        });

        btnMinimize.setOnMouseClicked(mouseEvent -> {
            stage.setIconified(true);

        });

    }
}
