/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller;

import com.vnhp.real.estate.jpa.PaymentPeriod_jpa;
import com.vnhp.real.estate.jpa.SaleContract_jpa;
import com.vnhp.real.estate.model.db.DbConnection;
import com.vnhp.real.estate.model.entities.DepositContract;
import com.vnhp.real.estate.model.entities.PaymentPeriod;
import com.vnhp.real.estate.model.entities.SaleReceipt;
import com.vnhp.real.estate.res.StringValue;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

public class CreditConstractController {

    ObservableList<PaymentPeriod> periodlist = FXCollections.observableArrayList();
    LinkedList<PaymentPeriod> arr = new LinkedList<>();
    Button[] button;
    TableView<PaymentPeriod> periodtable = new TableView<>();

    public void init(ScrollPane creditConstract, TextField searchBox) throws FileNotFoundException {
        PaymentPeriod_jpa period = new PaymentPeriod_jpa();
        arr = period.getAll();
        button = new Button[arr.size()];
        DepositConstract(creditConstract, searchBox);
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    LinkedList<PaymentPeriod> newList = new PaymentPeriod_jpa().getAll();
                    if (newList.size() > arr.size()) {
                        button = new Button[newList.size()];
                        periodlist.clear();
                        arr = newList;
                        for (int i = 0; i < newList.size(); i++) {
                            button[i] = new Button();
                            try {
                                PaymentPeriod item = new PaymentPeriod(
                                        newList.get(i).sc_id,
                                        newList.get(i).product_name,
                                        newList.get(i).sc_total_payment,
                                        newList.get(i).pp_payment,
                                        newList.get(i).pp_time,
                                        newList.get(i).pps_id,
                                        newList.get(i).dc_status,
                                        button[i]);
                                periodlist.add(item);
                            } catch (FileNotFoundException ex) {
                            }
                        }
                        periodtable.setItems(periodlist);
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {}
                }
            }
        };
        thread.setDaemon(true);
        thread.start();
    }

    public void refreshTable() throws FileNotFoundException {
        periodlist.clear();
        PaymentPeriod_jpa period = new PaymentPeriod_jpa();
        LinkedList<PaymentPeriod> arr1 = period.getAll();
        for (int i = 0; i < arr.size(); i++) {
            button[i] = new Button();
            button[i].setUserData(arr.get(i));
            button[i].setOnAction(this::handleButtonAction);
            PaymentPeriod item = new PaymentPeriod(
                    arr1.get(i).sc_id,
                    arr1.get(i).product_name,
                    arr1.get(i).sc_total_payment,
                    arr1.get(i).pp_payment,
                    arr1.get(i).pp_time,
                    arr1.get(i).pps_id,
                    arr1.get(i).dc_status,
                    button[i]);
            periodlist.add(item);
        }
        periodtable.setItems(periodlist);
    }

    private void DepositConstract(ScrollPane creditConstract, TextField searchBox) throws FileNotFoundException {
        periodtable.setPrefWidth(1169);
        periodtable.setPrefHeight(800);
        periodtable.setStyle("-fx-selection-bar: #b78c6e; -fx-selection-bar-non-focused: #d5bbaa;");

        // Create column
        TableColumn<PaymentPeriod, String> constractCol = new TableColumn<>("Constract");
        TableColumn<PaymentPeriod, String> productCol = new TableColumn<>("Product");
        TableColumn<PaymentPeriod, String> totalCol = new TableColumn<>("Total");
        TableColumn<PaymentPeriod, String> depositCol = new TableColumn<>("Payment");
        TableColumn<PaymentPeriod, String> createdatCol = new TableColumn<>("Payment date");
        TableColumn<PaymentPeriod, Boolean> statusCol = new TableColumn<>("Status");
        TableColumn<PaymentPeriod, String> receiptCol = new TableColumn<>("Receipt");

        constractCol.setPrefWidth(95);
        productCol.setPrefWidth(300);
        totalCol.setPrefWidth(170);
        depositCol.setPrefWidth(170);
        createdatCol.setPrefWidth(170);
        statusCol.setPrefWidth(125);
        receiptCol.setPrefWidth(120);

        constractCol.setCellValueFactory(new PropertyValueFactory<>("sc_id"));
        productCol.setCellValueFactory(new PropertyValueFactory<>("product_name"));
        totalCol.setCellValueFactory(new PropertyValueFactory<>("sc_total_payment"));
        depositCol.setCellValueFactory(new PropertyValueFactory<>("pp_payment"));
        createdatCol.setCellValueFactory(new PropertyValueFactory<>("pp_time"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("sttLabel"));
        receiptCol.setCellValueFactory(new PropertyValueFactory<>("button"));

        periodtable.getColumns().addAll(constractCol, productCol, totalCol, depositCol, createdatCol, statusCol, receiptCol);
        ObservableList<PaymentPeriod> itemList = getConstractList();
        periodtable.setItems(itemList);
        creditConstract.setContent(periodtable);

        FilteredList<PaymentPeriod> filteredData = new FilteredList<>(itemList, b -> true);
        searchBox.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(constract -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();
                if (String.valueOf(constract.getSc_id()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (constract.getProduct_name().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (String.valueOf(constract.getSc_total_payment()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (String.valueOf(constract.getPp_payment()).toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (constract.getPp_time().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else {
                    return false;
                }
            });
        });
        SortedList<PaymentPeriod> sortedData = new SortedList<>(filteredData);
        sortedData.comparatorProperty().bind(periodtable.comparatorProperty());
        periodtable.setItems(sortedData);

    }

    private ObservableList<PaymentPeriod> getConstractList() throws FileNotFoundException {
        for (int i = 0; i < arr.size(); i++) {
            button[i] = new Button();
            button[i].setUserData(arr.get(i));
            button[i].setOnAction(this::handleButtonAction);
            PaymentPeriod item = new PaymentPeriod(
                    arr.get(i).sc_id,
                    arr.get(i).product_name,
                    arr.get(i).sc_total_payment,
                    arr.get(i).pp_payment,
                    arr.get(i).pp_time,
                    arr.get(i).pps_id,
                    arr.get(i).dc_status,
                    button[i]);
            periodlist.add(item);
        }
        return periodlist;
    }

    private void handleButtonAction(ActionEvent event) {
        Node sourceComponent = (Node) event.getSource();
        PaymentPeriod subject = (PaymentPeriod) sourceComponent.getUserData();
        Stage newWindow = new Stage();
        StackPane newStk = confirmStackPane(subject, newWindow);

        newWindow.initStyle(StageStyle.TRANSPARENT);
        newWindow.initModality(Modality.APPLICATION_MODAL);
        newWindow.setScene(new Scene(newStk));
        newWindow.show();
    }

    private StackPane confirmStackPane(PaymentPeriod subject, Stage newWindow) {
        StackPane stack = new StackPane();
        stack.setPrefHeight(170);
        stack.setPrefWidth(350);
        stack.setStyle("-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.1), 100, 0.5, 0.0, 0.0)");

        VBox vbox = new VBox();

        Pane pane1 = new Pane();
        pane1.setPrefHeight(26);
        pane1.setPrefWidth(350);
        pane1.setStyle("-fx-background-color: #cccccc");

        Label message = new Label("Message");
        message.setPrefWidth(350);
        message.setPrefHeight(26);
        message.setStyle("-fx-font-style: italic; -fx-font-size: 14px");
        message.setAlignment(Pos.CENTER);

        Label noti = new Label("Are you sure to create receipt ?");
        noti.setAlignment(Pos.CENTER);
        noti.setPrefWidth(350);
        noti.setPrefHeight(90);
        noti.setStyle("-fx-font-size: 16");

        HBox hbox = new HBox();
        hbox.setAlignment(Pos.CENTER_RIGHT);
        hbox.setPrefHeight(42);
        hbox.setPrefWidth(350);
        hbox.setSpacing(20);

        Button trueBtn = new Button("Create");
        trueBtn.setPrefWidth(65);
        trueBtn.setPrefHeight(20);
        trueBtn.setStyle("-fx-font-weight: bold; fx-font-size: 14px;-fx-background-color: #007bff; -fx-text-fill: #ffffff");
        trueBtn.setOnAction(eh -> {
            exportReceipt(subject.pp_id);
            newWindow.close();
            PaymentPeriod_jpa newJpa = new PaymentPeriod_jpa();
            newJpa.UpdatePeriod(subject);
            newJpa.CheckContract(subject);
            try {
                refreshTable();
            } catch (FileNotFoundException ex) {
            }

        });

        Button falseBtn = new Button("Close");
        falseBtn.setPrefWidth(65);
        falseBtn.setPrefHeight(20);
        falseBtn.setStyle("-fx-font-weight: bold; fx-font-size: 14px;-fx-background-color: #6c757d;; -fx-text-fill: #ffffff");
        HBox.setMargin(falseBtn, new Insets(0, 20, 0, 0));
        falseBtn.setOnAction(eh -> {
            newWindow.close();
        });

        hbox.getChildren().addAll(trueBtn, falseBtn);
        pane1.getChildren().add(message);
        vbox.getChildren().addAll(pane1, noti, hbox);
        stack.getChildren().add(vbox);
        return stack;
    }

    public void exportReceipt(int pp_id) {
        try {
            JasperReport report = JasperCompileManager.compileReport(StringValue.Instance().CREDITRECEIPT);
            HashMap map = new HashMap();
            map.put("pp_id", pp_id);
            JasperPrint j = JasperFillManager.fillReport(report, map, DbConnection.Instance().Connect());
            JasperViewer.viewReport(j, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
