package com.vnhp.real.estate.controller.Business;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Account;
import com.vnhp.real.estate.model.entities.Position;
import com.vnhp.real.estate.res.StringValue;
import java.util.LinkedList;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.shape.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class BEInformationController {

    @FXML
    private ImageView beAvatar;

    @FXML
    private TextField beName, beAge, beAddress, beContact, beEmail, bePrePWD, beNewPWD;

    @FXML
    private Button beInfoSubmitBtn;

    @FXML
    private Label beInfoResult;

    @FXML
    private Pane uploadBtn;

    String inputName, inputAge, inputAddress, inputContact, inputEmail, inputPrePWD, inputNewPWD;

    FileChooser fileChooser;
    File filePath;

    public void init() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException, ClassNotFoundException, JsonProcessingException {
        borderAvatar();

        //GET INFORMATION FROM DB
        var tmp = new Account();
        tmp.account_id = 1;
        tmp.as_id = 1;
        System.out.println(tmp.getClass());

        var jsonString = BaseDao.Instance().getData(tmp,Account[].class, 1);
        
        for (Object object : jsonString) {
            var account = (Account) object;
            LinkedList<String> beInformation = new LinkedList<String>();
            beInformation.add(account.account_name);
            beInformation.add(account.account_dob);
            beInformation.add(account.account_email);
            beInformation.add(account.account_address);
            beInformation.add(account.account_contact);
            beInformation.add(account.account_image);

            beName.setText(beInformation.get(0));
            beAge.setText(beInformation.get(1));
            beAddress.setText(beInformation.get(2));
            beContact.setText(beInformation.get(3));
            beEmail.setText(beInformation.get(4));

            Image beProfile = new Image(beInformation.get(5));
            beAvatar.setImage(beProfile);
        }
        ObjectMapper mapper = new ObjectMapper();

        
        //CHANGE INFORMATION
        beInfoSubmitBtn.setOnMousePressed(mouseEvent -> {
            inputName = beName.getText();
            inputAge = beAge.getText();
            inputAddress = beAddress.getText();
            inputContact = beContact.getText();
            inputEmail = beEmail.getText();
            inputPrePWD = bePrePWD.getText();
            inputNewPWD = beNewPWD.getText();

            if (inputPrePWD.equals(inputNewPWD)) {
                beInfoResult.setText("This is your password. Please try a new one!");
            }

            Account account = new Account(1, inputName, inputAge, inputAddress, inputContact, inputEmail, inputNewPWD);
            BaseDao.Instance().update(account);
        });

        uploadBtn.setOnMousePressed(mouseEvent -> {
            //OPEN DIALOG TO GET IMAGE PATH
            Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();

            fileChooser = new FileChooser();
            fileChooser.setTitle("Upload Image");

            String userDirectoryString = System.getProperty("user.home") + "\\Desktop";
            File userDirectory = new File(userDirectoryString);

            if (!userDirectory.canRead()) {
                userDirectory = new File("c:/");
            }

            fileChooser.setInitialDirectory(userDirectory);
            filePath = fileChooser.showOpenDialog(stage);

            //UPDATE IMAGE PATH INTO DB
            try {
                var image = new Image(filePath.getPath());
                String imageToString = image.getUrl();
                System.out.println(imageToString);

                Account a = new Account(1, imageToString);
                String b = BaseDao.Instance().update(a);
                System.out.println(b);
            } catch (Exception e) {
            }
        });
    }

    void borderAvatar() {
        Circle clip = new Circle(112.5, 125.0, 112.5);
        beAvatar.setClip(clip);
    }
}
