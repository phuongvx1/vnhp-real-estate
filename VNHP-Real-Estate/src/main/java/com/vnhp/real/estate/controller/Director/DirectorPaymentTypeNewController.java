/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.PaymentType;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorPaymentTypeNewController{

     @FXML
    private TextField tfDirPMNewName;

    @FXML
    private Label tfDirPmNewResult;

    @FXML
    private Button btnDirPmNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirPmNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirPMNewName.getText();

            var data = new PaymentType(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirPmNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirPmNewResult, 2);
        });
    }
}
