/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;

/**
 *
 * @author FairyHunter
 */
public final class AccountLoginStatus {
    public Integer index;
    @ColumnId
    public Integer als_id;
    @ColumnName
    public String als_name;
    @ColumnStatus
    public Boolean als_status;
    public String btnChangePassword;
    public String btnUpdate;
    
    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }
    
    public void setAls_id(Integer als_id) {
        this.als_id = als_id;
    }

    public void setAls_name(String als_name) {
        this.als_name = als_name;
    }

    public void setAls_status(Boolean als_status) {
        this.als_status = als_status;
    }

    public Integer getAls_id() {
        return als_id;
    }

    public String getAls_name() {
        return als_name;
    }

    public Boolean getAls_status() {
        return als_status;
    }

    public AccountLoginStatus() {
    }

    public AccountLoginStatus(String als_name) {
        this.als_name = als_name;
    }

    public AccountLoginStatus(Integer als_id, String als_name) {
        this.als_id = als_id;
        this.als_name = als_name;
    }

    public AccountLoginStatus(Integer als_id, Boolean als_status) {
        this.als_id = als_id;
        this.als_status = als_status;
    }
       public AccountLoginStatus(Integer als_id, String als_name, Boolean als_status, Integer index) {
        this.index = index;
        this.als_id = als_id;
        this.als_name = als_name;
        this.als_status = als_status;
        this.btnChangePassword = "Change Status";
        this.btnUpdate = "Update";
    }
}
