/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class B_Status {

    public Integer index;
    @ColumnId
    public Integer bs_id;
    @ColumnName
    @DbColumn
    public String bs_name;
    @ColumnStatus
    @DbColumn
    public Boolean bs_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setBs_id(Integer bs_id) {
        this.bs_id = bs_id;
    }

    public void setBs_name(String bs_name) {
        this.bs_name = bs_name;
    }

    public void setBs_status(Boolean bs_status) {
        this.bs_status = bs_status;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getBs_id() {
        return bs_id;
    }

    public String getBs_name() {
        return bs_name;
    }

    public Boolean getBs_status() {
        return bs_status;
    }

    public Integer getIndex() {
        return index;
    }

    public B_Status() {
    }

    public B_Status(String bs_name) {
        this.bs_name = bs_name;
    }

    public B_Status(Integer bs_id, String bs_name) {
        this.bs_id = bs_id;
        this.bs_name = bs_name;
    }

    public B_Status(Integer bs_id, Boolean bs_status) {
        this.bs_id = bs_id;
        this.bs_status = bs_status;
    }

    public B_Status(Integer bs_id, String bs_name, Boolean bs_status, Integer index) {
        this.index = index;
        this.bs_id = bs_id;
        this.bs_name = bs_name;
        this.bs_status = bs_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
