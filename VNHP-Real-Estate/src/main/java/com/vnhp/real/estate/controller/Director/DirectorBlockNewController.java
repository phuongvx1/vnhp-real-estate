/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.GroupBlocks;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import com.vnhp.real.estate.res.UtilitiesManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorBlockNewController {

    @FXML
    private TextField tfDirBlockNewName;

    @FXML
    private Label tfDirBlockNewResult;

    @FXML
    private ComboBox<String> cbDirBlockNewGb, cbDirBlockNewStatus;

    @FXML
    private Pane pDirBlockNewUtilities;

    @FXML
    private Button btnDirBlockNew;

    public void init(Stage stage) {
        setupDefault(stage);
        setup();
    }

    private void setupDefault(Stage stage) {
        pDirBlockNewUtilities.getChildren().clear();
        pDirBlockNewUtilities.getChildren().add(UtilitiesManager.Instance().Utilities(stage));

        var gb = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().GROUP_BLOCKS_ACTIVE, GroupBlocks[].class));
        cbDirBlockNewGb.getItems().addAll(gb);
        cbDirBlockNewGb.setValue((String) gb.get(0));

        var bs = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().B_STATUS_ACTIVE, B_Status[].class));
        cbDirBlockNewStatus.getItems().addAll(bs);
        cbDirBlockNewStatus.setValue((String) bs.get(0));
    }

    private void setup() {
        btnDirBlockNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirBlockNewName.getText();
            var utilities = UtilitiesManager.Utilities.toString().substring(1, UtilitiesManager.Utilities.toString().length() - 1);
            var gb_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new GroupBlocks(cbDirBlockNewGb.getValue()), GroupBlocks[].class, 1), ColumnId.class);
            var bs_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new B_Status(cbDirBlockNewStatus.getValue()), B_Status[].class, 1), ColumnId.class);

            var data = new Blocks(name, utilities, gb_id, bs_id);
            var retult = BaseDao.Instance().insert(data);
            tfDirBlockNewResult.setText(retult);
            UtilitiesManager.Utilities.clear();
            TimeManager.Instance().clearResult(eh, tfDirBlockNewResult, 2);
        });
    }
}
