/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.db;

import com.vnhp.real.estate.model.entities.Investor;
import com.vnhp.real.estate.res.StringValue;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

/**
 *
 * @author phongv
 */
public class Investor_jpa {
    private static Investor newData(ResultSet rs, Integer index) {
        var tmp = new Investor();
        try {
            tmp = new Investor(rs.getInt(StringValue.Instance().INVESTOR_ID),
                               rs.getString(StringValue.Instance().INVESTOR_LOGO),
                               rs.getString(StringValue.Instance().INVESTOR_NAME), index);

        } catch (SQLException e) {
        }
        return tmp;
    }
    
    public static LinkedList<Investor> get(){
        var ls = new LinkedList<Investor>();
        Connection conn = DbConnection.Instance().Connect();

        try {
            try (Statement sm = conn.createStatement()) {
                ResultSet rs = sm.executeQuery(StringValue.Instance().INVESTOR_ALL);
                
                var index = 1;
                while (rs.next()) {
                    var item = newData(rs, index);
                    index++;
                    ls.add(item);
                }
            }
            conn.close();
        } catch (SQLException e) {
            String err = e.getMessage();
            System.out.println(err);
        }
        return ls;
    }
    
    public static LinkedList<Investor> joinProduct(){
        var ls = new LinkedList<Investor>();
        Connection conn = DbConnection.Instance().Connect();

        try {
            try (Statement sm = conn.createStatement()) {
                ResultSet rs = sm.executeQuery(StringValue.Instance().INVESTOR_PROJECTS);
                
                var index = 1;
                while (rs.next()) {
                    var item = newData(rs, index);
                    index++;
                    ls.add(item);
                }
            }
            conn.close();
        } catch (SQLException e) {
            String err = e.getMessage();
            System.out.println(err);
        }
        return ls;
    }
}
