/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

import com.vnhp.real.estate.model.entities.Utilities;

/**
 *
 * @author FairyHunter
 */
public enum TableName {
    Account,
    AccountLoginStatus,
    AccountStatus,
    B_Status,
    Blocks,
    CancelContract,
    ConstructionUnit,
    ConstructionUnitStatus,
    ContractStatus,
    ContractType,
    Customer,
    CustomerStatus,
    CustomerType,
    Department,
    DepartmentStatus,
    DepositContract,
    Direction,
    Gb_Status,
    GroupBlocks,
    Investor,
    InvestorStatus,
    PaymentPeriod,
    PaymentPeriodStatus,
    PaymentType,
    Position,
    Product,
    ProductStatus,
    Project,
    ProjectStatus,
    ProjectType,
    Region,
    RegionStatus,
    SaleContract,
    TranferContract,
    Utilities
}
