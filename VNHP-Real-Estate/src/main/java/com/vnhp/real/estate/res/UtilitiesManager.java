/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.Utilities;
import java.util.LinkedList;
import javafx.stage.Stage;

/**
 *
 * @author FairyHunter
 */
public final class UtilitiesManager {
    public static LinkedList<String> Utilities = new LinkedList<>();

    private static UtilitiesManager instance;

    private UtilitiesManager() {

    }

    public static UtilitiesManager Instance() {
        if (instance == null) {
            instance = new UtilitiesManager();
        }

        return instance;
    }
    
    public MyGridPane Utilities(Stage stage){
        var data = ConvertObject.Instance().toLists(BaseDao.Instance().getListData(StringValue.Instance().UTILITIES, Utilities[].class), 5);
        var gridData = new MyGridPane();

        gridData.createComponent(stage, data, TableName.Utilities);
        
        return gridData;
    }
}
