/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.StringValue;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author FairyHunter
 */
public final class PaymentPeriod {

    public Integer pp_id;
    public String pp_code;
    public String pp_time;
    public Float pp_payment;
    public Integer constract_id;
    public Short pp_print_count;
    public Integer pm_id;
    public String pm_name;
    public Integer pps_id;
    public String pps_name;
    public Integer sc_id;
    public String product_name;
    public String sc_total_payment;
    public Button button;
    public Label sttLabel;

    public int dc_status;

    public PaymentPeriod(Integer pp_id, Integer sc_id, String product_name, String sc_total_payment, Float pp_payment, String pp_time, Integer pps_id, int dc_status) {
        this.pp_id = pp_id;
        this.sc_id = sc_id;
        this.product_name = product_name;
        this.sc_total_payment = sc_total_payment;
        this.pp_payment = pp_payment;
        this.pp_time = pp_time;
        this.pps_id = pps_id;
        this.dc_status = dc_status;
    }

    public PaymentPeriod(Integer sc_id, String product_name, String sc_total_payment, Float pp_payment, String pp_time, Integer pps_id, int dc_status, Button button) throws FileNotFoundException {
        this.sc_id = sc_id;
        this.product_name = product_name;
        this.sc_total_payment = sc_total_payment;
        this.pp_payment = pp_payment;
        this.pp_time = pp_time;
        this.pps_id = pps_id;
        this.dc_status = dc_status;
        this.button = button;
        ImageView imageView = new ImageView(new Image(new FileInputStream(StringValue.Instance().ASSET+"printer.png")));
        if (this.dc_status ==0){this.button.setDisable(true);}
        if (this.pps_id == 2) {
            this.sttLabel = new Label("Paid");
            this.sttLabel.setPrefWidth(70);
            this.sttLabel.setPrefHeight(20);
            this.sttLabel.setAlignment(Pos.CENTER);
            this.sttLabel.setStyle("-fx-background-color: #007bff; -fx-text-fill: white;");
            this.button.setText("Export");
            this.button.setStyle("-fx-background-color: #28a745; -fx-text-fill: white;");
            this.button.setDisable(true);
            this.button.setGraphic(imageView);
        } else {
            this.sttLabel = new Label("Unpaid");
            this.sttLabel.setPrefWidth(70);
            this.sttLabel.setPrefHeight(20);
            this.sttLabel.setAlignment(Pos.CENTER);
            this.sttLabel.setStyle("-fx-background-color: #dc3545; -fx-text-fill: white;");
            this.button.setText("Export");
            this.button.setStyle("-fx-background-color: #28a745; -fx-text-fill: white;");
            this.button.setGraphic(imageView);
        }
    }

    public int getDc_status() {
        return dc_status;
    }

    public void setDc_status(int dc_status) {
        this.dc_status = dc_status;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }
    
    public Label getSttLabel() {
        return sttLabel;
    }

    public void setSttLabel(Label sttLabel) {
        this.sttLabel = sttLabel;
    }

    public Integer getSc_id() {
        return sc_id;
    }

    public void setSc_id(Integer sc_id) {
        this.sc_id = sc_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getSc_total_payment() {
        return sc_total_payment;
    }

    public void setSc_total_payment(String sc_total_payment) {
        this.sc_total_payment = sc_total_payment;
    }

    public void setConstract_id(Integer constract_id) {
        this.constract_id = constract_id;
    }

    public void setPm_id(Integer pm_id) {
        this.pm_id = pm_id;
    }

    public void setPm_name(String pm_name) {
        this.pm_name = pm_name;
    }

    public void setPp_code(String pp_code) {
        this.pp_code = pp_code;
    }

    public void setPp_id(Integer pp_id) {
        this.pp_id = pp_id;
    }

    public void setPp_payment(Float pp_payment) {
        this.pp_payment = pp_payment;
    }

    public void setPp_print_count(Short pp_print_count) {
        this.pp_print_count = pp_print_count;
    }

    public void setPp_time(String pp_time) {
        this.pp_time = pp_time;
    }

    public void setPps_id(Integer pps_id) {
        this.pps_id = pps_id;
    }

    public void setPps_name(String pps_name) {
        this.pps_name = pps_name;
    }

    public Integer getConstract_id() {
        return constract_id;
    }

    public Integer getPm_id() {
        return pm_id;
    }

    public String getPm_name() {
        return pm_name;
    }

    public String getPp_code() {
        return pp_code;
    }

    public Integer getPp_id() {
        return pp_id;
    }

    public Float getPp_payment() {
        return pp_payment;
    }

    public Short getPp_print_count() {
        return pp_print_count;
    }

    public String getPp_time() {
        return pp_time;
    }

    public Integer getPps_id() {
        return pps_id;
    }

    public String getPps_name() {
        return pps_name;
    }

    public PaymentPeriod() {
    }

    public PaymentPeriod(String pp_code, String pp_time, Float pp_payment, Integer constract_id, Short pp_print_count, Integer pm_id) {
        this.pp_code = pp_code;
        this.pp_time = pp_time;
        this.pp_payment = pp_payment;
        this.constract_id = constract_id;
        this.pp_print_count = pp_print_count;
        this.pm_id = pm_id;
    }

    public PaymentPeriod(Integer pp_id, String pp_code, String pp_time, Float pp_payment, Integer constract_id, Short pp_print_count, Integer pm_id) {
        this.pp_id = pp_id;
        this.pp_code = pp_code;
        this.pp_time = pp_time;
        this.pp_payment = pp_payment;
        this.constract_id = constract_id;
        this.pp_print_count = pp_print_count;
        this.pm_id = pm_id;
    }
    public PaymentPeriod(Integer sc_id, Integer pp_id, String pp_time){
        this.sc_id = sc_id;
        this.pp_id = pp_id;
        this.pp_time = pp_time;
    }

    public PaymentPeriod(Integer pp_id, Integer pps_id) {
        this.pp_id = pp_id;
        this.pps_id = pps_id;
    }

    public PaymentPeriod(Integer pp_id, String pp_code, String pp_time, Float pp_payment, Integer constract_id, Short pp_print_count, Integer pm_id, String pm_name, Integer pps_id, String pps_name) {
        this.pp_id = pp_id;
        this.pp_code = pp_code;
        this.pp_time = pp_time;
        this.pp_payment = pp_payment;
        this.constract_id = constract_id;
        this.pp_print_count = pp_print_count;
        this.pm_id = pm_id;
        this.pm_name = pm_name;
        this.pps_id = pps_id;
        this.pps_name = pps_name;
    }
}
