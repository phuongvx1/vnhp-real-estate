package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import java.util.Objects;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class AllCustomerChangeStatusController {

    @FXML
    private Label ccsName;
    @FXML
    private ComboBox<String> ccsSelect;
    @FXML
    public Label ccsResult;
    @FXML
    private Button ccsSubmitBtn;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, Customer data) {
        setupDefault(data);

        setup(grid, gridIndex, tableSetting, data);
    }

    private void setupDefault(Customer data) {
        ccsName.setText(data.getCustomer_name());

        var cs_Status = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CUSTOMER_STATUS_ACTIVE, CustomerStatus[].class));
        ccsSelect.getItems().addAll(cs_Status);
        ccsSelect.setValue(data.getCs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Customer data) {
        ccsSubmitBtn.addEventHandler(ActionEvent.ACTION, eh -> {
            var cs_status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new CustomerStatus(ccsSelect.getValue()), CustomerStatus[].class, 1), ColumnId.class);
            var tmpData = new Customer(data.getCustomer_id(), cs_status);
            var result = BaseDao.Instance().update(tmpData);
            ccsResult.setText(result);
            TimeManager.Instance().clearResult(eh, ccsResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Customer) data;
            tmp.setCs_name(ccsSelect.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.customer_id, tmp);
        });

    }

}
