/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.CustomerType;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorCtUpdateController {

      @FXML
    private TextField tfDirCTUpdateName;
    
    @FXML
    private Label tfDirCTUpdateResult;
    
    @FXML
    private Button btnDirCTUpdate;
    
    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, CustomerType data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }
    
    private void setupDefault(CustomerType data) {
        tfDirCTUpdateName.setText(data.getCt_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, CustomerType data) {
        btnDirCTUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirCTUpdateName.getText();
            
            var newData = new CustomerType(data.getCt_id(),name);
            var retult = BaseDao.Instance().update(newData);
            tfDirCTUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirCTUpdateResult, 2);
            
            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (CustomerType) data;
            tmp.setCt_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getCt_id(), tmp);
        });
    }
}
