/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.ConstructionUnitStatus;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorCUSNewController {

    @FXML
    private TextField tfDirCUSNewName;

    @FXML
    private Label tfDirCUSNewResult;

    @FXML
    private Button btnDirCUSNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirCUSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirCUSNewName.getText();

            var data = new ConstructionUnitStatus(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirCUSNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirCUSNewResult, 2);
        });
    }

}
