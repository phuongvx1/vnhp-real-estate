/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class ContractType {

    public Integer index;
    @ColumnId
    public Integer cont_id;
    @ColumnName
    @DbColumn
    public String cont_name;
    @ColumnStatus
    @DbColumn
    public Boolean cont_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setCont_id(Integer cont_id) {
        this.cont_id = cont_id;
    }

    public void setCont_name(String cont_name) {
        this.cont_name = cont_name;
    }

    public void setCont_status(Boolean cont_status) {
        this.cont_status = cont_status;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getCont_id() {
        return cont_id;
    }

    public String getCont_name() {
        return cont_name;
    }

    public Boolean getCont_status() {
        return cont_status;
    }

    public Integer getIndex() {
        return index;
    }

    public ContractType() {
    }

    public ContractType(String cont_name) {
        this.cont_name = cont_name;
    }

    public ContractType(Integer cont_id, String cont_name) {
        this.cont_id = cont_id;
        this.cont_name = cont_name;
    }

    public ContractType(Integer cont_id, Boolean cont_status) {
        this.cont_id = cont_id;
        this.cont_status = cont_status;
    }

    public ContractType(Integer cont_id, String cont_name, Boolean cont_status, Integer index) {
        this.index = index;
        this.cont_id = cont_id;
        this.cont_name = cont_name;
        this.cont_status = cont_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
