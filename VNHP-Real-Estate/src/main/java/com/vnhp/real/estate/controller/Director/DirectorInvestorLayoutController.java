/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorInvestorLayoutController {

    @FXML
    private Button btnDirInvestorNew, btnDirInvestorView, btnDirISNew, btnDirISView, btnDirPMNew, btnDirPMView;

    @FXML
    private Pane pDirInvestorNew;

    public void init() {
//        setupDefault();
        setup();
    }

    private void setupDefault() {
        try {
            var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_INVESTOR_NEW));
            AnchorPane dashboard = dataNew.load();
            pDirInvestorNew.getChildren().clear();
            pDirInvestorNew.getChildren().add(dashboard);
            var control = (DirectorInvestorNewController) dataNew.getController();
            control.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setup() {
        btnDirInvestorNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_INVESTOR_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirInvestorNew.getChildren().clear();
                pDirInvestorNew.getChildren().add(dashboard);
                var control = (DirectorInvestorNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirInvestorView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_INVESTOR_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirInvestorNew.getChildren().clear();
                pDirInvestorNew.getChildren().add(dashboard);
                var control = (DirectorInvestorViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirISNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_IS_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirInvestorNew.getChildren().clear();
                pDirInvestorNew.getChildren().add(dashboard);
                var control = (DirectorISNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirISView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_IS_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirInvestorNew.getChildren().clear();
                pDirInvestorNew.getChildren().add(dashboard);
                var control = (DirectorISViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPMNew.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PM_NEW));
                AnchorPane dashboard = dataNew.load();
                pDirInvestorNew.getChildren().clear();
                pDirInvestorNew.getChildren().add(dashboard);
                var control = (DirectorPaymentTypeNewController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnDirPMView.addEventHandler(ActionEvent.ACTION, eh -> {
            try {
                var dataView = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_PM_VIEW));
                AnchorPane dashboard = dataView.load();
                pDirInvestorNew.getChildren().clear();
                pDirInvestorNew.getChildren().add(dashboard);
                var control = (DirectorPaymentTypeViewController) dataView.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
