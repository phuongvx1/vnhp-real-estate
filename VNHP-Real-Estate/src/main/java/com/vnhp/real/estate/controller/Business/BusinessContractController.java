package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class BusinessContractController {

    @FXML
    private Button newSaleContractBtn, allSaleContractBtn, newTransferContractBtn, newDepositContractBtn, newCancelContractBtn;

    @FXML
    private ScrollPane businessContractBorder;

    public void init(Stage stage) {
//        FXMLLoader defaultLoad = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_SALE_CONTRACT));
//        try {
//            Pane businessSaleContract = defaultLoad.load();
//            businessContractBorder.setContent(businessSaleContract);
//
//            var control = (BusinessSaleContractController) defaultLoad.getController();
//            control.init(stage);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        newSaleContractBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_SALE_CONTRACT));

            try {
                Pane businessSaleContract = loader.load();
                businessContractBorder.setContent(businessSaleContract);
                businessContractBorder.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
                businessContractBorder.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

                var control = (BusinessSaleContractController) loader.getController();
                control.init(stage);

            } catch (IOException e) {
            }
        });

        allSaleContractBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_ALL_SALE_CONTRACT));

            try {
                Pane businessTransferContract = loader.load();
                businessContractBorder.setContent(businessTransferContract);
                businessContractBorder.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
                businessContractBorder.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

                var control = (AllSaleContractController) loader.getController();
                control.init(stage);

            } catch (IOException e) {
            }
        });
        
        newTransferContractBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_TRANSFER_CONTRACT));

            try {
                Pane businessTransferContract = loader.load();
                businessContractBorder.setContent(businessTransferContract);
                businessContractBorder.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
                businessContractBorder.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

                var control = (BusinessTransferContractController) loader.getController();
                control.init(stage);

            } catch (IOException e) {
            }
        });
        
        newDepositContractBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_DEPOSIT_CONTRACT));

            try {
                Pane businessDepositContract = loader.load();
                businessContractBorder.setContent(businessDepositContract);
                businessContractBorder.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
                businessContractBorder.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

                var control = (BusinessDepositContractController) loader.getController();
                control.init(stage);

            } catch (IOException e) {
            }
        });
        
        newCancelContractBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_CANCEL_CONTRACT));

            try {
                Pane businessCancelContract = loader.load();
                businessContractBorder.setContent(businessCancelContract);
                businessContractBorder.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
                businessContractBorder.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

                var control = (BusinessCancelContractController) loader.getController();
                control.init(stage);

            } catch (IOException e) {
            }
        });
    }

}
