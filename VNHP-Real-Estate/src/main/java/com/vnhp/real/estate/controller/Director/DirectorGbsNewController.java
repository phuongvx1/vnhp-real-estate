/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Gb_Status;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorGbsNewController {

    @FXML
    private TextField tfDirGbsNewName;

    @FXML
    private Label tfDirGbsNewResult;

    @FXML
    private Button btnDirGbsNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirGbsNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirGbsNewName.getText();

            var data = new Gb_Status(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirGbsNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirGbsNewResult, 2);
        });
    }

}
