/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.*;
import com.vnhp.real.estate.res.*;

/**
 *
 * @author FairyHunter
 */
public final class SaleContract {

    public Integer index;
    @ColumnId
    @DbColumn
    public Integer sc_id;
    @DbColumn
    public String sc_time_open;
    @DbColumn
    public Integer sc_number;
    public Float sc_deposit;
    @DbColumn
    public Float sc_total_payment;
    public Integer saler_id;
    @DbColumn
    public String saler_name;
    @DbColumn
    public Integer payer_id;
    public String payer_name;
    @DbColumn
    public String product_name;
    public Integer pm_id;
    public String pm_name;
    @DbColumn
    public String cont_name;
    @DbColumn
    public String cons_name;
    @DbColumn
    public Integer product_id;
    public Integer cont_id;
    @ColumnStatus
    public Integer cons_id;
    public Integer company_id;
    public String btnChangeStatus;
    public String btnUpdate;
    public String btnTransfer;

    public SaleContract(String sc_time_open) {
        this.sc_time_open = sc_time_open;
    }

    public SaleContract(String sc_time_open, Integer sc_number, Float sc_total_payment, Integer saler_id, Integer payer_id, Integer product_id, Integer pm_id, Integer cont_id, Integer cons_id, Integer company_id) {
        this.sc_time_open = sc_time_open;
        this.sc_number = sc_number;
        this.sc_total_payment = sc_total_payment;
        this.saler_id = saler_id;
        this.payer_id = payer_id;
        this.product_id = product_id;
        this.pm_id = pm_id;
        this.cont_id = cont_id;
        this.cons_id = cons_id;
        this.company_id = company_id;
    }

    public SaleContract(Integer sc_id, String sc_time_open, Integer sc_number, Float sc_total_payment, String saler_name, Integer payer_id, String product_name, String cont_name, String cons_name, Integer product_id, Integer cont_id, Integer cons_id, Integer saler_id, Integer index) {
        this.sc_id = sc_id;
        this.sc_time_open = sc_time_open;
        this.sc_number = sc_number;
        this.sc_total_payment = sc_total_payment;
        this.saler_id = saler_id;
        this.saler_name = saler_name;
        this.payer_id = payer_id;
        this.product_name = product_name;
        this.cont_name = cont_name;
        this.cons_name = cons_name;
        this.product_id = product_id;
        this.cont_id = cont_id;
        this.cons_id = cons_id;
        this.btnChangeStatus = "Change Status";
        this.btnUpdate = "Update";
        this.btnTransfer = "Transfer";
        this.index = index;
    }

    public SaleContract(Integer sc_id, String sc_time_open, Float sc_total_payment, String saler_name, String product_name, String payer_name, String cons_name, Integer cont_id) {
        this.sc_id = sc_id;
        this.sc_time_open = sc_time_open;
        this.sc_total_payment = sc_total_payment;
        this.saler_name = saler_name;
        this.product_name = product_name;
        this.payer_name = payer_name;
        this.cons_name = cons_name;
        this.cont_id = cont_id;
    }

    public void setCons_id(Integer cons_id) {
        this.cons_id = cons_id;
    }

    public void setCons_name(String cons_name) {
        this.cons_name = cons_name;
    }

    public void setCont_id(Integer cont_id) {
        this.cont_id = cont_id;
    }

    public void setCont_name(String cont_name) {
        this.cont_name = cont_name;
    }

    public void setPayer_id(Integer payer_id) {
        this.payer_id = payer_id;
    }

    public void setPayer_name1(String payer_name) {
        this.payer_name = payer_name;
    }

    public void setPm_id(Integer pm_id) {
        this.pm_id = pm_id;
    }

    public void setPm_name(String pm_name) {
        this.pm_name = pm_name;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public void setSaler_id(Integer saler_id) {
        this.saler_id = saler_id;
    }

    public void setSaler_name(String saler_name) {
        this.saler_name = saler_name;
    }

    public void setSc_deposit(Float sc_deposit) {
        this.sc_deposit = sc_deposit;
    }

    public void setSc_id(Integer sc_id) {
        this.sc_id = sc_id;
    }

    public void setSc_number(Integer sc_number) {
        this.sc_number = sc_number;
    }

    public void setSc_total_payment(Float sc_total_payment) {
        this.sc_total_payment = sc_total_payment;
    }

    public void setSc_time_open(String sc_time_open) {
        this.sc_time_open = sc_time_open;
    }

    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }

    public Integer getCons_id() {
        return cons_id;
    }

    public String getCons_name() {
        return cons_name;
    }

    public Integer getCont_id() {
        return cont_id;
    }

    public String getCont_name() {
        return cont_name;
    }

    public Integer getPayer_id() {
        return payer_id;
    }

    public String getPayer_name() {
        return payer_name;
    }

    public Integer getPm_id() {
        return pm_id;
    }

    public String getPm_name() {
        return pm_name;
    }

    public Integer getProduct_id() {
        return product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public Integer getSaler_id() {
        return saler_id;
    }

    public String getSaler_name() {
        return saler_name;
    }

    public Float getSc_deposit() {
        return sc_deposit;
    }

    public Integer getSc_id() {
        return sc_id;
    }

    public Integer getSc_number() {
        return sc_number;
    }

    public Float getSc_total_payment() {
        return sc_total_payment;
    }

    public String getSc_time_open() {
        return sc_time_open;
    }

    public Integer getCompany_id() {
        return company_id;
    }

    public SaleContract() {
    }

    public SaleContract(String sc_time_open, Integer sc_number, Float sc_deposit, Float sc_total_payment, Integer saler_id, Integer payer_id, Integer product_id, Integer pm_id, Integer cont_id, Integer cons_id) {
        this.sc_time_open = sc_time_open;
        this.sc_number = sc_number;
        this.sc_deposit = sc_deposit;
        this.sc_total_payment = sc_total_payment;
        this.saler_id = saler_id;
        this.payer_id = payer_id;
        this.product_id = product_id;
        this.pm_id = pm_id;
        this.cont_id = cont_id;
        this.cons_id = cons_id;
    }

    public SaleContract(Integer sc_id, String sc_time_open, Integer sc_number, Float sc_deposit, Float sc_total_payment, String saler_name, String payer_name, String product_name, String pm_name, String cont_name, String cons_name) {
        this.sc_id = sc_id;
        this.sc_time_open = sc_time_open;
        this.sc_number = sc_number;
        this.sc_deposit = sc_deposit;
        this.sc_total_payment = sc_total_payment;
        this.saler_name = saler_name;
        this.payer_name = payer_name;
        this.product_name = product_name;
        this.pm_name = pm_name;
        this.cont_name = cont_name;
        this.cons_name = cons_name;
    }
}
