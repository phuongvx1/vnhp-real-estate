/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class ProjectStatus {

    public Integer index;
    @ColumnId
    public Integer pjs_id;
    @ColumnName
    @DbColumn
    public String pjs_name;
    @ColumnStatus
    @DbColumn
    public Boolean pjs_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setPs_id(Integer ps_id) {
        this.pjs_id = ps_id;
    }

    public void setPs_name(String ps_name) {
        this.pjs_name = ps_name;
    }

    public void setPs_status(Boolean ps_status) {
        this.pjs_status = ps_status;
    }

    public Integer getPs_id() {
        return pjs_id;
    }

    public String getPs_name() {
        return pjs_name;
    }

    public Boolean getPs_status() {
        return pjs_status;
    }

    public ProjectStatus() {
    }

    public ProjectStatus(String ps_name) {
        this.pjs_name = ps_name;
    }

    public ProjectStatus(Integer ps_id, String ps_name) {
        this.pjs_id = ps_id;
        this.pjs_name = ps_name;
    }

    public ProjectStatus(Integer ps_id, Boolean ps_status) {
        this.pjs_id = ps_id;
        this.pjs_status = ps_status;
    }

    public ProjectStatus(Integer pjs_id, String pjs_name, Boolean pjs_status, Integer index) {
        this.index = index;
        this.pjs_id = pjs_id;
        this.pjs_name = pjs_name;
        this.pjs_status = pjs_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
