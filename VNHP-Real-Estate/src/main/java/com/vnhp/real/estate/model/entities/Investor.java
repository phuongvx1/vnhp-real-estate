/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.*;

/**
 *
 * @author FairyHunter
 */
public final class Investor {

    public Integer index;
    @ColumnId
    public Integer investor_id;
    @ColumnName
    @DbColumn
    public String investor_name;
    @ColumnLogo
    @DbColumn
    public String investor_logo;
    @DbColumn
    public Short investor_upcoming_project_quantity;
    @DbColumn
    public Short investor_open_project_quantity;
    @DbColumn
    public Short investor_done_project_quantity;
    @DbColumn
    public String investor_contact;
    @DbColumn
    public String investor_email;
    @DbColumn
    public String investor_address;
    @DbColumn
    public String investor_info;
    @ColumnStatus
    public Integer is_id;
    @DbColumn
    public String is_name;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setInvestor_address(String investor_address) {
        this.investor_address = investor_address;
    }

    public void setInvestor_contact(String investor_contact) {
        this.investor_contact = investor_contact;
    }

    public void setInvestor_done_project_quatity(Short investor_done_project_quantity) {
        this.investor_done_project_quantity = investor_done_project_quantity;
    }

    public void setInvestor_email(String investor_email) {
        this.investor_email = investor_email;
    }

    public void setInvestor_id(Integer investor_id) {
        this.investor_id = investor_id;
    }

    public void setInvestor_info(String investor_info) {
        this.investor_info = investor_info;
    }

    public void setInvestor_logo(String investor_logo) {
        this.investor_logo = investor_logo;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public void setInvestor_upcoming_project_quantity(Short investor_upcoming_project_quantity) {
        this.investor_upcoming_project_quantity = investor_upcoming_project_quantity;
    }

    public void setInvestor_open_project_quantity(Short investor_open_project_quantity) {
        this.investor_open_project_quantity = investor_open_project_quantity;
    }

    public void setInvestor_done_project_quantity(Short investor_done_project_quantity) {
        this.investor_done_project_quantity = investor_done_project_quantity;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setIs_id(Integer is_id) {
        this.is_id = is_id;
    }

    public void setIs_name(String is_name) {
        this.is_name = is_name;
    }

    public String getInvestor_address() {
        return investor_address;
    }

    public String getInvestor_contact() {
        return investor_contact;
    }

    public Short getInvestor_done_project_quantity() {
        return investor_done_project_quantity;
    }

    public String getInvestor_email() {
        return investor_email;
    }

    public Integer getInvestor_id() {
        return investor_id;
    }

    public String getInvestor_info() {
        return investor_info;
    }

    public String getInvestor_logo() {
        return investor_logo;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public Short getInvestor_open_project_quantity() {
        return investor_open_project_quantity;
    }

    public Integer getInvestor_status() {
        return is_id;
    }

    public Short getInvestor_upcoming_project_quantity() {
        return investor_upcoming_project_quantity;
    }

    public String getIs_name() {
        return is_name;
    }

    public Integer getIndex() {
        return index;
    }

    public Integer getIs_id() {
        return is_id;
    }

    public Investor() {
    }

    public Investor(String investor_name, String investor_logo, Short investor_upcoming_project_quantity, Short investor_open_project_quantity, Short investor_done_project_quantity, String investor_contact, String investor_email, String investor_address, String investor_info, Integer is_id, Integer index) {
        this.investor_name = investor_name;
        this.investor_logo = investor_logo;
        this.investor_upcoming_project_quantity = investor_upcoming_project_quantity;
        this.investor_open_project_quantity = investor_open_project_quantity;
        this.investor_done_project_quantity = investor_done_project_quantity;
        this.investor_contact = investor_contact;
        this.investor_email = investor_email;
        this.investor_address = investor_address;
        this.investor_info = investor_info;
        this.is_id = is_id;
        this.index = index;
    }

    public Investor(Integer investor_id, Integer is_id) {
        this.investor_id = investor_id;
        this.is_id = is_id;
    }

    public Investor(String investor_name) {
        this.investor_name = investor_name;
    }

    public Investor(String investor_name, String investor_logo, String investor_contact, String investor_email, String investor_address, String investor_info, Integer is_id) {
        this.investor_name = investor_name;
        this.investor_logo = investor_logo;
        this.investor_contact = investor_contact;
        this.investor_email = investor_email;
        this.investor_address = investor_address;
        this.investor_info = investor_info;
        this.is_id = is_id;
    }

    public Investor(Integer investor_id, String investor_name, String investor_logo, String investor_contact, String investor_email, String investor_address, String investor_info) {
        this.investor_id = investor_id;
        this.investor_name = investor_name;
        this.investor_logo = investor_logo;
        this.investor_contact = investor_contact;
        this.investor_email = investor_email;
        this.investor_address = investor_address;
        this.investor_info = investor_info;
    }

    public Investor(Integer investor_id, String investor_name, Short investor_upcoming_project_quantity, Short investor_open_project_quantity, Short investor_done_project_quantity) {
        this.investor_id = investor_id;
        this.investor_upcoming_project_quantity = investor_upcoming_project_quantity;
        this.investor_open_project_quantity = investor_open_project_quantity;
        this.investor_done_project_quantity = investor_done_project_quantity;
    }

    public Investor(Integer investor_id, String investor_logo, String investor_name, Integer index) {
        this.investor_id = investor_id;
        this.investor_name = investor_name;
        this.investor_logo = investor_logo;
        this.index = index;
    }

    public Investor(String investor_logo, String investor_name, Integer index) {
        this.investor_name = investor_name;
        this.investor_logo = investor_logo;
        this.index = index;
    }

    public Investor(Integer investor_id, String investor_name, String investor_logo, Short investor_upcoming_project_quantity, Short investor_open_project_quantity, Short investor_done_project_quantity, String investor_contact, String investor_email, String investor_address, String is_name, Integer index) {
        this.index = index;
        this.investor_id = investor_id;
        this.investor_name = investor_name;
        this.investor_logo = investor_logo;
        this.investor_upcoming_project_quantity = investor_upcoming_project_quantity;
        this.investor_open_project_quantity = investor_open_project_quantity;
        this.investor_done_project_quantity = investor_done_project_quantity;
        this.investor_contact = investor_contact;
        this.investor_email = investor_email;
        this.investor_address = investor_address;
        this.is_name = is_name;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
