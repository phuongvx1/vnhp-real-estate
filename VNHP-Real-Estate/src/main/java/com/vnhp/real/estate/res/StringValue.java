/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

/**
 *
 * @author FairyHunter
 */
public final class StringValue {

    private static StringValue instance;

    private StringValue() {

    }

    public static StringValue Instance() {
        if (instance == null) {
            instance = new StringValue();
        }

        return instance;
    }

    //ASSETS
    public final String IMAGE = "/com/vnhp/real/estate/assets/";
    public final String ASSET = "/Aptech/java-work/VNHP-Real-Estate/VNHP-Real-Estate/src/main/resources/com/vnhp/real/estate/assets/";
    public final String ASSET_INVESTOR = "/Aptech/java-work/VNHP-Real-Estate/VNHP-Real-Estate/src/main/resources/com/vnhp/real/estate/assets/business/investor/";
    public final String ASSET_BUSINESS = "/com/vnhp/real/estate/assets/business/";
    public final String ASSET_BUSINESS_INVESTOR = "/com/vnhp/real/estate/assets/business/investor/";
    public final String CHANGE_STATUS = ASSET + "status-change.png";
    public final String UPDATE_HAND = ASSET + "update-hand.png";
    public final String TRANSFER = ASSET + "transferIcon.png";

    //CHART
    public final String PRODUCT_ICON = "product.png";
    public final String PROJECT_ICON = "project.png";
    public final String REVENUE_ICON = "revenue.png";
    public final String CUSTOMER_ICON = "customer.png";
    public final String STAFF_ICON = "staff.png";
    public final String CURRENT_MONTH = "Current Month";
    public final String LAST_3_MONTHS = "Last 3 Months";
    public final String LAST_6_MONTHS = "Last 6 Months";
    public final String CURRENT_YEAR = "Current Year";
    public final String LAST_3_YEARS = "Last 3 Years";
    public final String LAST_5_YEARS = "Last 5 Years";
    public final String LAST_10_YEARS = "Last 10 Years";

    //VIEW
    private final String VIEW = "/com/vnhp/real/estate/view/";
    private final String VIEW_DIRECTOR = "/com/vnhp/real/estate/view/director/";
    private final String VIEW_BUSINESS = "/com/vnhp/real/estate/view/business/";
    private final String VIEW_HUMAN = "/com/vnhp/real/estate/view/human/";
    private final String VIEW_REPORT = "C:/Aptech/java-work/vnhp-real-estate/VNHP-Real-Estate/src/main/resources/com/vnhp/real/estate/iReport/";

    //LOGIN
    public final String LOGIN_PAGE = VIEW + "LoginPage.fxml";

    //REPORT
    public final String DEPOSITRECEIPT = VIEW_REPORT + "DepositReceipt.jrxml";
    public final String SALERECEIPT = VIEW_REPORT + "SaleReceipt.jrxml";
    public final String CREDITRECEIPT = VIEW_REPORT + "CreditReceipt.jrxml";

    //ACCOUNTANT
    public final String ACCOUNTANT_LAYOUT_PAGE = VIEW + "AccountantLayout.fxml";
    public final String DASHBOARD_PAGE = VIEW + "Dashboard.fxml";
    public final String SALARY = VIEW + "accountant/Salary.fxml";
    public final String MAINTANANCE = VIEW + "accountant/Maintanance.fxml";
    public final String INVOICE = VIEW + "accountant/Invoice.fxml";
    public final String REVENUE = VIEW + "accountant/Revenue.fxml";
    public final String NOTIPANE = VIEW + "accountant/NotificationPane.fxml";
    public final String DEPOSITCONSTRACT = VIEW + "accountant/DepositConstract.fxml";
    public final String SALECONSTRACT = VIEW + "accountant/SaleConstract.fxml";
    public final String CREDITCONSTRACT = VIEW + "accountant/CreditConstract.fxml";
    public final String LOGOUTPANE = VIEW + "accountant/LogoutPane.fxml";

    //BUSINESS
    public final String BUSINESS_LAYOUT_PAGE = VIEW_BUSINESS + "BusinessLayout.fxml";
    public final String BE_INFORMATION = VIEW_BUSINESS + "BEInformation.fxml";
    /////BUSINESS-PRODUCT
    public final String BUSINESS_PRODUCT = VIEW_BUSINESS + "BusinessProduct.fxml";
    public final String BUSINESS_ALL_PRODUCT = VIEW_BUSINESS + "AllProduct.fxml";
    public final String BUSINESS_INVESTOR = VIEW_BUSINESS + "Investor.fxml";
    public final String BI_INFOMATION = VIEW_BUSINESS + "InvestorInformation.fxml";
    public final String BUSINESS_PROJECT = VIEW_BUSINESS + "Project.fxml";
    /////BUSINESS-CUSTOMER
    public final String BUSINESS_CUSTOMER = VIEW_BUSINESS + "BusinessCustomer.fxml";
    public final String BUSINESS_ALL_CUSTOMER = VIEW_BUSINESS + "AllCustomer.fxml";
    public final String BUSINESS_ADD_CUSTOMER = VIEW_BUSINESS + "AddCustomer.fxml";
    public final String BUSINESS_ALL_CHANGESTT = VIEW_BUSINESS + "AllCustomerChangeStatus.fxml";
    /////BUSINESS-CONTRACT
    public final String BUSINESS_CONTRACT = VIEW_BUSINESS + "BusinessContract.fxml";
    public final String BUSINESS_ALL_SALE_CONTRACT = VIEW_BUSINESS + "AllSaleContract.fxml";
    public final String BUSINESS_SALE_CONTRACT = VIEW_BUSINESS + "BusinessSaleContract.fxml";
    public final String BUSINESS_TRANSFER_CONTRACT = VIEW_BUSINESS + "BusinessTransferContract.fxml";
    public final String BUSINESS_DEPOSIT_CONTRACT = VIEW_BUSINESS + "BusinessDepositContract.fxml";
    public final String BUSINESS_CANCEL_CONTRACT = VIEW_BUSINESS + "BusinessCancelContract.fxml";
    public final String BUSINESS_HIRE_CONTRACT = VIEW_BUSINESS + "BusinessHireContract.fxml";

    //HUMAN RESOURCE
    public final String HUMAN_RESOURCE_LAYOUT = VIEW_HUMAN + "HRLayout.fxml";
    public final String HUMAN_RESOURCE_ADDNEW = VIEW_HUMAN + "HRAddNewStaff.fxml";
    public final String HUMAN_RESOUCE_ADDNEWLAYOUT = VIEW_HUMAN + "HRAddNewStaffLayout.fxml";
    public final String HUMAN_RESOUCE_UPDATE = VIEW_HUMAN + "HRUpdateStaff.fxml";
    public final String HUMAN_RESOUCE_VIEW = VIEW_HUMAN + "HRViewStaff.fxml";
    public final String HUMAN_RESOUCE_NEWDEPTLAYOUT = VIEW_HUMAN + "HRAddNewDepartmentLayout.fxml";
    public final String HR_POSITION_LAYOUT = VIEW_HUMAN + "HRPositionLayout.fxml";
    public final String HR_POSITION_NEW = VIEW_HUMAN + "HRPositionNew.fxml";
    public final String HR_POSITION_VIEW = VIEW_HUMAN + "HRPositionView.fxml";
    public final String HUMAN_RESOUCE_UPDATEDEPTLAYOUT = VIEW_HUMAN + "HRUpdateDepartment.fxml";
    public final String HUMAN_RESOUCE_VIEWDEPTLAYOUT = VIEW_HUMAN + "HRViewDepartment.fxml";
    public final String HUMAN_RESOUCE_ADDNEWDEPT = VIEW_HUMAN + "HRAddNewDepartment.fxml";
    public final String HUMAN_RESOUCE_ACLOGINSTATUSNEW = VIEW_HUMAN + "HRAccountLoginStatusNew.fxml";
    public final String HUMAN_RESOUCE_ACLOGINSTATUSUPDATE = VIEW_HUMAN + "HRAccountLoginStatusUpdate.fxml";
    public final String HUMAN_RESOUCE_ACLOGINSTATUSVIEW = VIEW_HUMAN + "HRAccountLoginStatusView.fxml";
    public final String HUMAN_RESOUCE_ACSTATUSNEW = VIEW_HUMAN + "HRAccountStatusNew.fxml";
    public final String HUMAN_RESOUCE_ACSTATUSUPDATE = VIEW_HUMAN + "HRAccountStatusUpdate.fxml";
    public final String HR_ACCOUNT_STATUS_VIEW = VIEW_HUMAN + "HRAccountStatusView.fxml";
    public final String HUMAN_RESOUCE_DEPTSTATUSNEW = VIEW_HUMAN + "HRDepartmentStatusNew.fxml";
    public final String HUMAN_RESOUCE_DEPTSTATUSUPDATE = VIEW_HUMAN + "HRDepartmentStatusUpdate.fxml";
    public final String HUMAN_RESOUCE_DEPTSTATUSVIEW = VIEW_HUMAN + "HRDepartmentStatusView.fxml";
    public final String HR_CONTRACT = VIEW_HUMAN + "HRAddNewEmloyeeContract.fxml";
    //DIRECTOR
    public final String DIRECTOR_LAYOUT_PAGE = VIEW_DIRECTOR + "DirectorLayout.fxml";
    public final String DIRECTOR_DASHBOARD = VIEW_DIRECTOR + "DirectorDashboard.fxml";
    public final String DIRECTOR_CHART = VIEW_DIRECTOR + "DirectorChart.fxml";

    public final String DIRECTOR_ACCOUNT_LAYOUT = VIEW_DIRECTOR + "DirectorAccountLayout.fxml";
    public final String DIRECTOR_ACCOUNT_NEW = VIEW_DIRECTOR + "DirectorAccountNew.fxml";
    public final String DIRECTOR_ACCOUNT_UPDATE = VIEW_DIRECTOR + "DirectorAccountUpdate.fxml";
    public final String DIRECTOR_ACCOUNT_VIEW = VIEW_DIRECTOR + "DirectorAccountView.fxml";
    public final String DIRECTOR_ALS_NEW = VIEW_DIRECTOR + "DirectorAccountLoginStatusNew.fxml";
    public final String DIRECTOR_ALS_VIEW = VIEW_DIRECTOR + "DirectorAccountLoginStatusView.fxml";
    public final String DIRECTOR_ALS_UPDATE = VIEW_DIRECTOR + "DirectorAccountLoginStatusUpdate.fxml";
    public final String DIRECTOR_AS_NEW = VIEW_DIRECTOR + "DirectorAccountStatusNew.fxml";
    public final String DIRECTOR_AS_VIEW = VIEW_DIRECTOR + "DirectorAccountStatusView.fxml";
    public final String DIRECTOR_AS_UPDATE = VIEW_DIRECTOR + "DirectorAccountStatusUpdate.fxml";
    public final String DIRECTOR_DEPT_NEW = VIEW_DIRECTOR + "DirectorDepartmentNew.fxml";
    public final String DIRECTOR_DEPT_VIEW = VIEW_DIRECTOR + "DirectorDepartmentView.fxml";
    public final String DIRECTOR_DEPT_UPDATE = VIEW_DIRECTOR + "DirectorDepartmentUpdate.fxml";
    public final String DIRECTOR_DS_NEW = VIEW_DIRECTOR + "DirectorDepartmentStatusNew.fxml";
    public final String DIRECTOR_DS_VIEW = VIEW_DIRECTOR + "DirectorDepartmentStatusView.fxml";
    public final String DIRECTOR_DS_UPDATE = VIEW_DIRECTOR + "DirectorDepartmentStatusUpdate.fxml";

    public final String DIRECTOR_PRODUCT_LAYOUT = VIEW_DIRECTOR + "DirectorProductsLayout.fxml";
    public final String DIRECTOR_PRODUCT_NEW = VIEW_DIRECTOR + "DirectorProductNew.fxml";
    public final String DIRECTOR_PRODUCT_UPDATE = VIEW_DIRECTOR + "DirectorProductUpdate.fxml";
    public final String DIRECTOR_PRODUCT_CS = VIEW_DIRECTOR + "DirectorProductChangeStatus.fxml";
    public final String DIRECTOR_PRODUCT_VIEW = VIEW_DIRECTOR + "DirectorProductView.fxml";
    public final String DIRECTOR_PRODUCT_STATUS_NEW = VIEW_DIRECTOR + "DirectorProductStatusNew.fxml";
    public final String DIRECTOR_PRODUCT_STATUS_UPDATE = VIEW_DIRECTOR + "DirectorProductStatusUpdate.fxml";
    public final String DIRECTOR_PRODUCT_STATUS_VIEW = VIEW_DIRECTOR + "DirectorProductStatusView.fxml";
    public final String DIRECTOR_DIRECTION_NEW = VIEW_DIRECTOR + "DirectorDirectionNew.fxml";
    public final String DIRECTOR_DIRECTION_UPDATE = VIEW_DIRECTOR + "DirectorDirectionUpdate.fxml";
    public final String DIRECTOR_DIRECTION_VIEW = VIEW_DIRECTOR + "DirectorDirectionView.fxml";
    public final String DIRECTOR_CU_NEW = VIEW_DIRECTOR + "DirectorCUNew.fxml";
    public final String DIRECTOR_CU_UPDATE = VIEW_DIRECTOR + "DirectorCUUpdate.fxml";
    public final String DIRECTOR_CU_VIEW = VIEW_DIRECTOR + "DirectorCUView.fxml";
    public final String DIRECTOR_CU_CS = VIEW_DIRECTOR + "DirectorCUChangeStatus.fxml";
    public final String DIRECTOR_CUS_NEW = VIEW_DIRECTOR + "DirectorCUSNew.fxml";
    public final String DIRECTOR_CUS_UPDATE = VIEW_DIRECTOR + "DirectorCUSUpdate.fxml";
    public final String DIRECTOR_CUS_VIEW = VIEW_DIRECTOR + "DirectorCUSView.fxml";
    public final String DIRECTOR_REGION_NEW = VIEW_DIRECTOR + "DirectorRegionNew.fxml";
    public final String DIRECTOR_REGION_UPDATE = VIEW_DIRECTOR + "DirectorRegionUpdate.fxml";
    public final String DIRECTOR_REGION_VIEW = VIEW_DIRECTOR + "DirectorRegionView.fxml";
    public final String DIRECTOR_REGION_CS = VIEW_DIRECTOR + "DirectorRegionChangeStatus.fxml";
    public final String DIRECTOR_RS_NEW = VIEW_DIRECTOR + "DirectorRsNew.fxml";
    public final String DIRECTOR_RS_UPDATE = VIEW_DIRECTOR + "DirectorRsUpdate.fxml";
    public final String DIRECTOR_RS_VIEW = VIEW_DIRECTOR + "DirectorRsView.fxml";

    public final String DIRECTOR_PROJECT_LAYOUT = VIEW_DIRECTOR + "DirectorProjectLayout.fxml";
    public final String DIRECTOR_PROJECT_NEW = VIEW_DIRECTOR + "DirectorProjectNew.fxml";
    public final String DIRECTOR_PROJECT_UPDATE = VIEW_DIRECTOR + "DirectorProjectUpdate.fxml";
    public final String DIRECTOR_PROJECT_VIEW = VIEW_DIRECTOR + "DirectorProjectView.fxml";
    public final String DIRECTOR_PROJECT_CS = VIEW_DIRECTOR + "DirectorProjectChangeStatus.fxml";
    public final String DIRECTOR_PS_NEW = VIEW_DIRECTOR + "DirectorPSNew.fxml";
    public final String DIRECTOR_PS_UPDATE = VIEW_DIRECTOR + "DirectorPSUpdate.fxml";
    public final String DIRECTOR_PS_VIEW = VIEW_DIRECTOR + "DirectorPSView.fxml";
    public final String DIRECTOR_PT_NEW = VIEW_DIRECTOR + "DirectorPTNew.fxml";
    public final String DIRECTOR_PT_UPDATE = VIEW_DIRECTOR + "DirectorPTUpdate.fxml";
    public final String DIRECTOR_PT_VIEW = VIEW_DIRECTOR + "DirectorPTView.fxml";
    public final String DIRECTOR_UTILITIES_NEW = VIEW_DIRECTOR + "DirectorUtilitiesNew.fxml";
    public final String DIRECTOR_UTILITIES_UPDATE = VIEW_DIRECTOR + "DirectorUtilitiesUpdate.fxml";
    public final String DIRECTOR_UTILITIES_VIEW = VIEW_DIRECTOR + "DirectorUtilitiesView.fxml";
    public final String DIRECTOR_GB_NEW = VIEW_DIRECTOR + "DirectorGbNew.fxml";
    public final String DIRECTOR_GB_UPDATE = VIEW_DIRECTOR + "DirectorGbUpdate.fxml";
    public final String DIRECTOR_GB_VIEW = VIEW_DIRECTOR + "DirectorGbView.fxml";
    public final String DIRECTOR_GB_CS = VIEW_DIRECTOR + "DirectorGBChangeStatus.fxml";
    public final String DIRECTOR_GBS_NEW = VIEW_DIRECTOR + "DirectorGbsNew.fxml";
    public final String DIRECTOR_GBS_UPDATE = VIEW_DIRECTOR + "DirectorGbSUpdate.fxml";
    public final String DIRECTOR_GBS_VIEW = VIEW_DIRECTOR + "DirectorGbsView.fxml";
    public final String DIRECTOR_BLOCK_NEW = VIEW_DIRECTOR + "DirectorBlockNew.fxml";
    public final String DIRECTOR_BLOCK_UPDATE = VIEW_DIRECTOR + "DirectorBlockUpdate.fxml";
    public final String DIRECTOR_BLOCK_VIEW = VIEW_DIRECTOR + "DirectorBlockView.fxml";
    public final String DIRECTOR_BLOCK_CS = VIEW_DIRECTOR + "DirectorBlockChangeStatus.fxml";
    public final String DIRECTOR_BS_NEW = VIEW_DIRECTOR + "DirectorBsNew.fxml";
    public final String DIRECTOR_BS_UPDATE = VIEW_DIRECTOR + "DirectorBsUpdate.fxml";
    public final String DIRECTOR_BS_VIEW = VIEW_DIRECTOR + "DirectorBsView.fxml";

    public final String DIRECTOR_INVESTOR_LAYOUT = VIEW_DIRECTOR + "DirectorInvestorLayout.fxml";
    public final String DIRECTOR_INVESTOR_VIEW = VIEW_DIRECTOR + "DirectorInvestorView.fxml";
    public final String DIRECTOR_INVESTOR_NEW = VIEW_DIRECTOR + "DirectorInvestorNew.fxml";
    public final String DIRECTOR_INVESTOR_UPDATE = VIEW_DIRECTOR + "DirectorInvestorUpdate.fxml";
    public final String DIRECTOR_INVESTOR_CS = VIEW_DIRECTOR + "DirectorInvestorChangeStatus.fxml";
    public final String DIRECTOR_IS_VIEW = VIEW_DIRECTOR + "DirectorISView.fxml";
    public final String DIRECTOR_IS_NEW = VIEW_DIRECTOR + "DirectorISNew.fxml";
    public final String DIRECTOR_IS_UPDATE = VIEW_DIRECTOR + "DirectorISUpdate.fxml";
    public final String DIRECTOR_PM_VIEW = VIEW_DIRECTOR + "DirectorPaymentTypeView.fxml";
    public final String DIRECTOR_PM_NEW = VIEW_DIRECTOR + "DirectorPaymentTypeNew.fxml";
    public final String DIRECTOR_PM_UPDATE = VIEW_DIRECTOR + "DirectorPaymentTypeUpdate.fxml";

    public final String DIRECTOR_CONTRACT_LAYOUT = VIEW_DIRECTOR + "DirectorContractLayout.fxml";
    public final String DIRECTOR_CONS_NEW = VIEW_DIRECTOR + "DirectorConsNew.fxml";
    public final String DIRECTOR_CONS_VIEW = VIEW_DIRECTOR + "DirectorConsView.fxml";
    public final String DIRECTOR_CONS_UPDATE = VIEW_DIRECTOR + "DirectorConsUpdate.fxml";
    public final String DIRECTOR_CONT_NEW = VIEW_DIRECTOR + "DirectorContNew.fxml";
    public final String DIRECTOR_CONT_VIEW = VIEW_DIRECTOR + "DirectorContView.fxml";
    public final String DIRECTOR_CONT_UPDATE = VIEW_DIRECTOR + "DirectorContUpdate.fxml";
    public final String DIRECTOR_CS_NEW = VIEW_DIRECTOR + "DirectorCsNew.fxml";
    public final String DIRECTOR_CS_VIEW = VIEW_DIRECTOR + "DirectorCsView.fxml";
    public final String DIRECTOR_CS_UPDATE = VIEW_DIRECTOR + "DirectorCsUpdate.fxml";
    public final String DIRECTOR_CT_NEW = VIEW_DIRECTOR + "DirectorCtNew.fxml";
    public final String DIRECTOR_CT_VIEW = VIEW_DIRECTOR + "DirectorCtView.fxml";
    public final String DIRECTOR_CT_UPDATE = VIEW_DIRECTOR + "DirectorCtUpdate.fxml";
    public final String DIRECTOR_PPS_NEW = VIEW_DIRECTOR + "DirectorPpsNew.fxml";
    public final String DIRECTOR_PPS_VIEW = VIEW_DIRECTOR + "DirectorPpsView.fxml";
    public final String DIRECTOR_PPS_UPDATE = VIEW_DIRECTOR + "DirectorPpsUpdate.fxml";

    public final String FAIL = "Fail";
    public final String SUCCESSFULL = "Successfully";
    public final String NO_ITEM_FOULD = "No items found";
    public final String BTN_CHANGE_STATUS = "Change Status";
    public final String BTN_UPDATE = "Update";
//    public String POSITION_ID = "position_id";
//    public String POSITION_NAME = "position_name";
//    public String POSITION_STATUS = "position_status";
//
//    public String AS_ID = "as_id";
//    public String AS_NAME = "as_name";
//    public String AS_STATUS = "as_status";
//
//    public String ALS_ID = "als_id";
//    public String ALS_NAME = "als_name";
//    public String ALS_STATUS = "als_status";
//
//    public String DS_ID = "ds_id";
//    public String DS_NAME = "ds_name";
//    public String DS_STATUS = "ds_status";
//
//    public String DEPARTMENT_ID = "department_id";
//    public String DEPARTMENT_NAME = "department_name";
//    public String DEPARTMENT_INFO = "department_info";
//
//    public String ACCOUNT_ID = "account_id";
//    public String ACCOUNT_NAME = "account_name";
//    public String ACCOUNT_EMAIL = "account_email";
//    public String ACCOUNT_PASSWORD = "account_password";
//    public String ACCOUNT_IMAGE = "account_image";
//    public String ACCOUNT_ADDRESS = "account_address";
//    public String ACCOUNT_CONTACT = "account_contact";
//
//    public String IS_ID = "is_id";
//    public String IS_NAME = "is_name";
//    public String IS_STATUS = "is_status";
    public final String INVESTOR_ID = "investor_id";
    public final String INVESTOR_LOGO = "investor_logo";
    public final String INVESTOR_NAME = "investor_name";
//    public String INVESTOR_UPCOMING_PROJECT_QUANTITY = "investor_upcoming_project_quantity";
//    public String INVESTOR_OPEN_PROJECT_QUANTITY = "investor_open_project_quantity";
//    public String INVESTOR_DONE_PROJECT_QUANTITY = "investor_done_project_quantity";
//    public String INVESTOR_CONTACT = "investor_contact";
//    public String INVESTOR_EMAIL = "investor_email";
//    public String INVESTOR_ADDRESS = "investor_address";
//    public String INVESTOR_REFERENCES = "investor_references";
//    public String INVESTOR_INFO = "investor_info";
//
//    public String CUS_ID = "cus_id";
//    public String CUS_NAME = "cus_name";
//    public String CUS_STATUS = "cus_status";
//
//    public String CU_ID = "cu_id";
//    public String CU_NAME = "cu_name";
//    public String CU_EMAIL = "email";
//    public String CU_ADDRESS = "cu_address";
//    public String CU_LOGO = "cu_logo";
//    public String CU_DONE_PROJECT_QUANTITY = "cu_done_project_quatity";
//    public String CU_CONTACT = "cu_contact";
//
//    public String PJS_ID = "pjs_id";
//    public String PJS_NAME = "pjs_name";
//    public String PJS_STATUS = "pjs_status";
//
//    public String PT_ID = "pt_id";
//    public String PT_NAME = "pt_name";
//    public String PT_STATUS = "pt_status";
//
//    public String PROJECT_ID = "project_id";
//    public String PROJECT_NAME = "project_name";
//    public String PROJECT_INFO = "project_info";
//    public String PROJECT_LOCATION = "project_location";
//    public String PROJECT_REFERENCES = "project_references";
//    public String PROJECT_SITE = "project_site";
//    public String PROJECT_AREA = "project_area";
//    public String PROJECT_JURICICAL = "project_juridical";
//    public String PROJECT_BUILDING_DENSITY = "project_density";
//    public String PROJECT_UTILITIES = "project_utilities";
//
//    public String RS_ID = "rs_id";
//    public String RS_NAME = "rs_name";
//    public String RS_STATUS = "rs_status";
//
//    public String DIRECTION_ID = "direction_id";
//    public String DIRECTION_NAME = "direction_name";
//    public String DIRECTION_STATUS = "direction_status";
//
//    public String REGION_ID = "region_id";
//    public String REGION_NAME = "region_name";
//    public String REGION_AREA = "region_name";
//
//    public String PS_ID = "ps_id";
//    public String PS_NAME = "ps_name";
//    public String PS_STATUS = "ps_status";
//
//    public String PRODUCT_ID = "product_id";
//    public String PRODUCT_NAME = "product_name";
//    public String PRODUCT_STATUS = "product_descrbe";
//    public String PRODUCT_PRICE = "product_price";
//
//    public String PI_ID = "pi_id";
//    public String PI_NAME = "pi_name";
//    public String PI_STATUS = "pi_status";
//
//    public String CS_ID = "cs_id";
//    public String CS_NAME = "cs_name";
//    public String CS_STATUS = "cs_status";
//
//    public String CT_ID = "ct_id";
//    public String CT_NAME = "ct_name";
//    public String CT_STATUS = "ct_status";
//
//    public String CUSTOMER_ID = "customer_id";
//    public String CUSTOMER_NAME = "customer_name";
//    public String CUSTOMER_STATUS = "customer_email";
//    public String CUSTOMER_CONTACT = "customer_contact";
//    public String CUSTOMER_ADDRESS = "customer_address";
//    public String CUSTOMER_IMAGE = "customer_image";
//
//    public String CON_ID = "con_id";
//    public String CON_NAME = "con_name";
//    public String CON_STATUS = "con_status";
//
//    public String CONS_ID = "cons_id";
//    public String CONS_NAME = "cons_name";
//    public String CONS_STATUS = "cons_status";
//
//    public String CONSTRACT_ID = "constract_id";
//    public String CONSTRACT_CODE = "constract_code";
//    public String CONSTRACT_TITLE = "constract_title";
//    public String CONSTRACT_DEPOSIT = "constract_deposit";
//    public String CONSTRACT_PAYMENT = "constract_payment";
//    public String CONSTRACT_TIME = "constract_time";
//    public String STAFF_ID = "staff_id";
//    public String MANAGER_ID = "manager_id";
//    public String CUSTOMER_TRANSFER_ID = "customer_transfer_id";
//    public String CONSTRACT_TRANFER_MONEY = "constract_tranfer_money";
//    public String CONSTRACT_REMAIN_MONEY = "constract_remain_money";
//
//    public String PPS_ID = "pps_id";
//    public String PPS_NAME = "pps_name";
//    public String PPS_STATUS = "pps_status";
//
//    public String PMT_ID = "pmt_id";
//    public String PMT_NAME = "pmt_name";
//    public String PMT_STATUS = "pmt_status";
//
//    public String PP_ID = "pp_id";
//    public String PP_CODE = "pp_code";
//    public String PP_TIME = "pp_time";
//    public String PP_PRINT_COUNT = "pp_print_count";
//
//    public String PPF_ID = "ppf_id";
//    public String PPF_STATUS = "ppf_status";
//
//    public String CF_ID = "cf_id";
//    public String CF_STATUS = "cf_status";

    //QUERY
    public final String ACCOUNT = "* from Account order by account_id desc";
    public final String ACCOUNT_ACTIVE = "* from Account where account_id = 1 order by account_id desc";
    public final String ACCOUNT_INFO = "* from Account a "
            + "join AccountLoginStatus als on(a.als_id = als.als_id) "
            + "join AccountStatus as on(a.as_id = as.as_id) "
            + "join Department d on(a.department_id = d.department_id) "
            + "join Position p on(a.position_id = p.position_id) where a.account_id = 1 order by a.account_id desc";
    public final String ACCOUNT_LOGIN_STATUS = "* from AccountLoginStatus order by als_id desc";
    public final String ACCOUNT_LOGIN_STATUS_ACTIVE = "* from AccountLoginStatus where als_status = 1 order by als_id desc";
    public final String ACCOUNT_STATUS = "* from AccountStatus order by as_id";
    public final String ACCOUNT_STATUS_ACTIVE = "* from AccountStatus where as_status = 1 order by as_id";
    public final String BLOCKS = "* from Blocks order by block_id";
    public final String BLOCKS_ACTIVE = "* from Blocks order by block_id";
    public final String BLOCKS_ACTIVE_NEW = "b.block_name from Blocks b join GroupBlocks gb on (b.gb_id=gb.gb_id) where gb.gb_name like ";
    public final String BLOCKS_INFO = "b.block_id,b.block_name,gb.gb_name,b.block_utilities,bs.bs_name from Blocks b "
            + "join GroupBlocks gb on(b.gb_id = gb.gb_id) "
            + "join B_status bs on(b.bs_id = bs.bs_id) "
            + "order by block_id desc";
    public final String BLOCKS_JOIN = "b.block_id,b.block_name,gb.gb_name,b.block_utilities,bs.bs_name from Blocks b "
            + "join GroupBlocks gb on(b.gb_id = gb.gb_id) "
            + "join B_status bs on(b.bs_id = bs.bs_id) ";
    public final String B_STATUS = "* from b_Status order by bs_id";
    public final String B_STATUS_ACTIVE = "* from b_Status where bs_status = 1 order by bs_id";
    public final String COMPANY = "c.company_name, c.company_cnkd, c.company_address, c.company_sales_office, c.company_contact, c.company_fax, c.company_bank, a.account_name as director_name, p.position_name as director_position "
            + "from Company c "
            + "join Account a on (c.director_id=a.account_id) "
            + "join Position p on (a.position_id=a.position_id)";
    public final String CONTRUCTION_UNIT = "* from ConstructionUnit order by cu_id";
    public final String CONTRUCTION_ACTIVE = "cu_name from ConstructionUnit order by cu_id";
    public final String CONTRUCTION_UNIT_INFO = "cu.cu_id,cu.cu_name,cu.cu_email,cu.cu_address,cu.cu_logo,cu.cu_done_project_quatity,cu.cu_contact,cus.cus_name "
            + " from ConstructionUnit cu "
            + "join ConstructionUnitStatus cus on(cu.cus_id = cus.cus_id)"
            + " order by cu.cu_id desc";
    public final String CONTRUCTION_UNIT_JOIN = " cu.cu_id,cu.cu_name,cu.cu_email,cu.cu_address,cu.cu_logo,cu.cu_done_project_quatity,cu.cu_contact,cus.cus_name "
            + " from ConstructionUnit cu "
            + "join ConstructionUnitStatus cus on(cu.cus_id = cus.cus_id) ";
    public final String CONTRUCTION_UNIT_STATUS = "* from ConstructionUnitStatus order by cus_id desc";
    public final String CONTRUCTION_UNIT_STATUS_ACTIVE = "* from ConstructionUnitStatus where cus_status = 1 order by cus_id";
    public final String CONTRACT_FILE = "* from ContractFile order by cf_id";
    public final String CONTRACT_FILE_ACTIVE = "* from ContractFile where cf_status = 1 order by cf_id";
    public final String CONTRACT_STATUS = "* from ContractStatus order by cons_id desc ";
    public final String CONTRACT_STATUS_ACTIVE = "* from ContractStatus where cons_status = 1 order by cons_id desc ";
    public final String CONTRACT_TYPE = "* from ContractType order by cont_id";
    public final String CONTRACT_TYPE_ACTIVE = "* from ContractType where cont_status = 1 order by cont_id desc";
    public final String CUSTOMER = "* from Customer order by customer_id";
    public final String CUSTOMER_ALL = "c.customer_id, c.customer_name, c.customer_email, c.customer_contact, c.customer_dob, c.customer_address, c.customer_current_address, c.customer_cccd, c.customer_cccd_date, c.customer_cccd_place, c.ct_id, cs.cs_name, c.customer_note  "
            + "from Customer c " + "join CustomerStatus cs on (c.cs_id=cs.cs_id)";
    public final String CUSTOMER_TOP_PAYMENT = " c.customer_name,c.customer_contact, sum(sc.sc_total_payment) as sc_total_payment\n"
            + "from Customer c\n"
            + "join SaleContract sc on(c.customer_id = sc.payer_id)\n"
            + "group by c.customer_name,c.customer_contact\n"
            + "order by sc_total_payment desc";
    public final String CUSTOMER_ACTIVE = "* from Customer order by customer_id";
    public final String CUSTOMER_INFO = "* from Customer c "
            + "join CustomerType ct on(c.ct_id = ct.ct_id) "
            + "join CustomerStatus cs on(c.cs_id = cs.cs_id) "
            + " where c.cs_id = 1 order by c.customer_id";
    public final String CUSTOMER_STATUS = "* from CustomerStatus order by cs_id desc ";
    public final String CUSTOMER_STATUS_ACTIVE = "* from CustomerStatus where cs_status = 1 order by cs_id";
    public final String CUSTOMER_TYPE = "* from CustomerType order by ct_id desc";
    public final String CUSTOMER_TYPE_ACTIVE = "* from CustomerType order by ct_id";
    public final String CUSTOMER_DEPARTMENT = "* from Department order by department_id";
    public final String CUSTOMER_DEPARTMENT_ACTIVE = "* from Department where ds_id = 1 order by department_id";
    public final String CUSTOMER_DEPARTMENT_INFO = "* from Department d "
            + "join DepartmentStatus ds on(d.ds_id = ds.ds_id) "
            + "where ds_id = 1 order by d.department_id";
    public final String DEPARTMENT_ACTIVE = "department_name from Department order by department_id";
    public final String DIRECTION = "* from Direction order by direction_id desc";
    public final String DIRECTION_ACTIVE = "* from Direction where direction_status = 1 order by direction_id";
    public final String INVESTOR = "Investor order by investor_id";
    public final String INVESTOR_ACTIVE = "investor_name from Investor i order by investor_id";
    public final String INVESTOR_INFO = "i.investor_id,i.investor_name,i.investor_logo,i.investor_upcoming_project_quantity,i.investor_open_project_quantity,i.investor_done_project_quantity,"
            + "i.investor_contact,i.investor_email,i.investor_address,inv.is_name "
            + "from Investor i "
            + "join InvestorStatus inv on(i.is_id = inv.is_id) "
            + "order by i.investor_id desc";
    public final String INVESTOR_JOIN = "i.investor_id,i.investor_name,i.investor_logo,i.investor_upcoming_project_quantity,i.investor_open_project_quantity,i.investor_done_project_quantity,"
            + "i.investor_contact,i.investor_email,i.investor_address,inv.is_name "
            + "from Investor i "
            + "join InvestorStatus inv on(i.is_id = inv.is_id) ";
    public final String INVESTOR_ALL = "select * from investor";
    public final String INVESTOR_STATUS = "* from InvestorStatus order by is_id desc";
    public final String INVESTOR_STATUS_ACTIVE = "* from InvestorStatus where is_status = 1 order by is_id";
    public final String INVESTOR_PROJECTS = "select * from investor i join project p on (i.investor_id = p.investor_id)";
    public final String GROUP_BLOCKS = "* from GroupBlocks order by db_id";
    public final String GROUP_BLOCKS_ACTIVE = "* from GroupBlocks order by gb_id ";
    public final String GROUP_BLOCKS_ACTIVE_NEW = "gb.gb_name from GroupBlocks gb join Project p on (gb.project_id=p.project_id) where p.project_name like ";
    public final String GROUP_BLOCKS_INFO = "gb.gb_id,gb.gb_name,gb.gb_utilities,p.project_name,gbs.gbs_name "
            + "from GroupBlocks gb "
            + "join Project p on(gb.project_id = p.project_id) "
            + "join Gb_status gbs on(gb.gbs_id = gbs.gbs_id) "
            + "order by gb_id desc";
    public final String GROUP_BLOCKS_JOIN = "gb.gb_id,gb.gb_name,gb.gb_utilities,p.project_name,gbs.gbs_name "
            + "from GroupBlocks gb "
            + "join Project p on(gb.project_id = p.project_id) "
            + "join Gb_status gbs on(gb.gbs_id = gbs.gbs_id) ";
    public final String GB_STATUS = "* from Gb_Status order by gbs_id desc";
    public final String GB_STATUS_ACTIVE = "* from Gb_Status where gbs_status = 1 order by gbs_id";
    public final String PAYMENT_PERIOD = "* from PaymentPeriod od pp \"\n"
            + "            + \"join PaymentPeriodStatus pps on(pp.pps_id = rder by pp_id";
    public final String PAYMENT_PERIOD_ACTIVE = "* from PaymentPeriod where pps_id = 1 order by pp_id";
    public final String PAYMENT_PERIOD_INFO = "PaymentPeriopps.pps_id) "
            + "join PaymentType pm on(pp.pm_id = pm.pm_id) where pps_id = 1 order by pp_id";
    public final String PAYMENT_PERIOD_FILE = "* from PaymentPeriodFile order by ppf_id";
    public final String PAYMENT_PERIOD_FILE_ACTIVE = "* from PaymentPeriodFile where ppf_status = 1 order by ppf_id";
    public final String PAYMENT_PERIOD_STATUS = "* from PaymentPeriodStatus order by pps_id desc";
    public final String PAYMENT_PERIOD_STATUS_ACTIVE = "* from PaymentPeriodStatus where pps_status = 1 order by pps_id desc";
    public final String PAYMENT_TYPE = "* from PaymentType order by pm_id desc";
    public final String PAYMENT_TYPE_ACTIVE = "* from PaymentType where pm_status = 1 order by pm_id";
    public final String POSITION = "* from Position order by position_id";
    public final String POSITION_ACTIVE = "* from Position where position_status = 1 order by position_id";
    public final String PRODUCT = "p.product_id,p.product_name,p.product_price,ps.ps_name,r.region_name,d.direction_name "
            + "from Product p "
            //            + "join Blocks b on(p.block_id = b.block_id) "
            //            + "join GroupBlocks gb on(b.gb_id = gb.gb_id) "
            //            + "join Project pj on(gb.project_id = pj.project_id) "
            + "join ProductStatus ps on(p.ps_id = ps.ps_id) "
            + "join Region r on(p.region_id = r.region_id) "
            + "join Direction d on(p.direction_id = d.direction_id) "
            + "order by p.product_id";
    public final String PRODUCT_JOIN = "p.product_id,p.product_name,p.product_price,ps.ps_name,r.region_name,d.direction_name "
            + "from Product p "
            //            + "join Blocks b on(p.block_id = b.block_id) "
            //            + "join GroupBlocks gb on(b.gb_id = gb.gb_id) "
            //            + "join Project pj on(gb.project_id = pj.project_id) "
            + "join ProductStatus ps on(p.ps_id = ps.ps_id) "
            + "join Region r on(p.region_id = r.region_id)"
            + "join Direction d on(p.direction_id = d.direction_id) ";
    public final String PRODUCT_ACTIVE = "* from Product where ps_id = 1 order by product_id";
    public final String PRODUCT_ACTIVE_NEW = "p.product_name from Product p join Blocks b on (p.block_id=b.block_id) where b.block_name like ";
    public final String PRODUCT_BY_ID = "p.product_id,p.product_name,p.product_descrbe,p.product_price,ps.ps_name,r.region_name "
            + "from Product p "
            + "join ProductStatus ps on(p.ps_id = ps.ps_id) "
            + "join Region r on(p.region_id = r.region_id) "
            + "where p.product_id = ";
    public final String PRODUCT_DETAIL = "pj.project_name, gb.gb_name, b.block_name, pd.product_name "
            + "from SaleContract sc "
            + "join Product pd on (sc.product_id=pd.product_id) "
            + "join Blocks b on(pd.block_id=b.block_id) "
            + "join GroupBlocks gb on(b.gb_id=gb.gb_id) "
            + "join Project pj on (gb.project_id = pj.project_id) "
            + "where pd.product_id= ";
    public final String PRODUCT_INFO = "p.product_id,p.product_name,p.product_descrbe,p.product_price,ps.ps_name,r.region_name "
            + "from Product p "
            //            + "join Blocks b on(p.block_id = b.block_id) "
            //            + "join GroupBlocks gb on(b.gb_id = gb.gb_id) "
            //            + "join Project pj on(gb.project_id = pj.project_id) "
            + "join ProductStatus ps on(p.ps_id = ps.ps_id) "
            + "join Region r on(p.region_id = r.region_id) "
            + "where p.ps_id = 1 order by p.product_id";
    public final String PRODUCT_IMAGE = "* from ProductImage order by pi_id";
    public final String PRODUCT_IMAGE_ACTIVE = "* from ProductImage where pi_status = 1 order by pi_id";
    public final String PRODUCT_STATUS = "* from ProductStatus order by ps_id desc";
    public final String PRODUCT_STATUS_ACTIVE = "* from ProductStatus where ps_status = 1 order by ps_id";
    public final String PROJECT = "Project order by project_id";
    public final String PROJECT_ACTIVE = "* from Project order by project_id";
    public final String PROJECT_INFO = "p.project_id,p.project_name,p.project_location,p.project_area,p.project_juridical,pjs.pjs_name from Project p "
            + "join ConstructionUnit cu on(p.cu_id = cu.cu_id) "
            + "join Investor i on(p.investor_id = i.investor_id) "
            + "join ProjectType pt on(p.pt_id = pt.pt_id) "
            + "join ProjectStatus pjs on(p.pjs_id = pjs.pjs_id) "
            + " order by p.project_id";
    public final String PROJECT_JOIN = "p.project_id,p.project_name,p.project_location,p.project_area,p.project_juridical,pjs.pjs_name from Project p "
            + "join ConstructionUnit cu on(p.cu_id = cu.cu_id) "
            + "join Investor i on(p.investor_id = i.investor_id) "
            + "join ProjectType pt on(p.pt_id = pt.pt_id) "
            + "join ProjectStatus pjs on(p.pjs_id = pjs.pjs_id) ";
    public final String PROJECT_STATUS = "* from ProjectStatus order by pjs_id desc";
    public final String PROJECT_STATUS_ACTIVE = "* from ProjectStatus where ps_status = 1 order by pjs_id";
    public final String PROJECT_TYPE = "* from ProjectType order by pt_id desc";
    public final String PROJECT_TYPE_ACTIVE = "* from ProjectType where pt_status = 1 order by pt_id";
    public final String REGION = "* from Region order by region_id";
    public final String REGION_ACTIVE = "r.region_id,r.region_name,r.region_area,rs.rs_name "
            + "from Region r "
            + "join RegionStatus rs on(r.rs_id = rs.rs_id) "
            + " order by r.region_id";
    public final String REGION_INFO = "r.region_id,r.region_name,r.region_area,rs.rs_name "
            + "from Region r "
            + "join RegionStatus rs on(r.rs_id = rs.rs_id) "
            + "order by r.region_id desc";
    public final String REGION_JOIN = "r.region_id,r.region_name,r.region_area,rs.rs_name "
            + "from Region r "
            + "join RegionStatus rs on(r.rs_id = rs.rs_id) ";
    public final String REGION_STATUS = "* from RegionStatus order by rs_id";
    public final String REGION_STATUS_ACTIVE = "* from RegionStatus order by rs_id";
    public final String UTILITIES = "* from Utilities order by utilities_id desc";
    public final String UTILITIES_ACTIVE = "top 20 * from Utilities where utilities_status = 1 order by utilities_id desc";
    public final String DEPARTMENT_STATUS = "* from DepartmentStatus order by ds_id";
    public final String DEPARTMENT = "* from Department order by department_id";
    public final String DEPARTMENT_INFO = "d.department_id, d.department_name,ds.ds_name from Department d "
            + "join DepartmentStatus ds on(d.ds_id = ds.ds_id)";
    public final String STATISTIC_ALL = "* from Statistic";
    public final String SALE_CONTRACT_ALL = "sc.sc_id, sc.sc_time_open, sc.sc_number, sc.sc_total_payment, sc.saler_id, a.account_name as saler_name, sc.payer_id, p.product_name, ct.cont_name, cs.cons_name, sc.product_id, sc.cont_id, sc.cons_id  "
            + "from SaleContract sc "
            + "join ContractType ct on (sc.cont_id=ct.cont_id) "
            + "join ContractStatus cs on (sc.cons_id=cs.cons_id) "
            + "join Product p on (sc.product_id=p.product_id) "
            + "join Account a on (sc.saler_id=a.account_id) "
            + "where sc.saler_id= ";
    public final String SALE_CUSTOMER = "* from Customer where customer_id = ";
    public final String REVENUE_ALL = "* from ProductRevenue";
    public final String REVENUE_TOTAL = "* from ProductRevenue where pr_id = 1";
    public final String REVENUE_CURRENT_MONTH = "sum(pr_revenue) as 'pr_revenue' from ProductRevenue where pr_id > 1 and \n"
            + "MONTH(getDate()) = MONTH(dbo.convertBigIntToDateTime(pr_name)) and\n"
            + "Year(getDate()) = Year(dbo.convertBigIntToDateTime(pr_name))";
    public final String REVENUE_CURRENT_YEAR = "sum(pr_revenue) as 'pr_revenue' from ProductRevenue where pr_id > 1 and \n"
            + "Year(getDate()) = Year(dbo.convertBigIntToDateTime(pr_name))";
    public final String REVENUE_BY = "* from ProductRevenue where ";
    public final String PRODUCT_STATISTIC = "* from ProductStatistic\n"
            + "where Year(dbo.convertBigIntToDateTime(ps_name)) = ";
    public final String REVENUE_DEPT = "* from RevenueDept";
}
