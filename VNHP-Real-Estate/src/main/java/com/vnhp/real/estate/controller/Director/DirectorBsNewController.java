/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorBsNewController {

    @FXML
    private TextField tfDirBsNewName;

    @FXML
    private Label tfDirBsNewResult;

    @FXML
    private Button btnDirBsNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirBsNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirBsNewName.getText();

            var data = new B_Status(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirBsNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirBsNewResult, 2);
        });
    }
}
