/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;

/**
 *
 * @author FairyHunter
 */
public final class NameDepartment {

    public Integer index;
    @ColumnId
    public Integer dn_id;
    @ColumnName
    public String dn_name;
    @ColumnStatus
    public Boolean dn_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setdn_id(Integer dn_id) {
        this.dn_id = dn_id;
    }

    public void setdn_name(String dn_name) {
        this.dn_name = dn_name;
    }

    public void setdn_status(Boolean dn_status) {
        this.dn_status = dn_status;
    }

    public Integer getdn_id() {
        return dn_id;
    }

    public String getdn_name() {
        return dn_name;
    }

    public Boolean getdn_status() {
        return dn_status;
    }

    public NameDepartment() {
    }

    public NameDepartment(String dn_name) {
        this.dn_name = dn_name;
    }

    public NameDepartment(Integer dn_id, String dn_name) {
        this.dn_id = dn_id;
        this.dn_name = dn_name;
    }

    public NameDepartment(Integer dn_id, Boolean dn_status) {
        this.dn_id = dn_id;
        this.dn_status = dn_status;
    }

    public NameDepartment( Integer dn_id, String dn_name, Boolean dn_status, Integer index) {
        this.index = index;
        this.dn_id = dn_id;
        this.dn_name = dn_name;
        this.dn_status = dn_status;
        this.btnChangeStatus = "Change Status";
        this.btnUpdate = "Update";
    }
    
    
}
