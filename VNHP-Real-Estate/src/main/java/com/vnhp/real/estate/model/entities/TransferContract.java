/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.*;

/**
 *
 * @author FairyHunter
 */
public final class TransferContract {

    public Integer index;
    @ColumnId
    public Integer tc_id;
    public String tc_time_open;
    public String tc_number;
    public Float tc_fee;
    public Integer tc_saler_id;
    public String tc_saler_name;
    public Integer tc_transfer_id;
    public String tc_transfer_name;
    public Boolean tc_status;
    public Integer sc_id;

    public void setSc_id(Integer sc_id) {
        this.sc_id = sc_id;
    }

    public void setTc_saler_id(Integer tc_saler_id) {
        this.tc_saler_id = tc_saler_id;
    }

    public void setTc_saler_name(String tc_saler_name) {
        this.tc_saler_name = tc_saler_name;
    }

    public void setTc_fee(Float tc_fee) {
        this.tc_fee = tc_fee;
    }

    public void setTc_transfer_id(Integer tc_transfer_id) {
        this.tc_transfer_id = tc_transfer_id;
    }

    public void setTc_transfer_name(String tc_transfer_name) {
        this.tc_transfer_name = tc_transfer_name;
    }
 
    public void setTc_id(Integer tc_id) {
        this.tc_id = tc_id;
    }

    public void setTc_number(String tc_number) {
        this.tc_number = tc_number;
    }

    public void setTc_time_open(String tc_time_open) {
        this.tc_time_open = tc_time_open;
    }

    public void setTc_status(Boolean tc_status) {
        this.tc_status = tc_status;
    }
    

    public Integer getSc_id() {
        return sc_id;
    }

    public Integer getTc_saler_id() {
        return tc_saler_id;
    }

    public String getTc_saler_name() {
        return tc_saler_name;
    }

    public Float getTc_fee() {
        return tc_fee;
    }

    public Integer getTc_transfer_id() {
        return tc_transfer_id;
    }

    public String getTc_transfer_name() {
        return tc_transfer_name;
    }

    public Integer getTc_id() {
        return tc_id;
    }

    public String getTc_number() {
        return tc_number;
    }

    public String getTc_time_open() {
        return tc_time_open;
    }

    public Boolean getTc_status() {
        return tc_status;
    }
    

    public TransferContract() {
    }

    public TransferContract(String tc_time_open, String tc_number, Float tc_fee, Integer tc_saler_id, Integer tc_transfer_id, Integer sc_id) {
        this.tc_time_open = tc_time_open;
        this.tc_number = tc_number;
        this.tc_fee = tc_fee;
        this.tc_saler_id = tc_saler_id;
        this.tc_transfer_id = tc_transfer_id;
        this.sc_id = sc_id;
    }
    
    public TransferContract(Integer tc_id, String tc_time_open, String tc_number, Float tc_fee, String tc_saler_name, Integer sc_id) {
        this.tc_id = tc_id;
        this.tc_time_open = tc_time_open;
        this.tc_number = tc_number;
        this.tc_fee = tc_fee;
        this.tc_saler_name = tc_saler_name;
        this.sc_id = sc_id;
    }
}
