package com.vnhp.real.estate.controller.Business;

import static com.vnhp.real.estate.controller.Business.AllCustomerController.*;
import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class BusinessDepositContractController {

    @FXML
    private TextField customerName, customerDOB, customerCCCD, customerPlace, customerDate, customerAddress, customerCurrentAddress,
            companyName, companyCNDK, companyAddress, companySalesOffice, companyContact, companyFax, companyBank, companyDirectorName, companyDirectorPosition;

    @FXML
    private Button depositContractAddBtn;

    @FXML
    private Label addResult;

    public void init(Stage stage) {
        customerName.setText(customer.getCustomer_name());
        customerDOB.setText(customer.getCustomer_dob());
        customerCCCD.setText(customer.customer_cccd);
        customerPlace.setText(customer.getCustomer_cccd_place());
        customerDate.setText(customer.getCustomer_cccd_date());
        customerAddress.setText(customer.getCustomer_address());
        customerCurrentAddress.setText(customer.getCustomer_current_address());

        var company = (Company) ConvertObject.Instance().toData(BaseDao.Instance().getListData(StringValue.Instance().COMPANY, Company[].class));
        companyName.setText(company.getCompany_name());
        companyCNDK.setText(company.getCompany_cnkd());
        companyAddress.setText(company.getCompany_address());
        companySalesOffice.setText(company.getDirector_position());
        companyContact.setText(company.getCompany_contact());
        companyFax.setText(company.getCompany_fax());
        companyBank.setText(company.getCompany_bank());
        companyDirectorName.setText(company.getDirector_name());
        companyDirectorPosition.setText(company.getDirector_position());

        addDepositContract();
    }

    private void addDepositContract() {
        depositContractAddBtn.addEventHandler(ActionEvent.ACTION, mouseEvent -> {
            var dc_time_open = String.valueOf(TimeManager.Instance().getServerTime().getTimeInMillis());

            var statisticData = BaseDao.Instance().getListData(StringValue.Instance().STATISTIC_ALL, Statistic[].class);
            var dc_number = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "DepositContract") + 1;

            var dc_payment = (float) 0;

            var sc_id =  BusinessSaleContractController.transferFromSaleContract.getSc_id();

            var data = new DepositContract(dc_time_open, dc_number, dc_payment, sc_id);
            var result = BaseDao.Instance().insert(data);

            addResult.setText(result);
            TimeManager.Instance().clearResult(mouseEvent, addResult, 2);
        });
    }

}
