/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.model.entities.RegionStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorRsUpdateController {

    @FXML
    private TextField tfDirRSUpdateName;

    @FXML
    private Label tfDirRSUpdateResult;

    @FXML
    private Button btnDirRSUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, RegionStatus data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(RegionStatus data) {
        tfDirRSUpdateName.setText(data.getRs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, RegionStatus data) {
        btnDirRSUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirRSUpdateName.getText();

            var newData = new ProductStatus(data.getRs_id(), name);
            var retult = BaseDao.Instance().update(newData);
            tfDirRSUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirRSUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (RegionStatus) data;
            tmp.setRs_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getRs_id(), tmp);
        });
    }
}
