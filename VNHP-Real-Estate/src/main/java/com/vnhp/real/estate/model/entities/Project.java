/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class Project {

    public Integer index;
    @ColumnId
    public Integer project_id;
    @ColumnName
    @DbColumn
    public String project_name;
    @DbColumn
    public String project_info;
    @DbColumn
    public String project_location;
    @DbColumn
    public Float project_area;
    @DbColumn
    public String project_juridical;
    @DbColumn
    public String project_density;
    @DbColumn
    public String project_utilities;
    public Integer cu_id;
    @DbColumn
    public String cu_name;
    public Integer investor_id;
    @DbColumn
    public String investor_name;
    public Integer pt_id;
    @DbColumn
    public String pt_name;
    @ColumnStatus
    public Integer pjs_id;
    @DbColumn
    public String pjs_name;
    public String btnChangeStatus;
    public String btnUpdate;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setProject_density(String project_density) {
        this.project_density = project_density;
    }

    public String getProject_density() {
        return project_density;
    }

    public void setCu_id(Integer cu_id) {
        this.cu_id = cu_id;
    }

    public void setCu_name(String cu_name) {
        this.cu_name = cu_name;
    }

    public void setInvestor_id(Integer investor_id) {
        this.investor_id = investor_id;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public void setProject_area(Float project_area) {
        this.project_area = project_area;
    }

    public void setProject_attached_utilities(String project_utilities) {
        this.project_utilities = project_utilities;
    }

    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }

    public void setProject_info(String project_info) {
        this.project_info = project_info;
    }

    public void setProject_juridical(String project_juridical) {
        this.project_juridical = project_juridical;
    }

    public void setProject_location(String project_location) {
        this.project_location = project_location;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public void setProject_status(Integer pt_id) {
        this.pt_id = pt_id;
    }

    public void setProject_type(Integer pjs_id) {
        this.pjs_id = pjs_id;
    }

    public void setPs_name(String pjs_name) {
        this.pjs_name = pjs_name;
    }

    public void setPt_name(String pt_name) {
        this.pt_name = pt_name;
    }

    public Integer getCu_id() {
        return cu_id;
    }

    public String getCu_name() {
        return cu_name;
    }

    public Integer getInvestor_id() {
        return investor_id;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public Float getProject_area() {
        return project_area;
    }

    public String getProject_attached_utilities() {
        return project_utilities;
    }

    public Integer getProject_id() {
        return project_id;
    }

    public String getProject_info() {
        return project_info;
    }

    public String getProject_juridical() {
        return project_juridical;
    }

    public String getProject_location() {
        return project_location;
    }

    public String getProject_name() {
        return project_name;
    }

    public Integer getProject_status() {
        return pt_id;
    }

    public Integer getProject_type() {
        return pjs_id;
    }

    public String getPs_name() {
        return pjs_name;
    }

    public String getPt_name() {
        return pt_name;
    }

    public Project() {
    }

    public Project(String project_name, String project_info, String project_location, Float project_area, String project_juridical, String project_density, String project_utilities, Integer cu_id, Integer investor_id, Integer pt_id, Integer pjs_id) {
        this.project_name = project_name;
        this.project_info = project_info;
        this.project_location = project_location;
        this.project_area = project_area;
        this.project_juridical = project_juridical;
        this.project_density = project_density;
        this.project_utilities = project_utilities;
        this.cu_id = cu_id;
        this.investor_id = investor_id;
        this.pjs_id = pjs_id;
        this.pt_id = pt_id;
    }

    public Project(Integer project_id, String project_name, String project_info, String project_location, Float project_area, String project_juridical, String project_density, String project_utilities, Integer cu_id, Integer investor_id, Integer pjs_id) {
        this.project_id = project_id;
        this.project_name = project_name;
        this.project_info = project_info;
        this.project_location = project_location;
        this.project_area = project_area;
        this.project_juridical = project_juridical;
        this.project_density = project_density;
        this.project_utilities = project_utilities;
        this.cu_id = cu_id;
        this.investor_id = investor_id;
        this.pjs_id = pjs_id;
    }

    public Project(Integer project_id, Integer pjs_id) {
        this.project_id = project_id;
        this.pjs_id = pjs_id;
    }

    public Project(String project_name) {
        this.project_name = project_name;
    }

    public Project(Integer project_id, String project_name, String project_info, String project_location, Float project_area, String project_juridical, String project_density, String project_utilities, Integer cu_id, String cu_name, Integer investor_id, String investor_name, Integer pjs_id, String pt_name, Integer pt_id, String pjs_name) {
        this.project_id = project_id;
        this.project_name = project_name;
        this.project_info = project_info;
        this.project_location = project_location;
        this.project_area = project_area;
        this.project_juridical = project_juridical;
        this.project_density = project_density;
        this.project_utilities = project_utilities;
        this.cu_id = cu_id;
        this.cu_name = cu_name;
        this.investor_id = investor_id;
        this.investor_name = investor_name;
        this.pjs_id = pjs_id;
        this.pt_name = pt_name;
        this.pt_id = pt_id;
        this.pjs_name = pjs_name;
    }

    public Project(Integer project_id, String project_name, String project_location, Float project_area, String project_juridical, String pjs_name, Integer index) {
        this.index = index;
        this.project_id = project_id;
        this.project_name = project_name;
        this.project_location = project_location;
        this.project_area = project_area;
        this.project_juridical = project_juridical;
        this.pjs_name = pjs_name;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }

    public Project(String project_name, String project_location, Float project_area, String project_juridical, String project_density, String project_info, String project_utilities, Integer cu_id, Integer investor_id, Integer pt_id) {
        this.project_name = project_name;
        this.project_location = project_location;
        this.project_area = project_area;
        this.project_juridical = project_juridical;
        this.project_density = project_density;
        this.project_info = project_info;
        this.project_utilities = project_utilities;
        this.cu_id = cu_id;
        this.investor_id = investor_id;
        this.pt_id = pt_id;
    }
}
