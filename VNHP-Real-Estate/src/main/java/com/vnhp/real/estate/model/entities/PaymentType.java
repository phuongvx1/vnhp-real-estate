/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class PaymentType {

    public Integer index;
    @ColumnId
    public Integer pm_id;
    @ColumnName
    @DbColumn
    public String pm_name;
    @ColumnStatus
    @DbColumn
    public Boolean pm_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setPmt_id(Integer pm_id) {
        this.pm_id = pm_id;
    }

    public void setPmt_name(String pm_name) {
        this.pm_name = pm_name;
    }

    public void setPmt_status(Boolean pm_status) {
        this.pm_status = pm_status;
    }

    public Integer getPmt_id() {
        return pm_id;
    }

    public String getPmt_name() {
        return pm_name;
    }

    public Boolean getPmt_status() {
        return pm_status;
    }

    public PaymentType() {
    }

    public PaymentType(String pmt_name) {
        this.pm_name = pmt_name;
    }

    public PaymentType(Integer pm_id, String pm_name) {
        this.pm_id = pm_id;
        this.pm_name = pm_name;
    }

    public PaymentType(Integer pm_id, Boolean pm_status) {
        this.pm_id = pm_id;
        this.pm_status = pm_status;
    }

    public PaymentType(Integer pm_id, String pm_name, Boolean pm_status, Integer index) {
        this.index = index;
        this.pm_id = pm_id;
        this.pm_name = pm_name;
        this.pm_status = pm_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
