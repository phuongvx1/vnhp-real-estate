/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorDashboardController {

    @FXML
    private Button btnDirChart, btnDirView;

    @FXML
    private Pane pDirDashBoardView;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirChart.addEventHandler(ActionEvent.ACTION,eh -> {
            try {
                var dataNew = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_CHART));
                AnchorPane dashboard = dataNew.load();
                pDirDashBoardView.getChildren().clear();
                pDirDashBoardView.getChildren().add(dashboard);
                var control = (DirectorChartController) dataNew.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
