package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import com.vnhp.real.estate.controller.Business.AllCustomerController;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.web.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class AddCustomerController {

    @FXML
    private Pane uploadBtn;

    @FXML
    private ImageView customerAvatar;

    @FXML
    private TextField customerName, customerEmail, customerContact, customerAddress, customerCurrentAddress, customerID, customerDOB, customerIDDate, customerIDPlace;

    @FXML
    private ComboBox<String> customerType, customerStatus;

    @FXML
    private HTMLEditor customerNote;

    @FXML
    private Button addBtn, addSaleContractBtn;

    @FXML
    private Label addResult;

    FileChooser fileChooser;
    File filePath;

    public void init() {
        borderAvatar();

        uploadBtn.setOnMousePressed(mouseEvent -> {
            //OPEN DIALOG TO GET IMAGE PATH
            Stage stage = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();

            fileChooser = new FileChooser();
            fileChooser.setTitle("Upload Image");

            String userDirectoryString = System.getProperty("user.home") + "\\Desktop";
            File userDirectory = new File(userDirectoryString);

            if (!userDirectory.canRead()) {
                userDirectory = new File("c:/");
            }

            fileChooser.setInitialDirectory(userDirectory);
            filePath = fileChooser.showOpenDialog(stage);

            //UPDATE IMAGE PATH INTO DB
            try {
                var image = new Image(filePath.getPath());
                String imageToString = image.getUrl();
                System.out.println(imageToString);

                Account a = new Account(1, imageToString);
                String b = BaseDao.Instance().update(a);
                System.out.println(b);
            } catch (Exception e) {
            }
        });

        displaySelectField();
        addNewCustomer();
//        transferCustomer();
    }

    void borderAvatar() {
        Circle clip = new Circle(112.5, 125.0, 112.5);
        customerAvatar.setClip(clip);
    }

    private void displaySelectField() {
        var type = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CUSTOMER_TYPE_ACTIVE, CustomerType[].class));
        customerType.getItems().addAll(type);
        customerType.setValue((String) type.get(0));

        var status = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CUSTOMER_STATUS_ACTIVE, CustomerStatus[].class));
        customerStatus.getItems().addAll(status);
        customerStatus.setValue((String) status.get(0));
    }

    private void addNewCustomer() {
        addBtn.addEventHandler(ActionEvent.ACTION, mouseEvent -> {

            var name = customerName.getText();
            var email = customerEmail.getText();
            var contact = customerContact.getText();
            var address = customerAddress.getText();
            var current_address = customerCurrentAddress.getText();
            var id = customerID.getText();
            var dob = customerDOB.getText();
            var id_date = customerIDDate.getText();
            var id_place = customerIDPlace.getText();
            var note = customerNote.getHtmlText();

            var type = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new CustomerType(customerType.getValue()), CustomerType[].class, 1), ColumnId.class);
            var status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new CustomerStatus(customerStatus.getValue()), CustomerStatus[].class, 1), ColumnId.class);

            var data = new Customer(name, email, contact, address, current_address, id, dob, id_date, id_place, note, type, status);
            var result = BaseDao.Instance().insert(data);

            AllCustomerController.customer = (Customer) ConvertObject.Instance().toData(BaseDao.Instance().getData(data, Customer[].class, 1));

            addResult.setText(result);
            TimeManager.Instance().clearResult(mouseEvent, addResult, 2);
        });
    }

//    private void transferCustomer() {
//        //get customer info
//        var name = customerName.getText();
//        var email = customerEmail.getText();
//        var contact = customerContact.getText();
//        var address = customerAddress.getText();
//        var id = customerID.getText();
//
//        var type = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new CustomerType(customerType.getValue()), CustomerType[].class, 1), ColumnId.class);
//        var status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new CustomerStatus(customerStatus.getValue()), CustomerStatus[].class, 1), ColumnId.class);
//
//        var data = new Customer(name, email, contact, address, id, type, status);
//        var result = BaseDao.Instance().insert(data);
//        addResult.setText(result);
//
//    }
}
