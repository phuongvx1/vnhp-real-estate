/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class ProductStatus {

    public Integer index;
    @ColumnId
    public Integer ps_id;
    @ColumnName
    @DbColumn
    public String ps_name;
    @ColumnStatus
    @DbColumn
    public Boolean ps_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setPs_id(Integer ps_id) {
        this.ps_id = ps_id;
    }

    public void setPs_name(String ps_name) {
        this.ps_name = ps_name;
    }

    public void setPs_status(Boolean ps_status) {
        this.ps_status = ps_status;
    }

    public Integer getPs_id() {
        return ps_id;
    }

    public String getPs_name() {
        return ps_name;
    }

    public Boolean getPs_status() {
        return ps_status;
    }

    public ProductStatus() {
    }

    public ProductStatus(String ps_name) {
        this.ps_name = ps_name;
    }

    public ProductStatus(Integer ps_id, String ps_name) {
        this.ps_id = ps_id;
        this.ps_name = ps_name;
    }

    public ProductStatus(Integer ps_id, Boolean ps_status) {
        this.ps_id = ps_id;
        this.ps_status = ps_status;
    }

    public ProductStatus(Integer ps_id, String ps_name, Boolean ps_status, Integer index) {
        this.index = index;
        this.ps_id = ps_id;
        this.ps_name = ps_name;
        this.ps_status = ps_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }

}
