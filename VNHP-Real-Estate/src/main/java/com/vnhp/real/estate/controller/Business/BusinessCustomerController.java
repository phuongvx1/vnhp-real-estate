package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.model.entities.*;
import com.vnhp.real.estate.res.*;
import java.io.IOException;
import java.util.LinkedList;
import javafx.fxml.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class BusinessCustomerController {

    @FXML
    public Button showAllBtn, addCustomerBtn, editCustomerBtn;

    @FXML
    private ScrollPane businessCustomerBorder;

    public void init(Stage stage) {
        FXMLLoader defaultLoad = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_ALL_CUSTOMER));
        try {
            Pane allCustomer = defaultLoad.load();
            businessCustomerBorder.setContent(allCustomer);

            var control = (AllCustomerController) defaultLoad.getController();
            control.init();

        } catch (IOException e) {
        }

        showAllBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_ALL_CUSTOMER));

            try {
                Pane allCustomer = loader.load();
                businessCustomerBorder.setContent(allCustomer);

                var control = (AllCustomerController) loader.getController();
                control.init();

            } catch (IOException e) {
            }
        });

        addCustomerBtn.setOnMousePressed(mouseEvent -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_ADD_CUSTOMER));

            try {
                Pane addCustomer = loader.load();
                businessCustomerBorder.setContent(addCustomer);

                var control = (AddCustomerController) loader.getController();
                control.init();

            } catch (IOException e) {
            }
        });

//        editCustomerBtn.setOnMousePressed(mouseEvent -> {
//            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_PROJECT));
//
//            try {
//                Pane projectDisplay = loader.load();
//                businessCustomerBorder.setContent(projectDisplay);
//
//                var control = (ProjectController) loader.getController();
//                control.init();
//
//            } catch (IOException e) {
//            }
//        });
    }

}


