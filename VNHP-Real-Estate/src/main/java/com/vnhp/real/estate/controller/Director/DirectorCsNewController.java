/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.ContractStatus;
import com.vnhp.real.estate.model.entities.CustomerStatus;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorCsNewController {

    @FXML
    private TextField tfDirCsNewName;

    @FXML
    private Label tfDirCsNewResult;

    @FXML
    private Button btnDirCsNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirCsNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirCsNewName.getText();

            var data = new CustomerStatus(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirCsNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirCsNewResult, 2);
        });
    }
}
