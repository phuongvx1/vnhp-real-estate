package com.vnhp.real.estate.controller.Business;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Investor;
import com.vnhp.real.estate.res.ProjectInvestorComponent;
import com.vnhp.real.estate.res.StringValue;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;

import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.*;

/**
 * FXML Controller class
 *
 * @author phongv
 */
public class InvestorInformationController {

    @FXML
    ImageView btnClose, btnMinimize, logoImage;

    @FXML
    Label investorName, upcomingProjectVal, openingProjectVal, doneProjectVal, addressVal, contactVal, emailVal, biInfo1, biInfo2, biInfo3, biInfo4;

    @FXML
    Pane infoWrapper, projectInfoComponent;

    public void init(Stage subStage, Integer iId) {
        openClose(subStage);

        var investorInfo = new Investor();
        investorInfo.investor_id = iId;
        investorInfo.is_id = 1;

        var jsonString = BaseDao.Instance().getData(investorInfo, Investor[].class, 1);

        for (Object object : jsonString) {
            var iInfo = (Investor) object;

            var image = new Image(getClass().getResourceAsStream(StringValue.Instance().ASSET_BUSINESS_INVESTOR + iInfo.investor_logo));
            logoImage.setImage(image);
            logoImage.setFitHeight(100.0);
            logoImage.setFitWidth(100.0);

            investorName.setText(iInfo.investor_name);
            upcomingProjectVal.setText((iInfo.investor_upcoming_project_quantity).toString());
            openingProjectVal.setText((iInfo.investor_open_project_quantity).toString());
            doneProjectVal.setText((iInfo.investor_done_project_quantity).toString());
            addressVal.setText(iInfo.investor_address);
            contactVal.setText(iInfo.investor_contact);
            emailVal.setText(iInfo.investor_email);

            final HTMLEditor htmlEditor = new HTMLEditor();
            htmlEditor.setHtmlText(iInfo.investor_info);
            final WebView browser = new WebView();
            browser.setPrefHeight(300.0);
            browser.setPrefWidth(1000.0);

            final WebEngine webEngine = browser.getEngine();
            webEngine.loadContent(htmlEditor.getHtmlText());
            infoWrapper.getChildren().add(browser);

        }
        
        ProjectInvestorComponent pInvestor = new ProjectInvestorComponent();
        projectInfoComponent.getChildren().add(pInvestor);
    }

    void openClose(Stage subStage) {
        btnClose.setOnMouseClicked(mouseEvent -> {
            subStage.close();
        });

        btnMinimize.setOnMouseClicked(mouseEvent -> {
            subStage.setIconified(true);

        });

    }
}
