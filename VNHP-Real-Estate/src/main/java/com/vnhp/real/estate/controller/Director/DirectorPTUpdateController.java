/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.ProjectType;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorPTUpdateController {

    @FXML
    private TextField tfDirPTUpdateName;

    @FXML
    private Label tfDirPTUpdateResult;

    @FXML
    private Button btnDirPTUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, ProjectType data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(ProjectType data) {
        tfDirPTUpdateName.setText(data.getPt_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, ProjectType data) {
        btnDirPTUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirPTUpdateName.getText();

            var newData = new ProjectType(data.getPt_id(), name);
            var retult = BaseDao.Instance().update(newData);
            tfDirPTUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirPTUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (ProjectType) data;
            tmp.setPt_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getPt_id(), tmp);
        });
    }

}
