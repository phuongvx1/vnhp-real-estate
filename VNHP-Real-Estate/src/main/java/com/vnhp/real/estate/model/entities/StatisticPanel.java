/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.StringValue;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

/**
 *
 * @author FairyHunter
 */
public class StatisticPanel extends Pane {

    private final MyImage icon;
    private final Label tittle;
    private final Label content;

    public StatisticPanel(String icon, String tittle, String content) {
        this.setPrefHeight(50);
        this.setPrefWidth(137.5);
        this.getStyleClass().add("Dashboard-Background");

        this.icon = new MyImage(StringValue.Instance().ASSET + icon, 30, 30);
        this.icon.setLayoutX(25);
        this.icon.setLayoutY(10);

        this.tittle = new Label(tittle);
        this.tittle.getStyleClass().add("Dashboard-Tittle");
        this.tittle.setLayoutX(65);
        this.tittle.setLayoutY(8);

        this.content = new Label(content);
        this.content.getStyleClass().add("Dashboard-Content");
        this.content.setLayoutX(65);
        this.content.setLayoutY(22);

        this.getChildren().addAll(this.icon, this.tittle, this.content);
    }

    public StatisticPanel(String icon, String tittle, String content, Integer width) {
        this.setPrefHeight(50);
        this.setPrefWidth(width);
        this.icon = new MyImage(StringValue.Instance().ASSET + icon, 30, 30);
        this.icon.setLayoutX(65);
        this.icon.setLayoutY(10);
        this.getStyleClass().add("Dashboard-Background");

        this.tittle = new Label();
        this.tittle.setText(tittle);
        this.tittle.getStyleClass().add("Dashboard-Tittle");
        this.tittle.setLayoutX(105);
        this.tittle.setLayoutY(8);

        this.content = new Label();
        this.content.setText(content);
        this.content.getStyleClass().add("Dashboard-Content");
        this.content.setLayoutX(105);
        this.content.setLayoutY(22);
        this.getChildren().addAll(this.icon, this.tittle, this.content);
    }
}
