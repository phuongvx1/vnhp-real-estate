/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnDate;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnMoney;
import com.vnhp.real.estate.res.ColumnStatus;

/**
 *
 * @author FairyHunter
 */
public class ProductRevenue {

    @ColumnId
    public Integer pr_id;
    @ColumnDate
    public String pr_name;
    @ColumnMoney
    public Float pr_revenue;
    @ColumnStatus
    public Boolean pr_status;

    public void setPr_id(Integer pr_id) {
        this.pr_id = pr_id;
    }

    public void setPr_name(String pr_name) {
        this.pr_name = pr_name;
    }

    public void setPr_revenue(Float pr_revenue) {
        this.pr_revenue = pr_revenue;
    }

    public void setPr_status(Boolean pr_status) {
        this.pr_status = pr_status;
    }

    public Integer getPr_id() {
        return pr_id;
    }

    public String getPr_name() {
        return pr_name;
    }

    public Float getPr_revenue() {
        return pr_revenue;
    }

    public Boolean getPr_status() {
        return pr_status;
    }

    public ProductRevenue() {
    }

    public ProductRevenue(String pr_name) {
        this.pr_name = pr_name;
    }

    public ProductRevenue(Integer pr_id, String pr_name, Float pr_revenue, Boolean pr_status) {
        this.pr_id = pr_id;
        this.pr_name = pr_name;
        this.pr_revenue = pr_revenue;
        this.pr_status = pr_status;
    }
}
