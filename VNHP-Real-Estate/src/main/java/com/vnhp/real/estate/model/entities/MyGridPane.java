package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.DataCellView.UtilitiesCellView;
import com.vnhp.real.estate.res.ObjectType;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.InvestorComponent;
import com.vnhp.real.estate.res.TableName;
import java.util.LinkedList;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author FairyHunter
 */
public final class MyGridPane<T> extends GridPane {

    public MyGridPane() {
    }

    public void addTitles(LinkedList<String> titles, TableName tableName) {
        try {
            for (String title : titles) {
                this.addRow(0, new MyLabel(title, "Title", tableName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setRow(MyGridPane grid, LinkedList<T> ls, TableSetting settings, TableName tableName) {

        try {
            var index = 1;
            for (T data : ls) {
                var values = ConvertObject.Instance().dataToString(data);
                settings.setTableName(tableName);
                var id = ConvertObject.Instance().getInteger(data, ColumnId.class);
                addRow(grid, index, values, settings, id, data);
                index++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearAll() {
        this.getChildren().removeIf(node -> MyGridPane.getRowIndex(node) > 0);
    }

    public void addRow(MyGridPane grid, Integer index, LinkedList<String> values, TableSetting settings, Integer id, T data) {
        try {
            var i = 0;
            var tableName = settings.getTableName();
            for (Object label : settings.getLabels()) {
                var text = (String) label;
                if (text.equals(ObjectType.MyLabel.toString())) {
                    this.addRow(index, new MyLabel(values.get(i), "Label", tableName, index));
                }
                if (text.equals(ObjectType.MyButton.toString())) {
                    this.addRow(index, new MyButton(grid, values.get(i), "Button", tableName, settings, index, id, data, index));
                }
                if (text.equals(ObjectType.MyText.toString())) {
                    this.addRow(index, new MyText(values.get(i), "Text", tableName));
                }
                if (text.equals(ObjectType.MyImage.toString())) {
                    this.addRow(index, new MyImage(values.get(i), "Image", tableName, index));
                }
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //INVESTOR + PRODUCT
    public MyGridPane(Double height, Double width, String id, Double vgap, Double hgap) {

        this.setPrefHeight(height);
        this.setPrefWidth(width);
        this.setId(id);
        this.setVgap(vgap);
        this.setHgap(hgap);

    }

    public void createComponent(Integer index, Stage stage, LinkedList<T> iTab, TableName tablename) {
        for (T i : iTab) {
            if (tablename.equals(TableName.Investor)) {
                InvestorComponent investor = new InvestorComponent(i, stage);
                this.addRow(index, investor);
            }
//            if (tablename.equals(TableName.Product)) {
//                InvestorComponent investor = new InvestorComponent(i);
//                this.addRow(index, investor);
//            }
            if (tablename.equals(TableName.Utilities)) {
                var item = new UtilitiesCellView(i);
                this.addRow(index, item);
            }
        }
    }

    public void createComponent(Stage stage, LinkedList<LinkedList<T>> iTab, TableName tablename) {
        var index = 0;
        for (LinkedList<T> i : iTab) {
            createComponent(index++, stage, i, tablename);
        }
    }

}
