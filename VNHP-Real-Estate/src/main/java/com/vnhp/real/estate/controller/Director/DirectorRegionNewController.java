/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Region;
import com.vnhp.real.estate.model.entities.RegionStatus;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorRegionNewController {

    @FXML
    private TextField tfDirRegionNewName, tfDirRegionNewArea;

    @FXML
    private ComboBox<String> tfDirRegionNewRSName;
    @FXML
    private Label tfDirRegionNewResult;

    @FXML
    private Button btnDirRegionNew;

    public void init() {
        setupDefault();
        setup();
    }

    private void setupDefault() {
        var regionStatus = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().REGION_STATUS_ACTIVE, RegionStatus[].class));
        tfDirRegionNewRSName.getItems().addAll(regionStatus);
        tfDirRegionNewRSName.setValue((String) regionStatus.get(0));
    }

    private void setup() {
        btnDirRegionNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirRegionNewName.getText();
            var area = Float.parseFloat(tfDirRegionNewArea.getText());
            var rs_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new RegionStatus(tfDirRegionNewRSName.getValue()), RegionStatus[].class, 1), ColumnId.class);

            var data = new Region(name, area, rs_id);
            var retult = BaseDao.Instance().insert(data);
            tfDirRegionNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirRegionNewResult, 2);
        });
    }
}
