/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FairyHunter
 */
public class DbConnection {
    private static DbConnection instance = null;

    public Connection Connect() {
        try {
            class MyConnection {

                public Connection conn = null;
                private String URL_CONNECTION = "jdbc:sqlserver://115.73.212.222:8888;databaseName=vnhpt;encrypt=true;trustServerCertificate=true;";
                private String USER_NAME = "vnhpt";
                private String PASSWORD = "123";
                private String SQL_SERVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

                public MyConnection() {
                    try {
                        Class.forName(SQL_SERVER);
                        conn = DriverManager.getConnection(URL_CONNECTION, USER_NAME, PASSWORD);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SQLException ex) {
                        Logger.getLogger(DbConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            return new MyConnection().conn;

        } catch (Exception e) {
            String err = e.getMessage();
            System.out.println(err);
        }

        return null;
    }

    private DbConnection() {

    }

    public static DbConnection Instance() {
        if (instance == null) {
            instance = new DbConnection();
        }
        return instance;
    }
}