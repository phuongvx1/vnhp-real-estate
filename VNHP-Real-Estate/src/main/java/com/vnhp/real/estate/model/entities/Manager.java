/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;

/**
 *
 * @author FairyHunter
 */
public final class Manager {

    @ColumnId
    public Integer manager_id;
    @ColumnName
    public String manager_name;
    @ColumnStatus
    public Boolean manager_status;

    public void setmanager_id(Integer manager_id) {
        this.manager_id = manager_id;
    }

    public void setmanager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public void setmanager_status(Boolean manager_status) {
        this.manager_status = manager_status;
    }

    public Integer getmanager_id() {
        return manager_id;
    }

    public String getmanager_name() {
        return manager_name;
    }

    public Boolean getmanager_status() {
        return manager_status;
    }

    public Manager() {
    }

    public Manager(String manager_name) {
        this.manager_name = manager_name;
    }

    public Manager(Integer manager_id, String manager_name) {
        this.manager_id = manager_id;
        this.manager_name = manager_name;
    }

    public Manager(Integer manager_id, Boolean manager_status) {
        this.manager_id = manager_id;
        this.manager_status = manager_status;
    }
}
