/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.res.TimeManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProductStatusNewController {

    @FXML
    private TextField tfDirPSNewName;

    @FXML
    private Label tfDirPSNewResult;

    @FXML
    private Button btnDirPSNew;

    public void init() {
        setup();
    }

    private void setup() {
        btnDirPSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirPSNewName.getText();

            var data = new ProductStatus(name);
            var retult = BaseDao.Instance().insert(data);
            tfDirPSNewResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirPSNewResult, 2);
        });
    }
}
