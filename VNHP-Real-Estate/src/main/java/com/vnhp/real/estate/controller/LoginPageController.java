/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller;


import com.vnhp.real.estate.controller.Business.BusinessLayoutController;
import com.vnhp.real.estate.controller.Director.DirectorLayoutController;
import com.vnhp.real.estate.controller.human.HRLayoutController;
import com.vnhp.real.estate.res.AppSetting;
import com.vnhp.real.estate.res.StringValue;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class LoginPageController {

    String directorEmail = "1";
    String directorpassword = "1";

    String accountantEmail = "2";
    String accountantPassword = "2";

    String businessEmail = "3";
    String businessPassword = "3";

    String humanEmail = "4";
    String humanPassword = "4";

    @FXML
    private Button btnHomePageLogin;

    @FXML
    private Label lblHomePageResult;

    @FXML
    private TextField tfHomePageEmail;

    @FXML
    private TextField tfHomePagePassword;

    @FXML
    private AnchorPane mainPagePane;

    @FXML
    private Pane titlePane;

    @FXML
    private ImageView btnClose, btnMinimize;

    double x, y;

    @FXML
    public void init(Stage stage) {
        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);

        btnHomePageLogin.addEventHandler(ActionEvent.ACTION, eh -> {
            String inputEmail = tfHomePageEmail.getText();
            String inputPassword = tfHomePagePassword.getText();
            var checkDirector = inputEmail.equals(directorEmail) && inputPassword.equals(directorpassword);
            var checkAccountant = inputEmail.equals(accountantEmail) && inputPassword.equals(accountantPassword);
            var checkBusiness = inputEmail.equals(businessEmail) && inputPassword.equals(businessPassword);
            var checkHuman = inputEmail.equals(humanEmail) && inputPassword.equals(humanPassword);

            if (checkDirector) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().DIRECTOR_LAYOUT_PAGE));
                    AnchorPane dashboard = loader.load();
                    mainPagePane.getChildren().clear();
                    mainPagePane.getChildren().add(dashboard);
                    var control = (DirectorLayoutController) loader.getController();
                    control.init(stage);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(LoginPageController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (checkAccountant) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().ACCOUNTANT_LAYOUT_PAGE));
                    AnchorPane dashboard = loader.load();
                    mainPagePane.getChildren().clear();
                    mainPagePane.getChildren().add(dashboard);
                    var control = (AccountantLayoutController) loader.getController();
                    try {
                        control.init(stage);
                    } catch (SQLException ex) {
                        Logger.getLogger(LoginPageController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ParseException ex) {
                        Logger.getLogger(LoginPageController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (checkBusiness) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().BUSINESS_LAYOUT_PAGE));
                    AnchorPane dashboard = loader.load();
                    mainPagePane.getChildren().clear();
                    mainPagePane.getChildren().add(dashboard);
                    var control = (BusinessLayoutController) loader.getController();
                    control.init(stage);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (checkHuman) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.Instance().HUMAN_RESOURCE_LAYOUT));
                    AnchorPane dashboard = loader.load();
                    mainPagePane.getChildren().clear();
                    mainPagePane.getChildren().add(dashboard);
                    HRLayoutController control = (HRLayoutController) loader.getController();
                    control.init(stage);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                if (inputEmail.isEmpty() || inputEmail.isBlank() && inputPassword.isEmpty() || inputPassword.isBlank()) {
                    lblHomePageResult.setText("Please enter email and password.");
                } else {
                    if (!inputEmail.equals(directorEmail) || !inputEmail.equals(accountantEmail) || !inputEmail.equals(businessEmail)) {
                        lblHomePageResult.setText("Wrong email.");
                    }
                    if (!inputPassword.equals(directorpassword) || !inputPassword.equals(accountantPassword) || !inputPassword.equals(businessPassword)) {
                        lblHomePageResult.setText("Wrong password.");
                    }
                }
            }
        });
    }
}
