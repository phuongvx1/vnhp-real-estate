/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class ConstructionUnitStatus {

    public Integer index;
    @ColumnId
    public Integer cus_id;
    @ColumnName
    @DbColumn
    public String cus_name;
    @ColumnStatus
    @DbColumn
    public Boolean cus_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setCus_id(Integer cus_id) {
        this.cus_id = cus_id;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public void setCus_status(Boolean cus_status) {
        this.cus_status = cus_status;
    }

    public Integer getCus_id() {
        return cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public Boolean getCus_status() {
        return cus_status;
    }

    public ConstructionUnitStatus() {
    }

    public ConstructionUnitStatus(String cus_name) {
        this.cus_name = cus_name;
    }

    public ConstructionUnitStatus(Integer cus_id, String cus_name) {
        this.cus_id = cus_id;
        this.cus_name = cus_name;
    }

    public ConstructionUnitStatus(Integer cus_id, Boolean cus_status) {
        this.cus_id = cus_id;
        this.cus_status = cus_status;
    }

    public ConstructionUnitStatus(Integer cus_id, String cus_name, Boolean cus_status, Integer index) {
        this.cus_id = cus_id;
        this.cus_name = cus_name;
        this.cus_status = cus_status;
        this.index = index;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
