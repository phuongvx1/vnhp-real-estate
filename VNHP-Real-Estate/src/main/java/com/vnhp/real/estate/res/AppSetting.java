/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author FairyHunter
 */
public class AppSetting {

    private static AppSetting instance;

    private AppSetting() {

    }

    public static AppSetting Instance() {
        if (instance == null) {
            instance = new AppSetting();
        }

        return instance;
    }

    public void Setting(Stage stage, ImageView btnClose, ImageView btnMinimize) {
        btnClose.setOnMouseClicked(mouseEvent -> {
            stage.close();
        });

        btnMinimize.setOnMouseClicked(mouseEvent -> {
            stage.setIconified(true);

        });
    }
}
