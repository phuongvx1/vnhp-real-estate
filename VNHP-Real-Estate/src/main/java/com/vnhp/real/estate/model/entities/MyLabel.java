/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.TableName;
import javafx.scene.control.Label;

/**
 *
 * @author FairyHunter
 */
public final class MyLabel extends Label {

    public MyLabel(String title) {
        super();
        this.setText(title);
    }

    public MyLabel(String title, TableName tableName) {
        super();
        this.setText(title);
        this.getStyleClass().add(tableName + "-" + title);
    }

    public MyLabel(String value, String title, TableName tableName) {
        super();
        this.setText(value);
        this.getStyleClass().add(tableName + "-" + title);
        this.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
    }

    public MyLabel(String value, String title, TableName tableName, Integer index) {
        super();
        this.setText(value);
        if (index % 2 == 0) {
            this.getStyleClass().add(tableName + "-Even-" + title);
        } else {
            this.getStyleClass().add(tableName + "-Odd-" + title);
        }
        this.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
    }
}
