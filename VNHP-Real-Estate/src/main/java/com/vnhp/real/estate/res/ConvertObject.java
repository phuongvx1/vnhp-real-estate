/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.res;

import com.vnhp.real.estate.model.entities.Account;
import com.vnhp.real.estate.model.entities.AccountLoginStatus;
import com.vnhp.real.estate.model.entities.AccountStatus;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.ConstructionUnit;
import com.vnhp.real.estate.model.entities.ConstructionUnitStatus;
import com.vnhp.real.estate.model.entities.ContractStatus;
import com.vnhp.real.estate.model.entities.ContractType;
import com.vnhp.real.estate.model.entities.Customer;
import com.vnhp.real.estate.model.entities.CustomerStatus;
import com.vnhp.real.estate.model.entities.CustomerType;
import com.vnhp.real.estate.model.entities.Department;
import com.vnhp.real.estate.model.entities.DepartmentStatus;
import com.vnhp.real.estate.model.entities.Direction;
import com.vnhp.real.estate.model.entities.Gb_Status;
import com.vnhp.real.estate.model.entities.GroupBlocks;
import com.vnhp.real.estate.model.entities.Investor;
import com.vnhp.real.estate.model.entities.InvestorStatus;
import com.vnhp.real.estate.model.entities.PaymentPeriodStatus;
import com.vnhp.real.estate.model.entities.PaymentType;
import com.vnhp.real.estate.model.entities.Position;
import com.vnhp.real.estate.model.entities.Product;
import com.vnhp.real.estate.model.entities.ProductRevenue;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.model.entities.Project;
import com.vnhp.real.estate.model.entities.ProjectStatus;
import com.vnhp.real.estate.model.entities.ProjectType;
import com.vnhp.real.estate.model.entities.Region;
import com.vnhp.real.estate.model.entities.RegionStatus;
import com.vnhp.real.estate.model.entities.SaleContract;
import com.vnhp.real.estate.model.entities.Statistic;
import com.vnhp.real.estate.model.entities.Utilities;
import static com.vnhp.real.estate.res.TableName.Account;
import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author FairyHunter
 */
public class ConvertObject<T> {

    private static ConvertObject instance;

    private ConvertObject() {

    }

    public static ConvertObject Instance() {
        if (instance == null) {
            instance = new ConvertObject();
        }

        return instance;
    }

    public LinkedList<String> dataToString(T data) {
        try {
            var values = new LinkedList<String>();
            var dataFields = data.getClass().getFields();

            for (var dataField : dataFields) {
                if (dataField.get(data) == null || dataField.isAnnotationPresent(ColumnId.class)) {
                    continue;
                }
                values.add(dataField.get(data).toString());
            }

            return values;
        } catch (IllegalArgumentException | IllegalAccessException e) {
        }
        return null;
    }

    public String getData(T data, Class<? extends Annotation> className) {
        try {
            var value = "";

            var dataFields = data.getClass().getDeclaredFields();

            for (var dataField : dataFields) {
                if (dataField.isAnnotationPresent(className)) {
                    value = dataField.get(data).toString();
                }
            }

            return value;
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ConvertObject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Integer getInteger(T data, Class<? extends Annotation> className) {
        var value = 0;

        try {
            var dataFields = data.getClass().getDeclaredFields();

            for (var dataField : dataFields) {
                if (dataField.isAnnotationPresent(className)) {
                    value = Integer.parseInt(dataField.get(data).toString());
                }
            }
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ConvertObject.class.getName()).log(Level.SEVERE, null, ex);
        }

        return value;
    }

    public Boolean getBool(T data, Class<? extends Annotation> className) {
        var value = false;

        try {
            var dataFields = data.getClass().getDeclaredFields();

            for (var dataField : dataFields) {
                if (dataField.isAnnotationPresent(className)) {
                    value = Boolean.valueOf(dataField.get(data).toString());
                }
            }
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ConvertObject.class.getName()).log(Level.SEVERE, null, ex);
        }

        return !value;
    }

    public Integer getInteger(T[] data, Class<? extends Annotation> className) {
        var value = 0;

        for (T t : data) {
            try {
                var dataFields = t.getClass().getDeclaredFields();

                for (var dataField : dataFields) {
                    if (dataField.isAnnotationPresent(className)) {
                        value = Integer.parseInt(dataField.get(t).toString());
                    }
                }
                return value;

            } catch (IllegalAccessException ex) {
                Logger.getLogger(ConvertObject.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public LinkedList<T> toList(T[] data) {
        var value = new LinkedList<T>();

        for (T t : data) {
            value.add(t);
        }

        return value;
    }

    public LinkedList<Object> toList(T[] data, String service) {
        var listData = new LinkedList<Object>();
        listData.clear();
        var index = 1;

        if (service.equals(ServerService.Instance().GET_CONTRUCTION_UNIT)) {
            index = 1;
            for (T t : data) {
                var tmp = (ConstructionUnit) t;
                var newdata = new ConstructionUnit(tmp.getCu_id(), tmp.getCu_name(), tmp.getCu_email(), tmp.getCu_address(), tmp.getCu_logo(), tmp.getCu_done_project_quatity(), tmp.getCu_contact(), tmp.getCus_name(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_CONTRUCTION_UNIT_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (ConstructionUnitStatus) t;
                var newdata = new ConstructionUnitStatus(tmp.getCus_id(), tmp.getCus_name(), tmp.getCus_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_DIRECTION)) {
            index = 1;
            for (T t : data) {
                var tmp = (Direction) t;
                var newdata = new Direction(tmp.getDirection_id(), tmp.getDirection_name(), tmp.getDirection_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_PROJECT_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (ProjectStatus) t;
                var newdata = new ProjectStatus(tmp.getPs_id(), tmp.getPs_name(), tmp.getPs_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_PROJECT_TYPE)) {
            index = 1;
            for (T t : data) {
                var tmp = (ProjectType) t;
                var newdata = new ProjectType(tmp.getPt_id(), tmp.getPt_name(), tmp.getPt_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_REGION)) {
            index = 1;
            for (T t : data) {
                var tmp = (Region) t;
                var newdata = new Region(tmp.getRegion_id(), tmp.getRegion_name(), tmp.getRegion_area(), tmp.getRs_name(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_INVESTOR_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (InvestorStatus) t;
                var newdata = new InvestorStatus(tmp.getIs_id(), tmp.getIs_name(), tmp.getIs_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_PAYMENT_TYPE)) {
            index = 1;
            for (T t : data) {
                var tmp = (PaymentType) t;
                var newdata = new PaymentType(tmp.getPmt_id(), tmp.getPmt_name(), tmp.getPmt_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_PRODUCT_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (ProductStatus) t;
                var newdata = new ProductStatus(tmp.getPs_id(), tmp.getPs_name(), tmp.getPs_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_INVESTOR)) {
            index = 1;
            for (T t : data) {
                var tmp = (Investor) t;
                var newdata = new Investor(tmp.getInvestor_id(), tmp.getInvestor_name(), tmp.getInvestor_logo(), tmp.getInvestor_upcoming_project_quantity(), tmp.getInvestor_open_project_quantity(),
                        tmp.getInvestor_done_project_quantity(), tmp.getInvestor_contact(), tmp.getInvestor_email(), tmp.getInvestor_address(), tmp.getIs_name(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_PRODUCT)) {
            index = 1;
            for (T t : data) {
                var tmp = (Product) t;
                var newdata = new Product(tmp.getProduct_id(), tmp.getProduct_name(), tmp.getProduct_price(), tmp.getBlock_name(), tmp.getRegion_name(), tmp.getPs_name(), tmp.getDirection_name(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_GB_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (Gb_Status) t;
                var newdata = new Gb_Status(tmp.getGbs_id(), tmp.getGbs_name(), tmp.getGbs_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_UTILITIES)) {
            index = 1;
            for (T t : data) {
                var tmp = (Utilities) t;
                var newdata = new Utilities(tmp.getUtilities_id(), tmp.getUtilities_name(), tmp.getUtilities_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_BLOCK_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (B_Status) t;
                var newdata = new B_Status(tmp.getBs_id(), tmp.getBs_name(), tmp.getBs_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_GROUP_BLOCK)) {
            index = 1;
            for (T t : data) {
                var tmp = (GroupBlocks) t;
                var newdata = new GroupBlocks(tmp.getGb_id(), tmp.getGb_name(), tmp.getProject_name(), tmp.getGb_utilities(), tmp.getGbs_name(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_BLOCK)) {
            index = 1;
            for (T t : data) {
                var tmp = (Blocks) t;
                var newdata = new Blocks(tmp.getBlock_id(), tmp.getBlock_name(), tmp.getGb_name(), tmp.getBlock_utilities(), tmp.getBs_name(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_CONTRACT_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (ContractStatus) t;
                var newdata = new ContractStatus(tmp.getCons_id(), tmp.getCons_name(), tmp.getCons_status(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_CONTRACT_TYPE)) {
            index = 1;
            for (T t : data) {
                var tmp = (ContractType) t;
                var newdata = new ContractType(tmp.getCont_id(), tmp.getCont_name(), tmp.getCont_status(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_CUSTOMER)) {
            index = 1;
            for (T t : data) {
                var tmp = (Customer) t;
                var newdata = new Customer(tmp.getCustomer_id(), tmp.getCustomer_name(), tmp.getCustomer_email(), tmp.getCustomer_contact(), tmp.getCustomer_dob(), tmp.getCustomer_address(), tmp.getCustomer_current_address(), tmp.getCustomer_cccd(),
                        tmp.getCustomer_cccd_date(), tmp.getCustomer_cccd_place(), tmp.getCt_id(), tmp.getCs_name(), tmp.getCustomer_note(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_CUSTOMER_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (CustomerStatus) t;
                var newdata = new CustomerStatus(tmp.getCs_id(), tmp.getCs_name(), tmp.getCs_status(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_CUSTOMER_TYPE)) {
            index = 1;
            for (T t : data) {
                var tmp = (CustomerType) t;
                var newdata = new CustomerType(tmp.getCt_id(), tmp.getCt_name(), tmp.getCt_status(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_PPS_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (PaymentPeriodStatus) t;
                var newdata = new PaymentPeriodStatus(tmp.getPps_id(), tmp.getPps_name(), tmp.getPps_status(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_REGION_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (RegionStatus) t;
                var newdata = new RegionStatus(tmp.getRs_id(), tmp.getRs_name(), tmp.getRs_status(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_PROJECT)) {
            index = 1;
            for (T t : data) {
                var tmp = (Project) t;
                var newdata = new Project(tmp.getProject_id(), tmp.getProject_name(), tmp.getProject_location(), tmp.getProject_area(), tmp.getProject_juridical(), tmp.getPs_name(), index++);

                listData.add(newdata);
            }
        }
        
//        if (service.equals(ServerService.Instance().GET_ACCOUNT)) {
//            index = 1;
//            for (T t : data) {
//                var tmp = (Account) t;
//                var newdata = new Account(tmp.getAccount_id(), tmp.getAccount_name(), tmp.getAccount_contact(), tmp.getAccount_email(),tmp.getAccount_address(),tmp.getAccount_password(),tmp.getas_name(), tmp.getDeparment_name(), tmp.getPosition_name(),index++);
//
//                listData.add(newdata);
//            }
//        }
        if (service.equals(ServerService.Instance().GET_AC_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (AccountStatus) t;
                var newdata = new AccountStatus(tmp.getas_id(), tmp.getas_name(), tmp.getas_status(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_ACLOGIN_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (AccountLoginStatus) t;
                var newdata = new AccountLoginStatus(tmp.getAls_id(), tmp.getAls_name(), tmp.getAls_status(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_DEPARTMENT_STATUS)) {
            index = 1;
            for (T t : data) {
                var tmp = (DepartmentStatus) t;
                var newdata = new DepartmentStatus(tmp.getds_id(), tmp.getds_name(), tmp.getds_status(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_DEPARTMENT)) {
            index = 1;
            for (T t : data) {
                var tmp = (Department) t;
                var newdata = new Department(tmp.getDepartment_id(), tmp.getDepartment_name(), tmp.getds_name(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_POSITION)) {
            index = 1;
            for (T t : data) {
                var tmp = (Position) t;
                var newdata = new Position(tmp.getPosition_id(), tmp.getPosition_name(), tmp.getPosition_status(), index++);

                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_TOP_10_CUSTOMER_REVENUE)) {
            index = 1;
            for (T t : data) {
                var tmp = (Customer) t;
                var newdata = new Customer(tmp.getCustomer_id(), tmp.getCustomer_name(), tmp.getCustomer_contact(), tmp.getSc_total_payment(), index++);
                listData.add(newdata);
            }
        }

        if (service.equals(ServerService.Instance().GET_SALE_CONTRACT)) {
            index = 1;
            for (T t : data) {
                var tmp = (SaleContract) t;
                var newdata = new SaleContract(tmp.getSc_id(), tmp.getSc_time_open(), tmp.getSc_number(), tmp.getSc_total_payment(), tmp.getSaler_name(), tmp.getPayer_id(), tmp.getProduct_name(), tmp.getCont_name(),
                        tmp.getCons_name(), tmp.getProduct_id(), tmp.getCont_id(), tmp.getCons_id(), tmp.getSaler_id(), index++);
                listData.add(newdata);
            }
        }

        return listData;
    }

    public LinkedList<String> toListString(T[] data) {
        var value = new LinkedList<String>();

        for (T t : data) {
            var dataFields = t.getClass().getDeclaredFields();

            for (var dataField : dataFields) {
                if (dataField.isAnnotationPresent(ColumnName.class)) {
                    try {
                        value.add(dataField.get(t).toString());
                    } catch (IllegalArgumentException ex) {
                        Logger.getLogger(ConvertObject.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(ConvertObject.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        return value;
    }

    public LinkedList<LinkedList<T>> toLists(T[] data, Integer index) {
        var value = new LinkedList<LinkedList<T>>();
        var tmp = new LinkedList<T>();
        var tmpIndex = 1;

        for (T t : data) {
            tmp.add(t);
            if (tmpIndex == index) {
                var tmp1 = new LinkedList<T>(tmp);
                value.add(tmp1);
                tmpIndex = 1;
                tmp.clear();
                continue;
            }
            tmpIndex++;
        }

        return value;
    }

    public ObservableList<Object> toObservableList(LinkedList<T> data) {
        var value = FXCollections.observableArrayList();
        value.addAll(data);

        return value;
    }

    public T toData(T[] data) {
        for (T t : data) {
            return t;
        }

        return null;
    }

    public Integer toQuantity(T[] data, String service, String name) {
        var quantity = 0;
        if (service.equals(ServerService.Instance().GET_STATISTIC_QUANTITY)) {
            for (T t : data) {
                var tmp = (Statistic) t;
                if (tmp.getStatistic_name().equals(name)) {
                    quantity = tmp.getStatistic_quantity();
                    break;
                }
            }
        }

        return quantity;
    }

    public Float toRevenue(T[] data, String service) {
        var quantity = (float) 0;
        if (service.equals(ServerService.Instance().GET_PRODUCT_REVENUE)) {
            for (T t : data) {
                var tmp = (ProductRevenue) t;
                quantity = tmp.getPr_revenue();
            }
        }

        return quantity;
    }
}
