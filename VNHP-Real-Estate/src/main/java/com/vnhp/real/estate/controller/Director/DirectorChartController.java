/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Customer;
import com.vnhp.real.estate.model.entities.MyTable;
import com.vnhp.real.estate.model.entities.ProductRevenue;
import com.vnhp.real.estate.model.entities.RevenueDept;
import com.vnhp.real.estate.model.entities.Statistic;
import com.vnhp.real.estate.model.entities.StatisticPanel;
import com.vnhp.real.estate.res.ChartManager;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.RevenuePane;
import com.vnhp.real.estate.res.ServerService;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TimeManager;
import java.util.ArrayList;
import java.util.LinkedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorChartController {

    @FXML
    private AnchorPane pDirDashboardView;

    public void init() {
        setup();
    }

    private void setup() {
        var statisticData = BaseDao.Instance().getListData(StringValue.Instance().STATISTIC_ALL, Statistic[].class);
        var years = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().REVENUE_DEPT, RevenueDept[].class));

        var projectQuantity = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "Project");
        var project = new StatisticPanel(StringValue.Instance().PROJECT_ICON, "Project", projectQuantity.toString());
        project.setLayoutX(20);
        project.setLayoutY(10);
        var productQuantity = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "Product");
        var product = new StatisticPanel(StringValue.Instance().PRODUCT_ICON, "Product", productQuantity.toString());
        product.setLayoutX(165);
        product.setLayoutY(10);
        var customerQuantity = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "Customer");
        var potencialCustomerQuantity = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "Potencial Customer");
        var customer = new StatisticPanel(StringValue.Instance().CUSTOMER_ICON, "Customer", potencialCustomerQuantity + "/" + customerQuantity);
        customer.setLayoutX(20);
        customer.setLayoutY(70);
        var staffprojectQuantity = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "Account");
        var staff = new StatisticPanel(StringValue.Instance().STAFF_ICON, "Staff", staffprojectQuantity.toString());
        staff.setLayoutX(165);
        staff.setLayoutY(70);

        var totalProductRevenue = ConvertObject.Instance().toRevenue(BaseDao.Instance().getListData(StringValue.Instance().REVENUE_TOTAL, ProductRevenue[].class), ServerService.Instance().GET_PRODUCT_REVENUE);
        var totalRevenue = new StatisticPanel(StringValue.Instance().REVENUE_ICON, "Total Revenue", "$ " + totalProductRevenue, 280);
        totalRevenue.setLayoutX(20);
        totalRevenue.setLayoutY(130);

        var productLineChartPane = new Pane();
        productLineChartPane.setPrefHeight(170);
        productLineChartPane.setPrefWidth(530);
        productLineChartPane.getStyleClass().add("Dashboard-Background");
        productLineChartPane.setLayoutX(310);
        productLineChartPane.setLayoutY(10);

        var currentYear = TimeManager.Instance().getCurrentYear();
        var productLineChartData = ChartManager.Instance().convertData(String.valueOf(currentYear), ServerService.Instance().GET_PRODUCT_STATISTIC);

        var productLineChart = ChartManager.Instance().lineChart("Product over Year " + currentYear, productLineChartData);
        productLineChart.setPrefHeight(170);
        var productLineChartCb = new ComboBox<String>();
        productLineChartCb.setLayoutX(400);
        productLineChartCb.setLayoutY(140);
        productLineChartCb.getItems().addAll(years);
        productLineChartCb.setValue((String) years.get(0));
        productLineChartCb.addEventHandler(ActionEvent.ACTION, eh -> {
            var newLineChartData = ChartManager.Instance().convertData(productLineChartCb.getValue(), ServerService.Instance().GET_PRODUCT_STATISTIC);
            var newLineChart = ChartManager.Instance().lineChart("Product over Year " + productLineChartCb.getValue(), newLineChartData);
            newLineChart.setPrefHeight(170);
            productLineChartPane.getChildren().clear();
            productLineChartPane.getChildren().addAll(newLineChart, productLineChartCb);
        });
        productLineChartPane.getChildren().addAll(productLineChart, productLineChartCb);

        var monthChoiceBox = new LinkedList<String>();
        var tmpContent = ConvertObject.Instance().toRevenue(BaseDao.Instance().getListData(StringValue.Instance().REVENUE_CURRENT_MONTH, ProductRevenue[].class), ServerService.Instance().GET_PRODUCT_REVENUE);
        monthChoiceBox.add(StringValue.Instance().CURRENT_MONTH);
        monthChoiceBox.add(StringValue.Instance().LAST_3_MONTHS);
        monthChoiceBox.add(StringValue.Instance().LAST_6_MONTHS);
        var monthRevenue = new RevenuePane(StringValue.Instance().REVENUE_ICON, "Revenue of Month", "$ " + tmpContent.toString(), monthChoiceBox, 170, 340, "Dashboard-Background");
        monthRevenue.setLayoutX(847.5);
        monthRevenue.setLayoutY(10);

        tmpContent = ConvertObject.Instance().toRevenue(BaseDao.Instance().getListData(StringValue.Instance().REVENUE_CURRENT_YEAR, ProductRevenue[].class), ServerService.Instance().GET_PRODUCT_REVENUE);
        var yearChoiceBox = new LinkedList<String>();
        yearChoiceBox.add(StringValue.Instance().CURRENT_YEAR);
        yearChoiceBox.add(StringValue.Instance().LAST_3_YEARS);
        yearChoiceBox.add(StringValue.Instance().LAST_5_YEARS);
        yearChoiceBox.add(StringValue.Instance().LAST_10_YEARS);

        var YearRevenue = new RevenuePane(StringValue.Instance().REVENUE_ICON, "Revenue of Year", "$ " + tmpContent.toString(), yearChoiceBox, 170, 340, "Dashboard-Background");
        YearRevenue.setLayoutX(1195);
        YearRevenue.setLayoutY(10);

        var productPieChartPane = new Pane();
        productPieChartPane.setPrefHeight(475);
        productPieChartPane.setPrefWidth(280);
        productPieChartPane.getStyleClass().add("Dashboard-Background");
        productPieChartPane.setLayoutX(20);
        productPieChartPane.setLayoutY(185);

        var productPieChartData = new ArrayList<PieChart.Data>();
        var landQuantity = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "Land");
        var houseQuantity = ConvertObject.Instance().toQuantity(statisticData, ServerService.Instance().GET_STATISTIC_QUANTITY, "House");
        productPieChartData.add(new PieChart.Data("Land", landQuantity));
        productPieChartData.add(new PieChart.Data("House", houseQuantity));
        var caption = new Label();
        final PieChart productPieChart = ChartManager.Instance().pieChart("Sold Products", productPieChartData, caption);
        productPieChart.setLayoutX(-110);
        productPieChart.setLayoutY(25);
        productPieChartPane.getChildren().addAll(productPieChart, caption);

        var titles = new LinkedList<TableColumn<Customer, String>>();
        var index = new TableColumn("ID");
        index.setMinWidth(40);
        index.setCellValueFactory(new PropertyValueFactory<>("index"));
        var name = new TableColumn("Name");
        name.setMinWidth(200);
        name.setCellValueFactory(new PropertyValueFactory<>("customer_name"));
        var contact = new TableColumn("Contact");
        contact.setMinWidth(139);
        contact.setCellValueFactory(new PropertyValueFactory<>("customer_contact"));
        var payment = new TableColumn("Payment");
        payment.setMinWidth(150);
        payment.setCellValueFactory(new PropertyValueFactory<Customer, String>("sc_total_payment"));

        titles.add(index);
        titles.add(name);
        titles.add(contact);
        titles.add(payment);

        var selectTopCustomer = new LinkedList<String>();
        selectTopCustomer.add("Top 10");
        selectTopCustomer.add("Top 50");
        selectTopCustomer.add("Top 100");

        var listData = ConvertObject.Instance().toObservableList(ConvertObject.Instance().toList(BaseDao.Instance().getListData("Top 10" + StringValue.Instance().CUSTOMER_TOP_PAYMENT, Customer[].class), ServerService.Instance().GET_TOP_10_CUSTOMER_REVENUE));
        var customerRevenue = new MyTable("Top 10 Customer", titles, listData, 475, 530, "Dashboard-Background",selectTopCustomer);
        customerRevenue.setLayoutX(310);
        customerRevenue.setLayoutY(185);

        var productBarChartPane = new Pane();
        productBarChartPane.setPrefHeight(475);
        productBarChartPane.setPrefWidth(685);
        productBarChartPane.getStyleClass().add("Dashboard-Background");
        productBarChartPane.setLayoutX(850);
        productBarChartPane.setLayoutY(185);

        var netRevenue = ChartManager.Instance().convertData(years, ServerService.Instance().GET_NET_REVENUE);
        netRevenue.setName("Net Revenue");
        var dept = ChartManager.Instance().convertData(years, ServerService.Instance().GET_DEPT);
        dept.setName("Dept");

        var data2 = new ArrayList<XYChart.Series>();
        data2.add(netRevenue);
        data2.add(dept);
        var productBarChart = ChartManager.Instance().barChart("Year", "Revenue/Dept", data2);
        productBarChart.setTitle("Net Revenue/Dept");
        productBarChart.setLayoutX(70);
        productBarChart.setLayoutY(25);
        productBarChart.setScaleX(1.1);
        productBarChart.setScaleY(1.1);

        productBarChartPane.getChildren().add(productBarChart);
        pDirDashboardView.getChildren().addAll(project, product, customer, staff, totalRevenue, productLineChartPane, monthRevenue, YearRevenue, productPieChartPane, customerRevenue, productBarChartPane);
    }
}
