/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.DataCellView;

import com.vnhp.real.estate.model.entities.MyLabel;
import com.vnhp.real.estate.model.entities.Utilities;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.UtilitiesManager;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 *
 * @author FairyHunter
 * @param <T>
 */
public class UtilitiesCellView<T> extends Pane {

    private String name;

    public UtilitiesCellView(T data) {
        this.prefHeight(30);
        this.prefWidth(200);
        var hBox = new HBox();
        this.name = ConvertObject.Instance().getData(data, ColumnName.class);
        var text = new MyLabel(this.name);
        var checkBox = new CheckBox();

        checkBox.setOnMouseClicked(eh -> {
            if (checkBox.isSelected()) {
                UtilitiesManager.Utilities.add(name);
            } else {
                UtilitiesManager.Utilities.remove(name);
            }
        });

        hBox.getChildren().addAll(text, checkBox);
        this.getChildren().add(hBox);
    }
}
