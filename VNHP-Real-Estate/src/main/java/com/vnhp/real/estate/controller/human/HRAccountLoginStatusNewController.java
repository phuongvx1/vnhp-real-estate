/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.human;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.AccountLoginStatus;
import com.vnhp.real.estate.res.TimeManager;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Wil$ol
 */
public class HRAccountLoginStatusNewController  {
    @FXML
    public TextField tfHrALSNew;

    @FXML
    private Label tfHrALSResult;

    @FXML
    private Button btnAddALSNew;
   

    public void init() {
        setup();
    }

    private void setup() {
        btnAddALSNew.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfHrALSNew.getText();

            var data = new AccountLoginStatus(name);
            var retult = BaseDao.Instance().insert(data);
            tfHrALSResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfHrALSResult, 2);
        });
    }
}      
