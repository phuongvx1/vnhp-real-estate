/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Investor;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorInvestorUpdateController {

    @FXML
    private TextField tfDirInvestorUpdateName, tfDirInvestorUpdateLogo, tfDirInvestorUpdateContact, tfDirInvestorUpdateEmail, tfDirInvestorUpdateAddress;
    @FXML
    HTMLEditor tfDirInvestorUpdateInfo;

    @FXML
    private Label tfDirInvestorUpdateResult;

    @FXML
    private Button btnDirInvestorUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Investor data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(Investor data) {
        tfDirInvestorUpdateName.setText(data.getInvestor_name());
        tfDirInvestorUpdateLogo.setText(data.getInvestor_logo());
        tfDirInvestorUpdateContact.setText(data.getInvestor_contact());
        tfDirInvestorUpdateEmail.setText(data.getInvestor_email());
        tfDirInvestorUpdateAddress.setText(data.getInvestor_address());
        tfDirInvestorUpdateInfo.setHtmlText(data.getInvestor_info());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Investor data) {
        btnDirInvestorUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirInvestorUpdateName.getText();
            var logo = tfDirInvestorUpdateLogo.getText();
            var contact = tfDirInvestorUpdateContact.getText();
            var email = tfDirInvestorUpdateEmail.getText();
            var address = tfDirInvestorUpdateAddress.getText();
            var info = tfDirInvestorUpdateInfo.getHtmlText();

            var newData = new Investor(data.getInvestor_id(), name, logo, contact, email, address, info);
            var retult = BaseDao.Instance().update(newData);
            tfDirInvestorUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirInvestorUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Investor) data;
            tmp.setInvestor_name(name);

            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getInvestor_id(), tmp);
        });
    }

}
