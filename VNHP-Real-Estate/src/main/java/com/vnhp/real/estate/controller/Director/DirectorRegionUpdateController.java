/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.GroupBlocks;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.Region;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import com.vnhp.real.estate.res.UtilitiesManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorRegionUpdateController {

    @FXML
    private TextField tfDirRegionUpdateArea, tfDirRegionUpdateName;
    
    @FXML
    private Label tfDirRegionUpdateResult;

    @FXML
    private Button btnDirRegionUpdate;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Region data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(Region data) {
        tfDirRegionUpdateName.setText(data.getRegion_name());
        tfDirRegionUpdateArea.setText(data.getRegion_area().toString());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Region data) {
        btnDirRegionUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirRegionUpdateName.getText();
            var area = Float.parseFloat(tfDirRegionUpdateArea.getText());
            var newData = new Region(data.getRegion_id(), name, area);
            var retult = BaseDao.Instance().update(newData);
            tfDirRegionUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirRegionUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Region) data;
            tmp.setRegion_name(name);
            tmp.setRegion_area(area);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getRegion_id(), tmp);
        });
    }

}
