/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.ConstructionUnit;
import com.vnhp.real.estate.model.entities.GroupBlocks;
import com.vnhp.real.estate.model.entities.Investor;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.Project;
import com.vnhp.real.estate.model.entities.ProjectType;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import com.vnhp.real.estate.res.UtilitiesManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProjectUpdateController {

    @FXML
    TextField tfDirProjectUpdateName, tfDirProjectUpdateLocation, tfDirProjectUpdateArea, tfDirProjectUpdateJuridical, tfDirProjectUpdateDensity;

    @FXML
    Pane tfDirProjectUpdateUtilities;

    @FXML
    ComboBox<String> tfDirUpdateType, tfDirProjectUpdateCU, tfDirProjectUpdateInvestor;

    @FXML
    HTMLEditor tfDirProjectUpdateInfo;

    @FXML
    Button btnDirProjectUpdate;

    @FXML
    Label tfDirProjectUpdateResult;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Project data) {
        setupDefault(subStage, data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }

    private void setupDefault(Stage stage, Project data) {
        tfDirProjectUpdateName.setText(data.getProject_name());
        tfDirProjectUpdateLocation.setText(data.getProject_location());
        tfDirProjectUpdateArea.setText(data.getProject_area().toString());
        tfDirProjectUpdateJuridical.setText(data.getProject_juridical());
        tfDirProjectUpdateDensity.setText(data.getProject_density());
        tfDirProjectUpdateInfo.setHtmlText(data.getProject_info());

        tfDirProjectUpdateUtilities.getChildren().clear();
        tfDirProjectUpdateUtilities.getChildren().add(UtilitiesManager.Instance().Utilities(stage));

        var projectCU = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().CONTRUCTION_ACTIVE, ConstructionUnit[].class));
        tfDirProjectUpdateCU.getItems().addAll(projectCU);
        tfDirProjectUpdateCU.setValue(data.getCu_name());

        var investor = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().INVESTOR_ACTIVE, Investor[].class));
        tfDirProjectUpdateInvestor.getItems().addAll(investor);
        tfDirProjectUpdateInvestor.setValue(data.getInvestor_name());

        var type = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_TYPE, ProjectType[].class));
        tfDirUpdateType.getItems().addAll(type);
        tfDirUpdateType.setValue(data.getPt_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, Project data) {
        btnDirProjectUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirProjectUpdateName.getText();
            var location = tfDirProjectUpdateLocation.getText();
            var area = Float.parseFloat(tfDirProjectUpdateArea.getText());
            var juridical = tfDirProjectUpdateJuridical.getText();
            var density = tfDirProjectUpdateDensity.getText();
            var pInfo = tfDirProjectUpdateInfo.getHtmlText();
            var utilities = UtilitiesManager.Utilities.toString().substring(1, UtilitiesManager.Utilities.toString().length() - 1);
            var cu_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ConstructionUnit(tfDirProjectUpdateCU.getValue()), ConstructionUnit[].class, 1), ColumnId.class);
            var investor_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new Investor(tfDirProjectUpdateInvestor.getValue()), Investor[].class, 1), ColumnId.class);
            var pt_id = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ProjectType(tfDirUpdateType.getValue()), ProjectType[].class, 1), ColumnId.class);

            var newData = new Project(data.getProject_id(), name, pInfo, location, area, juridical, density, utilities, cu_id, investor_id, pt_id);
            var retult = BaseDao.Instance().update(newData);
            tfDirProjectUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirProjectUpdateResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Project) data;
            tmp.setProject_name(name);
            tmp.setProject_location(location);
            tmp.setProject_area(area);
            tmp.setProject_juridical(juridical);
//            tmp.setProject_info(pInfo);
//            tmp.setProject_attached_utilities(utilities);
//            tmp.setProject_density(density);
//            tmp.setCu_name(tfDirProjectUpdateCU.getValue());
//            tmp.setInvestor_name(tfDirProjectUpdateInvestor.getValue());
//            tmp.setPt_name(tfDirUpdateType.getValue());

            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getProject_id(), tmp);
        });
    }

}
