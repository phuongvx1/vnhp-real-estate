/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProductStatusUpdateController {
    
    @FXML
    private TextField tfDirPSUpdateName;
    
    @FXML
    private Label tfDirPSUpdateResult;
    
    @FXML
    private Button btnDirPSUpdate;
    
    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, ProductStatus data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }
    
    private void setupDefault(ProductStatus data) {
        tfDirPSUpdateName.setText(data.getPs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, ProductStatus data) {
        btnDirPSUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirPSUpdateName.getText();
            
            var newData = new ProductStatus(data.getPs_id(),name);
            var retult = BaseDao.Instance().update(newData);
            tfDirPSUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirPSUpdateResult, 2);
            
            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (ProductStatus) data;
            tmp.setPs_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getPs_id(), tmp);
        });
    }
}
