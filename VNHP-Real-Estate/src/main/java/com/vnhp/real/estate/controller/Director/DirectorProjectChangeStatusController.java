/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.Project;
import com.vnhp.real.estate.model.entities.ProjectStatus;
import com.vnhp.real.estate.model.entities.ProjectType;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorProjectChangeStatusController {

    @FXML
    private Label tfDirProjectCSName;
    @FXML
    private ComboBox<String> tfDirProjectCSStatus;
    @FXML
    public Label tfDirProjectCSResult;
    @FXML
    private Button btnDirProjectCS;
//    @FXML
//    private ImageView btnClose, btnMinimize;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, Project data) {
//        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        setupDefault(data);

        setup(grid, gridIndex, tableSetting, data);
    }

    private void setupDefault(Project data) {
        tfDirProjectCSName.setText(data.getProject_name());

        var status = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().PROJECT_STATUS, ProjectStatus[].class));
        tfDirProjectCSStatus.getItems().addAll(status);
        tfDirProjectCSStatus.setValue(data.getPs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Project data) {
        btnDirProjectCS.addEventHandler(ActionEvent.ACTION, eh -> {
            var status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new ProjectStatus(tfDirProjectCSStatus.getValue()), ProjectStatus[].class, 1), ColumnId.class);
            var tmpData = new Project(data.getProject_id(), status);
            var retult = BaseDao.Instance().update(tmpData);
            tfDirProjectCSResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirProjectCSResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Project) data;
            tmp.setPs_name(tfDirProjectCSStatus.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getProject_id(), tmp);
        });

    }
}
