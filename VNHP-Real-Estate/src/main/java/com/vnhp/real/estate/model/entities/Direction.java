/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class Direction {

    public Integer index;
    @ColumnId
    public Integer direction_id;
    @ColumnName
    @DbColumn
    public String direction_name;
    @ColumnStatus
    @DbColumn
    public Boolean direction_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setDirection_id(Integer direction_id) {
        this.direction_id = direction_id;
    }

    public void setDirection_name(String direction_name) {
        this.direction_name = direction_name;
    }

    public void setDirection_status(Boolean direction_status) {
        this.direction_status = direction_status;
    }

    public Integer getDirection_id() {
        return direction_id;
    }

    public String getDirection_name() {
        return direction_name;
    }

    public Boolean getDirection_status() {
        return direction_status;
    }

    public Direction() {
    }

    public Direction(String direction_name) {
        this.direction_name = direction_name;
    }

    public Direction(Integer direction_id, String direction_name) {
        this.direction_id = direction_id;
        this.direction_name = direction_name;
    }

    public Direction(Integer direction_id, Boolean direction_status) {
        this.direction_id = direction_id;
        this.direction_status = direction_status;
    }

    public Direction(Integer direction_id, String direction_name, Boolean direction_status, Integer index) {
        this.direction_id = direction_id;
        this.direction_name = direction_name;
        this.direction_status = direction_status;
        this.index = index;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }
}
