/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.model.entities.PaymentPeriodStatus;
import com.vnhp.real.estate.model.entities.ProductStatus;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorPpsUpdateController {

     @FXML
    private TextField tfDirPPSUpdateName;
    
    @FXML
    private Label tfDirPPSUpdateResult;
    
    @FXML
    private Button btnDirPPSUpdate;
    
    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, PaymentPeriodStatus data) {
        setupDefault(data);
        setup(grid, gridIndex, tableSetting, subStage, data);
    }
    
    private void setupDefault(PaymentPeriodStatus data) {
        tfDirPPSUpdateName.setText(data.getPps_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage subStage, PaymentPeriodStatus data) {
        btnDirPPSUpdate.addEventHandler(ActionEvent.ACTION, eh -> {
            var name = tfDirPPSUpdateName.getText();
            
            var newData = new PaymentPeriodStatus(data.getPps_id(),name);
            var retult = BaseDao.Instance().update(newData);
            tfDirPPSUpdateResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirPPSUpdateResult, 2);
            
            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (PaymentPeriodStatus) data;
            tmp.setPps_name(name);
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getPps_id(), tmp);
        });
    }

}
