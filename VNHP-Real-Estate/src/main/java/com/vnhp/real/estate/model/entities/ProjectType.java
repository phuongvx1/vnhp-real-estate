/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.real.estate.model.entities;

import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ColumnName;
import com.vnhp.real.estate.res.ColumnStatus;
import com.vnhp.real.estate.res.DbColumn;
import com.vnhp.real.estate.res.StringValue;

/**
 *
 * @author FairyHunter
 */
public final class ProjectType {

    public Integer index;
    @ColumnId
    public Integer pt_id;
    @ColumnName
    @DbColumn
    public String pt_name;
    @ColumnStatus
    @DbColumn
    public Boolean pt_status;
    public String btnChangeStatus;
    public String btnUpdate;

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public void setPt_id(Integer pt_id) {
        this.pt_id = pt_id;
    }

    public void setPt_name(String pt_name) {
        this.pt_name = pt_name;
    }

    public void setPt_status(Boolean pt_status) {
        this.pt_status = pt_status;
    }

    public Integer getPt_id() {
        return pt_id;
    }

    public String getPt_name() {
        return pt_name;
    }

    public Boolean getPt_status() {
        return pt_status;
    }

    public ProjectType() {
    }

    public ProjectType(String pt_name) {
        this.pt_name = pt_name;
    }

    public ProjectType(Integer pt_id, String pt_name) {
        this.pt_id = pt_id;
        this.pt_name = pt_name;
    }

    public ProjectType(Integer pt_id, Boolean pt_status) {
        this.pt_id = pt_id;
        this.pt_status = pt_status;
    }

    public ProjectType(Integer pt_id, String pt_name, Boolean pt_status, Integer index) {
        this.index = index;
        this.pt_id = pt_id;
        this.pt_name = pt_name;
        this.pt_status = pt_status;
        this.btnChangeStatus = StringValue.Instance().BTN_CHANGE_STATUS;
        this.btnUpdate = StringValue.Instance().BTN_UPDATE;
    }

}
