/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.real.estate.controller.Director;

import com.vnhp.real.estate.dao.BaseDao;
import com.vnhp.real.estate.model.entities.B_Status;
import com.vnhp.real.estate.model.entities.Blocks;
import com.vnhp.real.estate.model.entities.Investor;
import com.vnhp.real.estate.model.entities.InvestorStatus;
import com.vnhp.real.estate.model.entities.MyGridPane;
import com.vnhp.real.estate.res.ColumnId;
import com.vnhp.real.estate.res.ConvertObject;
import com.vnhp.real.estate.res.StringValue;
import com.vnhp.real.estate.res.TableSetting;
import com.vnhp.real.estate.res.TimeManager;
import java.util.Objects;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DirectorInvestorChangeStatusController {

    @FXML
    private Label tfDirInvestorCSName;
    @FXML
    private ComboBox<String> tfDirInvestorCSStatus;
    @FXML
    public Label tfDirInvestorCSResult;
    @FXML
    private Button btnDirInvestorCS;
//    @FXML
//    private ImageView btnClose, btnMinimize;

    public void init(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Stage stage, Investor data) {
//        AppSetting.Instance().Setting(stage, btnClose, btnMinimize);
        setupDefault(data);

        setup(grid, gridIndex, tableSetting, data);
    }

    private void setupDefault(Investor data) {
        tfDirInvestorCSName.setText(data.getInvestor_name());

        var is = ConvertObject.Instance().toListString(BaseDao.Instance().getListData(StringValue.Instance().INVESTOR_STATUS_ACTIVE, InvestorStatus[].class));
        tfDirInvestorCSStatus.getItems().addAll(is);
        tfDirInvestorCSStatus.setValue(data.getIs_name());
    }

    private void setup(MyGridPane grid, Integer gridIndex, TableSetting tableSetting, Investor data) {
        btnDirInvestorCS.addEventHandler(ActionEvent.ACTION, eh -> {
            var is_status = ConvertObject.Instance().getInteger(BaseDao.Instance().getData(new InvestorStatus(tfDirInvestorCSStatus.getValue()), InvestorStatus[].class, 1), ColumnId.class);
            var tmpData = new Investor(data.getInvestor_id(), is_status);
            var retult = BaseDao.Instance().update(tmpData);
            tfDirInvestorCSResult.setText(retult);
            TimeManager.Instance().clearResult(eh, tfDirInvestorCSResult, 2);

            grid.getChildren().removeIf(node -> Objects.equals(MyGridPane.getRowIndex(node), gridIndex));
            var tmp = (Investor) data;
            tmp.setIs_name(tfDirInvestorCSStatus.getValue());
            var values = ConvertObject.Instance().dataToString(tmp);
            grid.addRow(grid, gridIndex, values, tableSetting, data.getInvestor_id(), tmp);
        });

    }

}
